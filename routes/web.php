<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//RUTAS PARA ACCEDER A VENTANAS FRONTEND

//ADMIN - Acceso administrativo

Route::get('/', 'FrontController@login');
Route::get('acceder', 'FrontController@login');
Route::get('entrar', 'FrontController@login');
Route::post('api/v1/user/login','UserController@login');
Route::group(['middleware' =>  ['CheckSession']], function () {
 //SECCIONES PUBLICAS

Route::get('usuario/bienvenido', [
    'as'=> 'welcome',
    'uses' => 'FrontController@welcomeadmin'
]);

Route::get('seguridad/perfil', 'FrontController@perfiluser');

 Route::post('/api/v1/user/closesession', 'UserController@cerrarsesion');

});

Route::get('api/v1/users', 'UserController@getusuario');
Route::post('api/v1/usuarios/save', 'UserController@postusuario');

Route::post('api/v1/comentario/save', 'MasterController@savecomentario');


Route::get('api/v1/roles', 'MasterController@getroles');
Route::get('api/v1/comentarios', 'MasterController@getcomentarios');
Route::get('api/v1/fotos', 'MasterController@getfotos');
Route::post('api/v1/foto/save','MasterController@savefotos');

Route::get('api/v1/likes', 'MasterController@getlikes');


Route::put('api/v1/usuario/update/{idn}','UserController@updateusuario');

Route::put('api/v1/ciudad/update/{idn}','MasterController@updateciudad');

//MAESTROS
Route::get('api/v1/paises', 'MasterController@getpaises');
Route::put('api/v1/pais/update/{idn}','MasterController@updatepais');
Route::post('api/v1/pais/save','MasterController@savepais');

Route::get('api/v1/provincias', 'MasterController@getprovincias');
Route::put('api/v1/provincia/update/{idn}','MasterController@updateprovincia');
Route::post('api/v1/provincia/save','MasterController@saveprovincia');

Route::get('api/v1/ciudades', 'MasterController@getciudades');
Route::put('api/v1/ciudad/update/{idn}','MasterController@updateciudad');
Route::post('api/v1/ciudad/save','MasterController@saveciudad');

Route::get('api/v1/categorias', 'MasterController@getcategorias');
Route::put('api/v1/categoria/update/{idn}','MasterController@updatecategoria');
Route::post('api/v1/categoria/save','MasterController@savecategoria');

Route::post('api/v1/rol/save','UserController@postrol');

Route::put('api/v1/comentarios/update/{idn}','MasterController@updatecomentario');

Route::put('api/v1/roles/update/{idn}','UserController@putrol');

Route::put('api/v1/likes/update/{idn}','MasterController@updatelikes');
Route::put('api/v1/fotos/update/{idn}','MasterController@updatefoto');

//ANUNCIOS
Route::get('api/v1/anuncios/{idncategoria}', 'AnuncioController@getanunciosporcategoria');
Route::get('api/v1/anuncios', 'AnuncioController@getanuncios');

Route::get('api/v1/anunciosporvender', 'AnuncioController@getanunciosporvender');

Route::get('api/v1/anunciosvendidos', 'AnuncioController@getanunciosvendidos');

Route::get('api/v1/anunciosactivos', 'AnuncioController@getanunciosactivos');

Route::get('api/v1/anunciosinactivos', 'AnuncioController@getanunciosinactivos');

Route::post('api/v1/anuncios/save','AnuncioController@saveanuncios');
Route::put('api/v1/anuncios/update/{idn}', 'AnuncioController@updateanuncios');
Route::put('api/v1/anuncios/delete/{idn}', 'AnuncioController@deleteanuncios');

Route::put('api/v1/rol/delete/{idn}', 'UserController@deleterol');
Route::put('api/v1/usuario/delete/{idn}', 'UserController@deleteusuario');

Route::put('api/v1/pais/delete/{idn}', 'MasterController@deletepais');
Route::put('api/v1/provincia/delete/{idn}', 'MasterController@deleteprovincia');
Route::put('api/v1/ciudad/delete/{idn}', 'MasterController@deleteciudad');
Route::put('api/v1/categoria/delete/{idn}', 'MasterController@deletecategoria');
Route::put('api/v1/likes/delete/{idn}', 'MasterController@deletelikes');
Route::put('api/v1/comentario/delete/{idn}', 'MasterController@deletecomentario');
Route::put('api/v1/foto/delete/{idn}', 'MasterController@deletefoto');


Route::put('api/v1/desactivate/{table}/{idn}', 'MasterController@desactivate');
Route::put('api/v1/destroy/{table}/{idn}', 'MasterController@deletegeneric');