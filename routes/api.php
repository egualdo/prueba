<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();


});


//DESACTIVADO TEMPORALMENTE


// Metodos GET
/*
Route::get('api/v1/nationality', 'MasterController@getnationality');
Route::get('api/v1/sitrevista', 'MasterController@getsitmagazine');
Route::get('api/v1/modcontratacion', 'MasterController@getmodcontract');
Route::get('api/v1/carreras', 'MasterController@getcareer');
Route::get('api/v1/idiomas', 'MasterController@getlanguaje');
Route::get('api/v1/niveles', 'MasterController@getlevel');
Route::get('api/v1/categorias', 'MasterController@getcategory');
Route::get('api/v1/obrassociales', 'MasterController@getsocworks');
Route::get('api/v1/sindicatos', 'MasterController@getlabunion');
Route::get('api/v1/lugtrabajos', 'MasterController@getworkplaces');
Route::get('api/v1/ccts', 'MasterController@getccts');
Route::get('api/v1/condiciones', 'MasterController@getterms');
Route::get('api/v1/regprevionales', 'MasterController@getspecialreg');
Route::get('api/v1/tespeciales', 'MasterController@getspecialtyp');
Route::get('api/v1/codsiniestrados', 'MasterController@getlostcode');
Route::get('api/v1/ptosdeventa', 'MasterController@getpointsale');
Route::get('api/v1/impuestos', 'MasterController@gettaxes');
Route::get('api/v1/metpagos', 'MasterController@getpaymethod');
Route::get('api/v1/grupos', 'MasterController@getgroup');

// Metodos POST
Route::post('api/v1/nacionalidades', 'MasterController@postnationality');
Route::post('api/v1/sitrevista', 'MasterController@postsitmagazine');
Route::post('api/v1/modcontratacion', 'MasterController@postmodcontract');
Route::post('api/v1/carreras', 'MasterController@postcareer');
Route::post('api/v1/idiomas', 'MasterController@postlanguaje');
Route::post('api/v1/niveles', 'MasterController@postlevel');
Route::post('api/v1/categorias', 'MasterController@postcategory');
Route::post('api/v1/obrassociales', 'MasterController@postsocworks');
Route::post('api/v1/sindicatos', 'MasterController@postlabunion');
Route::post('api/v1/lugtrabajos', 'MasterController@postworkplaces');
Route::post('api/v1/ccts', 'MasterController@postccts');
Route::post('api/v1/condiciones', 'MasterController@postterms');
Route::post('api/v1/regprevionales', 'MasterController@postspecialreg');
Route::post('api/v1/tsespeciales', 'MasterController@postspecialtyp');
Route::post('api/v1/codsiniestrados', 'MasterController@postlostcode');
Route::post('api/v1/ptosdeventa', 'MasterController@postpointsale');
Route::post('api/v1/impuestos', 'MasterController@posttaxes');
Route::post('api/v1/metpagos', 'MasterController@postpaymethod');
Route::post('api/v1/grupos', 'MasterController@postgroup');
// Metodos PUT
Route::put('api/v1/nacionalidades', 'MasterController@putnacionality');
Route::put('api/v1/sitrevista', 'MasterController@putsitmagazine');
Route::put('api/v1/modcontratacion', 'MasterController@putmodcontract');
Route::put('api/v1/carreras', 'MasterController@putcareer');
Route::put('api/v1/idiomas', 'MasterController@putlanguaje');
Route::put('api/v1/niveles', 'MasterController@putlevel');
Route::put('api/v1/categorias', 'MasterController@putcategory');
Route::put('api/v1/obrassociales', 'MasterController@putsocworks');
Route::put('api/v1/sindicatos', 'MasterController@labunion');
Route::put('api/v1/lugtrabajos', 'MasterController@putworkplaces');
Route::put('api/v1/ccts', 'MasterController@putccts');
Route::put('api/v1/condiciones', 'MasterController@putterms');
Route::put('api/v1/regprevionales', 'MasterController@putspecialreg');
Route::put('api/v1/tespeciales', 'MasterController@putspecialtyp');
Route::put('api/v1/codsiniestrados', 'MasterController@putlostcode');
Route::put('api/v1/ptosdeventa', 'MasterController@putpointsale');
Route::put('api/v1/impuestos', 'MasterController@puttaxes');
Route::put('api/v1/metpagos', 'MasterController@putpaymethod');
Route::put('api/v1/grupos', 'MasterController@putgroup');
// Metodos DELETE
Route::delete('api/v1/nacionalidades', 'MasterController@deletenationality');
Route::delete('api/v1/sitrevista', 'MasterController@deletesitmagazine');
Route::delete('api/v1/modcontratacion', 'MasterController@deletemodcontract');
Route::delete('api/v1/carreras', 'MasterController@deletecareer');
Route::delete('api/v1/idiomas', 'MasterController@deletelanguaje');
Route::delete('api/v1/niveles', 'MasterController@deletelevel');
Route::delete('api/v1/categorias', 'MasterController@deletecategory');
Route::delete('api/v1/obrassociales', 'MasterController@deletesocworks');
Route::delete('api/v1/sindicatos', 'MasterController@deletelabunion');
Route::delete('api/v1/lugtrabajos', 'MasterController@deletewoorkplaces');
Route::delete('api/v1/ccts', 'MasterController@deleteccts');
Route::delete('api/v1/condiciones', 'MasterController@deleteterms');
Route::delete('api/v1/regprevionales', 'MasterController@deletespecialreg');
Route::delete('api/v1/tespeciales', 'MasterController@deletespecialtyp');
Route::delete('api/v1/codsiniestrados', 'MasterController@deletelostcode');
Route::delete('api/v1/ptosdeventa', 'MasterController@deletepointsale');
Route::delete('api/v1/impuestos', 'MasterController@deletetaxes');
Route::delete('api/v1/metpagos', 'MasterController@deletepaymethod');
Route::delete('api/v1/grupos', 'MasterController@deletegroup');
*/





