<?php

namespace App\Tools;

class TypeImportFileEnum
{
    public static $Legajo = 1;
    public static $ConstantesDeLegajo = 2;
    public static $ConstantesDeCCT = 3;
    public static $ConstantesDeLCT = 4;
    public static $ConstantesDeEmpresa = 5;
    public static $AsignacionesYDeducciones = 6;

}
