<?php

namespace App\Http\Controllers;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Hash;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Audit;

class AuditController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getaudit()
    {
    	  $result = DB::table('audit')
            ->join('user', 'user.idn', '=', 'idnuser')
      
            ->select('audit.idn',
                     'audit.idnuser',
                     'user.username',
                     'audit.accion',
                        'audit.window',
                     'audit.created_at',
                     'audit.updated_at'
                        )
                                                        
            //->where('roster.active',1)
            ->get();
            return \Response::json($result,200);

    }

  
}
