<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pais;
use App\Models\Provincia;
use App\Models\Ciudad;
use App\Models\Categoria;
use App\Models\User;
use App\Models\Rol;
use App\Models\Foto;
use App\Models\Comentario;
use App\Models\Likes;
use App\Models\Anuncio;

class MasterController extends Controller
{
    public function getpaises()
    {
    	  $result = Pais::select('pais')    
      
            ->select('idn',
                     'nombre',
                     'estatus'                   
                      )                                                        
            ->where('pais.estatus',1)
            ->get();
            return \Response::json($result,200);
    }
    public function getprovincias()
    {
    	  $result = Provincia::select('provincia')    
      
            ->select('idn',
                     'idnpais',
                     'nombre',
                     'estatus'                   
                      )                                                        
            ->where('provincia.estatus',1)
            ->get();
            return \Response::json($result,200);
    }
    public function getciudades()
    {
    	  $result = Ciudad::select('ciudad')    
      
            ->select('idn',
                     'idnprovincia',
                     'nombre',
                     'estatus'                   
                      )                                                        
            //->where('roster.active',1)
            ->get();
            return \Response::json($result,200);
    }
    public function getcategorias()
    {
    	  $result = Categoria::select('categoria')    
      
            ->select('idn',
                     'nombre',
                     'descripcion',
                     'estatus'                   
                      )                                                        
            //->where('roster.active',1)
            ->get();
            return \Response::json($result,200);
    }
       public function getcomentarios()
    {
    	  $result = Comentario::select('comentario')    
      
            ->select('idn',
                     'idnanuncio',
                     'descripcion',
                     'estatus'                   
                      )                                                        
            //->where('roster.active',1)
            ->get();
            return \Response::json($result,200);
    }
        public function getfotos()
    {
    	  $result = Foto::select('foto')    
      
            ->select('idn',
                     'codigo',
                     'ruta',
                     'estatus'                   
                      )                                                        
            //->where('roster.active',1)
            ->get();
            return \Response::json($result,200);
    }
      public function getlikes()
    {
    	  $result = Likes::select('likes')    
      
     ->join('user', 'user.idn', '=', 'likes.idnusuario')
    ->join('anuncio', 'anuncio.idn','=','likes.idnanuncio')
                  
            ->select('likes.idn', 'likes.idnusuario', 'likes.idnanuncio','likes.fecha', 'likes.estatus')
            ->where('likes.estatus', 1)
            ->get();
            return \Response::json($result,200);
           
        
    }
    
    
    
    
    public function savepais(Request $request)
    {
        //CACH DATA       
        $nombre = $request['nombre'];       
        if ($nombre == "") {
            return \Response::json("El Nombre no puede estar en nulo", 500);
        }
        try {
            $element = new Pais;
            $element->nombre = $nombre;           
            $element->save();
            return \Response::json('Guardado correctamente: ' . $element, 200);
        } //CATCH COMPROBACION
        catch (QueryException $e) {
            //CATCH ERROR CODE
            $errorCode = $e->errorInfo[1];
            //IF THE ERROR IS DUPLICATE ENTRY
            if ($errorCode == 1062) {
                return \Response::json("Ya existe un elemento igual", 409);
            } else {
                return \Response::json("ERROR AL OBTENER LOS DATOS", 500);
            }
        }
    }
       public function savefotos(Request $request)
    {
        //CACH DATA       
        $codigo = $request['codigo']; 
           $ruta = $request['ruta']; 
          
        if ($codigo  == "" or $ruta  == "") {
            return \Response::json("Hay campos vacios", 500);
        }
        try {
            $element = new Foto;
            $element->codigo = $codigo;
            $element->ruta = $ruta;
            $element->save();
            return \Response::json('Guardado correctamente: ' . $element, 200);
        } //CATCH COMPROBACION
        catch (QueryException $e) {
            //CATCH ERROR CODE
            $errorCode = $e->errorInfo[1];
            //IF THE ERROR IS DUPLICATE ENTRY
            if ($errorCode == 1062) {
                return \Response::json("Ya existe un elemento igual", 409);
            } else {
                return \Response::json("ERROR AL OBTENER LOS DATOS", 500);
            }
        }
    }
    public function saveprovincia(Request $request)
    {
        //CACH DATA     
        $idnpais = $request['idnpais'];  
        $nombre = $request['nombre'];       
        if ($nombre == "") {
            return \Response::json("El Nombre no puede estar en nulo", 500);
        }
        try {
            $element = new Provincia;
            $element->idnpais = $idnpais;  
            $element->nombre = $nombre;           
            $element->save();
            return \Response::json('Guardado correctamente: ' . $element, 200);
        } //CATCH COMPROBACION
        catch (QueryException $e) {
            //CATCH ERROR CODE
            $errorCode = $e->errorInfo[1];
            //IF THE ERROR IS DUPLICATE ENTRY
            if ($errorCode == 1062) {
                return \Response::json("Ya existe un elemento igual", 409);
            } else {
                return \Response::json("ERROR AL OBTENER LOS DATOS", 500);
            }
        }
    }
    public function saveciudad(Request $request)
    {
        //CACH DATA     
        $idnprovincia = $request['idnprovincia'];  
        $nombre = $request['nombre'];       
        if ($nombre == "") {
            return \Response::json("El Nombre no puede estar en nulo", 500);
        }
        try {
            $element = new Ciudad;
            $element->idnprovincia = $idnprovincia;  
            $element->nombre = $nombre;           
            $element->save();
            return \Response::json('Guardado correctamente: ' . $element, 200);
        } //CATCH COMPROBACION
        catch (QueryException $e) {
            //CATCH ERROR CODE
            $errorCode = $e->errorInfo[1];
            //IF THE ERROR IS DUPLICATE ENTRY
            if ($errorCode == 1062) {
                return \Response::json("Ya existe un elemento igual", 409);
            } else {
                return \Response::json("ERROR AL OBTENER LOS DATOS", 500);
            }
        }
    }
    public function savecategoria(Request $request)
    {
        //CACH DATA     
        $descripcion = $request['descripcion'];  
        $nombre = $request['nombre'];       
        if ($nombre == "") {
            return \Response::json("El Nombre no puede estar en nulo", 500);
        }
        try {
            $element = new Categoria;
            $element->descripcion = $descripcion;  
            $element->nombre = $nombre;           
            $element->save();
            return \Response::json('Guardado correctamente: ' . $element, 200);
        } //CATCH COMPROBACION
        catch (QueryException $e) {
            //CATCH ERROR CODE
            $errorCode = $e->errorInfo[1];
            //IF THE ERROR IS DUPLICATE ENTRY
            if ($errorCode == 1062) {
                return \Response::json("Ya existe un elemento igual", 409);
            } else {
                return \Response::json("ERROR AL OBTENER LOS DATOS", 500);
            }
        }
    }
     public function savecomentario(Request $request)
    {
        //CACH DATA     
        $descripcion = $request['descripcion'];  
        $idnanuncio = $request['idnanuncio'];  
         
        if ($descripcion == "") {
            return \Response::json("hay campos vacios", 500);
        }
        try {
            $element = new Comentario;
            $element->descripcion = $descripcion;  
            $element->idnanuncio = $idnanuncio;           
            $element->save();
            return \Response::json('Guardado correctamente: ' . $element, 200);
        } //CATCH COMPROBACION
        catch (QueryException $e) {
            //CATCH ERROR CODE
            $errorCode = $e->errorInfo[1];
            //IF THE ERROR IS DUPLICATE ENTRY
            if ($errorCode == 1062) {
                return \Response::json("Ya existe un elemento igual", 409);
            } else {
                return \Response::json("ERROR AL OBTENER LOS DATOS", 500);
            }
        }
    }
     public function savelikes(Request $request)
    {
        //CACH DATA     
        $idnusuario = $request['idnusuario'];  
        $idnanuncio = $request['idnanuncio'];
             $fecha = $request['fecha'];
         
        if ($idnusuario == "" or $idnanuncio == "") {
            return \Response::json("hay campos vacios", 500);
        }
        try {
            $element = new Likes;
            $element->idnusuario = $idnusuario;  
            $element->idnanuncio = $idnanuncio;  
            $element->fecha = $fecha;
            $element->save();
            return \Response::json('Guardado correctamente: ' . $element, 200);
        } //CATCH COMPROBACION
        catch (QueryException $e) {
            //CATCH ERROR CODE
            $errorCode = $e->errorInfo[1];
            //IF THE ERROR IS DUPLICATE ENTRY
            if ($errorCode == 1062) {
                return \Response::json("Ya existe un elemento igual", 409);
            } else {
                return \Response::json("ERROR AL OBTENER LOS DATOS", 500);
            }
        }
    }
    
    
    
      public function updatelikes(Request $request,$idn)
    {
        //CACH DATA     
       $idnusuario = $request['idnusuario'];
        $idnanuncio = $request['idnanuncio'];
          $fecha = $request['fecha'];
          
        if ($fecha == "") {
            return \Response::json("La fecha no puede estar en nulo", 500);
        }
        try {
            $element = Likes::find($idn);          
            $element->idnusuario = $idnusuario;
            $element->idnanuncio = $idnanuncio;
            $element->fecha= $fecha;
            $element->update();
            return \Response::json('Actualizado correctamente: ' . $element, 200);
        } //CATCH COMPROBACION
        catch (QueryException $e) {
            //CATCH ERROR CODE
            $errorCode = $e->errorInfo[1];
            //IF THE ERROR IS DUPLICATE ENTRY
            if ($errorCode == 1062) {
                return \Response::json("Ya existe un elemento igual", 409);
            } else {
                return \Response::json("ERROR AL OBTENER LOS DATOS", 500);
            }
        }
    }
     public function updatefoto(Request $request,$idn)
    {
        //CACH DATA     
       $ruta = $request['ruta'];
        $codigo = $request['codigo'];       
        if ($codigo == "") {
            return \Response::json("El codigo no puede estar en nulo", 500);
        }
        try {
            $element = Foto::find($idn);          
            $element->codigo = $codigo;
            $element->ruta = $ruta;
            $element->update();
            return \Response::json('Actualizado correctamente: ' . $element, 200);
        } //CATCH COMPROBACION
        catch (QueryException $e) {
            //CATCH ERROR CODE
            $errorCode = $e->errorInfo[1];
            //IF THE ERROR IS DUPLICATE ENTRY
            if ($errorCode == 1062) {
                return \Response::json("Ya existe un elemento igual", 409);
            } else {
                return \Response::json("ERROR AL OBTENER LOS DATOS", 500);
            }
        }
    }
    public function updatepais(Request $request)
    {
        //CACH DATA     
        $idn = $request['idn'];  
        $nombre = $request['nombre'];       
        if ($nombre == "") {
            return \Response::json("El Nombre no puede estar en nulo", 500);
        }
        try {
            $element = Pais::find($idn);          
            $element->nombre = $nombre;           
            $element->update();
            return \Response::json('Actualizado correctamente: ' . $element, 200);
        } //CATCH COMPROBACION
        catch (QueryException $e) {
            //CATCH ERROR CODE
            $errorCode = $e->errorInfo[1];
            //IF THE ERROR IS DUPLICATE ENTRY
            if ($errorCode == 1062) {
                return \Response::json("Ya existe un elemento igual", 409);
            } else {
                return \Response::json("ERROR AL OBTENER LOS DATOS", 500);
            }
        }
    }
    public function updateprovincia(Request $request)
    {
        //CACH DATA     
        $idn = $request['idn']; 
        $idnpais = $request['idnpais'];   
        $nombre = $request['nombre'];       
        if ($nombre == "") {
            return \Response::json("El Nombre no puede estar en nulo", 500);
        }
        try {
            $element = Provincia::find($idn); 
            $element->idnpais = $idnpais;          
            $element->nombre = $nombre;           
            $element->update();
            return \Response::json('Actualizado correctamente: ' . $element, 200);
        } //CATCH COMPROBACION
        catch (QueryException $e) {
            //CATCH ERROR CODE
            $errorCode = $e->errorInfo[1];
            //IF THE ERROR IS DUPLICATE ENTRY
            if ($errorCode == 1062) {
                return \Response::json("Ya existe un elemento igual", 409);
            } else {
                return \Response::json("ERROR AL OBTENER LOS DATOS", 500);
            }
        }
    }
    public function updateciudad(Request $request,$idn)
    {
        //CACH DATA      
        $idnprovincia = $request['idnprovincia'];   
        $nombre = $request['nombre'];       
        if ($nombre == "") {
            return \Response::json("El Nombre no puede estar en nulo", 500);
        }
        try {
            $element = Ciudad::find($idn); 
            $element->idnprovincia = $idnprovincia;          
            $element->nombre = $nombre;           
            $element->update();
            return \Response::json('Actualizado correctamente: ' . $element, 200);
        } //CATCH COMPROBACION
        catch (QueryException $e) {
            //CATCH ERROR CODE
            $errorCode = $e->errorInfo[1];
            //IF THE ERROR IS DUPLICATE ENTRY
            if ($errorCode == 1062) {
                return \Response::json("Ya existe un elemento igual", 409);
            } else {
                return \Response::json("ERROR AL OBTENER LOS DATOS", 500);
            }
        }
    }
    public function updatecategoria(Request $request)
    {
        //CACH DATA     
        $idn = $request['idn']; 
        $descripcion = $request['descripcion'];   
        $nombre = $request['nombre'];       
        if ($nombre == "") {
            return \Response::json("El Nombre no puede estar en nulo", 500);
        }
        try {
            $element = Categoria::find($idn); 
            $element->descripcion = $descripcion;          
            $element->nombre = $nombre;           
            $element->update();
            return \Response::json('Actualizado correctamente: ' . $element, 200);
        } //CATCH COMPROBACION
        catch (QueryException $e) {
            //CATCH ERROR CODE
            $errorCode = $e->errorInfo[1];
            //IF THE ERROR IS DUPLICATE ENTRY
            if ($errorCode == 1062) {
                return \Response::json("Ya existe un elemento igual", 409);
            } else {
                return \Response::json("ERROR AL OBTENER LOS DATOS", 500);
            }
        }
    }
    public function updatecomentario(Request $request,$idn)
    {
        //CACH DATA     
        $idnanuncio = $request['idnanuncio']; 
        $descripcion = $request['descripcion'];   
            
        if ($descripcion == "") {
            return \Response::json("La descripcion no puede estar en nula", 500);
        }
        try {
            $element = Comentario::find($idn); 
            $element->descripcion = $descripcion;          
            $element->idnanuncio = $idnanuncio;           
            $element->update();
            return \Response::json('Actualizado correctamente: ' . $element, 200);
        } //CATCH COMPROBACION
        catch (QueryException $e) {
            //CATCH ERROR CODE
            $errorCode = $e->errorInfo[1];
            //IF THE ERROR IS DUPLICATE ENTRY
            if ($errorCode == 1062) {
                return \Response::json("Ya existe un elemento igual", 409);
            } else {
                return \Response::json("ERROR AL OBTENER LOS DATOS", 500);
            }
        }
    }
    
    
     public function deletepais($idn)
    {
        try {

            $Pais = Pais::find($idn);
            $Pais->estatus = 0;
            $Pais->update();
            return \Response::json('Eliminado correctamente: ', 200);

        } //CATCH COMPROBACION
        catch (PDOException $e) {
            return \Response::json("ERROR AL OBTENER LOS DATOS", 500);

        }
    }
     public function deleteprovincia($idn)
    {
        try {

            $Provincia = Provincia::find($idn);
            $Provincia->estatus = 0;
            $Provincia->update();
            return \Response::json('Eliminado correctamente: ', 200);

        } //CATCH COMPROBACION
        catch (PDOException $e) {
            return \Response::json("ERROR AL OBTENER LOS DATOS", 500);

        }
    }    
     public function deleteciudad($idn)
    {
        try {

            $Ciudad = Ciudad::find($idn);
            $Ciudad->estatus = 0;
            $Ciudad->update();
            return \Response::json('Eliminado correctamente: ', 200);

        } //CATCH COMPROBACION
        catch (PDOException $e) {
            return \Response::json("ERROR AL OBTENER LOS DATOS", 500);

        }
    } 
     public function deletecategoria($idn)
    {
        try {

            $Categoria = Categoria::find($idn);
            $Categoria->estatus = 0;
            $Categoria->update();
            return \Response::json('Eliminado correctamente: ', 200);

        } //CATCH COMPROBACION
        catch (PDOException $e) {
            return \Response::json("ERROR AL OBTENER LOS DATOS", 500);

        }
    } 
     public function deletelikes($idn)
    {
        try {

            $Likes = Likes::find($idn);
            $Likes->estatus = 0;
            $Likes->update();
            return \Response::json('Eliminado correctamente: ', 200);

        } //CATCH COMPROBACION
        catch (PDOException $e) {
            return \Response::json("ERROR AL OBTENER LOS DATOS", 500);

        }
    } 
      public function deletecomentario($idn)
    {
        try {

            $Comentario = Comentario::find($idn);
            $Comentario->estatus = 0;
            $Comentario->update();
            return \Response::json('Eliminado correctamente: ', 200);

        } //CATCH COMPROBACION
        catch (PDOException $e) {
            return \Response::json("ERROR AL OBTENER LOS DATOS", 500);

        }
    }
     public function deletefoto($idn)
    {
        try {

            $Foto = Foto::find($idn);
            $Foto->estatus = 0;
            $Foto->update();
            return \Response::json('Eliminado correctamente: ', 200);

        } //CATCH COMPROBACION
        catch (PDOException $e) {
            return \Response::json("ERROR AL OBTENER LOS DATOS", 500);

        }
    }
  
 
   

}
