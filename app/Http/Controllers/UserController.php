<?php

namespace App\Http\Controllers;

use Illuminate\Database\QueryException;
use Carbon\Carbon;
//use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Session;
use Illuminate\Http\Request;
use App\Models\Usuario;
use App\Models\Rol;
use Hash;
use DB;

use Illuminate\Support\Facades\Log;
use Config;
use Crypt;
use Artisan;


class UserController extends Controller
{
    
    public function index()
    {
        $user = Usuario::select('user.idn', 'user.username', 'user.idnrol', 'user.password', 'rol.name as nombrerol', 'user.active')
            ->join('rol', 'rol.idn', '=', 'user.idnrol')
            ->where('user.active', 1)
            ->where('user.idn', (int)session()->get('IdnUsuario'))
            ->get()->first();

        return \Response::json($user, 200);
    }

    public function getusuario()
    {


        $users = Usuario::select('user')
            ->join('rol', 'rol.idn', '=', 'idnrol')
            ->select('user.idn', 'user.username', 'user.idnrol', 'rol.name as nombrerol', 'user.active')
            ->where('user.active', 1)
            ->get();


        return \Response::json($users, 200);
    }

    public function login(Request $request)
    {
        $Username = $request['username'];
        $Password = $request['password'];
        if ($Username == "") {
            return \Response::json("El Username no puede estar en nulo", 500);
        }

        $user = Usuario::select('usuario.idn', 'usuario.username', 'usuario.idnrol', 'usuario.password', 'rol.nombre as namerol', 'usuario.estatus')
            ->join('rol', 'rol.idn', '=', 'usuario.idnrol')
            ->where('usuario.estatus', 1)
            ->where('username', $Username)
            ->get()->first();

        if ($user === null) {
            return \Response::json("CREDENCIALES INCORRECTAS", 404);
        }
        //CHECKING PASSWORD
        if (Hash::check($Password, $user->password)) {


            $Idn = $user->idn;
            $IdnRol = $user->idnrol;
            $Username = $user->username;
            $Namerol = $user->namerol;
            session(['IdnUsuario' => $Idn]);
            session(['NameRol' => $Namerol]);
            session(['IdnRol' => $IdnRol]);
            session(['Username' => $Username]);
            session(['Loged' => 1]);

            if(!is_null($user->avatar) || $user->avatar !== ''){
                session(['avatar' => $user->avatar]);
            }

            /*  session()->put('IdnUsuario', $Idn);
              session()->put('IdnRolUsuario', $IdnRol);
              session()->put('UsernameUsuario', $Username);*/


            /*  $UserCompany = UserCompany::select('usercompany')
                ->join('user', 'user.idn', '=', 'usercompany.idnuser')
                ->join('company', 'company.idn', '=', 'usercompany.idncompany')
                ->join('datasource', 'datasource.idncompany', '=', 'company.idn')
                ->select('usercompany.idn',
                    'usercompany.idncompany',
                    'company.name as namecompany',
                    'company.cod as codcompany',
                    'company.cuit as cuitcompany',
                    'company.cuiladmin as cuiladmincompany',
                    'datasource.host',
                    'datasource.database',
                    'datasource.username',
                    'datasource.password',
                    'usercompany.active')
                ->where('user.idn', $Idn)
                ->where('usercompany.active', 1)
                ->get()->first();


            $IdnCompany = $UserCompany->idncompany;
            $CodCompany = $UserCompany->codcompany;
            $NameCompany = $UserCompany->namecompany;
            $CuitCompany = $UserCompany->cuitcompany;
            $DatabaseName = $UserCompany->database;
            $Host = $UserCompany->host;
            $User = $UserCompany->username;
            $Pass = $UserCompany->password;

            session(['IdCompany' => $IdnCompany]);
            session(['NameCompany' => $NameCompany]);
            session(['CuitCompany' => $CuitCompany]);
            //DATOS DE CONEXION
            session(['Database' => $DatabaseName]);
            session(['HostDB' => $Host]);
            session(['UserDB' => $User]);
            session(['PassDB' => $Pass]);
            Log::notice('El Usuario: '.$Username.' ha accedido en Fecha: '.Carbon::now());

            return \Response::json(["BIENVENIDO", $Idn, $IdnRol, $Username, $IdnCompany, session('Loged'), 200]);
        } else {

            return \Response::json("CREDENCIALES INCORRECTAS", 404);
        }*/

        }
    }

    public function cerrarsesion()
    {
        session()->flush();
        return \Response::json(["Hasta Luego", 200]);

    }

  
    public function postusuario(Request $request)
    {
        //CACH DATA
        $Idnrol = $request['idnrol'];
        $Username = $request['username'];
        $Password = $request['password'];
        $email = $request['email'];
        $nombre = $request['nombre'];
        $apellido = $request['apellido'];
        $cedula = $request['cedula'];
        $direccion = $request['direccion'];
        $telefono = $request['telefono'];
        $idnciudad = $request['idnciudad'];
        $sexo = $request['sexo'];
        
        if ($Username == "") {
            return \Response::json("El Username no puede estar en nulo", 500);
        }

        try {
            $user = new Usuario;
            $user->username = $Username;
            $user->password = $Password;
            $user->email = $email;
            $user->nombre = $nombre;
            $user->apellido = $apellido;
            $user->cedula = $cedula;
            $user->direccion = $direccion;
            $user->telefono = $telefono;
            $user->idnciudad = $idnciudad;
            $user->sexo = $sexo;
            $user->idnrol = $Idnrol;
            $user->save();
return \Response::json('Guardado correctamente:'.$Username, 200);

        } //CATCH COMPROBACION
        catch (QueryException $e) {
            //CATCH ERROR CODE
            $errorCode = $e->errorInfo[1];
            //IF THE ERROR IS DUPLICATE ENTRY
            if ($errorCode == 1062) {
                return \Response::json("Ya existe un elemento igual", 409);
            } else {
                return \Response::json("ERROR AL OBTENER LOS DATOS", 500);
            }


        }
    }

   
    public function updateusuario(Request $request, $idn)
    {

        $Idnrol = $request['idnrol'];
        $Username = $request['username'];

        if ($Username == "") {
            return \Response::json("El Username no puede estar en nulo", 500);
        }
        try {
            $user = Usuario::find($idn);
            $user->username = $Username;
            $user->idnrol = $Idnrol;
             $user->update();
              return \Response::json('Actualizado correctamente: ' . $user, 200);
           /* if ($request->has('password') && $request->get('password') !== '' && !is_null($request->get('password'))) {
                $user->password = bcrypt($request->get('password'));*/
            }
           /* if ($request->has('avatar')) {
                $avatar = $request['avatar'];
                if (!is_null($request->get('avatar')) && $request->get('avatar') !== 'null' && $request->get('avatar') !== '') {
                    try {
                        $avatar = preg_replace('#^data:image/[^;]+;base64,#', '', $avatar);
                        $name = uniqid() . '.png';
                        $image_path = 'img/user/' . $idn . '/avatar/' . $name;
                        Storage::put($image_path, base64_decode($avatar));
                        $user->avatar = $image_path;
                        session()->put('avatar', $image_path);
                    } catch (\Exception $e) {
                        \Log::error('catch_avatar_user', [$e]);
                    }
                }
            }*/
           
           /* $response = [
                'message' => 'Modificado correctamente: ' . $Username,
                'avatar' => is_null($request->get('avatar')) || $request->get('avatar') === '' ? 'null' : $user->avatar
            ];
            return \Response::json($response, 200);

        } //CATCH COMPROBACION
        catch (PDOException $e) {
            return \Response::json("ERROR AL OBTENER LOS DATOS", 500);

        }*/
         catch (QueryException $e) {
            //CATCH ERROR CODE
            $errorCode = $e->errorInfo[1];
            //IF THE ERROR IS DUPLICATE ENTRY
            if ($errorCode == 1062) {
                return \Response::json("Ya existe un elemento igual", 409);
            } else {
                return \Response::json("ERROR AL OBTENER LOS DATOS", 500);
            }
        }
    }

    public function updatepassword(Request $request, $idn)
    {
        //CACH DATA

        $Username = $request['username'];
        $Password = $request['password'];

        if ($Username == "") {
            return \Response::json("El Username no puede estar en nulo", 500);
        }
        try {

            $user = Usuario::find($idn);
            $user->password = bcrypt($Password);
            $user->save();
            return \Response::json('Modificado correctamente: ' . $Username, 200);

        } //CATCH COMPROBACION
        catch (PDOException $e) {
            return \Response::json("ERROR AL OBTENER LOS DATOS", 500);

        }
    }

    
    public function deleteusuario($idn)
    {

        try {

            $user = Usuario::find($idn);
            $user->estatus = 0;
            $user->update();
            return \Response::json('Eliminado correctamente: ', 200);

        } //CATCH COMPROBACION
        catch (PDOException $e) {
            return \Response::json("ERROR AL OBTENER LOS DATOS", 500);

        }
    }

    public function getrol()
    {
        $result = Rol::select('rol')
            ->select('idn', 'nombre', 'descripcion', 'estatus')
            ->where('estatus', 1)
            ->get();

        //Retorna en formato JSON con codigo 200
        return \Response::json($result, 200);
    }

    public function postrol(Request $request)
    {

        $Nombre = $request['nombre'];
        $Descripcion = $request['descripcion'];
        if ($Nombre == "") {
            return \Response::json("El Nombre no puede estar en nulo", 500);
        }

        try {
            $Rol = new Rol;
            $Rol->nombre = $Nombre;
            $Rol->descripcion = $Descripcion;
            $Rol->save();
            return \Response::json('Guardado correctamente: ', 200);

        } //CATCH COMPROBACION
        catch (QueryException $e) {
            //CATCH ERROR CODE
            $errorCode = $e->errorInfo[1];
            //IF THE ERROR IS DUPLICATE ENTRY
            if ($errorCode == 1062) {
                return \Response::json("Ya existe un elemento igual", 409);
            } else {
                return \Response::json("ERROR AL OBTENER LOS DATOS", 500);
            }
        }
    }

    public function putrol(Request $request, $idn)
    {

        $Nombre = $request['nombre'];
        $Descripcion = $request['descripcion'];
        if ($Nombre == "") {
            return \Response::json("El Nombre no puede estar en nulo", 500);
        }
        try {

            $Rol = Rol::find($idn);

            $Rol->nombre = $Nombre;
            $Rol->descripcion = $Descripcion;
            $Rol->update();
            return \Response::json('Modificado correctamente: ', 200);

        } //CATCH COMPROBACION
        catch (QueryException $e) {
            //CATCH ERROR CODE
            $errorCode = $e->errorInfo[1];
            //IF THE ERROR IS DUPLICATE ENTRY
            if ($errorCode == 1062) {
                return \Response::json("Ya existe un elemento igual", 409);
            } else {
                return \Response::json("ERROR AL OBTENER LOS DATOS", 500);
            }
        }
    }

    public function deleterol($idn)
    {
        try {

            $Rol = Rol::find($idn);
            $Rol->estatus = 0;
            $Rol->update();
            return \Response::json('Eliminado correctamente: ', 200);

        } //CATCH COMPROBACION
        catch (PDOException $e) {
            return \Response::json("ERROR AL OBTENER LOS DATOS", 500);

        }
    }


}
