<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FrontController extends Controller
{

    public function index()
    {
        ////
    }

    public function dashboardclient()
    {
        return view('clients/dashboard');
    }

    public function loginclientes()
    {
        return view('clients/Login');
    }

    public function clientsmewemployee()
    {
        return view('clients/newemployee');
    }
    
    public function clientspreemployee()
    {
        return view('clients/preemployee');
    }
    public function clientsmployeeactive()
    {
        return view('clients/employeeactives');
    }

    public function pruebadatapicker()
    {
        return view('reports/pruebadatapicker');
    }

    public function companyforuser()
    {
        return view('security/Companyforuser');
    }


    public function cuilcuit()
    {
        return view('modal/CuilCuit');
    }

    public function perfiluser()
    {
        return view('security/Perfil');
    }

    public function ayudauser()
    {
        return view('security/Ayuda');
    }


    //ADMIN
    public function login()
    {
        if(session('Loged') === 1){
            return redirect()->route('welcome');
        }

        return view('admin/Login');
    }

    public function welcomeadmin()
    {
        return view('admin/WelcomeAdmin');
    }

    public function welcomeuser()
    {
        return view('admin/WelcomeUser');
    }

    //COMPANY

    public function listcompanyactive()
    {
        return view('company/ListCompanyActive');
    }

    public function listcompanyinactive()
    {
        return view('company/ListCompanyInactive');
    }

    public function companydetail()
    {
        return view('company/CompanyDetail');
    }

    public function companyconstant()
    {
        return view('parametrization/CompanyConstant');
    }
    
      public function companybehavior()
    {
        return view('parametrization/Companybehavior');
    }

    public function province()
    {
        return view('master/Province');
    }

    public function lctconstant()
    {
        return view('parametrization/lctconstant');
    }

    public function cctconstant()
    {
        return view('parametrization/cctconstant');
    }

    public function runtype()
    {
        return view('master/RunType');
    }

    public function cctlist()
    {
        return view('parametrization/CCTlist');
    }

    public function cctformula()
    {
        return view('parametrization/cctformula');
    }

    public function companylist()
    {
        return view('parametrization/Companylist');
    }

    public function companyformula()
    {
        return view('parametrization/companyformula');
    }


    //EMPLOYEE

    public function categorysalary()
    {
        return view('employee/CategorySalary');
    }
    
      public function employeesicoss()
    {
        return view('employee/EmployeeSicoss');
    }

    public function employeepayrunhead()
    {
        return view('employee/payrunhead');
    }

    public function pruebaausencias()
    {
        return view('employee/pruebaausencias');
    }

    public function employeepositionjob()
    {
        return view('employee/Employeejobposition');
    }

    public function employeeconstant()
    {
        return view('employee/pruebaconstantemployee');
    }

    public function pruebahorastrabajadas()
    {
        return view('employee/pruebahoursworked');
    }

    public function employeeasigdeduc()
    {
        return view('employee/EmployeeDetailAsigDeduc');
    }

    public function employeedetail()
    {
        return view('employee/EmployeeDetail');
    }

    public function employeenew()
    {
        return view('employee/EmployeeNew');
    }

    public function listemployeeactive()
    {
        return view('employee/ListEmployeeActive');
    }

    public function listemployeeinactive()
    {
        return view('employee/ListEmployeesInactive');
    }

    public function orgranigram()
    {
        return view('employee/Organigram');
    }
    
    public function addlegajosgroups()
    {
        return view('employee/addlegajosgroups');
    }
    
    public function addlegajosvacations()
    {
        return view('employee/addlegajosvacations');
    }
    
    
    public function addlegajosabsences()
    {
        return view('employee/addlegajosabsences');
    }

    //PARAMETRIZATION

    public function period()
    {
        return view('parametrization/Period');
    }

    public function typeperiod()
    {
        return view('parametrization/Typeperiod');
    }

    public function subperiod()
    {
        return view('parametrization/Subperiod');
    }


    public function concepts()
    {
        return view('parametrization/Concepts');
    }

    public function constants()
    {
        return view('parametrization/Constants');
    }

    public function formulacct()
    {
        return view('parametrization/FormulaCCT');
    }

    public function formulacompany()
    {
        return view('parametrization/FormulaCompany');
    }

    public function formulalct()
    {
        return view('parametrization/FormulaLCT');
    }

    public function payrollworkers()
    {
        return view('parametrization/Roster');
    }

    public function conceptcondition()
    {
        return view('parametrization/ConceptCondition');
    }

    public function downloadreceives()
    {
        return view('reports/Downloadreports');
    }

    //PAYRUN
    public function paymethodpayrun()
    {
        return view('payrun/PayMethodPayRun');
    }

    public function payrollclosed()
    {
        return view('payrun/PayrollClosed');
    }

    public function payrollopen()
    {
        return view('payrun/PayrollOpen');
    }

    public function payrollopennew()
    {
        return view('payrun/PayrollOpenNews');
    }

    public function runpayroll()
    {
        return view('payrun/RunPayroll');
    }

    public function sicoss()
    {
        return view('payrun/Sicoss');
    }
    public function suss()
    {
        return view('payrun/Suss');
    }

    //SECURITY
    public function rol()
    {
        return view('security/Rol');
    }

    public function user()
    {
        return view('security/User');
    }

    public function usercompany()
    {
        return view('security/Usercompany');
    }

    public function systemactivity()
    {
        return view('security/SystemActivity');
    }

    //TOOLS
    public function importarchives()
    {
        return view('tools/ImportArchives');
    }

    //MESSENGER
    public function messages()
    {
        return view('messenger/Messages');
    }

    public function news()
    {
        return view('messenger/News');
    }

    public function notes()
    {
        return view('messenger/Notes');
    }
    public function editpayrun()
    {
        return view('payrun/EditPayrun');
    }
    
    public function addpayrun()
    {
        return view('payrun/AddPayrun');
    }
    
    
    public function addmasive()
    {
        return view('payrun/addmasive');
    }
    
    //MASTER

    public function absencetype()
    {
        return view('master/AbsenceType');
    }
    public function employeeupdatemasive()
    {
        return view('master/EmployeeUpdateMasive');
    }

    public function positionjob()
    {
        return view('master/PositionJob');
    }

    public function workcalendar()
    {
        return view('master/WorkCalendar');
    }

    public function costcenter()
    {
        return view('master/CostCenter');
    }
    
     public function datasource()
    {
        return view('master/Datasource');
    }

    public function nacionality()
    {
        return view('master/Nacionality');
    }

    public function typecompany()
    {
        return view('master/Typecompany');
    }

    public function passwords()
    {
        return view('security/Passwords');
    }

    public function department()
    {
        return view('master/Departments');
    }

    public function exitreason()
    {
        return view('master/ExitReason');
    }

    public function regprevisional()
    {
        return view('master/RegPrevisional');
    }

    public function employeecondition()
    {
        return view('master/EmployeeCondition');
    }

    public function medicalplan()
    {
        return view('payrun/MedicalPlan');
    }

//reports

    public function reportenetobanco()
    {
        return view('reports/Reportenetobanco');
    }

    public function resumencompany()
    {
        return view('reports/Resumencompany');
    }

    public function reportsindicate()
    {
        return view('reports/Reportsindicate');
    }

    public function sindicate()
    {
        return view('master/Sindicate');
    }

    public function reportlicenses()
    {
        return view('reports/Reportlicenses');
    }

    public function reportextrahours()
    {
        return view('reports/Reportextrahours');
    }

    public function reportgenerator()
    {
        return view('reports/Reportgenerator');
    }

    public function reportaditional()
    {
        return view('reports/Reportaditional');
    }

    public function reportconceptroster()
    {
        return view('reports/Reportconceptroster');
    }


    public function bookspecialpayments()
    {
        return view('reports/Bookspecialpayments');
    }

    public function reportcontrolsettlement()
    {
        return view('reports/Reportcontrolsettlement');
    }


    public function art80()
    {
        return view('reports/Art80');
    }

    public function employcertificate()
    {
        return view('reports/Employcertificate');
    }

    public function form649()
    {
        return view('reports/Form649');
    }


    //master
    public function zone()
    {
        return view('master/Zone');
    }

    public function contact()
    {
        return view('master/Contact');
    }

    public function activitytax()
    {
        return view('master/ActivityTax');
    }

    public function activity()
    {
        return view('master/Activity');
    }

    public function sitrevision()
    {
        return view('master/SitRevision');
    }

    public function modcontract()
    {
        return view('master/ModContract');
    }

    public function career()
    {
        return view('master/Career');
    }

    public function languaje()
    {
        return view('master/Languaje');
    }

    public function level()
    {
        return view('master/Level');
    }

    public function category()
    {
        return view('master/Category');
    }

    public function socworks()
    {
        return view('master/SocWorks');
    }

    public function labunion()
    {
        return view('master/LabUnion');
    }

    public function workplaces()
    {
        return view('master/WorkPlaces');
    }

    public function ccts()
    {
        return view('master/Ccts');
    }

    public function terms()
    {
        return view('master/Terms');
    }

    public function specialreg()
    {
        return view('master/SpecialReg');
    }

    public function specialtyp()
    {
        return view('master/SpecialTyp');
    }

    public function sinestercode()
    {
        return view('master/AccidentCode');
    }

    public function pointsale()
    {
        return view('master/PointSale');
    }

    public function accountingaccount()
    {
        return view('master/AccountingAccount');
    }

    public function accountconcept()
    {
        return view('parametrization/AccountConcept');
    }

    public function taxes()
    {
        return view('master/Taxes');
    }

    public function paymethod()
    {
        return view('master/PayMethod');
    }

    public function group()
    {
        return view('master/Group');
    }


    public function onbuildsite()
    {
        return view('OnBuild');
    }


}
