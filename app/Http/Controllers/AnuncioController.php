<?php

namespace App\Http\Controllers;
use App\Models\Anuncio;
use Illuminate\Http\Request;

class AnuncioController extends Controller
{
    public function getanuncios()
    {
    	  $result = Anuncio::select('anuncio')    
      
            ->select('idn',
                     'idnfoto',
                     'idncategoria',
                     'idnusuario',
                     'titulo',
                     'descripcion',
                     'fecha',
                     'estatuscompra',                     
                     'estatus'                   
                      )
            ->get();
            return \Response::json($result,200);
    }
    
   public function getanunciosporcategoria($idncategoria)
    {
    	  $result = Anuncio::select('anuncio')    
      
    
    ->join('categoria', 'categoria.idn','=','anuncio.idncategoria')
   
                  
            ->select('anuncio.idn',
                     'anuncio.idnfoto',
                     'anuncio.idncategoria',
                     'anuncio.idnusuario',
                     'anuncio.titulo',
                     'anuncio.descripcion',
                     'anuncio.fecha',
                     'anuncio.estatuscompra',
                     'anuncio.estatus')
            ->where('anuncio.estatus', 1)
             ->where('anuncio.idncategoria', '=', $idncategoria)
            ->get();
            return \Response::json($result,200);
           
        
    }
    
    
       public function getanunciosporvender()
    {
    	  $result = Anuncio::select('anuncio')    
      
    
            ->select('anuncio.idn',
                     'anuncio.idnfoto',
                     'anuncio.idncategoria',
                     'anuncio.idnusuario',
                     'anuncio.titulo',
                     'anuncio.descripcion',
                     'anuncio.fecha',
                     'anuncio.estatuscompra',
                     'anuncio.estatus')
            ->where('anuncio.estatuscompra', 1)
             ->where('anuncio.estatus', 1)
            ->get();
            return \Response::json($result,200);
           
        
    }
    
       public function getanunciosvendidos()
    {
    	  $result = Anuncio::select('anuncio')    
      
    
            ->select('anuncio.idn',
                     'anuncio.idnfoto',
                     'anuncio.idncategoria',
                     'anuncio.idnusuario',
                     'anuncio.titulo',
                     'anuncio.descripcion',
                     'anuncio.fecha',
                     'anuncio.estatuscompra',
                     'anuncio.estatus')
            ->where('anuncio.estatuscompra', 0)
            ->where('anuncio.estatus', 1)
            ->get();
            return \Response::json($result,200);
           
        
    }
    
       public function getanunciosactivos()
    {
    	  $result = Anuncio::select('anuncio')    
      
    
            ->select('anuncio.idn',
                     'anuncio.idnfoto',
                     'anuncio.idncategoria',
                     'anuncio.idnusuario',
                     'anuncio.titulo',
                     'anuncio.descripcion',
                     'anuncio.fecha',
                     'anuncio.estatuscompra',
                     'anuncio.estatus')
            ->where('anuncio.estatus', 1)
           
            ->get();
            return \Response::json($result,200);
           
        
    }
    
     public function getanunciosinactivos()
    {
    	  $result = Anuncio::select('anuncio')    
      
    
            ->select('anuncio.idn',
                     'anuncio.idnfoto',
                     'anuncio.idncategoria',
                     'anuncio.idnusuario',
                     'anuncio.titulo',
                     'anuncio.descripcion',
                     'anuncio.fecha',
                     'anuncio.estatuscompra',
                     'anuncio.estatus')
            ->where('anuncio.estatus', 0)
           
            ->get();
            return \Response::json($result,200);
           
        
    }
    public function saveanuncios(Request $request)
    {
        //CACH DATA     
        $idnfoto = $request['idnfoto'];  
        $idncategoria = $request['idncategoria'];  
        $idnusuario = $request['idnusuario'];  
        $titulo = $request['titulo'];  
        $descripcion = $request['descripcion'];
        $fecha = $request['fecha'];
        $estatuscompra = $request['estatuscompra'];
          
        if ($idnfoto == "" or $idncategoria == "" or $idnusuario == "" or $titulo == "" or $descripcion == "" or $fecha == "") {
            return \Response::json("El Nombre no puede estar en nulo", 500);
        }
        try {
            $element = new Anuncio;
            $element->idnfoto = $idnfoto;
            $element->idncategoria = $idncategoria;
            $element->idnusuario = $idnusuario;
            $element->titulo = $titulo;  
            $element->descripcion = $descripcion;  
            $element->fecha = $fecha;  
            $element->estatuscompra= $estatuscompra;  
            $element->save();
            return \Response::json('Guardado correctamente: ' . $element, 200);
        } //CATCH COMPROBACION
        catch (QueryException $e) {
            //CATCH ERROR CODE
            $errorCode = $e->errorInfo[1];
            //IF THE ERROR IS DUPLICATE ENTRY
            if ($errorCode == 1062) {
                return \Response::json("Ya existe un elemento igual", 409);
            } else {
                return \Response::json("ERROR AL OBTENER LOS DATOS", 500);
            }
        }
    }
    public function updateanuncios(Request $request,$idn)
    {
        $idnfoto = $request['idnfoto'];  
        $idncategoria = $request['idncategoria'];  
        $idnusuario = $request['idnusuario'];  
        $titulo = $request['titulo'];  
        $descripcion = $request['descripcion'];
        $fecha = $request['fecha'];
        $estatuscompra = $request['estatuscompra'];
        
        if (  $idncategoria=="") {
            return \Response::json("hay campos vacios", 500);
        }
        try {
            $element = Anuncio::find($idn);          
            $element->idnfoto = $idnfoto;
            $element->idncategoria = $idncategoria;
            $element->idnusuario = $idnusuario;
            $element->titulo = $titulo;  
            $element->descripcion = $descripcion;  
            $element->fecha = $fecha;  
            $element->estatuscompra= $estatuscompra;           
            $element->update();
            return \Response::json('Actualizado correctamente: ' . $element, 200);
        } //CATCH COMPROBACION
        catch (QueryException $e) {
            //CATCH ERROR CODE
            $errorCode = $e->errorInfo[1];
            //IF THE ERROR IS DUPLICATE ENTRY
            if ($errorCode == 1062) {
                return \Response::json("Ya existe un elemento igual", 409);
            } else {
                return \Response::json("ERROR AL OBTENER LOS DATOS", 500);
            }
        }
    }
    
     public function deleteanuncios($idn)
    {

        try {

            $anuncio = Anuncio::find($idn);
            $anuncio->estatus = 0;
            $anuncio->update();
            return \Response::json('Eliminado correctamente: ', 200);

        } //CATCH COMPROBACION
        catch (PDOException $e) {
            return \Response::json("ERROR AL OBTENER LOS DATOS", 500);

        }
    }
}
