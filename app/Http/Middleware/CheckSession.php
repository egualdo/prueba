<?php

namespace App\Http\Middleware;

use Closure;
use Config;
class CheckSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

       $Host = session('HostDB');
       $Database = session('Database');
       $Username = session('UserDB');
       $Password = session('PassDB');
       
       
          if (session('Loged') != 1)

            {
                
                return redirect()->to('/');
            }
            else
            {
             Config::set('database.connections.mysql2.host',$Host);
             Config::set('database.connections.mysql2.database',$Database);
             Config::set('database.connections.mysql2.username',$Username);
             Config::set('database.connections.mysql2.password',$Password);

          
                 return $next($request);
            }
    }
}
