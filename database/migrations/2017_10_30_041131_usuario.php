<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Usuario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
             Schema::create('usuario', function (Blueprint $table) {
            $table->increments('idn');
            $table->string('username')->unique();
            $table->string('password');
                 $table->string('email');
                 $table->string('nombre');
                  $table->string('apellido');
                 $table->string('cedula');
                 $table->string('direccion');
                 $table->string('telefono');
                 $table->integer('idnciudad');
                 $table->string('sexo');
            $table->integer('idnrol');
            $table->integer('estatus')->default(1);  
            $table->timestamps();
        });
               DB::table('usuario')->insert(array('username' => 'admino',
                                                'password'=>bcrypt('holamundo'),
                                               'email' => 'colmenares923@gmail.com',
                                               'nombre' => 'ely',
                                               'apellido' => 'colmenares',
                                               'cedula' => '20866843',
                                               'direccion' => 'calle 20 entre 24 y 25',
                                               'telefono' => '04160382361',
                                               'idnciudad' => 1,
                                               'sexo' => 'm',
                                               'idnrol'=>1));
            //   DB::table('usuario')->insert(array('usuarioname' => 'paula','password'=>bcrypt('paula123'),'idnrol'=>2));
              // DB::table('usuario')->insert(array('usuarioname' => 'moraima','password'=>bcrypt('moraima123'),'idnrol'=>3));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('usuario');
    }
}


