<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Anuncio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anuncio', function (Blueprint $table) {
            $table->increments('idn');
            $table->integer('idnfoto');
            $table->integer('idncategoria');
            $table->integer('idnusuario');
            $table->string('titulo');
            $table->string('descripcion');
            $table->string('fecha');
            $table->integer('estatuscompra')->default(1);
            $table->integer('estatus')->default(1);
            $table->timestamps();
        });
        DB::table('anuncio')->insert(array('idnfoto' => 1,'idncategoria' => 1 ,'idnusuario' => 1,'titulo' => 'lavadora en venta','descripcion' => 'me en canta ','fecha' => '12/12/2018'));
       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
               Schema::drop('anuncio');
    }
}
