<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Provincia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('provincia', function (Blueprint $table) {
            $table->increments('idn');
            $table->integer('idnpais');
            $table->string('nombre');
            $table->integer('estatus')->default(1);
            $table->timestamps();
        });

        DB::table('provincia')
            ->insert(array('nombre' => 'No definido','idnpais' => 1));
        
        DB::table('provincia')
            ->insert(array('nombre' => 'Buenos Aires','idnpais' => 1));
        
        DB::table('provincia')
            ->insert(array('nombre' => 'Catamarca','idnpais' => 1));
        
        DB::table('provincia')
            ->insert(array('nombre' => 'Ciudad Autónoma de Buenos Aires','idnpais' => 1
                 ));
        DB::table('provincia')
            ->insert(array('nombre' => 'Chaco','idnpais' => 1
                 ));
        DB::table('provincia')
            ->insert(array('nombre' => 'Chubut','idnpais' => 1
                 ));
        DB::table('provincia')
            ->insert(array('nombre' => 'Córdoba','idnpais' => 1
                 ));
        DB::table('provincia')
            ->insert(array('nombre' => 'Corrientes','idnpais' => 1
                 ));
        DB::table('provincia')
            ->insert(array('nombre' => 'Entre Ríos','idnpais' => 1
                 ));
        DB::table('provincia')
            ->insert(array('nombre' => 'Formosa','idnpais' => 1
                 ));
        DB::table('provincia')
            ->insert(array('nombre' => 'Jujuy','idnpais' => 1
                 ));
        DB::table('provincia')
            ->insert(array('nombre' => 'La Pampa','idnpais' => 1
                 ));
        DB::table('provincia')
            ->insert(array('nombre' => 'La Rioja','idnpais' => 1
                 ));
        DB::table('provincia')
            ->insert(array('nombre' => 'Mendoza','idnpais' => 1
                 ));
        DB::table('provincia')
            ->insert(array('nombre' => 'Misiones','idnpais' => 1
                 ));
        DB::table('provincia')
            ->insert(array('nombre' => 'Neuquén','idnpais' => 1
                 ));
        DB::table('provincia')
            ->insert(array('nombre' => 'Río Negro','idnpais' => 1
                 ));
        DB::table('provincia')
            ->insert(array('nombre' => 'Salta','idnpais' => 1
                 ));
        DB::table('provincia')
            ->insert(array('nombre' => 'San Juan','idnpais' => 1
                 ));
        DB::table('provincia')
            ->insert(array('nombre' => 'San Luis','idnpais' => 1
                 ));
        DB::table('provincia')
            ->insert(array('nombre' => 'Santa Cruz','idnpais' => 1
                 ));
        DB::table('provincia')
            ->insert(array('nombre' => 'Santa Fe','idnpais' => 1
                 ));
        DB::table('provincia')
            ->insert(array('nombre' => 'Santiago del Estero','idnpais' => 1
                 ));
        DB::table('provincia')
            ->insert(array('nombre' => 'Tierra del Fuego, Antártida e Islas del Atlántico Sur','idnpais' => 1
                 ));
        DB::table('provincia')
            ->insert(array('nombre' => 'Tucumán','idnpais' => 1
                 ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('provincia');
    }
}
