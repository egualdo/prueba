<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Categoria extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categoria', function (Blueprint $table) {
            $table->increments('idn');
            $table->string('nombre');
            $table->string('descripcion');
            $table->integer('estatus')->default(1);
            $table->timestamps();
        });
        DB::table('categoria')->insert(array('nombre' => 'automoviles','descripcion' => 'productos para automoviles' ));
        DB::table('categoria')->insert(array('nombre' => 'hogar','descripcion' => 'productos para el hogar' ));
        DB::table('categoria')->insert(array('nombre' => 'tecnologia','descripcion' => 'productos tecnologicos' ));
       
       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
               Schema::drop('categoria');
    }
}
