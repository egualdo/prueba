<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class User extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->increments('idn');
            $table->string('username')->unique();
            $table->string('password');
            $table->string('avatar')->nullable();
            $table->integer('idnrol');
            $table->integer('active')->default(1);
            $table->timestamps();
        });
        DB::table('user')->insert(array('username' => 'admino', 'password' => bcrypt('holamundo'), 'idnrol' => 1));
        }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user');
    }
}


