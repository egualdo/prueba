<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Follows extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('follows', function (Blueprint $table) {
            $table->increments('idn');
            $table->string('idnfollower');
            $table->string('idnfollowing');
            $table->integer('lock')->default(1);
            $table->integer('active')->default(1);
            $table->timestamps();
        });
        DB::table('follows')->insert(array('idnfollower' => 1,'idnfollowing' => 2 ));
        DB::table('follows')->insert(array('idnfollower' => 1,'idnfollowing' => 3 ));
        DB::table('follows')->insert(array('idnfollower' => 2,'idnfollowing' => 3 ));
       
       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
               Schema::drop('follows');
    }
}
