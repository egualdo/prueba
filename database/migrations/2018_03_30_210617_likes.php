<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Likes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('likes', function (Blueprint $table) {
            $table->increments('idn');
            $table->integer('idnusuario');
            $table->integer('idnanuncio');
            $table->string('fecha');
            $table->integer('estatus')->default(1);
            $table->timestamps();
        });
        DB::table('likes')->insert(array('idnusuario' => 1, 'idnanuncio' => 1,'fecha' => '12/12/2018'));
        DB::table('likes')->insert(array('idnusuario' => 1, 'idnanuncio' => 2,'fecha' => '12/12/2018'));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
          Schema::drop('likes');
    }
}
