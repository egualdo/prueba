<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Ciudad extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ciudad', function (Blueprint $table) {
            $table->increments('idn');
             $table->integer('idnprovincia');
            $table->string('nombre')->unique();
              $table->integer('estatus')->default(1);
            $table->timestamps();
        });

        DB::table('ciudad')
             ->insert(array('idnprovincia' => 1,'nombre' => ' Capital Federal',
                              ));
        
              
    
          }                 
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ciudad');
    }
}