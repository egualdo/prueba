<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Comentario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comentario', function (Blueprint $table) {
            $table->increments('idn');
            $table->integer('idnanuncio');
            $table->string('descripcion');
            $table->integer('estatus')->default(1);
            $table->timestamps();
        });
        DB::table('comentario')->insert(array('idnanuncio' => 1, 'descripcion' => 'me en canta '));
        DB::table('comentario')->insert(array('idnanuncio' => 1, 'descripcion' => 'como hago para comprar'));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
               Schema::drop('comentario');
    }
}
