<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Foto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('foto', function (Blueprint $table) {
            $table->increments('idn');
            $table->string('codigo');
            $table->string('ruta');
            $table->integer('estatus')->default(1);
            $table->timestamps();
        });
        DB::table('foto')->insert(array('codigo' => 'p1','ruta' => '/publicalo/img/imagen.jpg' ));
       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
               Schema::drop('foto');
    }
}
