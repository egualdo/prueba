<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Rol extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rol', function (Blueprint $table) {
            $table->increments('idn');
            $table->string('nombre')->unique();
            $table->string('descripcion');
            $table->integer('estatus')->default(1);
            $table->timestamps();
        });
        DB::table('rol')->insert(array('nombre' => 'Admin - Sistemas', 'descripcion' => 'Usuario Root'));
        DB::table('rol')->insert(array('nombre' => 'Normal', 'descripcion' => 'comprador o vendedor'));
        DB::table('rol')->insert(array('nombre' => 'Usuario Empresa', 'descripcion' => 'Usuario perteniciente a la empresa'));

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rol');
    }
}
