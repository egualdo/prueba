@section('FatherMensajeria','true')
@section('ChildMensajeria','show')
        <!DOCTYPE html>
<html>
@include('layouts.heads')
<body>
<!-- Side Navbar -->
@include('layouts.sidebaradmin')
<div class="page forms-page">
    <!-- navbar-->
    @include('layouts.navbar')
    <div id="vuedata">
        <!-- Modal -->
        <!-- MODAL -->
        <div id="ModalEdicion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"
             class="modal text-left">
            <div role="document" class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 id="exampleModalLabel" class="modal-title">Editar Elemento</h5>
                        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span
                                    aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <p>Indica los datos solicitados</p>

                        <div class="row">
                            <div class="col-md-6">
                                
                               
                                    
                                <div class="form-group">
                                <label>Usuario</label>
                                <select v-model="fd.User" class="form-control">
                                    <option value="">Seleccione un Usuario</option>
                                    <option v-for="OneUser in fd.Useroptions" v-bind:value="OneUser.idn">
                                        @{{ OneUser.name}}
                                    </option>
                                </select>
                                </div>
                               
                                
                                   <div class="form-group">
                                <label>Post</label>
                                <select v-model="fd.Posts" class="form-control">
                                    <option value="">Seleccione un Post</option>
                                    <option v-for="OnePost in fd.Postsoptions" v-bind:value="OnePost.idn">
                                        @{{ OnePost.tittle}}
                                    </option>
                                </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Fecha </label>
                                    <input placeholder="" class="form-control" type="text" id="Date"
                                           v-model="fd.Date">
                                </div>
                            </div>

                        </div>


                    </div>
                    <div class="modal-footer">
                        <div v-if="ff.save === true">
                            <button v-on:click="guardar" class="btn btn-sucess btn-fill pull-left">Guardar</button>
                            <button type="button" class="btn btn-primary btn-fill pull-rigth" data-dismiss="modal">
                                Cerrar
                            </button>
                        </div>

                        <div v-if="ff.edit === true">
                            <button v-on:click="editar" class="btn btn-info btn-fill pull-left">Editar</button>
                            <button v-on:click="borrar" class="btn btn-warning btn-fill pull-rigth">Eliminar</button>
                            <button type="button" class="btn btn-primary btn-fill pull-rigth" data-dismiss="modal">
                                Cerrar
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- FIN MODAL -->
        <!-- FIN DE LA MODAL -->


        <!-- CABECERA GRIS-->
        <div class="breadcrumb-holder fixed-top">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-2 border">
                    </div>
                    <div class="col-md-7 border">
                        <!-- TITULO DE VENTANA -->
                        <h1> Likes</h1>
                    </div>
                    <div class="col-md-3">
                        <!--<p>Dato de Prueba</p>-->
                        <button v-on:click="openmodaltonew" class="btn btn-info btn-fill pull-left">Nuevo Elemento
                        </button>
                    </div>

                </div>
            </div>
        </div>
        <!-- FIN DE LA CABECERA GRIS -->
    </div>

    <section class="forms">
        <div class="container-fluid">

            <!-- Tarjeta -->
            <div class="card">
                <!-- Cabecera de Tarjeta  -->

                <!-- Cuerpo de Tarjeta -->
                <div class="card-body">

                    <div class="row">
                        <!-- === PANEL DE TABLA === -->
                        <!-- ====== DATATABLE ======= -->
                        <table id="TablaMaster" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>Idn</th>
                                <th>Usuario</th>
                                <th>Post</th>
                                <th>Fecha</th>
                                
                            </tr>
                            </thead>
                        </table>
                        <!-- ====== FIN DATATABLE======= -->
                    </div>
                </div>
            </div>
            <!-- FIN de Tarjeta Hija -->
        </div>
    </section>


    @include('layouts.footer')
</div>
@include('layouts.libraries')
<script src="../js/messengerjs/likes.js"></script>
</body>
</html>