@section('FatherParametrizacion','true')
@section('ChildParametrizacion','show')
<!DOCTYPE html>
<html>
@include('layouts.heads')
<body>
<!-- Side Navbar -->
@include('layouts.sidebaradmin')
<div class="page forms-page">
    <!-- navbar-->
    @include('layouts.navbar')
    <div id="vuedata">
        <!-- Modal -->


        <!-- MODAL -->
        <div id="ModalEdicion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"
             class="modal text-left">
            <div role="document" class="modal-dialog">
                <div class="modal-content" style="min-width: 700px;">
                    <div class="modal-header">
                        <h5 id="exampleModalLabel" class="modal-title">Editar Elemento</h5>
                        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span
                                    aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <p>Indica los datos solicitados</p>
                        <div class="row">
                            <div class="col-md-4">

                                <div class="form-group">
                                    <label>Titulo</label>
                                    <input v-model="fd.Title" type="text" placeholder="" class="form-control">
                                </div>


                                <div class="form-group">
                                    <label>Calendario</label>
                                    <v-select v-model="fd.Calendar" :options="fd.Calendaroptions"></v-select>
                                </div>

                                <div class="form-group">
                                    <label>Fecha Inicio</label>
                                    <input placeholder="" class="form-control" type="text" id="StartDate"
                                           v-model="fd.StartDate">
                                </div>

                                <div class="form-group ">
                                    <label>Fecha Fin</label>

                                    <input placeholder="" class="form-control" type="text" id="FinishDate"
                                           v-model="fd.FinishDate">
                                </div>


                            </div>
                            <div class="col-md-4">


                        
                                   {{--  <label>Dias</label> --}}
                                    <input v-model.number="fd.Dias" type="text" v-on:click="calculardias" id="dias"
                                           class="form-control" v-if="false">
                               

                                <div class="form-group">
                                    <label>Año</label>
                                    <input v-model="fd.Year" type="number" placeholder="" class="form-control">
                                </div>

                                <div class="form-group">
                                    <label>Tipo de Periodo</label>
                                    <v-select v-model="fd.Typeperiod" :options="fd.Typeperiodoptions"></v-select>

                                </div>


                                <div class="form-group">
                                    <label>Mes</label>
                                    <v-select v-model="fd.Month" :options="fd.Monthoptions"></v-select>

                                </div>
                            </div>
                            <div class="col-md-4">


                              

                                <div class="form-group">
                                    <label>Sicoss Activo</label>
                                    <v-select v-model="fd.Sicoss" :options="fd.SiccossOptions"></v-select>
                                </div>

                                <div class="form-group">
                                    <label>Especial</label>
                                    <v-select v-model="fd.Special" :options="fd.SpecialOptions"></v-select>

                                </div>

                                <div class="form-group">
                                    <label>Cantidad de Periodos</label>
                                    <input v-model="fd.Numperiodmonth" type="number" placeholder=""
                                           class="form-control">
                                </div>

                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <div v-if="ff.save == true">
                            <button v-on:click="guardar" class="btn btn-sucess btn-fill pull-left">Guardar</button>
                            <button type="button" class="btn btn-primary btn-fill pull-rigth" data-dismiss="modal">
                                Cerrar
                            </button>
                        </div>

                        <div v-else-if="ff.edit == true">
                            <button v-on:click="editar" class="btn btn-info btn-fill pull-left">Editar</button>
                            <button v-on:click="borrar" class="btn btn-warning btn-fill pull-rigth">Eliminar</button>
                            <button type="button" class="btn btn-primary btn-fill pull-rigth" data-dismiss="modal">
                                Cerrar
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- FIN MODAL -->

        <!-- FIN DE LA MODAL -->


        <!-- CABECERA GRIS-->
        <div class="breadcrumb-holder fixed-top">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-2 border">
                    </div>
                    <div class="col-md-7 border">
                        <!-- TITULO DE VENTANA -->
                        <h1> Periodos</h1>
                    </div>
                    <div class="col-md-3">
                        <!--<p>Dato de Prueba</p>-->
                        <button v-on:click="openmodaltonew" class="btn btn-info btn-fill pull-left">Nuevo Elemento
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- FIN DE LA CABECERA GRIS -->
    </div>

    <section class="forms">
        <div class="container-fluid">

            <!-- Tarjeta -->
            <div class="card">
                <!-- Cabecera de Tarjeta  -->

                <!-- Cuerpo de Tarjeta -->
                <div class="card-body">

                    <div class="row">

                        <table id="TablaPeriodo" class="table table-striped table-bordered" cellspacing="0"
                               width="100%">
                            <thead>
                            <tr>
                                <th>Idn</th>
                                <th>Titulo</th>
                                <th>Calendario</th>
                                <th>Tipo periodo</th>
                                <th>Fecha inicio</th>
                                <th>Fecha fin</th>

                            </tr>
                            </thead>
                        </table>
                        <!-- === PANEL DE TABLA === -->
                        <!-- ====== DATATABLE ======= -->

                        <!-- ====== FIN DATATABLE======= -->
                    </div>
                </div>
            </div>
            <!-- FIN de Tarjeta Hija -->
        </div>
    </section>


    @include('layouts.footer')
</div>
@include('layouts.libraries')
<script src="../js/parametrizationjs/period.js"></script>
</body>
</html>