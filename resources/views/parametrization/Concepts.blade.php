@section('FatherParametrizacion','true')
@section('ChildParametrizacion','show')
<!DOCTYPE html>
  <html>
  @include('layouts.heads')
  <body>
    <!-- Side Navbar -->
    @include('layouts.sidebaradmin')
    <div class="page forms-page">
      <!-- navbar-->
      @include('layouts.navbar')
      <div id="vuedata">
       <!-- Modal -->

       <!-- MODAL -->
       <div id="ModalEdicion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal text-left">
        <div role="document" class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 id="exampleModalLabel" class="modal-title">Editar Elemento</h5>
              <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
              <p>Indica los datos solicitados</p>

              <div class="form-group">
                <label>Codigo</label>
                <input v-model="fd.Code" type="text" placeholder="" class="form-control">
              </div>

              <div class="form-group">       
                <label>Nombre</label>
                <input v-model="fd.Name" type="text" placeholder="" class="form-control">
              </div>

              <div class="form-group">
                <label>Columna</label>
              </br>
                <v-select v-model="fd.Column" :options="fd.Columnoptions"></v-select>
            </div>

            <div class="form-group">
              <label>Orden a mostrar en el Recibo</label>
            </br>
             <input v-model="fd.Order" type="text" placeholder="" class="form-control">
          <!--  <select class="form-control" name="Condition" v-model="fd.Condition" >
              <option value=1>Pagos</option>
              <option value=2>No Remunerativos</option>
              <option value=3>Deducciones</option>
              <option value=4>Excentos</option>
            </select>-->
          </div>

          
        </div>
        <div class="modal-footer">
          <div v-if="ff.save == true">
            <button v-on:click="guardar" class="btn btn-sucess btn-fill pull-left">Guardar</button>
            <button type="button" class="btn btn-primary btn-fill pull-rigth" data-dismiss="modal">Cerrar</button>
          </div>

          <div v-else-if="ff.edit == true">
            <button v-on:click="editar" class="btn btn-info btn-fill pull-left">Editar</button>
            <button v-on:click="borrar" class="btn btn-warning btn-fill pull-rigth">Eliminar</button>
            <button type="button" class="btn btn-primary btn-fill pull-rigth" data-dismiss="modal">Cerrar</button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- FIN MODAL -->

  <!-- FIN DE LA MODAL -->


  <!-- CABECERA GRIS-->
  <div class="breadcrumb-holder fixed-top">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-2 border">
        </div>
        <div class="col-md-7 border">
          <!-- TITULO DE VENTANA -->
          <h1> Conceptos</h1>
        </div>
        <div class="col-md-3">
          <!--<p>Dato de Prueba</p>-->
          <button v-on:click="openmodaltonew" class="btn btn-info btn-fill pull-left">Nuevo Elemento</button>
        </div>
      </div>
    </div>
  </div>
  <!-- FIN DE LA CABECERA GRIS -->
</div>

<section class="forms">
  <div class="container-fluid">

    <!-- Tarjeta -->
    <div class="card" >
      <!-- Cabecera de Tarjeta  -->

      <!-- Cuerpo de Tarjeta -->
      <div class="card-body">  

        <div class="row">

         <table id="TablaConcept" class="table table-striped table-bordered" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>Idn</th>
              <th>Código</th>
              <th>Nombre</th>
              <th>Columna</th>
              <th>Orden en Recibo</th>

            </tr>
          </thead>
        </table>
        <!-- === PANEL DE TABLA === --> 
        <!-- ====== DATATABLE ======= -->

        <!-- ====== FIN DATATABLE======= -->
      </div>
    </div>
  </div>
  <!-- FIN de Tarjeta Hija -->
</div>
</section>



@include('layouts.footer')
</div>
@include('layouts.libraries')
<script src="../js/parametrizationjs/concepts.js"></script>
</body>
</html>