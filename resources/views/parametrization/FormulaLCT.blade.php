@section('FatherParametrizacion','true')
@section('ChildParametrizacion','show')
<!DOCTYPE html>
<html>
@include('layouts.heads')
<body>
<!-- Side Navbar -->
@include('layouts.sidebaradmin')
<div class="page forms-page">
    <!-- navbar-->
@include('layouts.navbar')

<!-- FIN DE LA MODAL -->
    <!-- CABECERA GRIS-->
    <div class="breadcrumb-holder fixed-top">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-2 border">
                </div>
                <div class="col-md-7 border">
                    <!-- TITULO DE VENTANA -->
                    <h1> Formulas LCT</h1>
                </div>
                <div class="col-md-3">
                    <div id="ActionForm">
                        <!--<p>Dato de Prueba</p>-->
                        <button v-on:click="openmodal" class="btn btn-info btn-fill pull-left">Nueva Formula</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- FIN DE LA CABECERA GRIS -->
    <section class="forms">
        <div class="container-fluid">

            <!-- Tarjeta -->
            <div class="card">
                <!-- Cabecera de Tarjeta  -->

                <!-- Cuerpo de Tarjeta -->
                <div class="card-body">

                    <div class="row">

                        <table id="TablaFormulaLCT" class="table table-striped table-bordered" cellspacing="0"
                               width="100%">
                            <thead>
                            <tr>
                                <th>Idn</th>
                                <th>Concepto</th>
                                <th>Calculo</th>

                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <!-- FIN de Tarjeta Hija -->
        </div>

    </section>

    <!-- Modal -->

    <!-- MODAL -->
    <div id="ModalEdicion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"
         class="modal text-left">
        <div role="document" class="modal-dialog" style="min-width: 1100px;">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 id="exampleModalLabel" class="modal-title">Editar Elemento</h5>
                    <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span
                                aria-hidden="true"></span></button>
                </div>
                <div class="modal-body">
                    <div id="ModalForm">
                        <div class="row">
                            <div class="col-md-6">

                                <div class="form-group">
                                    <v-select v-model="fd.Concept" :options="Conceptoptions"></v-select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div v-if="ff.save == true">
                                    <button v-on:click="guardar" class="btn btn-sucess btn-fill pull-left">Guardar
                                    </button>
                                    <button type="button" class="btn btn-primary btn-fill pull-rigth"
                                            data-dismiss="modal">Cerrar
                                    </button>
                                </div>

                                <div v-else-if="ff.edit == true">
                                    <button type="button" v-on:click="editar" class="btn btn-info btn-fill pull-left">
                                        Editar
                                    </button>
                                    <button type="button" v-on:click="borrar"
                                            class="btn btn-warning btn-fill pull-rigth">Eliminar
                                    </button>
                                    <button type="button" class="btn btn-primary btn-fill pull-rigth"
                                            data-dismiss="modal">Cerrar
                                    </button>
                                </div>

                            </div>
                        </div>


                    </div>

                    <textarea id="code" name="code">
          </textarea>

                    @include('layouts.codemirrorlibraries')

                    <div class="form-group">
                        <input type="input" placeholder="idn" v-model="fd.Cct" class="form-control"
                               style="visibility:hidden">
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- FIN MODAL -->

    <!-- FIN DE LA MODAL -->

    @include('layouts.footer')
</div>

@include('layouts.libraries')
<script src="../js/generic/generic.js" type="text/javascript"></script>
<script src="../js/parametrizationjs/formulaLCT.js"></script>
</body>
</html>

