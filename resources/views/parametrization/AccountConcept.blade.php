@section('FatherParametrizacion','true')
@section('ChildParametrizacion','show')
<!DOCTYPE html>
  <html>
  @include('layouts.heads')
  <body>
    <!-- Side Navbar -->
    @include('layouts.sidebaradmin')
    <div class="page forms-page">
      <!-- navbar-->
      @include('layouts.navbar')
      <div id="vuedata">
       <!-- Modal -->
       <!-- MODAL -->
       <div id="ModalAccountConcept" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal text-left">
        <div role="document" class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 id="exampleModalLabel" class="modal-title">Editar Elemento</h5>
              <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
              <p>Indica los datos solicitados</p>
              
               <div class="form-group">       
                <label>Conceptos</label>
                                  <select v-model="fd.Concept" class="form-control">
                                <option value="">Seleccione</option>
                               <option v-for="OneConcept in fd.Conceptoptions" v-bind:value="OneConcept.idn">
                                @{{ OneConcept.name}}
                                 </option>
                                 </select>
              </div> 
                 
                    <div class="form-group">       
                 <label>Cuentas Contables</label>
                                  <select v-model="fd.AccountingAccount" class="form-control">
                                <option value="">Seleccione</option>
                               <option v-for="OneAccountingAccount in fd.Accountingaccountoptions" v-bind:value="OneAccountingAccount.idn">
                                @{{ OneAccountingAccount.name}}
                                 </option>
                                 </select>
                </div>
              
              
            </div>
            <div class="modal-footer">
              <div v-if="ff.save == true">
                <button v-on:click="guardarAccountConcept" class="btn btn-sucess btn-fill pull-left">Guardar</button>
                <button type="button" class="btn btn-primary btn-fill pull-rigth" data-dismiss="modal">Cerrar</button>
              </div>

              <div v-else-if="ff.edit == true">
                <button v-on:click="editarAccountConcept" class="btn btn-info btn-fill pull-left">Editar</button>
                <button v-on:click="borrarAccountConcept" class="btn btn-warning btn-fill pull-rigth">Eliminar</button>
                <button type="button" class="btn btn-primary btn-fill pull-rigth" data-dismiss="modal">Cerrar</button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- FIN MODAL -->
      <!-- FIN DE LA MODAL -->

      <!-- CABECERA GRIS-->
      <div class="breadcrumb-holder fixed-top">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-2 border">
            </div>
            <div class="col-md-7 border">
              <!-- TITULO DE VENTANA -->
              <h1> Cuentas Contables / Concepto</h1>
            </div>
            <div class="col-md-3">
              <!--<p>Dato de Prueba</p>-->
              <button v-on:click="openmodaltonew" class="btn btn-info btn-fill pull-left">Nuevo Elemento</button>
            </div>

          </div>
        </div>
      </div>
      <!-- FIN DE LA CABECERA GRIS -->
    </div>

    <section class="forms">
      <div class="container-fluid">

        <!-- Tarjeta -->
        <div class="card" >
          <!-- Cabecera de Tarjeta  -->

          <!-- Cuerpo de Tarjeta -->
          <div class="card-body">  

            <div class="row">
              <!-- === PANEL DE TABLA === --> 
              <!-- ====== DATATABLE ======= -->
              <table id="TablaAccountConcept" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>Idn</th>
                    <th>Concepto</th>
                      <th>Cuenta Contable</th>
                   
                  </tr>
                </thead>
              </table>
              <!-- ====== FIN DATATABLE======= -->
            </div>
          </div>
        </div>
        <!-- FIN de Tarjeta Hija -->
      </div>
    </section>



    @include('layouts.footer')
  </div>
  @include('layouts.libraries')
  <script src="../js/parametrizationjs/accountconcept.js"></script>
</body>
</html>