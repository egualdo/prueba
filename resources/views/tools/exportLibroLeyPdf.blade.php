<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link href="{{ asset('css/receivestyle.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
    * {
        font-size: 10px;
    }
    table, th, td {
        border: 1px solid black !important;;
        border-collapse: collapse;
        border-color: #000000 !important;
    }
</style>
</head>
<body>
    <div>
        <h4>Ley 20.744 ART. 52 - HOJAS MÓVILES ( {{\Carbon\Carbon::parse($payruns[0]->payrunstart)->format('d/m/Y')}} al  {{\Carbon\Carbon::parse($payruns[0]->payrunend)->format('d/m/Y')}} )</h4>
        <table style="width:100%;; border: none; border-color: transparent !important;">
            <tr style="font-weight: 900;">
                <td style="width:25%; text-align: center; border: none; border-color: transparent !important;">Razón Social <br>{{session('NameCompany')}}</td>
                <td style="width:25%; text-align: center; border: none; border-color: transparent !important;">Domicilio legal <br>{{$company->fiscaldomicile}}
                </td>
                <td style="width:25%; text-align: center; border: none; border-color: transparent !important;">Actividad <br>{{$company->activity}}</td>
                <td style="width:25%; text-align: center; border: none; border-color: transparent !important;">CUIT <br>{{session('CuitCompany')}}</td>
            </tr>
        </table>
        <br>
        <div class="clearfix"></div>
        @php
        $fathab = 0;
        $fatdesc = 0;
        $fattotal = 0;
        @endphp
        @foreach($payruns as $payrun)
        <table style="width:100%">
            <tr style="font-weight: 900">
                <td style="width:25%; text-align: center">Legajo: {{$payrun->cod}} CUIL: {{$payrun->cuil}} <br> ingreso:
                    {{\Carbon\Carbon::parse($payrun->employee->dateentry)->format('d/m/Y')}}
                </td>
                <td style="width:25%; text-align: center">{{$payrun->fullname}}</td>
                <td style="width:25%; text-align: center">Cargo: <br> {{$payrun->namepositionjob}}</td>
                <td style="width:25%; text-align: center">Domicilio: <br>{{$payrun->employee->city}} {{$payrun->employee->province}} {{$payrun->employee->postalcode}} {{$payrun->employee->street}}</td>
            </tr>
        </table>
        <table style="width:100%">
            <tr style="font-weight: 900">
                <td style="width:25%; text-align: center">Detalle de Liquidación</td>
                <td style="width:8%; text-align: center"></td>
                <td style="width:12%; text-align: center">Hab.Suj.Aport</td>
                <td style="width:12%; text-align: center">Otros Haberes</td>
                <td style="width:12%; text-align: center">Salario Famil</td>
                <td style="width:12%; text-align: center">Descuentos</td>
                <td style="width:12%; text-align: center">Neto Cobrado</td>
            </tr>
            @php
            $hab = 0;
            $desc = 0;
            @endphp
            @foreach($payrun->payrunbody as $body)
            @php
            if($body->columnconcept === 1){
            $hab = $hab + $body->amount;
        }
        if($body->columnconcept === 3){
        $desc = $desc + $body->amount;
    }
    @endphp
    <tr>
        <td>{{$body->nameconcept}}</td>
        <td style="text-align: center"></td>
        <td style="text-align: right">{{$body->columnconcept === 1 ? $body->amount : '' }}</td>
        <td style="text-align: right"></td>
        <td style="text-align: right"></td>
        <td style="text-align: right">{{$body->columnconcept === 3 ? $body->amount : '' }}</td>
        <td style="text-align: right"></td>
    </tr>
    @endforeach
    <tr>
        <td>Redondeo</td>
        <td style="text-align: center"></td>
        <td style="text-align: right"></td>
        <td style="text-align: right"></td>
        <td style="text-align: right"></td>
        <td style="text-align: right"></td>
        <td style="text-align: right"></td>
    </tr>
    <tr>
        <td>Totales del Legajo</td>
        <td style="text-align: center"></td>
        <td style="text-align: right">{{$hab}}</td>
        <td style="text-align: right"></td>
        <td style="text-align: right"></td>
        <td style="text-align: right">{{$desc}}</td>
        <td style="text-align: right">{{$payrun->totalpay}}</td>
    </tr>
</table>
@php
$fathab = $fathab + $hab;
$fatdesc = $fatdesc + $desc;
$fattotal = $fattotal + $payrun->totalpay;
@endphp
@endforeach
<table style="width:100%">
    <tr style="font-weight: 900; font-size: 12px !important;">
        <td style="width:25%; ">TOTALES EMPRESA</td>
        <td style="width:8%; text-align: center"></td>
        <td style="width:12%; text-align: right">{{$fathab}}</td>
        <td style="width:12%; text-align: right"></td>
        <td style="width:12%; text-align: right"></td>
        <td style="width:12%; text-align: right">{{$fatdesc}}</td>
        <td style="width:12%; text-align: right">{{$fattotal}}</td>
    </tr>
</table>
</div>
</body>
</html>
