<!DOCTYPE html>
<html>
@include('layouts.heads')
<body>
@include('layouts.sidebaradmin')
<div class="page forms-page">
    @include('layouts.navbar')
    <div id="vuedata">
        <div class="breadcrumb-holder fixed-top">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-2 border">
                    </div>
                    <div class="col-md-6 border">
                        <h1>Importacion de Archivos</h1>
                    </div>
                    <div class="col-md-4">

                    </div>
                </div>
            </div>
        </div>
        <section class="forms">
            <div class="container-fluid">
                <div class="card">
                    <div class="card-body">
                        <h1>Instrucciones</h1>
                        <div class="row">
                            <div class="col-md-12">
                                <ul style="list-style: none; font-size:12px;">
                                    <li>· El formato de fichero debe ser CSV.
                                    </li>
                                    <li>· La primera línea debe contener los nombre de los campos. Debe utilizar nombres
                                        de
                                        campos válidos según el tipo de importación que elija (al elegir un tipo se
                                        informa
                                        los nombres de campo válidos).
                                    </li>
                                    <li>· Los campos deben estar en orden</li>
                                    <li>· Todos los campos son obligatorios incluir (al elegir un tipo se informa
                                        cuales campos debe incluir).
                                    </li>
                                </ul>
                                <br>
                                <div v-show="fd.TypeImportFile === 1" style="display: none;" >
                                    <h1>Plantilla Legajo</h1>
                                    <a href="../examples/employee.csv" download="ExampleFileEmployee.csv" class="btn btn-primary btn-fill pull-rigth" style="cursor:pointer">Descargar Archivo de Ejemplo</a>
                                </div>
                                <div v-show="fd.TypeImportFile === 2" style="display: none;" >
                                    <h1>Plantilla Constante Legajo</h1>
                                    <a href="../examples/constant_employee.csv" download="ExampleFileConstantEmployee.csv" class="btn btn-primary btn-fill pull-rigth" style="cursor:pointer">Descargar Archivo de Ejemplo</a>
                                </div>
                                <div v-show="fd.TypeImportFile === 3" style="display: none;" >
                                    <h1>Plantilla Constante CCT</h1>
                                    <a href="../examples/constant_cct.csv" download="ExampleFileConstantCct.csv" class="btn btn-primary btn-fill pull-rigth" style="cursor:pointer">Descargar Archivo de Ejemplo</a>
                                </div>
                                <div v-show="fd.TypeImportFile === 4" style="display: none;" >
                                    <h1>Plantilla Constante LCT</h1>
                                    <a href="../examples/constant_lct.csv" download="ExampleFileConstantLct.csv" class="btn btn-primary btn-fill pull-rigth" style="cursor:pointer">Descargar Archivo de Ejemplo</a>
                                </div>
                                <div v-show="fd.TypeImportFile === 5" style="display: none;" >
                                    <h1>Plantilla Constante por Empresa</h1>
                                    <a href="../examples/constant_company.csv" download="ExampleFileConstantCompany.csv" class="btn btn-primary btn-fill pull-rigth" style="cursor:pointer">Descargar Archivo de Ejemplo</a>
                                </div>
                                <div v-show="fd.TypeImportFile === 6" style="display: none;" >
                                    <h1>Plantilla Asignaciones/Deducciones</h1>
                                    <a href="../examples/asignation.csv" download="ExampleFileAsignation.csv" class="btn btn-primary btn-fill pull-rigth" style="cursor:pointer">Descargar Archivo de Ejemplo</a>
                                </div>
                               </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="forms">
            <div class="container-fluid">
                <div class="card">
                    <div class="card-body">
                        <h1>Importar Archivo</h1>
                        <div class="row">
                            <div class="col-md-6">
                                <label>Tipo (Requerido)</label>
                                <select class="form-control" v-model="fd.TypeImportFile">
                                    <option v-for="TypeImportFile in fd.TypeImportFiles"
                                            :selected="TypeImportFile.id == 2 ? 'selected' : ''"
                                            :value="TypeImportFile.id">
                                        @{{ TypeImportFile.name }}
                                    </option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label>Archivo</label>
                                <br>
                                <label for="file" class="btn btn-warning" style="cursor:pointer">1- Seleccionar Archivo</label>
                                <input type="file" accept=".csv"  v-on:change="fd.File = this.value" id="file" style="display:none">
                                <label v-on:click="verificar" class="btn btn-success  " style="cursor:pointer">2- Verificar</label>
                                <label v-on:click="importar" class="btn btn-primary  pull-rigth" style="cursor:pointer; border-radius: 5px;">3- Importar</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="forms">
            <div class="container-fluid">
                <div class="card">
                    <div class="card-body">
                        <h1>Campos Validos</h1>
                        <div class="row">
                            <div v-show="fd.TypeImportFile === 1" style="display: none;" class="table-responsive">
                                <table class="table table-hover" style="font-size: 12px">
                                    <tbody>
                                    <tr>
                                        <th><h2>Campo</h2></th>
                                        <th><h2>Obligatorio</h2></th>
                                        <th><h2>Tipo</h2></th>
                                        <th><h2>Descripción</h2></th>
                                        <th><h2>Valores válidos</h2></th>
                                    </tr>
                                    </tbody>
                                    <tr>
                                        <th>legajo</th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>cod</th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>firstname
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>lastname
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>gender
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>dni
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>cuil
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>birthdate
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>photo
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>city
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>province
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>postalcode
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>street
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>number
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>floor
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>department
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>telephone
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>cellphone
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>workemail
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>personemail
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>idnnationality
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>idncivilstatus
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>idnlevelstudies
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>idncostcenter
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>idndepartment
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>idnpositionjob
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>idnworkcalendar
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>idncct
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>idncategorysalary
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>idnexitreason
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>idnsocialwork
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>idncompany
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>idnsitrevision
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>idnmodcontract
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>idnworkplace
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>idnroster
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>idnsindicate
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>idnregprevisional
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>idnsinestercode
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>idnemployeecondition
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>idnmedicalplan
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>basicsalary
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>employeestatus
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>dateentry
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>dateentryrecognized
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>dateexit
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>antiquity
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>adherents
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>numberofcap
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>affiliate
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>lifesecure
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>benefitsoccharges</th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>agreed</th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>lock
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>active
                                        </th>
                                        <td>Sí</td>
                                        <td>Valores predeterminados</td>
                                        <td>nro de legajo</td>
                                        <td></td>
                                    </tr>
                                </table>
                            </div>
                            <div v-show="fd.TypeImportFile === 2" style="display: none;" class="table-responsive">
                                <table class="table table-hover" style="font-size: 12px">
                                    <tbody>
                                    <tr>
                                        <th><h2>Campo</h2></th>
                                        <th><h2>Tipo</h2></th>
                                        <th><h2>Descripción</h2></th>
                                        <th><h2>Valores válidos</h2></th>
                                    </tr>
                                    </tbody>
                                    <tr>
                                        <th>periodo</th>
                                        <td>Texto</td>
                                        <td>Indica a que periodo pertenece</td>
                                        <td>Si el tipo de periodo es mensual: <h6>Enero 2018</h6> <br>
                                            Si el tipo de periodo es quincenal: <h6>Primera quincena de Enero 2018</h6>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>constante</th>
                                        <td>Texto</td>
                                        <td>Es el codigo de la constante</td>
                                        <td>Ejemplo: <h6>PRESENTVALOR</h6></td>
                                    </tr>
                                    <tr>
                                        <th>valor</th>
                                        <td>Númerico (Punto Flotante)</td>
                                        <td>El Valor que será asignado</td>
                                        <td>Ejemplo: <h6>12.00</h6></td>
                                    </tr>
                                    <tr>
                                        <th>comentario</th>
                                        <td>Texto</td>
                                        <td>Una Breve descripción</td>
                                        <td>Ejemplo: <h6>Concepto de Presentismo</h6></td>
                                    </tr>
                                    <tr>
                                        <th>codigolegajo</th>
                                        <td>Númerico (Entero)</td>
                                        <td>Es el Codigo del Legajo</td>
                                        <td>Ejemplo: <h6>1000</h6></td>
                                    </tr>
                                </table>
                            </div>
                            <div v-show="fd.TypeImportFile === 3" style="display: none;" class="table-responsive">

                                <table class="table table-hover" style="font-size: 12px">
                                    <tbody>
                                    <tr>
                                        <th><h2>Campo</h2></th>
                                        <th><h2>Tipo</h2></th>
                                        <th><h2>Descripción</h2></th>
                                        <th><h2>Valores válidos</h2></th>
                                    </tr>
                                    </tbody>
                                    <tr>
                                        <th>periodo</th>
                                        <td>Texto</td>
                                        <td>Indica a que periodo pertenece</td>
                                        <td>Si el tipo de periodo es mensual: <h6>Enero 2018</h6> <br>
                                            Si el tipo de periodo es quincenal: <h6>Primera quincena de Enero 2018</h6>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>constante</th>
                                        <td>Texto</td>
                                        <td>Es el codigo de la constante</td>
                                        <td>Ejemplo: <h6>PRESENTVALOR</h6></td>
                                    </tr>
                                    <tr>
                                        <th>valor</th>
                                        <td>Númerico (Punto Flotante)</td>
                                        <td>El Valor que será asignado</td>
                                        <td>Ejemplo: <h6>12.00</h6></td>
                                    </tr>
                                    <tr>
                                        <th>comentario</th>
                                        <td>Texto</td>
                                        <td>Una Breve descripción</td>
                                        <td>Ejemplo: <h6>Modificar asociación civil</h6></td>
                                    </tr>
                                    <tr>
                                        <th>cct</th>
                                        <td>Texto</td>
                                        <td>Es el Codigo del CCT</td>
                                        <td>Ejemplo: <h6>asocciv</h6></td>
                                    </tr>
                                </table>
                            </div>
                            <div v-show="fd.TypeImportFile === 4" style="display: none;" class="table-responsive">

                                <table class="table table-hover" style="font-size: 12px">
                                    <tbody>
                                    <tr>
                                        <th><h2>Campo</h2></th>
                                        <th><h2>Tipo</h2></th>
                                        <th><h2>Descripción</h2></th>
                                        <th><h2>Valores válidos</h2></th>
                                    </tr>
                                    </tbody>
                                    <tr>
                                        <th>periodo</th>
                                        <td>Texto</td>
                                        <td>Indica a que periodo pertenece</td>
                                        <td>Si el tipo de periodo es mensual: <h6>Enero 2018</h6> <br>
                                            Si el tipo de periodo es quincenal: <h6>Primera quincena de Enero 2018</h6>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>constante</th>
                                        <td>Texto</td>
                                        <td>Es el codigo de la constante</td>
                                        <td>Ejemplo: <h6>PRESENTVALOR</h6></td>
                                    </tr>
                                    <tr>
                                        <th>valor</th>
                                        <td>Númerico (Punto Flotante)</td>
                                        <td>El Valor que será asignado</td>
                                        <td>Ejemplo: <h6>12.00</h6></td>
                                    </tr>
                                    <tr>
                                        <th>comentario</th>
                                        <td>Texto</td>
                                        <td>Una Breve descripción</td>
                                        <td>Ejemplo: <h6>Comentario </h6></td>
                                    </tr>
                                </table>
                            </div>
                            <div v-show="fd.TypeImportFile === 5" style="display: none;" class="table-responsive">

                                <table class="table table-hover" style="font-size: 12px">
                                    <tbody>
                                    <tr>
                                        <th><h2>Campo</h2></th>
                                        <th><h2>Tipo</h2></th>
                                        <th><h2>Descripción</h2></th>
                                        <th><h2>Valores válidos</h2></th>
                                    </tr>
                                    </tbody> <tr>
                                        <th>periodo</th>
                                        <td>Texto</td>
                                        <td>Indica a que periodo pertenece</td>
                                        <td>Si el tipo de periodo es mensual: <h6>Enero 2018</h6> <br>
                                            Si el tipo de periodo es quincenal: <h6>Primera quincena de Enero 2018</h6>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>constante</th>
                                        <td>Texto</td>
                                        <td>Es el codigo de la constante</td>
                                        <td>Ejemplo: <h6>PRESENTVALOR</h6></td>
                                    </tr>
                                    <tr>
                                        <th>valor</th>
                                        <td>Númerico (Punto Flotante)</td>
                                        <td>El Valor que será asignado</td>
                                        <td>Ejemplo: <h6>12.00</h6></td>
                                    </tr>
                                    <tr>
                                        <th>comentario</th>
                                        <td>Texto</td>
                                        <td>Una Breve descripción</td>
                                        <td>Ejemplo: <h6>Comentario </h6></td>
                                    </tr>
                                    <tr>
                                        <th>empresa</th>
                                        <td>Texto</td>
                                        <td>Es el codigo de la Empresa</td>
                                        <td>Ejemplo: <h6>DYKTEL</h6></td>
                                    </tr>
                                </table>
                            </div>
                            <div v-show="fd.TypeImportFile === 6" style="display: none;" class="table-responsive">

                                <table class="table table-hover" style="font-size: 12px">
                                    <tbody>
                                    <tr>
                                        <th><h2>Campo</h2></th>
                                        <th><h2>Obligatorio</h2></th>
                                        <th><h2>Descripción</h2></th>
                                        <th><h2>Valores válidos</h2></th>
                                    </tr>
                                    </tbody>
                                    <tr>
                                        <th>periodo</th>
                                        <td>Texto</td>
                                        <td>Indica a que periodo pertenece</td>
                                        <td>Si el tipo de periodo es mensual: <h6>Enero 2018</h6> <br>
                                            Si el tipo de periodo es quincenal: <h6>Primera quincena de Enero 2018</h6>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>concepto</th>
                                        <td>Texto</td>
                                        <td>Indica el concepto</td>
                                        <td>Ejemplo: <h6>suelbasi</h6></td>
                                    </tr>
                                    <tr>
                                        <th>monto</th>
                                        <td>Númerico (Punto Flotante)</td>
                                        <td>El Valor que será asignado</td>
                                        <td>Ejemplo: <h6>12.00</h6></td>
                                    </tr>
                                    <tr>
                                        <th>fechainicio</th>
                                        <td>Fecha</td>
                                        <td>Es la Fecha Inicial</td>
                                        <td>Formato: <h6>yyyy-mm-dd</h6><br>
                                        Ejemplo: <h6>2017-02-25</h6></td>
                                    </tr>
                                    <tr>
                                        <th>fechafinal</th>
                                        <td>Fecha</td>
                                        <td>Es la Fecha Final</td>
                                        <td>Formato: <h6>yyyy-mm-dd</h6><br>
                                            Ejemplo: <h6>2017-02-25</h6></td>
                                    </tr>
                                    <tr>
                                        <th>cantidad</th>
                                        <td>Númerico (Entero)</td>
                                        <td>Es la Cantidad</td>
                                        <td>Ejemplo: <h6>25</h6></td>
                                    </tr>
                                    <tr>
                                        <th>tipoespecial</th>
                                        <td>Númerico (Entero)</td>
                                        <td>Tipo Especial</td>
                                        <td>
                                            Si no esta definido: <h6>1</h6><br>
                                            Si es Remunerativo: <h6>2</h6>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>codigolegajo</th>
                                        <td>Númerico (Entero)</td>
                                        <td>Es el Codigo del Legajo</td>
                                        <td>Ejemplo: <h6>1000</h6></td>
                                    </tr>
                                    <tr>
                                        <th>descripcion</th>
                                        <td>Texto</td>
                                        <td>Es la descripción de la asignación</td>
                                        <td>Ejemplo: <h6>Descripción</h6></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @include('layouts.footer')
</div>
@include('layouts.libraries')
<script src="../js/tools/importfiles.js"></script>
<script src="../js/librariesjs/papaparse.js"></script>
</body>
</html>
