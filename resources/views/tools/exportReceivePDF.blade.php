<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link href="{{ asset('css/receivestyle.min.css') }}" rel="stylesheet">
    <style>

        body {
            font-family: sans-serif;
            font-size: 12px;
        }
    </style>
</head>
<body>

@foreach($head as $index => $head)
    <div><img src="{{$logoshow}}" style="width: 100px;height: 100px;    position: absolute;
                top: 125px;
                left: 180px;">
        <table style="width:100%" id="Reporte" class="quitar-abajo">
            <tr>
                <th class="titulo padding_left padding_bot padding_top" colspan="4">{{$company[0]->namecompany}}</th>
                <th colspan="3" class="cuit titulo padding_left padding_bot padding_top">
                    CUIT: {{$company[0]->cuitcompany}}</th>
            </tr>
            <tr>
                <td colspan="7" class="padding_left padding_bot padding_top">{{$company[0]->fiscaldomicile}}</td>
            </tr>
            <tr class="centrar negrita">
                <td colspan="4">APELLIDO Y NOMBRE</td>
                <td colspan="1">CUIL</td>
                <td colspan="2">LEGAJO</td>
            </tr>
            <tr class="centrar ">
                <td colspan="4" class="nombres">{{$head->lastname." ".$head->firstname}}</td>
                <td colspan="1" class="nombres" style="width: 150px;">{{$head->cuil}}</td>
                <td colspan="2" class="nombres">{{$head->codlegajo}}</td>
            </tr>
            <tr>
                <td colspan="2" rowspan="2"
                    class="quitar-superior quitar-abajo padding_left padding_top-1 padding_bot-1"
                    style="border-right: none">
                    <span class="negrita"> CATEGORIA</span>

                    <div valign="top "> {{$head->categorysalary}}</div>
                </td>
                <td colspan="2" rowspan="4"
                    class="quitar-superior quitar-abajo padding_left padding_top-1 padding_bot-1"
                    style="border-left: none; align-items: center" width="150px">
                </td>
                <td colspan="1" class="negrita padding_left" style="">FECHA DE <br> INGRESO</td>
                <td colspan="2" class="negrita padding_left padding_top">REMUNERACION <br> ASIGNADA</td>
            </tr>
            <tr>
                <td colspan="1" class="centrar">{{$head->dateformat}}</td>
                <td colspan="2" class="centrar">$ {{number_format($head->salaryamount, 2, ",", ".")}}</td>
            </tr>
            <tr>
                <td colspan="4" class="quitar-abajo quitar-superior  negrita padding_left padding_top-1 padding_bot-1"
                    valign="top">CALIF. PROFESIONAL
                </td>
                <td colspan="3" class="padding_left padding_top"><strong>PERIODO DE
                        PAGO:</strong> {{$head->titleperiod}}
                </td>
            </tr>
            <tr>
                <td colspan="4" class="quitar-superior quitar-abajo padding_left  padding_top-0 "
                    valign="top">{{$head->namepositionjob}}    </td>
                <td colspan="3" class=" quitar-abajo negrita padding_left padding_top" valign="top">CONTRATACION</td>
            </tr>
            <tr>
                <td colspan="4" class="quitar-superior quitar-abajo negrita padding_left padding_top" valign="">LUGAR DE
                    TRABAJO
                </td>
                <td colspan="3" rowspan="2" class="quitar-superior quitar-abajo padding_left" valign="top"
                    height="30">{{$head->namemodcontract}}</td>
            </tr>
            <tr>
                <td colspan="4" class="quitar-superior quitar-abajo padding_left padding_bot"
                    valign="top ">{{$head->nameworkplaces}} <br></td>
            </tr>
        </table>
        <table>
            <tr>
                <td colspan="2" class="centrar quitar-superior padding_top" style="width:200px;">
                    <strong>CONCEPTO</strong>
                </td>
                <td class="centrar quitar-superior  padding_top" colspan="1" style="width: 60px;"><strong>CANT</strong>
                </td>
                <td class="centrar quitar-superior  padding_top" colspan="1" style="width: 100px;"><strong>REMUN.SUJ.A
                        RET.</strong></td>
                <td class="centrar quitar-superior  padding_top" colspan="1" style="width: 100px;"><strong>NO
                        <br>REMUNERAT.</strong></td>
                <td class="centrar quitar-superior  padding_top" colspan="1" style="width: 100px;"><strong>DESCUENTOS</strong></td>
                <td class="centrar quitar-superior  padding_top" colspan="1" style="width: 100px;">
                    <strong>REMUN.
                        EXENTAS</strong>
                </td>
            </tr>
            @foreach ($head->body as $row)
                <tr>
                    <td style="height: 23px;" colspan="2"
                        class="quitar-abajo quitar-superior padding_left">{{$row->nameconcept}}</td>
                    <td class="centrar quitar-abajo quitar-superior">
                        {{number_format($row->cant, 2, ",", ".")}}
                    </td>
                    <td class="centrar quitar-abajo quitar-superior">
                        @if($row->columnconcept==1)
                            {{number_format($row->amount, 2, ",", ".")}}
                        @endif
                    </td>
                    <td class="centrar quitar-abajo quitar-superior">
                        @if($row->columnconcept==2)
                            {{number_format($row->amount, 2, ",", ".")}}
                        @endif
                    </td>
                    <td class="centrar quitar-abajo quitar-superior">
                        @if($row->columnconcept==3)
                            {{number_format($row->amount, 2, ",", ".")}}
                        @endif
                    </td>
                    <td class="centrar quitar-abajo quitar-superior">
                        @if($row->columnconcept==4)
                            {{number_format($row->amount, 2, ",", ".")}}
                        @endif
                    </td>
                </tr>
            @endforeach
            @for ($i = count($head->body); $i < 15; $i++)
                <tr>
                    <td style="height: 23px;" colspan="2" class="quitar-abajo quitar-superior"></td>
                    <td class="centrar quitar-abajo quitar-superior"></td>
                    <td class="centrar quitar-abajo quitar-superior"></td>
                    <td class="centrar quitar-abajo quitar-superior"></td>
                    <td class="centrar quitar-abajo quitar-superior"></td>
                    <td class="centrar quitar-abajo quitar-superior"></td>
                </tr>
            @endfor
            <tr class="negrita">
                <td colspan="3" class="quitar-abajo padding_left"></td>
                <td class="centrar quitar-abajo padding_left">{{number_format($head->payments, 2, ",", ".")}} </td>
                <td class="centrar quitar-abajo padding_left">{{number_format($head->unremuns, 2, ",", ".")}}</td>
                <td class="centrar quitar-abajo padding_left">{{number_format($head->deductions, 2, ",", ".")}}</td>
                <td class="centrar quitar-abajo padding_left">{{number_format($head->exempts, 2, ",", ".")}}</td>
            </tr>
            <tr>
                <td colspan="4" class="quitar-abajo negrita  padding_left">LUGAR Y FECHA DE PAGO</td>
                <td colspan="2" class="quitar-abajo  padding_left">TOTAL NETO</td>
                <td colspan="1" rowspan="2"
                    class="quitar-abajo centrar negrita padding_left"> {{number_format($head->totalpay, 2, ",", ".")}}</td>
            </tr>
            <tr>
                <td colspan="4"
                    class="quitar-abajo quitar-superior  padding_left padding_bot">{{$head->fiscaldomicilecompany}}</td>
                <td colspan="2" class="quitar-abajo quitar-superior padding_left">FORMA DE PAGO</td>
            </tr>
            <tr>
                <td colspan="7" class="quitar-abajo padding_left"><strong>SON PESOS:</strong></td>
            </tr>
            <tr>
                <td colspan="7"
                    class="quitar-abajo quitar-superior padding_left">{{NumeroALetras::convertir($head->totalpay)}}</td>
            </tr>
            <tr>
                <td style="height: 70px; " colspan="7" class="quitar-abajo  negrita padding_left">OBSERVACIONES:<br>
                    <p class="">
                        {{$head->comment}}
                    </p>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="negrita " style="font-size: 10px; margin-left: 50px;">
                    <p style="margin-left: 5px;">ART. 12 LEY 17.250 <br>
                        MES: Noviembre <br>
                        BANCO: Red Link <br>
                        FECHA DE DEPOSITO: <br>
                        13/12/2017</td>
                </p>
                <td colspan="3" class="centrar negrita" style="font-size: 10px;">RECIBI EL IMPORTE NETO DE ESTA
                    LIQUIDACION
                    EN
                    PAGO DE MI REMUNERACION CORRESPONDIENTE AL PERIODO INDICADO Y DUPLICADO DE LA MISMA CONFORME A LA
                    LEY
                    VIGENTE.
                </td>
                <td colspan="2" class="centrar negrita " align="right" valign="bottom">FIRMA DEL EMPLEADO</td>
            </tr>
        </table>
    </div>
    <div style="page-break-after: always;"></div>
    <div>

            <img src="{{asset('/uploads/'.$logoshow)}}" style="width: 100px;height: 100px;    position: absolute;
                top: 125px;
                left: 180px;">

        <table style="width:100%" id="Reporte" class="quitar-abajo">
            <tr>
                <th class="titulo padding_left padding_bot padding_top" colspan="4">{{$company[0]->namecompany}}</th>
                <th colspan="3" class="cuit titulo padding_left padding_bot padding_top">
                    CUIT: {{$company[0]->cuitcompany}}</th>
            </tr>
            <tr>
                <td colspan="7" class="padding_left padding_bot padding_top">{{$company[0]->fiscaldomicilecompany}}</td>
            </tr>
            <tr class="centrar negrita">
                <td colspan="4">APELLIDO Y NOMBRE</td>
                <td colspan="1">CUIL</td>
                <td colspan="2">LEGAJO</td>
            </tr>
            <tr class="centrar ">
                <td colspan="4" class="nombres">{{$head->lastname." ".$head->firstname}}</td>
                <td colspan="1" class="nombres" style="width: 150px;">{{$head->cuil}}</td>
                <td colspan="2" class="nombres">{{$head->codlegajo}}</td>
            </tr>
            <tr>
                <td colspan="2" rowspan="2"
                    class="quitar-superior quitar-abajo padding_left padding_top-1 padding_bot-1"
                    style="border-right: none">
                    <span class="negrita"> CATEGORIA</span>

                    <div valign="top "> {{$head->categorysalary}}</div>
                </td>
                <td colspan="2" rowspan="4"
                    class="quitar-superior quitar-abajo padding_left padding_top-1 padding_bot-1"
                    style="border-left: none; align-items: center" width="150px">
                </td>
                <td colspan="1" class="negrita padding_left" style="">FECHA DE <br> INGRESO</td>
                <td colspan="2" class="negrita padding_left padding_top">REMUNERACION <br> ASIGNADA</td>
            </tr>
            <tr>
                <td colspan="1" class="centrar">{{$head->dateformat}}</td>
                <td colspan="2" class="centrar">$ {{number_format($head->salaryamount, 2, ",", ".")}}</td>
            </tr>
            <tr>
                <td colspan="4" class="quitar-abajo quitar-superior  negrita padding_left padding_top-1 padding_bot-1"
                    valign="top">CALIF. PROFESIONAL
                </td>
                <td colspan="3" class="padding_left padding_top"><strong>PERIODO DE
                        PAGO:</strong> {{$head->titleperiod}}
                </td>
            </tr>
            <tr>
                <td colspan="4" class="quitar-superior quitar-abajo padding_left  padding_top-0 "
                    valign="top">{{$head->namepositionjob}}    </td>
                <td colspan="3" class=" quitar-abajo negrita padding_left padding_top" valign="top">CONTRATACION</td>
            </tr>
            <tr>
                <td colspan="4" class="quitar-superior quitar-abajo negrita padding_left padding_top" valign="">LUGAR DE
                    TRABAJO
                </td>
                <td colspan="3" rowspan="2" class="quitar-superior quitar-abajo padding_left" valign="top"
                    height="30">{{$head->namemodcontract}}</td>
            </tr>
            <tr>
                <td colspan="4" class="quitar-superior quitar-abajo padding_left padding_bot"
                    valign="top ">{{$head->nameworkplaces}} <br></td>
            </tr>
        </table>
        <table>
            <tr>
                <td colspan="2" class="centrar quitar-superior padding_top" style="width:200px;">
                    <strong>CONCEPTO</strong>
                </td>
                <td class="centrar quitar-superior  padding_top" colspan="1" style="width: 60px;"><strong>CANT</strong>
                </td>
                <td class="centrar quitar-superior  padding_top" colspan="1" style="width: 100px;"><strong>REMUN.SUJ.A
                        RET.</strong></td>
                <td class="centrar quitar-superior  padding_top" colspan="1" style="width: 100px;"><strong>NO
                        <br>REMUNERAT.</strong></td>
                <td class="centrar quitar-superior  padding_top" colspan="1" style="width: 100px;"><strong>DESCUENTOS</strong></td>
                <td class="centrar quitar-superior  padding_top" colspan="1" style="width: 100px;">
                    <strong>REMUN.
                        EXENTAS</strong>
                </td>
            </tr>
            @foreach ($head->body as $row)
                <tr>
                    <td style="height: 23px;" colspan="2"
                        class="quitar-abajo quitar-superior padding_left">{{$row->nameconcept}}</td>
                    <td class="centrar quitar-abajo quitar-superior">
                        {{number_format($row->cant, 2, ",", ".")}}
                    </td>
                    <td class="centrar quitar-abajo quitar-superior">
                        @if($row->columnconcept==1)
                            {{number_format($row->amount, 2, ",", ".")}}
                        @endif
                    </td>
                    <td class="centrar quitar-abajo quitar-superior">
                        @if($row->columnconcept==2)
                            {{number_format($row->amount, 2, ",", ".")}}
                        @endif
                    </td>
                    <td class="centrar quitar-abajo quitar-superior">
                        @if($row->columnconcept==3)
                            {{number_format($row->amount, 2, ",", ".")}}
                        @endif
                    </td>
                    <td class="centrar quitar-abajo quitar-superior">
                        @if($row->columnconcept==4)
                            {{number_format($row->amount, 2, ",", ".")}}
                        @endif
                    </td>
                </tr>
            @endforeach
            @for ($i = count($head->body); $i < 15; $i++)
                <tr>
                    <td style="height: 23px;" colspan="2" class="quitar-abajo quitar-superior"></td>
                    <td class="centrar quitar-abajo quitar-superior"></td>
                    <td class="centrar quitar-abajo quitar-superior"></td>
                    <td class="centrar quitar-abajo quitar-superior"></td>
                    <td class="centrar quitar-abajo quitar-superior"></td>
                    <td class="centrar quitar-abajo quitar-superior"></td>
                </tr>
            @endfor
            <tr class="negrita">
                <td colspan="3" class="quitar-abajo padding_left"></td>
                <td class="centrar quitar-abajo padding_left">{{number_format($head->payments, 2, ",", ".")}} </td>
                <td class="centrar quitar-abajo padding_left">{{number_format($head->unremuns, 2, ",", ".")}}</td>
                <td class="centrar quitar-abajo padding_left">{{number_format($head->deductions, 2, ",", ".")}}</td>
                <td class="centrar quitar-abajo padding_left">{{number_format($head->exempts, 2, ",", ".")}}</td>
            </tr>
            <tr>
                <td colspan="4" class="quitar-abajo negrita  padding_left">LUGAR Y FECHA DE PAGO</td>
                <td colspan="2" class="quitar-abajo  padding_left">TOTAL NETO</td>
                <td colspan="1" rowspan="2"
                    class="quitar-abajo centrar negrita padding_left"> {{number_format($head->totalpay, 2, ",", ".")}}</td>
            </tr>
            <tr>
                <td colspan="4"
                    class="quitar-abajo quitar-superior  padding_left padding_bot">{{$company[0]->fiscaldomicile}}</td>
                <td colspan="2" class="quitar-abajo quitar-superior padding_left">FORMA DE PAGO</td>
            </tr>
            <tr>
                <td colspan="7" class="quitar-abajo padding_left"><strong>SON PESOS:</strong></td>
            </tr>
            <tr>
                <td colspan="7"
                    class="quitar-abajo quitar-superior padding_left">{{NumeroALetras::convertir($head->totalpay)}}</td>
            </tr>
            <tr>
                <td style="height: 70px; " colspan="7" class="quitar-abajo  negrita padding_left">OBSERVACIONES:<br>
                    <p class="">
                        {{$head->comment}}
                    </p>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="negrita " style="font-size: 10px; margin-left: 50px;">
                    <p style="margin-left: 5px;">ART. 12 LEY 17.250 <br>
                        MES: Noviembre <br>
                        BANCO: Red Link <br>
                        FECHA DE DEPOSITO: <br>
                        13/12/2017</td>
                </p>
                <td colspan="3" class="centrar negrita" style="font-size: 10px;">RECIBI EL IMPORTE NETO DE ESTA
                    LIQUIDACION
                    EN
                    PAGO DE MI REMUNERACION CORRESPONDIENTE AL PERIODO INDICADO Y DUPLICADO DE LA MISMA CONFORME A LA
                    LEY
                    VIGENTE.
                </td>
                <td colspan="2" class="centrar negrita " align="right" valign="bottom">FIRMA DEL EMPLEADO</td>
            </tr>
        </table>
    </div>

    <div style="page-break-after: always;"></div>




@endforeach


</body>
</html>
