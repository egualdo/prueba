@section('FatherLiquidacion','true')
  @section('ChildLiquidacion','show')
<!DOCTYPE html>
<html>
@include('layouts.heads')
<body>
@include('layouts.sidebaradmin')
<div class="page forms-page">
    @include('layouts.navbar')
    <div id="vuedata">
        <div class="breadcrumb-holder fixed-top">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-2 border">
                    </div>
                    <h1>Generar Reporte SUSS</h1>
                </div>
            </div>
        </div>
        <section class="forms">
            <div class="container-fluid">
                <div class="card">
                    <div class="card-body">
                        <div class="card-header">
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Por favor Seleccione una Nomina</label>
                                    <select v-model="fd.Roster" class="form-control">
                                        <option v-for="OneRoster in fd.Rosterlist" v-bind:value="OneRoster.idn">
                                            @{{ OneRoster.description}}
                                        </option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Por favor Seleccione un tipo de reporte</label>
                                    <select v-model="fd.Typereport" class="form-control">
                                        <option value="1">Periodo</option>
                                        <option value="2">Rango de Fechas</option>
                                    </select>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group"  v-show="fd.Typereport === '1'" style="display: none;" >
                                    <label>Por favor Seleccione un tipo de periodo</label>
                                    <select v-model="fd.Typeperiod" @change="TypePeriodOnChange" class="form-control">
                                        <option value="1">Mensual</option>
                                        <option value="2">Quincenal</option>
                                    </select>
                                </div>
                                <div class="form-group" v-show="fd.Typereport === '1'" style="display: none;">
                                    <label>Periodo</label>
                                    <select v-model="fd.Period" class="form-control">
                                        <option v-for="OnePeriod in fd.Periodlist" v-bind:value="OnePeriod.idn">
                                            @{{ OnePeriod.title}}
                                        </option>
                                    </select>
                                </div>
                                <div class="form-group" v-show="fd.Typereport === '2'">
                                    <label>Rango de Fechas</label>
                                    <input type="text" id="dateranges" v-model="fd.Range" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button class="btn btn-primary pull-right" v-on:click="DownloadReport">Generar XLS</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @include('layouts.footer')
</div>
@include('layouts.libraries')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/moment-with-locales.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<script src="../js/payrunjs/suss.js" type="text/javascript"></script>
<script>
    $(document).ready(function () {
        var start = moment().subtract(29, 'days');
        var end = moment();




        function cb(start, end) {
            $('#dateranges span').val(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
            run.fd.Range = start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD');
        }

        $('#dateranges').daterangepicker({
            startDate: start,
            endDate: end,
            "locale": {
                "format": 'YYYY-MM-DD'
            },
            opens: "center",
            drops: "down",
            ranges: {
                'Hoy': [moment().startOf('day'), moment().endOf('day')],
                'Ayer': [moment().subtract(1, 'days').startOf('day'), moment().subtract(1, 'days').endOf('day')],
                'Ultimos 7 Dias': [moment().subtract(6, 'days').startOf('day'), moment().endOf('day')],
                'Ultimos 30 dias': [moment().subtract(29, 'days').startOf('day'), moment().endOf('day')],
                'Este Mes': [moment().startOf('month').startOf('day'), moment().endOf('month').endOf('day')],
                'Ultimo Mes': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month').endOf('day')]
            }
        }, cb);

        cb(start, end);
    });
</script>
</body>
</html>