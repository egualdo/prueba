<html>
@include('layouts.heads')
<body>
<!-- Side Navbar -->
@include('layouts.sidebaradmin')
<div class="page forms-page">
    <!-- navbar-->
    @include('layouts.navbar')
    <div id="vuedata">
        <!-- Modal -->

        <!-- FIN DE LA MODAL -->


        <!-- CABECERA GRIS-->
        <div class="breadcrumb-holder fixed-top">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-2 border">
                    </div>
                    <div class="col-md-7 border">
                        <!-- TITULO DE VENTANA -->
                        <h1> Descarga de Reportes para Liquidacion</h1>
                    </div>

                </div>
            </div>
        </div>
        <!-- FIN DE LA CABECERA GRIS -->
    </div>

    <section class="forms">
        <div class="container-fluid">

            <!-- Tarjeta -->
            <div class="card">
                <!-- Cabecera de Tarjeta  -->

                <!-- Cuerpo de Tarjeta -->
                <div class="card-body">


                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#recibospdf">Recibos PDF</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#txtsicoss">Txt Sicoss</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#suss">Suss</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#report_book">Libro de Sueldo</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#controlliquidacion">Control Liquidacion</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#report_net">Netos</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#sicore">Sicore</a>
                        </li>
                    </ul>
                    <br>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane active container" id="recibospdf">

                            <h2>Descarga de Recibos PDF</h2>
                            <br>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label>Periodo (Dato Obligatorio)</label>
                                        <v-select v-model="fd.Period" :options="fd.Periodlist"></v-select>
                                    </div>
                                    <div class="form-group">
                                        <label>Tipo de Corrida (Dato Opcional)</label>
                                        <v-select v-model="fd.Typerun" :options="fd.Typerunlist"></v-select>
                                    </div>
                                </div>

                                <div class="col">
                                    <label>Rango de Fechas</label>
                                    <input type="text" id="dateranges4" v-model="fd.Range" class="form-control">
                                </div>

                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <br>
                                        <button v-on:click="DownloadPDF(2)" class="btn btn-primary btn-fill pull-left">
                                            Descargar por Periodo
                                        </button>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <br>
                                        <button v-on:click="DownloadPDF(3)" class="btn btn-primary btn-fill pull-left">
                                            Descargar por Rango de Fecha
                                        </button>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="tab-pane container" id="txtsicoss">

                            <h2>Descarga de Archivo Sicoss TXT</h2>
                            <br>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label>Periodo (Dato Obligatorio)</label>
                                        <v-select v-model="fd.Period" :options="fd.Periodlist"></v-select>
                                    </div>
                                </div>
                                <div class="col">
                                    <label>Tipo de Corrida (Dato Opcional)</label>
                                    <v-select v-model="fd.Typerun" :options="fd.Typerunlist"></v-select>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col">
                                    <label>Nomina (Dato Opcional)</label>
                                    <v-select v-model="fd.Roster" :options="fd.Rosterlist"></v-select>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <br>
                                        <button v-on:click="Downloadsic" class="btn btn-primary btn-fill pull-left">
                                            Descargar
                                        </button>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="tab-pane container" id="suss">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Por favor Seleccione una Nomina</label>
                                        <select v-model="fd.Roster" class="form-control">
                                            <option v-for="OneRoster in fd.Rosterlist" v-bind:value="OneRoster.idn">
                                                @{{ OneRoster.description}}
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Por favor Seleccione un tipo de reporte</label>
                                        <select v-model="fd.Typereport" class="form-control">
                                            <option value="1">Periodo</option>
                                            <option value="2">Rango de Fechas</option>
                                        </select>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="form-group" v-show="fd.Typereport === '1'" style="display: none;">
                                        <label>Por favor Seleccione un tipo de periodo</label>
                                        <select v-model="fd.Typeperiod" @change="TypePeriodOnChange"
                                                class="form-control">
                                            <option value="1">Mensual</option>
                                            <option value="2">Quincenal</option>
                                        </select>
                                    </div>
                                    <div class="form-group" v-show="fd.Typereport === '1'" style="display: none;">
                                        <label>Periodo</label>
                                        <select v-model="fd.Period" class="form-control">
                                            <option v-for="OnePeriod in fd.Periodlist" v-bind:value="OnePeriod.idn">
                                                @{{ OnePeriod.title}}
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form-group" v-show="fd.Typereport === '2'">
                                        <label>Rango de Fechas</label>
                                        <input type="text" id="dateranges" v-model="fd.Range" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <button class="btn btn-primary pull-right" v-on:click="DownloadReport">Generar XLS
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane container" id="report_book">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Por favor Seleccione una Nomina</label>
                                        <select v-model="fd.Roster" class="form-control">
                                            <option v-for="OneRoster in fd.Rosterlist" v-bind:value="OneRoster.idn">
                                                @{{ OneRoster.description}}
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Por favor Seleccione un tipo de reporte</label>
                                        <select v-model="fd.Typereport" class="form-control">
                                            <option value="1">Periodo</option>
                                            <option value="2">Rango de Fechas</option>
                                        </select>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="form-group" v-show="fd.Typereport === '1'" style="display: none;">
                                        <label>Por favor Seleccione un tipo de periodo</label>
                                        <select v-model="fd.Typeperiod" @change="TypePeriodOnChange"
                                                class="form-control">
                                            <option value="1">Mensual</option>
                                            <option value="2">Quincenal</option>
                                        </select>
                                    </div>
                                    <div class="form-group" v-show="fd.Typereport === '1'" style="display: none;">
                                        <label>Periodo</label>
                                        <select v-model="fd.Period" class="form-control">
                                            <option v-for="OnePeriod in fd.Periodlist" v-bind:value="OnePeriod.idn">
                                                @{{ OnePeriod.title}}
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form-group" v-show="fd.Typereport === '2'">
                                        <label>Rango de Fechas</label>
                                        <input type="text" id="dateranges3" v-model="fd.Range" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <button class="btn btn-primary pull-right" v-on:click="DownloadReport">Generar Pdf
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane container" id="controlliquidacion">

                            <h2>Descarga de Control de Liquidacion</h2>
                            <br>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label>Periodo (Dato Obligatorio)</label>
                                        <v-select v-model="fd.Period" :options="fd.Periodlist"></v-select>
                                    </div>
                                </div>
                                <div class="col">
                                    <label>Tipo de Corrida (Dato Opcional)</label>
                                    <v-select v-model="fd.Typerun" :options="fd.Typerunlist"></v-select>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col">
                                    <label>Nomina (Dato Opcional)</label>
                                    <v-select v-model="fd.Roster" :options="fd.Rosterlist"></v-select>

                                </div>

                                <div class="col">
                                    <div class="form-group">
                                        <br>
                                        <input type="radio" id="one" value="0" v-model="fd.Horizontal">
                                        <label for="one">Formato Vertical</label>
                                        <br>
                                        <input type="radio" id="two" value="1" v-model="fd.Horizontal">
                                        <label for="two">Formato Horizontal</label>
                                        <br>
                                        <br>
                                        <button v-on:click="DownloadReport" class="btn btn-primary btn-fill pull-left">
                                            Descargar
                                        </button>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="tab-pane container" id="report_net">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Por favor Seleccione una Nomina</label>
                                        <select v-model="fd.Roster" class="form-control">
                                            <option v-for="OneRoster in fd.Rosterlist" v-bind:value="OneRoster.idn">
                                                @{{ OneRoster.description}}
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Por favor Seleccione un tipo de reporte</label>
                                        <select v-model="fd.Typereport" class="form-control">
                                            <option value="1">Periodo</option>
                                            <option value="2">Rango de Fechas</option>
                                        </select>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="form-group" v-show="fd.Typereport === '1'" style="display: none;">
                                        <label>Por favor Seleccione un tipo de periodo</label>
                                        <select v-model="fd.Typeperiod" @change="TypePeriodOnChange"
                                                class="form-control">
                                            <option value="1">Mensual</option>
                                            <option value="2">Quincenal</option>
                                        </select>
                                    </div>
                                    <div class="form-group" v-show="fd.Typereport === '1'" style="display: none;">
                                        <label>Periodo</label>
                                        <select v-model="fd.Period" class="form-control">
                                            <option v-for="OnePeriod in fd.Periodlist" v-bind:value="OnePeriod.idn">
                                                @{{ OnePeriod.title}}
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form-group" v-show="fd.Typereport === '2'">
                                        <label>Rango de Fechas</label>
                                        <input type="text" id="dateranges2" v-model="fd.Range" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <button class="btn btn-primary pull-right" v-on:click="DownloadReport">Generar XLS
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane container" id="sicore">

                            <h2>En Proceso de Desarrollo</h2>

                            <div class="row">
                                <div class="col">

                                </div>
                                <div class="col">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- FIN de Tarjeta Hija -->
        </div>
    </section>


    @include('layouts.footer')
</div>
@include('layouts.libraries')
<script src="../js/librariesjs/moment-with-locales.min.js"></script>
<script type="text/javascript" src="../js/librariesjs/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="../css/daterangepicker.css"/>
<script src="../js/payrunjs/suss.js" type="text/javascript"></script>
<script>
    $(document).ready(function () {
        var start = moment().subtract(29, 'days');
        var end = moment();


        function cb(start, end) {
            $('#dateranges span').val(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
            run.fd.Range = start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD');
        }

        function cb2(start, end) {
            $('#dateranges2 span').val(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
            repnet.fd.Range = start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD');
        }

        function cb3(start, end) {
            $('#dateranges3 span').val(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
            book.fd.Range = start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD');
        }
        function cb4(start, end) {
            $('#dateranges5 span').val(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
            dpdf.fd.Range = start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD');
        }

        $('#dateranges').daterangepicker({
            startDate: start,
            endDate: end,
            "locale": {
                "format": 'YYYY-MM-DD'
            },
            opens: "center",
            drops: "down",
            ranges: {
                'Hoy': [moment().startOf('day'), moment().endOf('day')],
                'Ayer': [moment().subtract(1, 'days').startOf('day'), moment().subtract(1, 'days').endOf('day')],
                'Ultimos 7 Dias': [moment().subtract(6, 'days').startOf('day'), moment().endOf('day')],
                'Ultimos 30 dias': [moment().subtract(29, 'days').startOf('day'), moment().endOf('day')],
                'Este Mes': [moment().startOf('month').startOf('day'), moment().endOf('month').endOf('day')],
                'Ultimo Mes': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month').endOf('day')]
            }
        }, cb);
        $('#dateranges2').daterangepicker({
            startDate: start,
            endDate: end,
            "locale": {
                "format": 'YYYY-MM-DD'
            },
            opens: "center",
            drops: "down",
            ranges: {
                'Hoy': [moment().startOf('day'), moment().endOf('day')],
                'Ayer': [moment().subtract(1, 'days').startOf('day'), moment().subtract(1, 'days').endOf('day')],
                'Ultimos 7 Dias': [moment().subtract(6, 'days').startOf('day'), moment().endOf('day')],
                'Ultimos 30 dias': [moment().subtract(29, 'days').startOf('day'), moment().endOf('day')],
                'Este Mes': [moment().startOf('month').startOf('day'), moment().endOf('month').endOf('day')],
                'Ultimo Mes': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month').endOf('day')]
            }
        }, cb2);

        $('#dateranges3').daterangepicker({
            startDate: start,
            endDate: end,
            "locale": {
                "format": 'YYYY-MM-DD'
            },
            opens: "center",
            drops: "down",
            ranges: {
                'Hoy': [moment().startOf('day'), moment().endOf('day')],
                'Ayer': [moment().subtract(1, 'days').startOf('day'), moment().subtract(1, 'days').endOf('day')],
                'Ultimos 7 Dias': [moment().subtract(6, 'days').startOf('day'), moment().endOf('day')],
                'Ultimos 30 dias': [moment().subtract(29, 'days').startOf('day'), moment().endOf('day')],
                'Este Mes': [moment().startOf('month').startOf('day'), moment().endOf('month').endOf('day')],
                'Ultimo Mes': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month').endOf('day')]
            }
        }, cb3);
        $('#dateranges4').daterangepicker({
            startDate: start,
            endDate: end,
            "locale": {
                "format": 'YYYY-MM-DD'
            },
            opens: "center",
            drops: "down",
            ranges: {
                'Hoy': [moment().startOf('day'), moment().endOf('day')],
                'Ayer': [moment().subtract(1, 'days').startOf('day'), moment().subtract(1, 'days').endOf('day')],
                'Ultimos 7 Dias': [moment().subtract(6, 'days').startOf('day'), moment().endOf('day')],
                'Ultimos 30 dias': [moment().subtract(29, 'days').startOf('day'), moment().endOf('day')],
                'Este Mes': [moment().startOf('month').startOf('day'), moment().endOf('month').endOf('day')],
                'Ultimo Mes': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month').endOf('day')]
            }
        }, cb4);

        cb(start, end);
        cb2(start, end);
        cb3(start, end);
        cb4(start, end);
    });
</script>
<script src="../js/payrunjs/downloadreports.js"></script>
</body>
</html>