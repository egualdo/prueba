@section('FatherLiquidacion','true')
  @section('ChildLiquidacion','show')
<!DOCTYPE html>
<html>
@include('layouts.heads')
<body>
@include('layouts.sidebaradmin')
<div class="page forms-page">
    @include('layouts.navbar')
    <div id="vuedata">
        <div class="breadcrumb-holder fixed-top">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-2 border">
                    </div>
                    <div class="col-md-7 border">
                        <h1> Corridas Abiertas con Novedades</h1>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <section class="forms">
        <div class="container-fluid">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <table id="TablaMaster" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>Legajo</th>
                                <th>Nombre</th>
                                <th>Nomina</th>
                                <th>Periodo</th>
                                <th>Pagos</th>
                                <th>Deducciones</th>
                                <th>No remun.</th>
                                <th>Pago Neto</th>
                                <th>Vacaciones</th>
                                <th>Horas Extras</th>
                                <th>Ausencias Justificadas</th>
                                <th>Ausencias Injustificadas</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('layouts.footer')
</div>
@include('layouts.libraries')
<script src="../js/masterjs/payrollopennews.js"></script>
</body>
</html>