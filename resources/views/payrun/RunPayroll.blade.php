@section('FatherLiquidacion','true')
  @section('ChildLiquidacion','show')
<!DOCTYPE html>
<html>
@include('layouts.heads')
<body>
<!-- Side Navbar -->
@include('layouts.sidebaradmin')
<div class="page forms-page">
    <!-- navbar-->
    @include('layouts.navbar')
    <div id="vue-Payrunall">
        <!-- CABECERA GRIS-->
        <div class="breadcrumb-holder fixed-top">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-2 border">
                    </div>
                    <div class="col-md-6 border">
                        <!-- TITULO DE VENTANA -->
                        <h1> Corridas de Nomina</h1>

                        <!--<p>Dato de Prueba</p>-->

                    </div>
                    <div class="col-md-4">

                        <button type="button"  v-on:click="openModalDelete" class="btn btn-warning btn-fill pull-rigth">Eliminar</button>
                        <button type="button"  v-on:click="openModalClose" class="btn btn-primary btn-fill pull-rigth">Cerrar</button>
                        <button type="button"  v-on:click="openModalReopen" class="btn btn-info btn-fill pull-left">Reabrir</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- FIN DE LA CABECERA GRIS -->
        <br>
        <section class="forms">
            <div class="container-fluid">
                <div class="card">
                    <!-- Cabecera de Tarjeta Hija -->
                    <div class="card-header">
                        <h2> Córrida de Nómina</h2>
                    </div>
                    <!-- Cuerpo de Tarjeta Hija-->
                    <div class="card-body">
                        <div class="row">
                            <!-- Division del cuerpo en 3 columns -->
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Tipo de Periodo</label>
                                    <select v-model="fd.Typeperiod" v-on:change="TypePeriodOnChange"
                                            class="form-control">
                                        <option value="1">Mensual</option>
                                        <option value="2">Quincenal</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Periodo</label>
                                    <select v-model="fd.Period" class="form-control" v-on:change="loadMonth">
                                        <option value="">Seleccione ...</option>
                                        <option v-for="Perio in fd.Periodlist" v-bind:selected="fd.Period"
                                                v-bind:value="Perio.idn">
                                            @{{ Perio.title }}
                                        </option>
                                    </select>
                                </div>
                               
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Mes</label>
                                    <select v-model="fd.Month" class="form-control">
                                        <option value="">Seleccione ...</option>
                                        <option value="Enero">Enero</option>
                                        <option value="Febrero">Febrero</option>
                                        <option value="Marzo">Marzo</option>
                                        <option value="Abril">Abril</option>
                                        <option value="Mayo">Mayo</option>
                                        <option value="Junio">Junio</option>
                                        <option value="Julio">Julio</option>
                                        <option value="Agosto">Agosto</option>
                                        <option value="Septiembre">Septiembre</option>
                                        <option value="Octubre">Octubre</option>
                                        <option value="Noviembre">Noviembre</option>
                                        <option value="Diciembre">Diciembre</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Fecha de Pago de contribuciones</label>
                                    <input id="DPickerPayContributiondate" v-model="fd.PayContributiondate" type="input"
                                           class="form-control">
                                </div>
                                
                            </div>
                            <div class="col-md-3">
                            <div class="form-group">
                                    <label>Fecha de Pago</label>
                                    <input id="DPickerPaymentdate" v-model="fd.PaymentRundate" type="input"
                                           class="form-control">
                                </div>
                            <div class="form-group">
                                    <label>Método de Pago</label>
                                    <select v-model="fd.Paymentmethod" class="form-control">
                                        <option value="">Seleccione ...</option>
                                        <option v-for="paym in fd.Paymentmethodlist" v-bind:value="paym.idn">
                                            @{{ paym.name }}
                                        </option>
                                    </select>
                                </div>
                               
                            </div>
                            <div class="col-md-3">
                            <div class="form-group">
                                    <label>Nomina</label>
                                    <select v-model="fd.Roster" class="form-control">
                                        <option value="">Seleccione ...</option>
                                        <option v-for="Rost in fd.Rosterlist" v-bind:selected="fd.Roster"
                                                v-bind:value="Rost.idn">
                                            @{{ Rost.name }}
                                        </option>
                                    </select>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Tipo de Corrida</label>
                                    <input v-model="fd.Typerun" type="input" class="form-control" disabled>
                                </div>
                                    </div>
                                    <div class="col-md-6">
                                    <div class="form-group">
                                    <br>
                                    <button v-on:click="RunComplete" class="btn btn-primary btn-fill pull-left">Correr
                                    </button>
                                </div>
                                    </div>
                                </div>
                               
                               
                            </div>
                        </div>
                    </div>
                    <!--<div class="card-footer">
                    </div>-->
                    <div class="row subtitle">
                        <div v-show="ff.openPayruns == false & ff.closedPayruns==true"  class="col-md-8" style="display:none">
                            <p>Mostrando actualmente corridas cerradas que solo pueden ser visualizadas</p>
                        </div>
                        <div v-show="ff.openPayruns == true & ff.closedPayruns==false"  class="col-md-8" style="display:none">
                            <p>Mostrando actualmente corridas abiertas que pueden ser actualizadas</p>
                        </div>
                        <div class="col-md-4">

                            <button type="button" v-on:click="showOpen" class="btn btn-primary btn-sm">Corridas Abiertas</button>
                            <button type="button" v-on:click="showClosed" class="btn btn-danger btn-sm" style="border-radius: 0">Corridas Cerradas</button>
                        </div>
                    </div>
                </div>
                <div v-show="ff.openPayruns == true" id="ListPayrunOpen">
                    <!-- Tarjeta hija -->
                    <div class="card">
                        <!-- Cabecera de Tarjeta Hija -->
                        <div class="card-header">
                            <h2> Córrida Abiertas</h2>
                        </div>
                        <!-- Cuerpo de Tarjeta Hija-->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <table id="TablePayrunOpen" class="table table-striped table-bordered"
                                               cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th>Nombre</th>
                                                <th>Tipo</th>
                                                <th>Nómina</th>
                                                <th>Periodo</th>
                                                <th>Pagos</th>
                                                <th>Deducciones</th>
                                                <th>No Remun</th>
                                                <th>Pago Neto</th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div v-show="ff.closedPayruns == true" id="ListPayrunClosed">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-9">
                                    <h2>Datos Históricos</h2>
                                </div>
                                <div class="col-md-3">
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <!-- Division del cuerpo en 3 columns -->
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <table id="TablePayrunClosed" class="table table-striped table-bordered"
                                               cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th>Nombre</th>
                                                <th>Tipo</th>
                                                <th>Nómina</th>
                                                <th>Periodo</th>
                                                <th>Pagos</th>
                                                <th>Deducciones</th>
                                                <th>No Remun</th>
                                                <th>Pago Neto</th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        @include('modal.ModalGestionRun')


    </div>

    @include('layouts.footer')

    <div id="vue-Payrundetail">
        @include('modal.ModalPayrunDetail')
    </div>
</div>
@include('layouts.libraries')
<script src="../js/generic/generic.js" type="text/javascript"></script>



<script src="../js/payrunjs/payrundetail.js"></script>
<script src="../js/payrunjs/runpayroll.js"></script>



</body>

</html>