  <link rel="stylesheet" href="../codemirror/lib/codemirror.css">

            <link rel="stylesheet" href="../codemirror/theme/monokai.css">



            <script src="../codemirror/lib/codemirror.js"></script>
            <script src="../codemirror/addon/edit/matchbrackets.js"></script>
            <script src="../codemirror/mode/htmlmixed/htmlmixed.js"></script>
            <script src="../codemirror/mode/xml/xml.js"></script>
            <script src="../codemirror/mode/javascript/javascript.js"></script>
            <script src="../codemirror/mode/css/css.js"></script>
            <script src="../codemirror/mode/clike/clike.js"></script>
            <script src="../codemirror/mode/php/php.js"></script>

            <script src="../codemirror/keymap/sublime.js"></script>
            <style type="text/css">
            .CodeMirror {
              border-top: 1px solid black;
              border-bottom: 1px solid black;
              font-size: 15px;
            }
            .CodeMirror pre {
              padding: 0 40px;
              font-size: 20px; /* Horizontal padding of content */
            }
          </style>