<!-- Side Navbar -->
<nav class="side-navbar">
    <div class="side-navbar-wrapper wrap" style="margin-top:60px;">
        <div class="sidenav-header d-flex align-items-center justify-content-center">
            <div class="sidenav-header-inner text-center">
                @if(session()->has('avatar'))
                    <img src="/uploads/{{session()->get('avatar')}}" alt="person" id="AvatarUser"
                         class="img-fluid rounded-circle">
                @else
                    <img src="../img/default-avatar.png" alt="person" id="AvatarUser" class="img-fluid rounded-circle">
                @endif
                <h2 class="h5 text-uppercase">{{ session('Username') }}</h2><span
                        class="text-uppercase">{{ session('NameRol') }}</span>
            </div>
            <div class="sidenav-header-logo"><a href="index.html" class="brand-small text-center">
                    <strong>B</strong><strong class="text-primary">D</strong></a></div>
        </div>
        <div class="main-menu">
            <ul id="side-main-menu" class="side-menu list-unstyled">
                <li class="active"><a href="../usuario/bienvenido"> <i class="icon-home"></i><span>Inicio</span></a></li>
            </ul>
        </div>
        <div class="admin-menu">
            <ul id="side-admin-menu" class="side-menu list-unstyled">

                <!--panel principal-->
                @if (session('IdnRol')===1)
               <!-- <li ><a href="#pages-nav-list2" data-toggle="collapse"  aria-expanded="@yield('FatherEmpresas')"><i
                                class="icon-page"></i><span>Empresas</span>
                    </a>
                    <ul id="pages-nav-list2" class="collapse list-unstyled @yield('ChildEmpresas')">
                        <li><a href="../compania/empresasactivas">Empresas Activas</a></li>
                        <li><a href="../compania/empresasinactivas">Empresas Inactivas</a></li>

                    </ul>
                </li>-->
                @endif
                @if (session('IdnRol')===2 || session('IdnRol')===1)
               <!-- <li><a href="#pages-nav-list29" data-toggle="collapse" aria-expanded="@yield('FatherParametrizacion')"><i
                                class="icon-pencil-case"></i><span>Parametrización</span>
                    </a>
                    <ul id="pages-nav-list29" class="collapse list-unstyled  @yield('ChildParametrizacion')">
                        <li><a href="../parametros/conceptos">Conceptos</a></li>
                        <li><a href="../parametros/constantes">Constantes</a></li>
                        <li><a href="../parametros/cctconstant">Constantes Cct</a></li>
                        <li><a href="../parametros/companyconstant">Constantes de Compañia</a></li>
                        <li><a href="../parametros/lctconstant">Constantes Lct</a></li>
                        <li><a href="../parametros/formulaLCT">Formulas LCT</a></li>
                        <li><a href="../parametros/CCTlist">Formulas CCT</a></li>
                        <li><a href="../parametros/companylist">Formulas por Empresa</a></li>
                        <li><a href="../parametros/gestionNominas">Nóminas</a></li>
                        <li><a href="../parametros/periodos">Periodos</a></li>
                        <li><a href="../parametros/cuentasconceptos">Cuentas Contables/Conceptos</a></li>
                       


                    </ul>
                </li>-->
                @endif
                @if (session('IdnRol')===1)
                <!--maestro empresas-->
                <li><a href="#pages-nav-list22" data-toggle="collapse" aria-expanded="@yield('FatherMaster')"><i
                                class="icon-flask"></i><span>Maestros de Sistema </span>
                    </a>
                    <ul id="pages-nav-list22" class="collapse list-unstyled @yield('ChildMaster')">
                       <!-- <li><a href="../employee/positionjob">Puestos de Trabajo de Legajo </a></li> -->
                        
                       <!-- <li><a href="../master/accountingaccount">Cuentas Contables</a></li>-->
                       <!-- <li><a href="../config/departamentos">Departamentos</a></li>-->
                        
                       <!-- <li><a href="../config/centrocosto">Centro de Costo</a></li>-->
                        
                        
                        
                        <li><a href="../master/Province">Provincias</a></li>  
                      <!--<li><a href="../config/actividades">Actividad</a></li>-->
                        <!--<li><a href="../config/nacionalidades">Nacionalidades</a></li>-->
                        <!--<li><a href="../config/tipoauscencia">Tipo de Ausencia</a></li>-->
                        <!--<li><a href="../config/sitrevista">Situación Revista</a></li>-->
                        <!--<li><a href="../config/modcontratacion">Modalidad de Contratación</a></li>-->
                        <!--<li><a href="../config/carreras">Carreras</a></li>-->
                        <!--<li><a href="../config/lenguajes">Lenguajes</a></li>-->
                        <!--<li><a href="../config/niveles">Niveles</a></li>-->
                        <li><a href="../master/Category">Categorias</a></li>
                        <!--<li><a href="../config/obrasociales">Obra Social</a></li>-->
                        <!--<li><a href="../master/sindicate">Sindicato</a></li>-->
                        <!--<li><a href="../config/zonatrabajo">Lugar de Trabajo</a></li>-->
                        <!--<li><a href="../config/ccts">Convenio Colectivo de trabajo</a></li>-->
                        <li><a href="../config/zonas">Zonas</a></li>
                        <!--<li> <a href="../config/terms">Condicion</a></li>-->
                        <!--<li><a href="../config/regespecial">Regimen previsional</a></li>-->
                        <!--<li><a href="../config/tipoespecial">Tipo especial de asignacion y deducción</a></li>-->
                        <!--<li><a href="../config/codigosiniestro">Codigo de siniestrado</a></li>-->
                        <!--<li><a href="../config/puntosventa">Puntos de venta</a></li>-->
                        <!--<li> <a href="../config/taxes">Impuestos</a></li>-->
                        <!--<li><a href="../config/metodospago">Metodo de Pago</a></li>-->
                        <!--<li><a href="../config/grupos">Grupo</a></li>-->
                        <!--<li><a href="../parametros/tiposperiodo">Tipos de Periodo</a></li>-->
                    </ul>
                </li>

                <!--panel principal-->
                <li><a href="#pages-nav-admin-seguridad" data-toggle="collapse"  aria-expanded="@yield('FatherSeguridad')"><i
                                class="icon-user"></i><span>Seguridad</span>
                    </a>
                    <ul id="pages-nav-admin-seguridad" class="collapse list-unstyled @yield('ChildSeguridad')">
                        <li><a href="../seguridad/usuarios">Usuarios</a></li>
                        <li><a href="../seguridad/roles">Roles</a></li>
                       <!-- <li><a href="../seguridad/auditoria">Actividad en el Sistema</a></li>-->
                       <!-- <li><a href="../seguridad/usuarioempresa">Usuarios por Empresa</a></li>-->
                        <li><a href="../master/datasource">Fuentes de Datos</a></li>  

                    </ul>
                </li>
                @endif

                <!-- HEADER MENU DE EMPRESA -->

               

                        <!-- <img src="../img/defaultlogo.png" alt="person" class="img-fluid rounded-circle">-->
                       <!-- <h2 class="h5 text-uppercase">Datos de Empresa</h2>-->
                 
                <!-- FIN HEADER MENU EMPRESA -->

                <!--listado de empresas y organigrama-->
              <!--  <li><a id="Organigrama" href="#pages-nav-list-company-legajos" data-toggle="collapse" aria-expanded="@yield('FatherOrganigrama')"><i
                                class="icon-presentation"></i><span>Organigrama</span>
                    </a>
                    <ul id="pages-nav-list-company-legajos" class="collapse list-unstyled @yield('ChildOrganigrama')">
                        <li><a href="../legajo/legajosactivos">Legajos Activos</a></li>
                        <li><a href="../legajo/legajosinactivos">Legajos Inactivos</a></li>
                        {{--<li><a href="../legajo/organigram">Orgranigrama Gráfico</a></li>--}}
                        {{--<li><a href="../legajo/categorysalary">Categoria de salario</a></li>--}}
                        {{--<li><a href="../legajo/legajonuevo">Registro Legajos </a></li>--}}
                        

                    </ul>
                </li>  FIN opciones

                <!--liquidacion-->
             <!--  <li><a href="#pages-nav-list-company-liquidacion" data-toggle="collapse" aria-expanded="@yield('FatherLiquidacion')"><i
                                class="icon-bill"></i><span>Liquidación</span>
                    </a>

                    <ul id="pages-nav-list-company-liquidacion" class="collapse list-unstyled @yield('ChildLiquidacion')">
                        <li><a href="../corrida/corrernomina">Correr Nóminas</a></li>
                        <li><a href="../corrida/corridascerradas">Corridas Cerradas </a></li>
                        <li><a href="../corrida/corridasabiertas">Corridas Abiertas</a></li>
                        <li><a href="../corrida/corridasnovedades">Corridas con Novedades </a></li>
                        <li><a href="../corrida/sicoss">Editor de SICOSS </a></li>
                        

                    </ul>

                </li>-->
               <!-- <li><a href="../corrida/descargar"><i class="icon-bill"></i><span>Reportes Principales </span>
                    </a></li>-->
              
                <!--<li><a href="#pages-nav-list-company-reportes" data-toggle="collapse" aria-expanded="@yield('FatherReportes')"><i
                                class="icon-line-chart"></i><span>Reportes</span>
                    </a>

                    <ul id="pages-nav-list-company-reportes" class="collapse list-unstyled @yield('ChildReportes')">
                        <li><a href="../reports/Form649">Form. 649 </a></li>
                        <li><a href="../reports/Art80">Certif. Art.80 </a></li>
                        <li><a href="../reports/Employcertificate">Certificado Empleo </a></li>
                        <li><a href="../reports/Reportconceptroster">Reporte de conceptos de nómina </a></li>

                    </ul>

                </li>

                <li><a href="#pages-nav-list-company-otrosreportes" data-toggle="collapse" aria-expanded="@yield('FatherOtrosReportes')"><i
                                class="icon-line-chart"></i><span>Otros Reportes</span>
                    </a>

                    <ul id="pages-nav-list-company-otrosreportes" class="collapse list-unstyled @yield('ChildOtrosReportes')">
                        <li><a href="../reports/Resumencompany">Resumen de Empresa</a></li>
                        <li><a href="../reports/Reportaditional">Reportes Adicionales </a></li>
                        <li><a href="../reports/Reportextrahours">Reporte Horas Extra</a></li>
                        <li><a href="../reports/Reportlicenses">Reporte de licencias y ausentismos </a></li>
                        <li><a href="../reports/Reportenetobanco">Reporte de netos por banco</a></li>
                        <li><a href="../reports/Reportsindicate">Reporte sindicatos </a></li>

                    </ul>

                </li>-->
              <!--  <li><a href="../herramientas/importararchivos"><i
                                class="icon-bars"></i><span>Importación de Archivos</span>
                    </a>

                </li>-->
               <!-- <li><a href="#pages-nav-listpara" data-toggle="collapse"aria-expanded="@yield('FatherParametrizacion2')"><i
                                class="icon-pencil-case"></i><span>Parametrización</span>
                    </a>
                    <ul id="pages-nav-listpara" class="collapse list-unstyled @yield('FatherParametrizacion2')">

                        <li><a href="../parametros/constantes">Valor de Constantes</a></li>
                        <li><a href="../parametros/companyconstant">Constantes por Empresa</a></li>
                        <li><a href="../parametros/companyformula">Formulas por Empresa</a></li>


                    </ul>
                </li>-->
                <!--panel principal-->
                <li><a href="#pages-nav-company-mensajeria" data-toggle="collapse" aria-expanded="@yield('FatherMensajeria')"><i
                                class="icon-mail"></i><span>Mensajería</span>
                    </a>
                    <ul id="pages-nav-company-mensajeria" class="collapse list-unstyled @yield('ChildMensajeria')">
                        <li><a href="../mensajeria/mensajes">Mensajería</a></li>
                        <li><a href="../mensajeria/notas">Chat</a></li>
                        <li><a href="../mensajeria/noticias">Noticias</a></li>
                    </ul>
                </li>


                <li><a> <span></span></a></li>
                <li><a> <span></span></a></li>
                <li><a> <span></span></a></li>
                <li><a> <span></span></a></li>
                <li><a> <span></span></a></li>
                <li><a> <span></span></a></li>
                <li><a> <span></span></a></li>
                <li><a> <span></span></a></li>
                <li><a> <span></span></a></li>

            </ul>
           

            </ul>
        </div>
      
    </div>
</nav>

