  <!-- Javascript files-->

  <script src="../js/librariesjs/jquery.min.js"></script>
  <script src="../js/librariesjs/toastr.min.js"></script>
<!--<script src="../js/librariesjs/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh"></script>-->
  <script>
      toastr.options = {
          "closeButton": false,
          "debug": false,
          "newestOnTop": false,
          "progressBar": true,
          "positionClass": "toast-bottom-full-width",
          "preventDuplicates": false,
          "onclick": null,
          "showDuration": "300",
          "hideDuration": "1000",
          "timeOut": "5000",
          "extendedTimeOut": "1000",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
      }
  </script>
<script src="../js/librariesjs/popper.min.js"></script>

  <script src="../js/librariesjs/tether.min.js"></script>
  <script src="../js/librariesjs/bootstrap.min.js"></script>
  <script src="../js/librariesjs/jquery.cookie.js"> </script>

  <script src="../js/librariesjs/grasp_mobile_progress_circle-1.0.0.min.js"></script>
  <script src="../js/librariesjs/jquery.nicescroll.min.js"></script>
  <script src="../js/librariesjs/jquery.validate.min.js"></script>
  <script src="../js/librariesjs/front.js"></script>

  
<script src="../js/librariesjs/sweetalert2.js" type="text/javascript"></script>

  <script src="../js/librariesjs/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="../js/librariesjs/dataTables.bootstrap4.min.js" type="text/javascript"></script>
<script src="../js/librariesjs/dataTables.scroller.min.js"></script>

  <script src="../js/librariesjs/fnReloadAjax.js"></script>
  
  <script src="../js/librariesjs/vue.js" type="text/javascript"></script>
  <script src="../js/librariesjs/v-mask.min.js"></script>
  <script src="../js/librariesjs/vue-router.js" type="text/javascript"></script>
  <script src="../js/librariesjs/vue-select.js"></script>
  <script src="../js/librariesjs/vue-resource.min.js" type="text/javascript"></script>
  <script src="../js/librariesjs/bootstrap-datepicker.js" type="text/javascript"></script>
  <script src="../js/generic/generic.js" type="text/javascript"></script>
  <script src="../js/librariesjs/nprogress.js" type="text/javascript"></script>
 <script> $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    $.fn.dataTable
        .tables( { visible: true, api: true } )
        .scroller.measure();
})</script>


<script>
Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');
   $('#Logout').on("click",function(e) {
  
  
  Vue.http.post('../api/v1/user/closesession')
             .then(response => {

             if (response.status === 200)
         {
             // success callback
             swal({
                 title: "Sesión Cerrada",
                 text:'Hasta Luego',
                 type: "success"
             }).then(function ()
             {
                 window.location.href = "/";
             });

         }

     }).then().catch(function (data){
         if(data.status === 404) {
             swal('Credenciales Incorrectas','','error');
         }
         if(data.status === 500) {
             swal('Ocurrio un error interno','','error');
         }
     });

   });

Vue.component('v-select', VueSelect.VueSelect);
</script>
<script>
// As a plugin
Vue.use(VueMask.VueMaskPlugin);

// Or as a directive
Vue.directive('mask', VueMask.VueMaskDirective);
</script>


