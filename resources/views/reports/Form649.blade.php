@section('FatherReportes','true')
@section('ChildReportes','show')
<!DOCTYPE html>
  <html>
  @include('layouts.heads')
  <body>
    <!-- Side Navbar -->
    @include('layouts.sidebaradmin')
    <div class="page forms-page">
      <!-- navbar-->
      @include('layouts.navbar')
      <div id="vuedata">
 <!-- Modal -->
                                              
          <!-- FIN DE LA MODAL -->


      <!-- CABECERA GRIS-->
      <div class="breadcrumb-holder fixed-top">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-2 border">
            </div>
            <div class="col-md-7 border">
              <!-- TITULO DE VENTANA -->
              <h1> Form. 649</h1>
            </div>
         
          </div>
        </div>
      </div>
      <!-- FIN DE LA CABECERA GRIS -->

        <section class="forms">
        <div class="container-fluid">

          <!-- Tarjeta -->
          <div class="card" >
            <!-- Cabecera de Tarjeta  -->
            
           <!-- Cuerpo de Tarjeta -->
           <div class="card-body">  
               <div class="card-header">
                 <div class="row">
                   <div class="col-md-4">
                   </div>
                   <div class="col-md-4">
                     <button v-on:click="openmodaltonew" class="btn btn-info btn-fill pull-center">Generar</button>
                   </div>  
                   <div class="col-md-4">
                   </div>

                 </div>
               </div>  
                   
            
               <div class="row">

                            <div class="col-md-6">
                                

                                <label>Legajos</label>
                                  <select v-model="fd.Employee" class="form-control">
                                <option value="">Seleccione un legajo</option>
                               <option v-for="OneEmployee in fd.Employeeoptions" v-bind:value="OneEmployee.idn">
                                @{{ OneEmployee.firstname}}
                                 </option>
                                 </select>
          
                    </div>
                     <div class="col-md-6">

                     <div class="form-group">
                        <label>Rango de fechas</label>
                              <input v-model="fd.Rangedate" type="text" placeholder="__/__/____-__/__/____" class="form-control">
                                      </div>

                              </div>
                     
            </div>
          </div>
        </div>
        <!-- FIN de Tarjeta Hija -->
      </div>
    </section>
 </div>

    

 <div id="ModalEdicion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal text-left">
                                                    <div role="document" class="modal-dialog modal-lg">
                                                      <div class="modal-content">
                                                        <div class="modal-header">
                                                          <h5 id="exampleModalLabel" class="modal-title">Libro especiale de sueldo</h5>
                                                          <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                                                        </div>
                                                        <div class="modal-body">
                                                       
                                                         
                                                    <div class="row"> 
                                                        <!-- table -->
                                                        <div class="col-md-12">

                                                           <table id="TablaMaster" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                                                <thead>
                                                                  <tr>
                                                                    <th>Idn</th>
                                                                    <th>Nombre</th>
                                                                   
                                                                      
                                                                  </tr>
                                                                </thead>
                                                              </table>

                                                      </div>


                                                        </div>
                                                       <!-- <div class="modal-footer">
                                                              <button v-on:click="descargarpdf" class="btn btn-info btn-fill pull-left">Descargar pdf</button>
                                                             
                                                          </div>




                                                                    columnas legales de la tabla
                                                                    <th>CUIT Administrador</th>
                                                                    <th>Zona</th>
                                                                    <th>Domicilio Fiscal</th>
                                                                    <th>Mes de Cierre</th>
                                                                      <th>Actividad</th>
                                                                      <th>Observaciones</th>
                                                                      <th>Contactos</th>
                                                                       <th>Contrasenas</th>
                                                                       <th>Actividades (Impuestos)</th>







                                                              <button v-on:click="descargarexcel" class="btn btn-info btn-fill pull-left">Descargar excel</button>
                                                             
                                                              <button type="button" class="btn btn-primary btn-fill pull-rigth" data-dismiss="modal">Cerrar</button>  
                                                          </div>-->
                                                        </div>
                                                      </div>
                                                    </div>  
                                                
                                                 <!-- FIN MODAL -->
        


      <!-- CABECERA GRIS-->

      <!-- FIN DE LA CABECERA GRIS -->

     </div> 







    @include('layouts.footer')
  </div>
  @include('layouts.libraries')
 <script src="../js/reportsjs/form649.js"></script>
</body>
</html>