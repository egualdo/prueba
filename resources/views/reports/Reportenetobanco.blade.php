@section('FatherOtrosReportes','true')
@section('ChildOtrosReportes','show')
<!DOCTYPE html>
  <html>
  @include('layouts.heads')
  <body>
    <!-- Side Navbar -->
    @include('layouts.sidebaradmin')
    <div class="page forms-page">
      <!-- navbar-->
      @include('layouts.navbar')
      <div id="vuedata">
 <!-- Modal -->
                    <div id="ModalEdicion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal text-left">
                                                    <div role="document" class="modal-dialog">
                                                      <div class="modal-content">
                                                        <div class="modal-header">
                                                          <h5 id="exampleModalLabel" class="modal-title">Editar Elemento</h5>
                                                          <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                                                        </div>
                                                        <div class="modal-body">
                                                          <p>Indica los datos solicitados</p>
                                                         
                                                           <div class="row"> 
                                                         <div class="col-md-6">
                                                            <div class="form-group">
                                                              <label>Titulo</label>
                                                              <input v-model="fd.Tittle" type="text" placeholder="" class="form-control">
                                                            </div>
                                                            <div class="form-group">       
                                                              <label>Descripción</label>
                                                              <input v-model="fd.Description" type="text" placeholder="" class="form-control">
                                                            </div>
                                                          
                                                           <label>Compañia</label>
                                  <select v-model="fd.Company" class="form-control">
                                <option value="">Seleccione una Compañia</option>
                               <option v-for="OneCompany in fd.Companyoptions" v-bind:value="OneCompany.idn">
                                @{{ OneCompany.name}}
                                 </option>
                                 </select>
                                    </div> 

                                            <div class="col-md-6">
                                          <label>Emisor</label>
                                  <select v-model="fd.Transmitter" class="form-control">
                                <option value="">Seleccione un Emisor</option>
                               <option v-for="OneTransmitter in fd.Transmitteroptions" v-bind:value="OneTransmitter.idn">
                                @{{ OneTransmitter.username}}
                                 </option>
                                 </select>

                                                         <label>Receptor</label>
                                  <select v-model="fd.Receiver" class="form-control">
                                <option value="">Seleccione un Receptor</option>
                               <option v-for="OneReceiver in fd.Receiveroptions" v-bind:value="OneReceiver.idn">
                                @{{ OneReceiver.username}}
                                 </option>
                                 </select>
                                                  </div>

                                                      </div>


                                                        </div>
                                                        <div class="modal-footer">
                                                          <div v-if="ff.save == true">
                                                              <button v-on:click="guardar" class="btn btn-sucess btn-fill pull-left">Guardar</button>
                                                              <button type="button" class="btn btn-primary btn-fill pull-rigth" data-dismiss="modal">Cerrar</button>
                                                          </div>

                                                           <div v-else-if="ff.edit == true">
                                                              <button v-on:click="editar" class="btn btn-info btn-fill pull-left">Editar</button>
                                                              <button v-on:click="borrar" class="btn btn-warning btn-fill pull-rigth">Eliminar</button>
                                                              <button type="button" class="btn btn-primary btn-fill pull-rigth" data-dismiss="modal">Cerrar</button>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </div>
                                                 </div>
                                                 <!-- FIN MODAL -->                           
       


      <!-- CABECERA GRIS-->
      <div class="breadcrumb-holder fixed-top">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-2 border">
            </div>
            <div class="col-md-7 border">
              <!-- TITULO DE VENTANA -->
              <h1> Reporte de netos por banco</h1>
            </div>
         
          </div>
        </div>
      </div>
      <!-- FIN DE LA CABECERA GRIS -->
 

 <section class="forms">
        <div class="container-fluid">

          <!-- Tarjeta -->
          <div class="card" >
            <!-- Cabecera de Tarjeta  -->
             <div class="card-header" >
              <div class="row">
                <div class="col-md-4">
                </div>
                <div class="col-md-4">

                  <button v-on:click="openmodaltonew" class="btn btn-info btn-fill pull-center">Generar</button>
                </div>
                <div class="col-md-4">
                </div>

              </div>
  
             </div>
           <!-- Cuerpo de Tarjeta -->
           <div class="card-body">  
              
            
               <div class="row">
                    
                    <div class="col-md-4">

                      <label>Periodo</label>
                                  <select v-model="fd.Period" class="form-control">
                                <option value="">Seleccione un periodo</option>
                             <option v-for="OnePeriod in fd.Periodoptions" v-bind:value="OnePeriod.idn">
                                @{{OnePeriod.title}}


                                
                                 </option>
                                 </select>
                    </div>
               
                    
                    <div class="col-md-4">
                     <div class="form-group">
                        <label>Rango de fechas</label>
                              <input v-model="fd.Rangedate" type="text" placeholder="__/__/____-__/__/____" class="form-control">
                                      </div>
                    </div>
                     
                    
                    <div class="col-md-4">
                        
                        <label>Incluir baja del Mes</label>
                                  <select v-model="fd.Bajames" class="form-control">
                                <option value="">Seleccione un periodo</option>
                               <option value="1">Si</option>
                               <option value="2">No</option>
                               
                                 </select>
                    </div>
                    
              
            </div>
          </div>
        </div>
        <!-- FIN de Tarjeta Hija -->
      </div>
    </section>












 </div>

     



    @include('layouts.footer')
  </div>
  @include('layouts.libraries')
 <script src="../js/reportsjs/reportsbybank.js"></script>
</body>
</html>