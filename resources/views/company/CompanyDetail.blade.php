@section('FatherEmpresas','true')
@section('ChildEmpresas','show')
<!DOCTYPE html>
<html>
@include('layouts.heads')
<body>
<!-- Side Navbar -->
@include('layouts.sidebaradmin')
<div class="page forms-page">
    <!-- navbar-->
    <div id="vuedata">
    @include('layouts.navbar')
    <!-- CABECERA GRIS-->
        <div class="breadcrumb-holder fixed-top">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-2 border">
                    </div>
                    <div class="col-md-5 border">
                        <h1>Detalle de la Empresa</h1>
                    </div>
                    <div class="col-md-2">
                        {{--<p>Dato de Prueba</p>--}}
                    </div>


                    <div class="col-md-3">
                        <button v-on:click="openmodaltoclone" class="btn btn-sm btn-primary btn-fill pull-left">Clonar</button>
                        <button v-on:click="edit" class="btn  btn-sm btn-info btn-fill pull-left">Guardar</button>
                        <button v-on:click="reactivate" v-show="fd.Active === 0" class="btn  btn-sm btn-success btn-fill pull-rigth">Reactivar</button>
                 
                        <button v-on:click="deletes" v-show="fd.Active === 1" class="btn  btn-sm btn-warning btn-fill pull-rigth">Inactivar</button>
                   </div>

                </div>
            </div>
        </div>
        <!-- FIN DE LA CABECERA GRIS -->
        <section class="forms">
            <div class="container-fluid">

                <!-- TARJETA PADRE -->
                <div class="card">
                    <!-- Titulo Tarjeta Padre-->

                    <!-- Tabs de Navegacion -->
                    <ul class="nav nav-tabs flex-wrap" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" href="#Information" role="tab" data-toggle="tab">Info.Basica</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#Image" role="tab" data-toggle="tab">Imagenes</a>
                        </li>
                    </ul>


                    <!--Cuerpo de Tarjeta Padre-->
                    <div class="card-body">
                        <!-- Contenido Total de La navegacion-->
                        <div class="tab-content">
                            <!-- Primera Pestaña -->
                            <div role="tabpanel" class="tab-pane active" id="Information">
                                <div class="card">
                                    <!-- Cabecera de Tarjeta Hija -->
                                    <div class="card-header">
                                        <h2> Información basica</h2>
                                    </div>
                                    <!-- Cuerpo de Tarjeta Hija-->
                                    <div class="card-body">
                                        <div class="row">
                                            <!-- Division del cuerpo en 3 columns -->
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Nombre</label>
                                                    <input type="input" placeholder="Nombre" v-model="fd.Name"
                                                           class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>CUIT</label>
                                                    <input type="input" placeholder="CUIT" v-model="fd.Cuit"
                                                           class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>CUIT del administrador</label>
                                                    <input type="input" placeholder="CUIT del administrador"
                                                           v-model="fd.Cuiladmin" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>Tipo de persona</label>
                                                    </br>
                                                    <v-select v-model="fd.Typeperson" :options="TypePerson"></v-select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Tipo</label>
                                                    <v-select v-model="fd.Companytype" :options="Typecompanyoptions"></v-select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Actividad</label>
                                                    <v-select v-model="fd.Companyactivity" :options="Activityoptions"></v-select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Zona</label>
                                                    </br>
                                                    <v-select v-model="fd.Companyzone" :options="Zoneoptions"></v-select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Domicilio fiscal</label>
                                                    <input type="input" placeholder="Domicilio Fiscal"
                                                           v-model="fd.Fiscaldomicile" class="form-control">
                                                </div>

                                                <div class="form-group">
                                                    <label>Beneficio de reducción de Cargas Sociales</label>
                                                    <form action="/action_page.php">
                                                        <input type="checkbox" v-model="fd.Benefitsoccharges"
                                                               name="respuesta" value="SI"> SI<br>
                                                        <input type="checkbox" v-model="fd.Benefitsoccharges2"
                                                               name="respuesta" value="NO"> NO<br>

                                                    </form>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <!--<div class="form-group">
                                                  <label>Firma digitalizada</label>
                                                  <input type="input" v-model="fd.Photosing" placeholder="firma digitalizada" class="form-control">
                                                </div>-->

                                                <!--   <div class="form-group">
                                                     <label>logo</label>
                                                     <input type="input" v-model="fd.Photologo" placeholder="logo" class="form-control">
                                                   </div>-->
                                                <div class="form-group">
                                                    <label>CCT</label>
                                                    <input type="input" v-model="fd.companycct" placeholder="cct"
                                                           class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>Mes de cierre </label>
                                                    <v-select name="mescierre"  :options="fd.Meses" v-model="fd.Monthclose"></v-select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Actividad y Rama</label>
                                                    <input type="input" v-model="fd.TCompanyactivity"
                                                           placeholder="actividad y rama" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-3">

                                            <!--   <div class="form-group">
                        <label>Actividad (impuestos)</label>
              <select v-model="fd.Taxes" class="form-control">
              <option value="">Seleccione una actividad</option>
              <option v-for="Taxesoptions in Taxesoptions" v-bind:value="Taxesoptions.idn">
                  @{{ Taxesoptions.name }}
                                                    </option>
                                                    </select>
                                                  </div> -->
                                                <div class="form-group">
                                                    <label>Observaciones</label>
                                                    <textarea class="form-control" rows="4" v-model="fd.Observation" cols="15">

              </textarea>
                                                </div>

                                                <div class="form-group">
                                                    <label>Grupo de Esta Empresa</label>
                                                    <v-select  :options="fd.CompanyGrouplist" v-model="fd.CompanyGroup"></v-select>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <!-- FIN de Tarjeta Hija -->

                                <!-- Nueva Tarjeta hija -->

                                <!-- MODAL -->
                                <div id="ModalEdicion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                     aria-hidden="true" class="modal text-left">
                                    <div role="document" class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 id="exampleModalLabel" class="modal-title">Crear Elemento</h5>
                                                <button type="button" data-dismiss="modal" aria-label="Close"
                                                        class="close"><span aria-hidden="true">×</span></button>
                                            </div>
                                            <div class="modal-body">
                                                <p>Indica los datos solicitados</p>

                                                <div class="form-group">
                                                    <label>Nombre</label>
                                                    <input type="input" placeholder="Nombre" v-model="fd.Namec"
                                                           class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>Email</label>
                                                    <input type="input" placeholder="Email" v-model="fd.Emailc"
                                                           class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>Telefono</label>
                                                    <input type="input" placeholder="Telefono" v-model="fd.Telephonec"
                                                           class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <input type="input" placeholder="idn" v-model="fd.Idn"
                                                           class="form-control" style="visibility:hidden">
                                                </div>
                                                <div class="form-group">
                                                    <input type="input" style="visibility:hidden" placeholder="idncont"
                                                           v-model="fd.Idncont" class="form-control">
                                                </div>


                                            </div>
                                            <div class="modal-footer">
                                                <div v-if="ff.savec == true">
                                                    <button v-on:click="guardarc"
                                                            class="btn btn-sucess btn-fill pull-left">Guardar
                                                    </button>
                                                    <button type="button" class="btn btn-primary btn-fill pull-rigth"
                                                            data-dismiss="modal">Cerrar
                                                    </button>
                                                </div>

                                                <div v-else-if="ff.editc == true">
                                                    <button v-on:click="editarc"
                                                            class="btn btn-info btn-fill pull-left">Editar
                                                    </button>
                                                    <button v-on:click="borrarc"
                                                            class="btn btn-warning btn-fill pull-rigth">Eliminar
                                                    </button>
                                                    <button type="button" class="btn btn-primary btn-fill pull-rigth"
                                                            data-dismiss="modal">Cerrar
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- FIN MODAL -->

                                <div id="ModalClonar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                     aria-hidden="true" class="modal text-left">
                                    <div role="document" class="modal-dialog">

                                        <div class="modal-content">
                                            <div class="loader" style="display: none">
                                                <div class="spinner">
                                                    <div class="bounce1"></div>
                                                    <div class="bounce2"></div>
                                                    <div class="bounce3"></div>
                                                </div>
                                            </div>
                                            <div class="modal-header">
                                                <h5 id="exampleModalLabel" class="modal-title">Clonar Compañia</h5>
                                                <button type="button" data-dismiss="modal" aria-label="Close"
                                                        class="close"><span aria-hidden="true">×</span></button>
                                            </div>
                                            <div class="modal-body">
                                                <p>Indica los datos solicitados</p>

                                                <div class="form-group">
                                                    <label>Nuevo Nombre</label>
                                                    <input type="input" placeholder="Nombre" v-model="fd.NameClone"
                                                           class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>Nuevo Cuit</label>
                                                    <input type="input" v-mask="'##-########-#'" placeholder="Cuit" v-model="fd.CuitClone"
                                                           class="form-control">
                                                </div>

                                                <div class="form-group">
                                                    <label>Nuevo Code</label>
                                                    <input type="input" placeholder="Code" v-model="fd.CodeClone"
                                                           class="form-control">
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <div>
                                                    <button v-on:click="clonar"
                                                            class="btn btn-success btn-fill pull-left">Clonar
                                                    </button>
                                                    <button type="button" class="btn btn-primary btn-fill pull-rigth"
                                                            data-dismiss="modal">Cerrar
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <!-- FIN DE LA MODAL -->


                                <div class="card">
                                    <div class="card-header">
                                        <div class="row">
                                            <div class="col-md-9">
                                                <h2>Contacto</h2>
                                            </div>
                                            <div class="col-md-3">
                                                <button v-on:click="openmodaltonew" type="button"
                                                        class="btn btn-primary btn-fill pull-left">Agregar Contacto
                                                </button>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <!-- Division del cuerpo en 3 columns -->
                                            <div class="col-md-12">

                                                <div class="form-group">
                                                    <table id="TablaContactCompany"
                                                           class="table table-striped table-bordered" cellspacing="0"
                                                           width="100%">
                                                        <thead>
                                                        <tr>
                                                            <th>Idn</th>
                                                            <th>Nombre</th>
                                                            <th>Email</th>
                                                            <th>Telefono</th>
                                                        </tr>
                                                        </thead>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div role="tabpanel" class="tab-pane active" id="Image">
                                <div class="card">
                                    <!-- Cabecera de Tarjeta Hija -->
                                    <div class="card-header">
                                        <h2> Imagenes de La Empresa</h2>
                                    </div>
                                    <!-- Cuerpo de Tarjeta Hija-->
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6">

                                                <label for="LogoInput" class="btn btn-success">Buscar Logo </label>
                                                <input type="file" style="display: none;" id="LogoInput">
                                                <br>
                                                @{{ fd.FileLogoName }}
                                                <br>
                                                <img v-show="fd.PhotologoImg !== ''" :src="fd.PhotologoImg"  alt="" style="display: none; width: 100%" id="ImgLogo">
                                            </div>
                                            <div class="col-md-6">
                                                <label for="SingInput" class="btn btn-success">Buscar Firma</label>
                                                <br>
                                                @{{  fd.FileSingName }}
                                                <input type="file" style="display: none; width: 100%" id="SingInput">
                                                <br>

                                                <img v-show="fd.PhotosingImg !== ''" :src="fd.PhotosingImg" alt="" style="display: none; width: 100%" id="ImgSing">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                    </div>
                    <!-- FIN DE PRIMERA PESTAÑA -->
                </div>
            </div>
            <br>

        </section>
        <br>
        <br>
        <br>


        @include('layouts.footer')
    </div>
</div>
@include('layouts.libraries')

<script src="../js/generic/generic.js" type="text/javascript"></script>
<script src="../js/mycompanyjs/companydetailinfo.js"></script>
</body>
</html>
