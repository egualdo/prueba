@section('FatherEmpresas','true')
@section('ChildEmpresas','show')
<!DOCTYPE html>
<html>
@include('layouts.heads')
<body>
<!-- Side Navbar -->
@include('layouts.sidebaradmin')
<div class="page forms-page">
    <!-- navbar-->
    @include('layouts.navbar')
    <div id="vuedata">
        <!-- Modal -->

        <!-- MODAL -->
        <div id="ModalEdicion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"
             class="modal text-left">
            <div role="document" class="modal-dialog" style="min-width: 900px">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 id="exampleModalLabel" class="modal-title">Crear Una nueva Empresa</h5>
                        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span
                                    aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <p>Indica los datos solicitados</p>

                        <div class="row">
                            <!-- FIRST COLUMN -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Codigo</label>
                                    <input type="input" placeholder="Codigo" v-model="fd.Code" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Grupo de Empresa</label>
                                    <v-select v-model="fd.CompanyGroup" :options="CompanyGroupList"></v-select>
                                </div>
                                <div class="form-group">
                                    <label>Nombre</label>
                                    <input type="input" placeholder="Nombre" v-model="fd.Name" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>CUIT</label>
                                    <input type="input" placeholder="CUIT" v-model="fd.Cuit" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>CUIT del administrador</label>
                                    <input type="input" placeholder="CUIT del administrador" v-model="fd.Cuiladmin"
                                           class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Tipo de persona</label>
                                    </br>
                                    <v-select v-model="fd.Typeperson" :options="TypePerson"></v-select>
                                </div>
                                <div class="form-group">
                                    <label>Tipo</label>
                                    <v-select v-model="fd.Companytype" :options="Typecompanyoptions"></v-select>
                                </div>
                            </div>
                            <!-- SECOND COLUMN -->
                            <div class="col-md-4">


                                <div class="form-group">
                                    <label>Actividad</label>
                                    <v-select v-model="fd.Companyactivity" :options="Activityoptions"></v-select>
                                </div>

                                <div class="form-group">
                                    <label>Zona</label>
                                    </br>
                                    <v-select v-model="fd.Companyzone" :options="Zoneoptions"></v-select>

                                </div>
                                <div class="form-group">
                                    <label>Domicilio fiscal</label>
                                    <input type="input" placeholder="Domicilio Fiscal" v-model="fd.Fiscaldomicile"
                                           class="form-control">
                                </div>

                                <div class="form-group">
                                    <label>Beneficio de reducción de Cargas Sociales</label>

                                        <input type="radio" v-model="fd.Benefitsoccharges" name="respuesta"
                                               value=1> SI<br>
                                        <input type="radio" v-model="fd.Benefitsoccharges" name="respuesta"
                                               value=0 checked> NO<br>




                                </div>
                                <div class="form-group">
                                    <label>Actividad y Rama</label>
                                    <input type="input" v-model="fd.TCompanyactivity" placeholder="actividad y rama"
                                           class="form-control">
                                </div>


                                <div class="form-group">
                                    <label>CCT</label>
                                    <input type="input" v-model="fd.companycct" placeholder="cct" class="form-control">
                                </div>

                            </div>

                            <!-- TREE COLUMN -->

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Mes de cierre </label>
                                    <v-select name="mescierre" id="CmbMeses" :options="fd.Meses" v-model="fd.Monthclose"></v-select>
                                </div>

                                <div class="form-group">
                                    <label>Observaciones</label>
                                    </br>
                                    <textarea class="form-control" rows="4" v-model="fd.Observation">

          </textarea>
                                </div>

                                <div class="form-group">


                                    <label>Credenciales</label>
                                    <input type="input" v-model="fd.DBUsername" placeholder="Usuario de Base de Datos"
                                           class="form-control">
                                    <br>
                                    <input type="input" v-model="fd.DBPassword" placeholder="Clave de Base de Datos"
                                           class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="">Imagen de Logo y Firma Digital</label>
                                    <br>
                                    <label for="LogoInput" class="btn btn-success">Buscar Logo </label>
                                    <input type="file" style="display: none;" id="LogoInput">
                                    <br>
                                    @{{ fd.FileLogoName }}
                                    <br>
                                    <label for="SingInput" class="btn btn-success">Buscar Firma</label>
                                    <br>
                                    @{{  fd.FileSingName }}
                                    <input type="file" style="display: none;" id="SingInput">
                                </div>
                            </div>


                        </div>


                    </div>
                    <div class="modal-footer">
                        <div v-if="ff.save === true">
                            <button v-on:click="guardar" class="btn btn-sucess btn-fill pull-left">Guardar</button>
                            <button type="button" class="btn btn-primary btn-fill pull-rigth" data-dismiss="modal">
                                Cerrar
                            </button>
                        </div>

                        <div v-else-if="ff.edit === true">
                            <button v-on:click="editar" class="btn btn-info btn-fill pull-left">Editar</button>
                            <button v-on:click="borrar" class="btn btn-warning btn-fill pull-rigth">Eliminar</button>
                            <button type="button" class="btn btn-primary btn-fill pull-rigth" data-dismiss="modal">
                                Cerrar
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- FIN MODAL -->

        <!-- FIN DE LA MODAL -->


        <!-- CABECERA GRIS-->
        <div class="breadcrumb-holder fixed-top">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-2 border">
                    </div>
                    <div class="col-md-7 border">
                        <!-- TITULO DE VENTANA -->
                        <h1> Empresas Activas</h1>
                    </div>
                    <div class="col-md-3">
                        <!--<p>Dato de Prueba</p>-->
                        <button v-on:click="openmodaltonew" class="btn btn-info btn-fill pull-left">Nuevo Elemento
                        </button>
                    </div>

                </div>
            </div>
        </div>
        <!-- FIN DE LA CABECERA GRIS -->
    </div>

    <section class="forms">
        <div class="container-fluid">

            <!-- Tarjeta -->
            <div class="card">
                <!-- Cabecera de Tarjeta  -->

                <!-- Cuerpo de Tarjeta -->
                <div class="card-body">

                    <div class="row">
                        <!-- === PANEL DE TABLA === -->
                        <!-- ====== DATATABLE ======= -->
                        <table id="TablaMaster" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>Idn</th>
                                <th>Código</th>
                                <th>Nombre</th>
                                <th>Cuit</th>
                                <th>Tipo</th>
                                <th>CCT</th>
                            </tr>
                            </thead>
                        </table>
                        <!-- ====== FIN DATATABLE======= -->
                    </div>
                </div>
            </div>
            <!-- FIN de Tarjeta Hija -->
        </div>
    </section>


    @include('layouts.footer')
</div>
@include('layouts.libraries')
<script src="../js/mycompanyjs/listcompanyactive.js"></script>
<script src="../js/generic/generic.js" type="text/javascript"></script>

</body>
</html>