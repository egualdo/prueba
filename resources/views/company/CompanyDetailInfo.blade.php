   <!-- Tarjeta hija -->

<div class="card" >
                  <!-- Cabecera de Tarjeta Hija -->
  <div class="card-header">
    <h2> Información basica</h2>
  </div>
                  <!-- Cuerpo de Tarjeta Hija-->
  <div class="card-body">  
    <div class="row">
                      <!-- Division del cuerpo en 3 columns -->
      <div class="col-md-3">
        <div class="form-group">
                          <label>Nombre</label>
                          <input v-model="mod.fd.Name" type="input" placeholder="Nombre" class="form-control">
        </div>
        <div class="form-group">       
                          <label>CUIT</label>
                          <input v-model="mod.fd.Cuit" type="input" placeholder="CUIT" class="form-control">
        </div>
        <div class="form-group">       
                          <label>CUIT del administrador</label>
                          <input type="input" placeholder="CUIT del administrador" class="form-control">
        </div>
        <div class="form-group">       
                        <label>Tipo de persona</label>
                        </br>
                       <select name="Typeperson" v-model="mod.fd.Typeperson" class="form-control">
                        <option>Persona Fisica</option>
                        <option>Persona Juridica</option>
                        </select>
        </div>
        <div class="form-group">       
                        <label>Tipo</label>
              <select v-model="mod.fd.Companytype" class="form-control">
              <option value="">Seleccione un Tipo</option>
              <option v-for="Typecompanyoptions in Typecompanyoptions" v-bind:value="Typecompanyoptions.idn">
                  @{{ Typecompanyoptions.name }}
              </option>
              </select>
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">       
                        <label>Actividad</label>
              <select v-model="mod.fd.Companyactivity" class="form-control">
              <option value="">Seleccione una Actividad</option>
              <option v-for="Activityoptions in Activityoptions" v-bind:value="Activityoptions.idn">
                  @{{ Activityoptions.name }}
              </option>
              </select>
        </div>
        <div class="form-group">       
                        <label>Zona</label>
                         </br>
              <select v-model="mod.fd.Companyzone" class="form-control">
              <option value="">Seleccione una Zona</option>
              <option v-for="Zoneoptions in Zoneoptions" v-bind:value="Zoneoptions.idn">
                  @{{ Zoneoptions.name }}
              </option>
              </select>
        </div>
        <div class="form-group">
                        <label>Domicilio fiscal</label>
                        <input type="input" placeholder="Domicilio Fiscal" class="form-control">
        </div>

        <div class="form-group">
                        <label>Beneficio de reducción de Cargas Sociales</label>
                        <form action="/action_page.php">
                      <input type="checkbox" name="respuesta" value="SI"> SI<br>
                      <input type="checkbox" name="respuesta" value="NO" checked> NO<br>
                     
                    </form>
        </div>
       </div>

      <div class="col-md-3">
                      <div class="form-group">       
                        <label>Firma digitalizada</label>
                        <input type="input" placeholder="firma digitalizada" class="form-control">
                      </div>

                      <div class="form-group">
                        <label>logo</label>
                        <input type="input" placeholder="logo" class="form-control">
                      </div>

                      <div class="form-group">
                        <label>Actividad y Rama</label>
                        <input type="input" v-model="mod.fd.Companyactivity" placeholder="actividad y rama" class="form-control"> 
                      </div>
      </div>
      <div class="col-md-3">
                      <div class="form-group">
                        <label>CCT</label>
                        <input type="input" placeholder="cct" class="form-control"> 
                      </div>
                     <div class="form-group">       
                        <label>Mes de cierre </label>
                        <select name="mescierre" class="form-control">
                        <option>Enero</option>
                        <option>Febrero</option>
                        <option>Marzo</option>
                        <option>Abril</option>
                         <option>Mayo</option>
                       <option>Junio</option>
                        <option>Julio</option>
                        <option>Agosto</option>
                        <option>Septiembre</option>
                        <option>Octubre</option>
                        <option>Noviembre</option>
                       <option>Diciembre</option>
                        </select>
                      </div>
                       <div class="form-group">       
                        <label>Actividad (impuestos)</label>
              <select v-model="mod.fd.Taxes" class="form-control">
              <option value="">Seleccione una actividad</option>
              <option v-for="Taxesoptions in Taxesoptions" v-bind:value="Taxesoptions.idn">
                  @{{ Taxesoptions.name }}
              </option>
              </select>
                      </div>
                     <div class="form-group">       
                        <label>Observaciones</label>
                      <textarea rows="4" cols="15">
 
                      </textarea>
                       </div>
      </div>

    </div>
  </div>
</div>

              <!-- FIN de Tarjeta Hija -->

              <!-- Nueva Tarjeta hija -->

<div class="card" >
  <div class="card-header">
    <div class="row">
      <div class="col-md-9">
             <h2>Contacto</h2>
      </div>
      <div class="col-md-3">
             <button type="button" class="btn btn-primary">Agregar Contacto</button>
      </div>
    </div>
  </div>
  <div class="card-body">
    <div class="row">
          <!-- Division del cuerpo en 3 columns -->
      <div class="col-md-12">

        <div class="form-group">
              <table id="Tablacontactos" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>Nombre</th>
                    <th>Email</th>
                    <th>Telefono</th>
                    <th>Acciones</th>
                  </tr>
                </thead>
              </table>
        </div>
      </div>
    </div>
  </div>
</div>

