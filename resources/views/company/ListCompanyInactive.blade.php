@section('FatherEmpresas','true')
@section('ChildEmpresas','show')
<!DOCTYPE html>
  <html>
  @include('layouts.heads')
  <body>
    <!-- Side Navbar -->
    @include('layouts.sidebaradmin')
    <div class="page forms-page">
      <!-- navbar-->
      @include('layouts.navbar')
      <div id="vuedata">
 <!-- Modal -->
                                              
          <!-- FIN DE LA MODAL -->


      <!-- CABECERA GRIS-->
      <div class="breadcrumb-holder fixed-top">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-2 border">
            </div>
            <div class="col-md-7 border">
              <!-- TITULO DE VENTANA -->
              <h1> Empresas Inactivas</h1>
            </div>
         
          </div>
        </div>
      </div>
      <!-- FIN DE LA CABECERA GRIS -->
 </div>

      <section class="forms">
        <div class="container-fluid">

          <!-- Tarjeta -->
          <div class="card" >
            <!-- Cabecera de Tarjeta  -->
            
           <!-- Cuerpo de Tarjeta -->
           <div class="card-body">  

            <div class="row">
              <!-- === PANEL DE TABLA === --> 
              <!-- ====== DATATABLE ======= -->
             <table id="TablaMaster" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                  <tr>
                   <th>Idn</th>
                     <th>Código</th>
                      <th>Nombre</th>
                       <th>Cuit</th>
                        <th>Tipo</th>
                         <th>CCT</th>
                  </tr>
                </thead>
              </table>
              <!-- ====== FIN DATATABLE======= -->
            </div>
          </div>
        </div>
        <!-- FIN de Tarjeta Hija -->
      </div>
    </section>



    @include('layouts.footer')
  </div>
  @include('layouts.libraries')
 <script src="../js/mycompanyjs/listcompanyinactive.js"></script>
</body>
</html>