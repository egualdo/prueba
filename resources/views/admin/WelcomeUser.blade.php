<!DOCTYPE html>
<html>
@include('layouts.heads')
<body>
<!-- Side Navbar -->
@include('layouts.sidebar')
<div class="page forms-page">
    <!-- navbar-->
@include('layouts.navbar')
<!-- CABECERA GRIS-->
    <div class="breadcrumb-holder fixed-top">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-2 border">
                </div>
                <div class="col-md-5 border">
                    <p>Dato de Prueba</p>
                </div>
                <div class="col-md-5">
                    <p>Dato de Prueba</p>
                </div>

            </div>
        </div>
    </div>
    <!-- FIN DE LA CABECERA GRIS -->
    <section class="forms">
        <div class="container-fluid">

            <!-- TARJETA PADRE -->
            <div class="card">
                <!-- Titulo Tarjeta Padre-->

                <!-- Tabs de Navegacion -->
                <ul class="nav nav-tabs flex-wrap" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" href="#Informacion" role="tab" data-toggle="tab">Info.Basica</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#Formacion" role="tab" data-toggle="tab">Formacion</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#RelacionLaboral" role="tab" data-toggle="tab">Relacion Laboral</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#AsignacionDeducciones" role="tab" data-toggle="tab">Asignacion/Deducciones</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#AuscenciaHorasT" role="tab" data-toggle="tab">Auscencias/Horas
                            Trabajadas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#Vacaciones" role="tab" data-toggle="tab">Vacaciones</a>
                    </li>

                </ul>


                <!--Cuerpo de Tarjeta Padre-->
                <div class="card-body">

                    <!-- Contenido Total de La navegacion-->
                    <div class="tab-content">

                        <!-- Primera Pestaña -->
                        <div role="tabpanel" class="tab-pane active" id="Informacion">

                            <!-- Tarjeta hija -->
                            <div class="card">
                                <!-- Cabecera de Tarjeta Hija -->
                                <div class="card-header">
                                    <h1> Primera Pestaña</h1>
                                </div>
                                <!-- Cuerpo de Tarjeta Hija-->
                                <div class="card-body">

                                    <div class="row">
                                        <!-- Division del cuerpo en 3 columns -->
                                        <div class="col-md-4">

                                            <div class="form-group">
                                                <label>Nro Legajo</label>
                                                <input type="input" placeholder="Nro " class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label>Nombres</label>
                                                <input type="input" placeholder="Nombres" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label>Nacionalidad</label>
                                                <input type="input" placeholder="Nacionalidad" class="form-control">
                                            </div>


                                        </div>
                                        <div class="col-md-4">

                                            <div class="form-group row">


                                                <div class="form-group">
                                                    <label>DNI</label>
                                                    <input type="input" placeholder="0_.__.___" class="form-control">
                                                </div>

                                                <div class="form-group">
                                                    <label>Apellidos</label>
                                                    <input type="input" placeholder="Apellidos" class="form-control">
                                                </div>

                                                <label class="col-sm-2 form-control-label">Sexo
                                                    <small class="text-primary"></small>
                                                </label>
                                                <div class="col-sm-10">

                                                    <div>
                                                        <input id="masculino" type="radio" checked="" value="option1"
                                                               name="optionsRadios">
                                                        <label for="optionsRadios1">masculino</label>
                                                    </div>
                                                    <div>
                                                        <input id="femenino" type="radio" value="option2"
                                                               name="optionsRadios">
                                                        <label for="optionsRadios2">femenino</label>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label>subir un archivo</label>
                                                    <input id="file-1" type="file" class="file" multiple=true
                                                           data-preview-file-type="any">
                                                </div>
                                                <div class="form-group">
                                                    <button class="btn btn-primary">Submit</button>
                                                    <button class="btn btn-default" type="reset">Reset</button>
                                                </div>

                                            </div>


                                        </div>

                                        <div class="col-md-4">


                                            <div class="form-group">
                                                <label>CUIL</label>
                                                <input type="input" placeholder="27-0_____-_" class="form-control">
                                            </div>

                                            <div class="form-group">
                                                <label>Estado Civil</label>
                                                <input type="input" placeholder="Estado Civil " class="form-control">
                                            </div>


                                            <div class="form-group">
                                                <label>Fecha de Nacimiento</label>
                                                <div class='input-group date' id='datetimepicker1'>
                                                    <input type='text' class="form-control"/>
                                                    <span class="input-group-addon">
                                                  <span class="fa fa-calendar"></span>
                                                </span>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- FIN de Tarjeta Hija -->

                            <!-- Nueva Tarjeta hija -->
                            <div class="card">
                                <div class="card-header">
                                    <h1>Lugar de Residencia</h1>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <!-- Division del cuerpo en 3 columns -->
                                        <div class="col-md-4">

                                            <div class="form-group">
                                                <label>Provincia</label>
                                                <input type="input" placeholder="Provincia " class="form-control">

                                                <button type="button" class="btn btn-primary">+</button>
                                            </div>
                                            <div class="form-group">
                                                <label>Codigo Postal</label>
                                                <input type="input" placeholder="Codigo " class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label>Numero</label>
                                                <input type="input" placeholder="Numero" class="form-control">
                                            </div>


                                        </div>

                                        <div class="col-md-4">

                                            <div class="form-group row">


                                                <div class="form-group">
                                                    <label>Ciudad</label>
                                                    <input type="input" placeholder="Ciudad" class="form-control">
                                                </div>

                                                <div class="form-group">
                                                    <label>Departamento</label>
                                                    <input type="input" placeholder="Departamento" class="form-control">
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-4">

                                            <div class="form-group">
                                                <label>Calle</label>
                                                <input type="input" placeholder="Calle" class="form-control">
                                            </div>

                                            <div class="form-group">
                                                <label>Piso</label>
                                                <input type="input" placeholder="Piso" class="form-control">
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>


                            <!-- Fin Nueva Tarjeta Hija -->

                            <!-- Nueva Tarjeta hija -->
                            <div class="card">
                                <div class="card-header">
                                    <h1>Informacion de contacto</h1>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <!-- Division del cuerpo en 3 columns -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Telefono</label>
                                                <input type="input" placeholder="Telefono" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label>Email de Trabajo</label>
                                                <input type="Email" placeholder=" " class="form-control">
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Celular</label>
                                                <input type="input" placeholder="Celular" class="form-control">
                                            </div>

                                            <div class="form-group">
                                                <label>Email Personal</label>
                                                <input type="Email" placeholder="" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <!-- Fin Nueva Tarjeta Hija -->


                            <!-- Nueva Tarjeta hija -->
                            <div class="card">
                                <div class="card-header">
                                    <h1>Familiares</h1>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <!-- Division del cuerpo en 3 columns -->
                                        <div class="col-md-12">

                                            <div class="form-group">
                                                <button type="button" class="btn btn-primary">+ agregar familiares
                                                </button>

                                                <table id="tablafamiliares" class="table table-striped table-bordered"
                                                       cellspacing="0" width="100%">
                                                    <thead>
                                                    <tr>
                                                        <th>Tipos</th>
                                                        <th>Nombres</th>
                                                        <th>Apellidos</th>
                                                        <th>Acciones</th>

                                                    </tr>
                                                    </thead>

                                                    <tfoot>
                                                    <tr>
                                                        <th>Tipos</th>
                                                        <th>Nombres</th>
                                                        <th>Apellidos</th>
                                                        <th>Acciones</th>

                                                    </tr>
                                                    </tfoot>


                                                    <tbody>
                                                    <tr>
                                                        <td>Tiger Nixon</td>
                                                        <td>System Architect</td>
                                                        <td>Edinburgh</td>
                                                        <td>61</td>

                                                    </tr>
                                                    <tr>
                                                        <td>Garrett Winters</td>
                                                        <td>Accountant</td>
                                                        <td>Tokyo</td>
                                                        <td>63</td>

                                                    </tr>
                                                    <tr>
                                                        <td>Ashton Cox</td>
                                                        <td>Junior Technical Author</td>
                                                        <td>San Francisco</td>
                                                        <td>66</td>

                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <!-- Nueva Tarjeta hija -->
                            <div class="card">
                                <div class="card-header">
                                    <h1>Cuentas Bancarias</h1>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <!-- Division del cuerpo en 3 columns -->
                                        <div class="col-md-12">

                                            <div class="form-group">
                                                <button type="button" class="btn btn-primary">+ Ingresar Cuentas
                                                    Bancarias
                                                </button>

                                                <table id="tablacuentas" class="table table-striped table-bordered"
                                                       cellspacing="0" width="100%">
                                                    <thead>
                                                    <tr>
                                                        <th>Metodo de pago</th>
                                                        <th>Tipo Cuenta</th>
                                                        <th>Banco</th>
                                                        <th>Numero</th>
                                                        <th>Porcentaje</th>
                                                        <th>Acciones</th>
                                                    </tr>
                                                    </thead>

                                                    <tfoot>
                                                    <tr>
                                                        <th>Metodo de pago</th>
                                                        <th>Tipo Cuenta</th>
                                                        <th>Banco</th>
                                                        <th>Numero</th>
                                                        <th>Porcentaje</th>
                                                        <th>Acciones</th>

                                                    </tr>
                                                    </tfoot>


                                                    <tbody>
                                                    <tr>
                                                        <td>Tiger Nixon</td>
                                                        <td>System Architect</td>
                                                        <td>Edinburgh</td>
                                                        <td>61</td>
                                                        <td>Edinburgh</td>
                                                        <td>61</td>

                                                    </tr>
                                                    <tr>
                                                        <td>Garrett Winters</td>
                                                        <td>Accountant</td>
                                                        <td>Tokyo</td>
                                                        <td>63</td>
                                                        <td>Tokyo</td>
                                                        <td>63</td>

                                                    </tr>
                                                    <tr>
                                                        <td>Ashton Cox</td>
                                                        <td>Junior Technical Author</td>
                                                        <td>San Francisco</td>
                                                        <td>66</td>
                                                        <td>San Francisco</td>
                                                        <td>66</td>

                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <!-- Nueva Tarjeta hija -->
                            <div class="card">
                                <div class="card-header">
                                    <h1>Adjuntos</h1>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <!-- Division del cuerpo en 3 columns -->
                                        <div class="col-md-12">

                                            <div class="form-group">
                                                <button type="button" class="btn btn-primary">+ Ingresar adjuntos
                                                </button>

                                                <table id="tablaadjuntos" class="table table-striped table-bordered"
                                                       cellspacing="0" width="100%">
                                                    <thead>
                                                    <tr>
                                                        <th>Fecha</th>
                                                        <th>Comentarios</th>
                                                        <th>Fichero</th>
                                                        <th>Acciones</th>

                                                    </tr>
                                                    </thead>

                                                    <tfoot>
                                                    <tr>
                                                        <th>Fecha</th>
                                                        <th>Comentarios</th>
                                                        <th>Fichero</th>
                                                        <th>Acciones</th>

                                                    </tr>
                                                    </tfoot>


                                                    <tbody>
                                                    <tr>
                                                        <td>Tiger Nixon</td>
                                                        <td>System Architect</td>
                                                        <td>Edinburgh</td>
                                                        <td>61</td>

                                                    </tr>
                                                    <tr>
                                                        <td>Garrett Winters</td>
                                                        <td>Accountant</td>
                                                        <td>Tokyo</td>
                                                        <td>63</td>

                                                    </tr>
                                                    <tr>
                                                        <td>Ashton Cox</td>
                                                        <td>Junior Technical Author</td>
                                                        <td>San Francisco</td>
                                                        <td>66</td>

                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- FIN DE PRIMERA PESTAÑA -->

                        <div role="tabpanel" class="tab-pane fade" id="Formacion">

                            <div class="row">

                                <!-- Tarjeta hija -->
                                <div class="card">
                                    <!-- Cabecera de Tarjeta Hija -->
                                    <div class="card-header">
                                        <h1> Formacion</h1>
                                    </div>
                                    <!-- Cuerpo de Tarjeta Hija-->
                                    <div class="card-body">

                                        <div class="row">
                                            <!-- Division del cuerpo en 3 columns -->
                                            <div class="col-md-12">

                                                <div class="form-group">
                                                    <label>Nivel maximo de estudios</label>
                                                    <input type="input" placeholder=" " class="form-control">
                                                    <button type="button" class="btn btn-primary">+</button>

                                                </div>

                                                <table id="tablacuentas" class="table table-striped table-bordered"
                                                       cellspacing="0" width="100%">
                                                    <thead>
                                                    <tr>
                                                        <th>Metodo de pago</th>
                                                        <th>Tipo Cuenta</th>
                                                        <th>Banco</th>
                                                        <th>Numero</th>
                                                        <th>Porcentaje</th>
                                                        <th>Acciones</th>
                                                    </tr>
                                                    </thead>

                                                    <tfoot>
                                                    <tr>
                                                        <th>Metodo de pago</th>
                                                        <th>Tipo Cuenta</th>
                                                        <th>Banco</th>
                                                        <th>Numero</th>
                                                        <th>Porcentaje</th>
                                                        <th>Acciones</th>

                                                    </tr>
                                                    </tfoot>


                                                    <tbody>
                                                    <tr>
                                                        <td>Tiger Nixon</td>
                                                        <td>System Architect</td>
                                                        <td>Edinburgh</td>
                                                        <td>61</td>
                                                        <td>Edinburgh</td>
                                                        <td>61</td>

                                                    </tr>
                                                    <tr>
                                                        <td>Garrett Winters</td>
                                                        <td>Accountant</td>
                                                        <td>Tokyo</td>
                                                        <td>63</td>
                                                        <td>Tokyo</td>
                                                        <td>63</td>

                                                    </tr>
                                                    <tr>
                                                        <td>Ashton Cox</td>
                                                        <td>Junior Technical Author</td>
                                                        <td>San Francisco</td>
                                                        <td>66</td>
                                                        <td>San Francisco</td>
                                                        <td>66</td>

                                                    </tr>
                                                    </tbody>
                                                </table>

                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>

                            <!-- FIN de Tarjeta Hija -->
                        </div>


                    </div>
                </div>
                <br>
                <br>
                <br>
                <br>

            </div>


        </div>
    </section>

    @include('layouts.footer')
</div>
@include('layouts.libraries')
</body>
</html>