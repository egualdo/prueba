<!DOCTYPE html>
<html>
@include('layouts.heads')
<body>
<!-- Side Navbar -->
@include('layouts.sidebaradmin')
<div class="page forms-page">
    <!-- navbar-->
    @include('layouts.navbar')
  
        <!-- CABECERA GRIS-->
        <div class="breadcrumb-holder fixed-top">
            <div class="container-fluid">
                <div class="row headercolums">

                    <div class="colum1">
                        <h1 style="font-size: 25px;">Bienvenido {{ session('Username') }}</h1>


                    </div>
                    <div class="colum2">
                        <div class="row" style="font-size: 12px;">
                            <div class="colum21">

                            </div>
                            <div class="colum22">

                            </div>
                        </div>
                        <div class="row" style="font-size: 12px;">
                            <div class="colum23">

                            </div>
                            <div class="colum24">

                            </div>
                        </div>

                    </div>
                    <div class="colum3">
                        <div class="row labelsup" style="margin-left: 10px">


                        </div>

                    </div>
                </div>
            </div>
        </div>
   

    <section class="forms">
        <div class="container-fluid">
            <br>
            <br>
            <!-- Tarjeta -->
            
            <div class="card">
                <div class="card-body">
                    <div id="modedit">
                        
         <div id="ModalEdicion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal text-left">
        <div role="document" class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 id="exampleModalLabel" class="modal-title">Contacto</h5>
              <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
              <p>Perfil del vendedor</p>
             <div class="card">
                     <div class="row">
                         <div class="col-md-6">
              <div class="sidenav-header-inner text-center">
                @if(session()->has('avatar'))
                    <img src="/uploads/{{session()->get('avatar')}}" alt="person" id="AvatarUser"
                         class="img-fluid rounded-circle">
                @else
                    <img src="../img/default-avatar.png" alt="person" id="AvatarUser" class="img-fluid rounded-circle">
                @endif
                <h2 class="h5 text-uppercase">{{ session('Username') }}</h2><span
                        class="text-uppercase">{{ session('NameRol') }}</span>
            </div>
                     </div>
                         
                          <div class="col-md-6">
                                  <h1><strong>Stefany Soteldo</strong> </h1>
                                  <h2> Nro. de telefono:  04145724153</h2>
                                  <h2> Email: stefany@gmail.com</h2>
                         </div>
              </div>        
              </div>
            </div>
            <div class="modal-footer">
              <div v-if="save == true">
                <button v-on:click="guardar" class="btn btn-sucess btn-fill pull-left">Enviar mensaje</button>
                <button type="button" class="btn btn-primary btn-fill pull-rigth" data-dismiss="modal">Cerrar</button>
              </div>

              <div v-else-if="edit == true">
                <button v-on:click="editar" class="btn btn-info btn-fill pull-left">Editar</button>
                <button v-on:click="borrar" class="btn btn-warning btn-fill pull-rigth">Eliminar</button>
                <button type="button" class="btn btn-primary btn-fill pull-rigth" data-dismiss="modal">Cerrar</button>
              </div>
            </div>
          </div>
        </div>
      </div>
                        
                      <div class="row">
                        <!-- === PANEL DE TABLA === -->
                        <div class="col-md-12">
                            <div class="form-group">
                                 <div class="card">
                            <div class="card-header">
                                <div class="row">
                                <div class="col-md-9">
                                  <h2> Stefany Soteldo</h2>
                                </div>
                                <div class="col-md-3">
                                  <button type="button" v-on:click="openmodaltonew" class="btn btn-success">comprar</button>
                                </div>
                              </div>
                              </div>
                          <div class="card-body">
                          
                            <p class="card-text">compra tus procustos con nosotros a los mejores precios!!</p>
                                
                              <div class="row">
                                   <div class="col-sm-6 col-md-3">
                           
                                <a href="#" class="thumbnail">
                                  <img src="/img/best-smartphones.jpg"  style="width:200px" alt="title">
                                </a>
                              </div>
                                  
                                   <div class="col-sm-6 col-md-3">
                           
                                <a href="#" class="thumbnail">
                                  <img src="/img/tablets-810x456.jpg"  style="width:200px" alt="title">
                                </a>
                              </div>
                                  
                                   <div class="col-sm-6 col-md-3">
                           
                                <a href="#" class="thumbnail">
                                  <img src="/img/smartphones-caracteristica.jpg" style="width:200px" alt="title">
                                </a>
                              </div>
                              
                            </div>

                                  </div>
                            </div>
                           </div>
                             <div class="form-group">
                            <div class="card">
                            <div class="card-header">
                                <div class="row">
                                <div class="col-md-9">
                                  <h2> Ely Colmenares</h2>
                                </div>
                                <div class="col-md-3">
                                  <button type="button" v-on:click="openmodaltonew" class="btn btn-success">comprar</button>
                                </div>
                              </div>
                              </div>
                          <div class="card-body">
                          
                            <p class="card-text">Busca lo que necesitas somo tienda fisica en barquisimeto</p>
                                
                              <div class="row">
                                   <div class="col-sm-6 col-md-3">
                           
                                <a href="#" class="thumbnail">
                                  <img src="/img/luke-porter-232839-1-e1497435965697.jpg"  style="width:200px" alt="title">
                                </a>
                              </div>
                                  
                              
                            </div>

                                  </div>
                            </div>
                        </div>     
                        </div>

                    </div>
                    
                    
                    </div>
                    
                    
                </div>
            </div>
            <!-- FIN de Tarjeta Hija -->
        </div>
    </section>
    

    @include('layouts.footer')
</div>
@include('layouts.libraries')

<script src="../js/formjs/welcomeadmin.js"></script>

<style type="text/css">
    a {
        color: whitesmoke;
        text-decoration: none;

    }

    h1 a:hover {
        display: inline-block;
        color: dodgerblue;
    }
</style>


</body>
</html>
