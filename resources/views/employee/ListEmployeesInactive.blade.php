@section('FatherOrganigrama','true')
  @section('ChildOrganigrama','show')
<!DOCTYPE html>
  <html>
  @include('layouts.heads')
  <body>
    <!-- Side Navbar -->
    @include('layouts.sidebaradmin')
    <div class="page forms-page">
      <!-- navbar-->
      @include('layouts.navbar')
      <div id="vuedata">
 <!-- Modal -->
                                              
          <!-- FIN DE LA MODAL -->


      <!-- CABECERA GRIS-->
      <div class="breadcrumb-holder fixed-top">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-2 border">
            </div>
            <div class="col-md-7 border">
              <!-- TITULO DE VENTANA -->
              <h1> Empleados Inactivos</h1>
            </div>
              <div class="col-md-3">
                   <button v-on:click="abrirnuevolegajo" class="btn btn-info btn-fill pull-left" >Nuevo Elemento</button>
              </div>
         
          </div>
        </div>
      </div>
      <!-- FIN DE LA CABECERA GRIS -->
 </div>

      <section class="forms">
        <div class="container-fluid">

          <!-- Tarjeta -->
          <div class="card" >
            <!-- Cabecera de Tarjeta  -->
            
           <!-- Cuerpo de Tarjeta -->
           <div class="card-body">  

            <div class="row">
              <!-- === PANEL DE TABLA === --> 
              <!-- ====== DATATABLE ======= -->
             <table id="TablaEmployee" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                  <tr>
                      <th>Numero</th>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>Cuil</th>
                    <th>Ingreso</th>
                    <th>Cat.Salario</th>
                    <th>CCT</th>
                    <th>Jornada</th>
                  </tr>
                </thead>
              </table>
              <!-- ====== FIN DATATABLE======= -->
            </div>
          </div>
        </div>
        <!-- FIN de Tarjeta Hija -->
      </div>
    </section>



    @include('layouts.footer')
  </div>
  @include('layouts.libraries')
 <script src="../js/employeejs/listemployeeinactive.js"></script>
</body>
</html>