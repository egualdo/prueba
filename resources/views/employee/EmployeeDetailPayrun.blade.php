<div id="vue-Payrun">
@include('modal.ModalPayrunDetail')
<!-- Tarjeta hija -->
    <div class="card">
        <!-- Cabecera de Tarjeta Hija -->
        <div class="card-header">
            <h2> Córrida de Nómina</h2>
        </div>
        <!-- Cuerpo de Tarjeta Hija-->
        <div class="card-body">

            <div class="row">
                <!-- Division del cuerpo en 3 columns -->
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Tipo de Periodo</label>
                        {{--  <select v-model="fd.Typeperiod"  v-on:change="TypePeriodOnChange" class="form-control">
                           <option value="1">Mensual</option>
                           <option value="2">Quincenal</option>

                         </select> --}}
                        <v-select v-model="fd.Typeperiod" :value="fd.Typeperiod" :options="fd.Typeperiodlist"
                                  :on-change="TypePeriodOnChange"></v-select>
                    </div>
                    <div class="form-group">
                        <label>Periodo</label>
                        {{--    <select v-model="fd.Period" class="form-control">
                            <option value="">Seleccione ...</option>
                               <option v-for="Perio in fd.Periodlist" v-bind:selected="fd.Period" v-bind:value="Perio.idn">
                                      @{{ Perio.title }}
                                    </option>
                              </select> --}}
                        <v-select v-model="fd.Period" :value="fd.Period" :options="fd.Periodlist" :on-change="loadMonth"></v-select>
                    </div>
                 


                </div>
                <div class="col-md-3">

                       <div class="form-group">
                        <label>Mes</label>
                        <select v-model="fd.Month" class="form-control">
                            <option value="">Seleccione ...</option>
                            <option value="Enero">Enero</option>
                            <option value="Febrero">Febrero</option>
                            <option value="Marzo">Marzo</option>
                            <option value="Abril">Abril</option>
                            <option value="Mayo">Mayo</option>
                            <option value="Junio">Junio</option>
                            <option value="Julio">Julio</option>
                            <option value="Agosto">Agosto</option>
                            <option value="Septiembre">Septiembre</option>
                            <option value="Octubre">Octubre</option>
                            <option value="Noviembre">Noviembre</option>
                            <option value="Diciembre">Diciembre</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Fecha de Pago de contribuciones</label>
                        <input id="DPickerPayContributiondate" v-model="fd.PayContributiondate" type="input"
                               class="form-control">

                    </div>
                   


                </div>
                <div class="col-md-3">
                <div class="form-group">
                        <label>Fecha de Pago</label>
                        <input id="DPickerPaymentdate" v-model="fd.PaymentRundate" type="input" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Método de Pago</label>
                        {{--   <select v-model="fd.Paymentmethod" class="form-control">
                            <option value="">Seleccione ...</option>
                               <option v-for="paym in fd.Paymentmethodlist" v-bind:value="paym.idn">
                                      @{{ paym.name }}
                                    </option>
                              </select> --}}
                        <v-select v-model="fd.Paymentmethod" :options="fd.Paymentmethodlist"></v-select>
                    </div>
                   
                   

                </div>
                <div class="col-md-3">
                <div class="form-group">
                        <label>Tipo de Corrida</label>
                        {{--  <select v-model="fd.Typerun" class="form-control">
                           <option value="">Seleccione ...</option>
                              <option v-for="trun in fd.Typerunlist" v-bind:value="trun.idn">
                                     @{{ trun.name }}
                                   </option>
                             </select> --}}
                        <v-select v-model="fd.Typerun" :options="fd.Typerunlist"></v-select>
                    </div>
                <div class="form-group">
                        <br>
                        <button v-on:click="Run" class="btn btn-primary btn-fill pull-left">Correr</button>
                    </div>
                </div>
            </div>
        </div>
        <!--<div class="card-footer">

        </div>-->
    </div>
    <div class="row subtitle">
        <div v-show="ff.openPayruns == false & ff.closedPayruns==true"  class="col-md-8" style="display:none">
            <p>Mostrando actualmente corridas cerradas que solo pueden ser visualizadas</p>
        </div>
        <div v-show="ff.openPayruns == true & ff.closedPayruns==false"  class="col-md-8" style="display:none">
            <p>Mostrando actualmente corridas abiertas que pueden ser actualizadas</p>
        </div>
        <div class="col-md-4">

            <button type="button" v-on:click="showOpen" class="btn btn-primary btn-sm">Corridas Abiertas</button>
            <button type="button" v-on:click="showClosed" class="btn btn-danger btn-sm" style="border-radius: 0">Corridas Cerradas</button>
        </div>
    </div>
</div>

<div id="ListPayrunOpen">
    <!-- Tarjeta hija -->
    <div class="card">
        <!-- Cabecera de Tarjeta Hija -->
        <div class="card-header">
            <h2> Córrida Abiertas</h2>
        </div>
        <!-- Cuerpo de Tarjeta Hija-->
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <table id="TablePayrunOpenByEmployee" class="table table-striped table-bordered" cellspacing="0"
                               width="100%">
                            <thead>
                            <tr>
                                <th>Idn</th>
                                <th>Tipo</th>
                                <th>Nómina</th>
                                <th>Periodo</th>
                                <th>Pagos</th>
                                <th>Deducciones</th>
                                <th>No Remun</th>
                                <th>Pago Neto</th>

                            </tr>
                            </thead>

                        </table>
                    </div>
                </div>
            </div>

        </div>


    </div>
</div>
<div id="ListPayrunClosed">
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-md-9">
                    <h2>Datos Históricos</h2>
                </div>
                <div class="col-md-3">

                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <!-- Division del cuerpo en 3 columns -->
                <div class="col-md-12">

                    <div class="form-group">


                        <table id="TablePayrunClosedByEmployee" class="table table-striped table-bordered"
                               cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>Idn</th>
                                <th>Tipo</th>
                                <th>Nómina</th>
                                <th>Periodo</th>
                                <th>Pagos</th>
                                <th>Deducciones</th>
                                <th>No Remun</th>
                                <th>Pago Neto</th>

                            </tr>
                            </thead>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>