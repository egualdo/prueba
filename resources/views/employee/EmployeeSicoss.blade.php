@section('FatherLiquidacion','true')
  @section('ChildLiquidacion','show')
<!DOCTYPE html>
<html>
@include('layouts.heads')
<body>
<!-- Side Navbar -->
@include('layouts.sidebaradmin')
<div class="page forms-page">a
    <!-- navbar-->
    <div id="vuedata">
    @include('layouts.navbar')
    <!-- CABECERA GRIS-->
        <div class="breadcrumb-holder fixed-top">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-2 border">
                    </div>
                    <div class="col-md-5 border">
                        <h1>Detalle del SICOSS</h1>
                    </div>
                    <div class="col-md-2">
                        <p>Dato de Prueba</p>
                    </div>


                    <div class="col-md-3">
                        <button v-on:click="editar" class="btn btn-info btn-fill pull-left">Editar</button>
                        
                       
                    </div>

                </div>
            </div>
        </div>
        
          <!-- MODAL -->
                                                  <div id="ModalEdicion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal text-left">
                                                    <div role="document" class="modal-dialog modal-lg">
                                                      <div class="modal-content">
                                                        <div class="modal-header">
                                                          <h5 id="exampleModalLabel" class="modal-title">Editar Elemento</h5>
                                                          <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <ul class="nav nav-tabs flex-wrap" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" href="#Columna1" role="tab" data-toggle="tab">Columnas 1-20</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#Columna2" role="tab" data-toggle="tab">Columnas 21-40</a>
                        </li>
                        
                        <li class="nav-item">
                            <a class="nav-link" href="#Columna3" role="tab" data-toggle="tab">Columnas 41-57</a>
                        </li>
                    </ul>
                  </br>
                  </br>
                  

                                                         
                                                                   <section class="forms">
            <div class="container-fluid">

  
                    <!-- Titulo Tarjeta Padre-->

                    <!-- Tabs de Navegacion -->
                  
                              <!-- Contenido Total de La navegacion-->
                        <div class="tab-content">
                            <!-- Primera Pestaña -->
                            <div role="tabpanel" class="tab-pane active" id="Columna1">
                                <div class="row">
                                            <!-- Division del cuerpo en 4 columns -->
                                            <div class="col-md-3"><!-- Columna1  -->
                                                <div class="form-group">
                                                    <label>1.Cuil</label>
                                                    <input type="input" placeholder="Cuil" v-model="fd.Cuil"
                                                           class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>2.Nombre</label>
                                                    <input type="input" placeholder="Nombre" v-model="fd.Names"
                                                           class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>3.Conyugue</label>
                                                    <input type="input" placeholder="Conyugue"
                                                           v-model="fd.Spouse" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>4.Hijos</label>
                                                    <input type="input" placeholder="Hijos"
                                                           v-model="fd.Sons" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>5.Sit. Revista</label>
                                                    <input type="input" placeholder="Sit. Revista"
                                                           v-model="fd.Sitrevision" class="form-control">
                                                </div>
                                            </div><!-- Fin Columna1  -->
                                        
                                            <div class="col-md-3"><!-- Columna2  -->
                                               <div class="form-group">
                                                    <label>6.Condicion</label>
                                                    <input type="input" placeholder="Condicion"
                                                           v-model="fd.Condition" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>7.Actividad</label>
                                                    <input type="input" placeholder="Actividad"
                                                           v-model="fd.Activity" class="form-control">
                                                </div>

                                                 <div class="form-group">
                                                  <label>8.Zona</label>
                                                  <input type="input" v-model="fd.Zone" placeholder="Zona" class="form-control">
                                                </div>
                                                    <div class="form-group">
                                                    <label>9. % Ap. Ad. S.S.</label>
                                                    <input type="input" placeholder="% Ap. Ad. S.S."
                                                           v-model="fd.Appss" class="form-control">
                                                </div>
                                                 <div class="form-group">
                                                    <label>10.Mod. Contrat.</label>
                                                    <input type="input" placeholder="Mod. de Contrat."
                                                           v-model="fd.Modcontract" class="form-control">
                                                </div>
                                                
                                            </div><!-- fin Columna2  -->

                                            <div class="col-md-3"> <!-- Columna3  -->
                                                <div class="form-group">
                                                  <label>11.Obra Social</label>
                                                  <input type="input" v-model="fd.Obsocial" placeholder="Obra Social" class="form-control">
                                                </div>

                                                   <div class="form-group">
                                                     <label>12.Aderentes</label>
                                                     <input type="input" v-model="fd.Adherents" placeholder="Aderentes" class="form-control">
                                                   </div>
                                                
                                                <div class="form-group">
                                                    <label>13.Total Bruto</label>
                                                    <input type="input" v-model="fd.Totalbrute" placeholder="Total Bruto"
                                                           class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label> 14.Rem.Imp.1</label>
                                                    <input type="input" placeholder=" Rem.Imp.1"
                                                           v-model="fd.Remimp1" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>15.Asig.Fam.</label>
                                                    <input type="input" v-model="fd.Asigfam"
                                                           placeholder="Asig.Fam." class="form-control">
                                                </div>
                                            </div><!-- fin Columna3  -->
                                            
                                    <div class="col-md-3">

                                            
                                                 <div class="form-group">
                                                  <label>16.Aporte Vol.</label>
                                                  <input type="input" v-model="fd.Aportevol" placeholder="  Aporte Vol." class="form-control">
                                                </div>

                                                   <div class="form-group">
                                                     <label>17.Adic. O.S.</label>
                                                     <input type="input" v-model="fd.Adicos" placeholder="  Adic. O.S." class="form-control">
                                                   </div>
                                                
                                                <div class="form-group">
                                                    <label>18.Exced. S.S.</label>
                                                    <input type="input" v-model="fd.Excedos" placeholder="Exced. S.S."
                                                           class="form-control">
                                                </div>
                                        
                                         <div class="form-group">
                                                    <label>19.Adic .O.S. 2</label>
                                                    <input type="input" placeholder=" Adic .O.S.  2"
                                                           v-model="fd.Adicos2" class="form-control">
                                                </div>
                                        
                                         <div class="form-group">
                                                    <label>20.Provincia</label>
                                                    <input type="input" placeholder="Provincia"
                                                           v-model="fd.Province" class="form-control">
                                                </div>

                                            </div><!-- fin Columna4  -->

                                        </div>
                             
                            </div>
                           
                    
                    <div role="tabpanel" class="tab-pane" id="Columna2">
                          <div class="row">

                                            <!-- Division del cuerpo en 4 columns -->
                                            <div class="col-md-3">
                                              <div class="form-group">
                                                  <label>21.Rem.imp.2</label>
                                                  <input type="input" v-model="fd.Remimp2" placeholder="Rem.imp.2" class="form-control">
                                                </div>

                                                   <div class="form-group">
                                                     <label>22.Rem.imp.3</label>
                                                     <input type="input" v-model="fd.Remimp3" placeholder="Rem.imp.3" class="form-control">
                                                   </div>
                                                
                                                <div class="form-group">
                                                    <label>23.Rem.imp.4</label>
                                                    <input type="input" v-model="fd.Remimp4" placeholder="Rem.imp.4"
                                                           class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>24.Cod. Siniestro</label>
                                                    <input type="input" placeholder="Cod.de Siniestro"
                                                           v-model="fd.Codsinester" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>25.Marca reduc.</label>
                                                    <input type="input" v-model="fd.Markreduc"
                                                           placeholder="Marca reduc." class="form-control">
                                                </div>
                                            </div>
                                              <div class="col-md-3">
                                              <div class="form-group">
                                                  <label>26.Recomp. LRT</label>
                                                  <input type="input" v-model="fd.Recomplrt" placeholder="Recomp. LRT" class="form-control">
                                                </div>

                                                   <div class="form-group">
                                                     <label>27.Tipo Empresa</label>
                                                     <input type="input" v-model="fd.Typecompany" placeholder="Tipo Empresa" class="form-control">
                                                   </div>
                                                
                                                <div class="form-group">
                                                    <label>28.Adic. O.S. 3</label>
                                                    <input type="input" v-model="fd.Adicos3" placeholder="Adic. O.S. 3"
                                                           class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>29.Reg.Previ.</label>
                                                    <input type="input" placeholder="Reg. previsional"
                                                           v-model="fd.Regprevisional" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>30.Cod. Revista 1</label>
                                                    <input type="input" v-model="fd.Codrevision1"
                                                           placeholder="  Cod. Revista 1" class="form-control">
                                                </div>
                                            </div>
                                              <div class="col-md-3">
                                              <div class="form-group">
                                                  <label>31.Dia Revista 1</label>
                                                  <input type="input" v-model="fd.Dayrevision1" placeholder="Dia Revista 1" class="form-control">
                                                </div>

                                                   <div class="form-group">
                                                     <label>32.Cod. Revista 2</label>
                                                     <input type="input" v-model="fd.Codrevision2" placeholder="Cod. Revista 2" class="form-control">
                                                   </div>
                                                
                                                
                                                     <div class="form-group">
                                                  <label>33.Dia Revista 2</label>
                                                  <input type="input" v-model="fd.Dayrevision2" placeholder="Dia Revista 2" class="form-control">
                                               
                                                </div>
                                                <div class="form-group">
                                                     <label>34.Cod. Revista 3</label>
                                                     <input type="input" v-model="fd.Codrevision3" placeholder="Cod. Revista 3" class="form-control">
                                                   </div>
                                                
                                                
                                                     <div class="form-group">
                                                  <label>35.Dia Revista 3</label>
                                                  <input type="input" v-model="fd.Dayrevision3" placeholder="Dia Revista 3" class="form-control">
                                               
                                                </div>
                                            </div>
                                              <div class="col-md-3">
                                              <div class="form-group">
                                                  <label>36.Sueldos y Adic.</label>
                                                  <input type="input" v-model="fd.Salaryadditionals" placeholder="Sueldos y Adicionales" class="form-control">
                                                </div>

                                                   <div class="form-group">
                                                     <label>37.SAC</label>
                                                     <input type="input" v-model="fd.Sac" placeholder="SAC" class="form-control">
                                                   </div>
                                                
                                                <div class="form-group">
                                                    <label>38.Hrs Ext Monto</label>
                                                    <input type="input" v-model="fd.Extrahoursamount" placeholder="Horas extras monto"
                                                           class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>39.Zona desfav.</label>
                                                    <input type="input" placeholder="Zona desfav."
                                                           v-model="fd.Desfavzone" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>40.Vacaciones</label>
                                                    <input type="input" v-model="fd.Vacations"
                                                           placeholder="Vacaciones" class="form-control">
                                                </div>
                                            </div>
                                            
                                            </div>
                                        </div>
                      
                    
                     <div role="tabpanel" class="tab-pane" id="Columna3">
                        <div class="row">
                                            <!-- Division del cuerpo en 4 columns -->
                                            <div class="col-md-3">
                                              <div class="form-group">
                                                  <label>41.Dias Trab.</label>
                                                  <input type="input" v-model="fd.Daysworked" placeholder="Dias Trabajados" class="form-control">
                                                </div>

                                                   <div class="form-group">
                                                     <label>42.Rem.imp.5</label>
                                                     <input type="input" v-model="fd.Remimp5" placeholder="Rem.imp.5" class="form-control">
                                                   </div>
                                                
                                                <div class="form-group">
                                                    <label>43.Acuerdo S.S.</label>
                                                    <input type="input" v-model="fd.Agreess" placeholder="Acuerdo S.S."
                                                           class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>44.Rem. imp. 6</label>
                                                    <input type="input" placeholder="Rem. imp. 6"
                                                           v-model="fd.Remimp6" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>45.Tipo operacion</label>
                                                    <input type="input" v-model="fd.Operationtype"
                                                           placeholder="Tipo operacion" class="form-control">
                                                </div>
                                            </div>
                                              <div class="col-md-3">
                                              <div class="form-group">
                                                  <label>46.Adicionales</label>
                                                  <input type="input" v-model="fd.Adics" placeholder="Adicionales" class="form-control">
                                                </div>

                                                   <div class="form-group">
                                                     <label>47.Premios</label>
                                                     <input type="input" v-model="fd.Awards" placeholder="Premios" class="form-control">
                                                   </div>
                                                
                                                <div class="form-group">
                                                    <label>48.Rem.imp.8</label>
                                                    <input type="input" v-model="fd.Remimp8" placeholder="Rem.imp.8"
                                                           class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>49.Rem.imp.7</label>
                                                    <input type="input" placeholder="Rem.imp.7"
                                                           v-model="fd.Remimp7" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>50.Horas extras</label>
                                                    <input type="input" v-model="fd.Extrahours"
                                                           placeholder="Horas extras" class="form-control">
                                                </div>
                                            </div>
                                              <div class="col-md-3">
                                              <div class="form-group">
                                                  <label>51.Tot.no remun.</label>
                                                  <input type="input" v-model="fd.Totnoremun" placeholder=" Tot.no remun." class="form-control">
                                                </div>

                                                   <div class="form-group">
                                                     <label>52.Maternidad</label>
                                                     <input type="input" v-model="fd.Maternity" placeholder="Maternidad" class="form-control">
                                                   </div>
                                                
                                                <div class="form-group">
                                                    <label>53.Rect.Remun.</label>
                                                    <input type="input" v-model="fd.Rectremuns" placeholder=" Rect.Remun."
                                                           class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>54.Rem.imp.9</label>
                                                    <input type="input" placeholder="Rem.imp.9"
                                                           v-model="fd.Remimp9" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>55.Tarea dif.%</label>
                                                    <input type="input" v-model="fd.Homeworkdif"
                                                           placeholder="Tarea dif.%" class="form-control">
                                                </div>
                                            </div>
                                              <div class="col-md-3">
                                              <div class="form-group">
                                                  <label>56.Horas Trab.</label>
                                                  <input type="input" v-model="fd.Hoursworked" placeholder="  Horas Trabajadas" class="form-control">
                                                </div>

                                                   <div class="form-group">
                                                     <label>57.Seguro Oblig.</label>
                                                     <input type="input" v-model="fd.Segobli" placeholder="Seguro Oblig." class="form-control">
                                                   </div>
                                                
                                               
                                            </div>
                                            
                                            </div>
                                        </div>
                        <br>
                    </div>
            <br>

        </section>
                                                          
                                                         
                                                        </div>
                                                        <div class="modal-footer">
                                                          <div v-if="ff.save == true">
                                                             
                                                              <button type="button" class="btn btn-primary btn-fill pull-rigth" data-dismiss="modal">Cerrar</button>
                                                          </div>

                                                           <div v-else-if="ff.edit == true">
                                                              <button v-on:click="editar" class="btn btn-info btn-fill pull-left">Editar</button>
                                                             
                                                              <button type="button" class="btn btn-primary btn-fill pull-rigth" data-dismiss="modal">Cerrar</button>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </div>
                                                 </div>
                                                 <!-- FIN MODAL -->
        <!-- FIN DE LA CABECERA GRIS -->

        <br>
        <br>
        <br>
           </div>
      <section class="forms">
        <div class="container-fluid">

          <!-- Tarjeta -->
          <div class="card" >
            <!-- Cabecera de Tarjeta  -->
           
           <!-- Cuerpo de Tarjeta -->
           <div class="card-body">  

            <div class="row">
              <!-- === PANEL DE TABLA === --> 
              <!-- ====== DATATABLE ======= -->
             <table id="TablaMaster" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>Idn</th>
                    <th>Empleado</th>
                      <th>Tipo Corrida</th>
                      <th>Periodo</th>
                      <th>Activo</th>
                  </tr>
                </thead>
              </table>
              <!-- ====== FIN DATATABLE======= -->
            </div>
          </div>
        </div>
        <!-- FIN de Tarjeta Hija -->
      </div>
    </section>

        @include('layouts.footer')
 
</div>
@include('layouts.libraries')

<script src="../js/generic/generic.js" type="text/javascript"></script>
<script src="../js/employeejs/employeesicoss.js"></script>
</body>
</html>
