@section('FatherMaster','true')
@section('ChildMaster','show')
<!DOCTYPE html>
  <html>
  @include('layouts.heads')
  <body>
    <!-- Side Navbar -->
    @include('layouts.sidebaradmin')
    <div class="page forms-page">
      <!-- navbar-->
      @include('layouts.navbar')
      <div id="vuedata">

<!-- Modal -->
      
       <!-- MODAL -->
       <div id="ModalPositionjob" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal text-left">
        <div role="document" class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 id="exampleModalLabel" class="modal-title">Editar Elemento</h5>
              <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
              <p>Indica los datos solicitados</p>
             
             <div class="row">
              <div class="col-md-6">
                  
                   <div class="form-group">     
                     <label>Fecha Desde</label>
                         <br>
                <input v-model="fd.Startdate" type="date" id="fechainicio" class="form-control" name="fecha">
                </div>
                 
                  <div class="form-group">       
                <label>Centro de Costo</label>
                                  <select v-model="fd.CostCenter" class="form-control">
                                <option value="">Seleccione</option>
                               <option v-for="OneCostcenter in fd.CostCenteroptions" v-bind:value="OneCostcenter.idn">
                                @{{ OneCostcenter.name}}
                                 </option>
                                 </select>
              </div> 
                 
                    <div class="form-group">       
                 <label>Categoria Salarial</label>
                                  <select v-model="fd.CategorySalary" class="form-control">
                                <option value="">Seleccione</option>
                               <option v-for="OneCategory in fd.CategorySalaryoptions" v-bind:value="OneCategory.idn">
                                @{{ OneCategory.name}}
                                 </option>
                                 </select>
                </div>
                 </div>
              <div class="col-md-6">
               
             
                  
             
          <div class="form-group">     
                     <label>Fecha Hasta</label>
                         <br>
                <input v-model="fd.Finishdate" class="form-control" type="date" id="fechafin" name="fecha">
                </div>
                 
                   <div class="form-group">       
                <label>Puesto de Trabajo</label>
                                  <select v-model="fd.PositionJob" class="form-control">
                                <option value="">Seleccione</option>
                               <option v-for="OnePositionjob in fd.PositionJoboptions" v-bind:value="OnePositionjob.idn">
                                @{{ OnePositionjob.name}}
                                 </option>
                                 </select>
              </div>

            </div>
                 
            <div class="modal-footer">
              <div v-if="ff.save == true">
                <button v-on:click="guardarpositionjob" class="btn btn-sucess btn-fill pull-left">Guardar</button>
                <button type="button" class="btn btn-primary btn-fill pull-rigth" data-dismiss="modal">Cerrar</button>
              </div>

              <div v-else-if="ff.edit == true">
                <button v-on:click="editarpositionjob" class="btn btn-info btn-fill pull-left">Editar</button>
                <button v-on:click="borrarpositionjob" class="btn btn-warning btn-fill pull-rigth">Eliminar</button>
                <button type="button" class="btn btn-primary btn-fill pull-rigth" data-dismiss="modal">Cerrar</button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- FIN MODAL -->
    </div>
            </div>
       



  <div class="breadcrumb-holder fixed-top">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-2 border">
            </div>
            <div class="col-md-7 border">
              <!-- TITULO DE VENTANA -->
              <h1>  Puestos de Trabajo</h1>
            </div>
            <div class="col-md-3">
              <!--<p>Dato de Prueba</p>-->
               <button v-on:click="openmodaltonew" class="btn btn-info btn-fill pull-left">Nuevo Elemento</button>
            </div>

          </div>
        </div>
      </div>
              </div>
      <!-- FIN DE LA CABECERA GRIS -->


      <section class="forms">
        <div class="container-fluid">

          <!-- Tarjeta -->
          <div class="card" >
            <!-- Cabecera de Tarjeta  -->
           
           <!-- Cuerpo de Tarjeta -->
           <div class="card-body">  

            <div class="row">
              <!-- === PANEL DE TABLA === --> 
              <!-- ====== DATATABLE ======= -->
             <table id="TablePositionjob" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>Idn</th>
                    <th>Puesto Trabajo</th>
                      <th>Salario</th>
                      <th>Legajo</th>
                      <th>Centro de Costo</th>
                      <th>Fecha Desde</th>
                      <th>Fecha Hasta</th>
                      <th>Activo</th>
                  </tr>
                </thead>
              </table>
              <!-- ====== FIN DATATABLE======= -->
            </div>
          </div>
        </div>
        <!-- FIN de Tarjeta Hija -->
      </div>
    </section>
                                 


    @include('layouts.footer')
  </div>
  @include('layouts.libraries')
 <script src="../js/employeejs/employeepositionjob.js"></script>
</body>
</html>