   <!-- Tarjeta hija -->
   <div id="vue-WorkRelation">
    <div class="card" >
      <!-- Relacion laboral -->
      <div class="card-header">
       <div class="row">
        <div class="col-md-9">
          <h2> Relacion Laboral</h2>
        </div>
        <div class="col-md-3">
          <button type="button" v-on:click="updateWorkRelation" class="btn btn-success">Actualizar Relación</button>
        </div>
      </div>
    </div>
    <!-- Cuerpo de Tarjeta Hija-->
    <div class="card-body">  

      <div class="row">
        <!-- Division del cuerpo en 3 columns -->
        <div class="col-md-3">

          <div class="form-group">
            <label>Centro de Costo</label>
           {{--  <select v-model="fd.Costcenter" class="form-control">
              <option v-for="Cost in fd.Costcenterlist" v-bind:value="Cost.idn">
                @{{ Cost.name }}
              </option>                 
            </select> --}}
             <v-select v-model="fd.Costcenter" :options="fd.Costcenterlist"></v-select>
          </div>
          <div class="form-group">       
            <label>Puesto</label>
           {{--  <select v-model="fd.Positionjob"  class="form-control">
              <option v-for="Position in fd.Positionjoblist" v-bind:value="Position.idn">
                @{{ Position.name }}
              </option>                 
            </select> --}}
             <v-select v-model="fd.Positionjob" :options="fd.Positionjoblist"></v-select>
          </div>           



        </div>
        <div class="col-md-3">
         <div class="form-group">       
          <label>Categoria</label>
         {{--  <select v-model="fd.Categorysalary" class="form-control">
            <option v-for="Category in fd.Categorysalarylist" v-bind:value="Category.idn">
              @{{ Category.name }}
            </option>                 
          </select> --}}
           <v-select v-model="fd.Categorysalary" :options="fd.Categorysalarylist"></v-select>
        </div>

        <div class="form-group">       
          <label>Calendario Laboral</label>
         {{--  <select v-model="fd.Workcalendar" class="form-control">
            <option v-for="Wor in fd.Workcalendarlist" v-bind:value="Wor.idn">
              @{{ Wor.name }}
            </option>                 
          </select> --}}
           <v-select v-model="fd.Workcalendar" :options="fd.Workcalendarlist"></v-select>
        </div>

      </div>

      <div class="col-md-3">


        <div class="form-group">       
          <label>Departamento</label>
          {{-- <select v-model="fd.Department" class="form-control">
            <option v-for="Dep in fd.Departmentlist" v-bind:value="Dep.idn">
              @{{ Dep.name }}
            </option>                 
          </select> --}}
           <v-select v-model="fd.Department" :options="fd.Departmentlist"></v-select>
        </div>

        <div class="form-group">
          <label>Convenio Colectivo</label>
        {{--   <select v-model="fd.Cct" class="form-control">
            <option v-for="ccts in fd.Cctlist" v-bind:value="ccts.idn">
              @{{ ccts.name }}
            </option>                 
          </select> --}}
           <v-select v-model="fd.Cct" :options="fd.Cctlist"></v-select>
        </div>





      </div>
      <div class="col-md-3">

        <div class="form-group">       
          <label>Salario Según Categoria</label>
          <input type="input" v-model="fd.CategorySalaryAmount" placeholder="" class="form-control" disabled>
        </div>

        <div class="form-group">       
          <label>Salario Básico</label>
          <input type="input" v-model="fd.Basicsalary" placeholder="" class="form-control" >
        </div>



      </div>
    </div>
  </div>
</div>
<!-- FIN de Tarjeta Hija -->
</div>


<div id="vue-CompanyActivity">
  <!-- Nueva Tarjeta hija -->
  <div class="card" >
   <div class="card-header">
     <div class="row">
      <div class="col-md-9">
        <h2> Actividad en la Empresa</h2>
      </div>
      <div class="col-md-3">
        <button type="button" v-on:click="updateWorkRelation" class="btn btn-success">Actualizar Actividad</button>
      </div>
    </div>
  </div>
  <div class="card-body">
    <div class="row">
      <!-- Division del cuerpo en 3 columns -->
      <div class="col-md-3">

        <div class="form-group">
          <label>Situación</label>
          <select v-model="fd.Situation" class="form-control">
            <option value="1">Activo</option>   
            <option value="2">Inactivo</option>                
          </select>
        </div>
        <div class="form-group">       
          <label>Condición</label>
        {{--   <select v-model="fd.Condition" class="form-control">
            <option v-for="cond in fd.Conditionlist" v-bind:value="cond.idn">
              @{{ cond.name }}
            </option>                 
          </select> --}}
           <v-select v-model="fd.Employeecondition" :options="fd.Employeeconditionlist"></v-select>
        </div>



      </div>
      <div class="col-md-3">
       <div class="form-group">       
        <label>Fecha de Ingreso</label>
        <input placeholder="Fecha de Ingreso" class="form-control" type="text" id="DPDateentry" v-model="fd.Dateentry">
      </div>

      <div class="form-group">       
        <label>Fecha Ingreso Reconocida</label>
        <input placeholder="Fecha de Ingreso Reconocida" class="form-control" type="text" id="DPDateentryrecognized" v-model="fd.Dateentryrecognized">
      </div>


    </div>

    <div class="col-md-3">


      <div class="form-group">       
        <label>Fecha de Egreso</label>
        <input id="DPDateexit" v-model="fd.Dateexit" type="input" placeholder="Fecha de Egreso" class="form-control">
      </div>

      <div class="form-group">
        <label>Motivo de Egreso</label>
      {{--   <select v-model="fd.Exitreason" class="form-control">
          <option v-for="ext in fd.Exitreasonlist" v-bind:value="ext.idn">
            @{{ ext.name }}
          </option>                 
        </select> --}}
         <v-select v-model="fd.Exitreason" :options="fd.Exitreasonlist"></v-select>
      </div>




    </div>
    <div class="col-md-3">


      <div class="form-group">       
        <label>Modalidad de Contratacion</label>
        {{-- <select v-model="fd.Modcontract" class="form-control">
          <option v-for="mcont in fd.Modcontractlist" v-bind:value="mcont.idn">
            @{{ mcont.name }}
          </option>                 
        </select> --}}
         <v-select v-model="fd.Modcontract" :options="fd.Modcontractlist"></v-select>
      </div>

      <div class="form-group">
        <label>Nómina</label>
       {{--  <select v-model="fd.Roster" class="form-control">
          <option v-for="rost in fd.Rosterlist" v-bind:value="rost.idn">
            @{{ rost.name }}
          </option>                 
        </select> --}}
         <v-select v-model="fd.Roster" :options="fd.Rosterlist"></v-select>
      </div>





    </div>
  </div>
</div>
</div>
</div>
<!-- Fin Nueva Tarjeta Hija -->

<div id="vue-Obsocial">
 <!-- Nueva Tarjeta hija -->
 <div class="card" >
   <div class="card-header">
     <div class="row">
      <div class="col-md-9">
        <h2> Datos Especiales</h2>
      </div>
      <div class="col-md-3">
        <button type="button" v-on:click="updateObsocial" class="btn btn-success">Actualizar Datos</button>
      </div>
    </div>
  </div>
 <div class="card-body">
  <div class="row">
    <!-- Division del cuerpo en 3 columns -->
    <div class="col-md-3">

      <div class="form-group">
        <label>Obra Social</label>
        {{-- <select v-model="fd.Obsocial" class="form-control">
          <option v-for="obsoc in fd.Obsociallist" v-bind:value="obsoc.idn">
            @{{ obsoc.name }}
          </option>                 
        </select> --}}
         <v-select v-model="fd.Obsocial" :options="fd.Obsociallist"></v-select>
      </div>




    </div>
    <div class="col-md-3">
     <div class="form-group">       
      <label>Adherentes</label>
      <input type="input" placeholder="" v-model="fd.Adherents" class="form-control">
    </div>




  </div>

  <div class="col-md-3">


    <div class="form-group">       
      <label>Plan Médico</label>
  {{--     <select v-model="fd.Medicplan" class="form-control">
        <option v-for="medic in fd.Medicplanlist" v-bind:value="medic.idn">
          @{{ medic.name }}
        </option>                 
      </select> --}}
       <v-select v-model="fd.Medicplan" :options="fd.Medicplanlist"></v-select>
    </div>





  </div>
  <div class="col-md-3">


    <div class="form-group">       
      <label>Cantidad Cápitas</label>
      <input type="input" placeholder="" v-model="fd.Numberofcap" class="form-control">
    </div>






  </div>
</div>
</div>
</div>
</div>
<!-- Fin Nueva Tarjeta Hija -->
<div id="vue-Othersdata">
<!-- Nueva Tarjeta hija -->
<div class="card" >
    <div class="card-header">
     <div class="row">
      <div class="col-md-9">
        <h2>Otros Datos</h2>
      </div>
      <div class="col-md-3">
        <button type="button" v-on:click="updateOthers" class="btn btn-success">Actualizar Otros Datos</button>
      </div>
    </div>
  </div>
 <div class="card-body">
  <div class="row">
    <!-- Division del cuerpo en 3 columns -->
    <div class="col-md-3">

      <div class="form-group">
        <label>Situación Revista</label>

       <v-select v-model="fd.Sitrevision" :options="fd.Sitrevisionlist"></v-select>
      </div> 
      <div class="form-group">
        <label>Lugar de Trabajo</label>

       <v-select v-model="fd.Workplace" :options="fd.Workplacelist"></v-select>
      </div>




    </div>
    <div class="col-md-3">

    <div class="form-group">       
      <label>Sindicato</label>

       <v-select v-model="fd.Sindicate" :options="fd.Sindicatelist"></v-select>
    </div>

    <div class="form-group">       
      <label>Afiliado</label>
        <select v-model="fd.Affiliate" class="form-control">
        <option v-bind:value="1">Si</option>
        <option v-bind:value="0">No</option>
      </select>
    </div>




  </div>

  <div class="col-md-3">


   <div class="form-group">       
    <label>Régimen previsional</label>

       <v-select v-model="fd.Regprevisional" :options="fd.Regprevisionallist"></v-select>
  </div>

  <div class="form-group">       
    <label>Seg. Colectivo de Vida Oblig.</label>
       <select v-model="fd.Lifesecure" class="form-control">
         <option v-bind:value="1">Si</option>
        <option v-bind:value="0">No</option>                 
      </select>
      
  </div>

  <div class="form-group">       
    <label style="font-size: 12px">Benef. de reducción de Cargas Soc.</label>
       <select v-model="fd.Benefitsoc" class="form-control">
         <option v-bind:value="1">Si</option>
        <option v-bind:value="0">No</option>              
      </select> 
      
  </div>





</div>
<div class="col-md-3">


  <div class="form-group">       
    <label>Convencionado</label>
      <select v-model="fd.Agreed" class="form-control">
        <option v-bind:value="1">Si</option>
        <option v-bind:value="2">No</option>                    
      </select>
  </div>

  <div class="form-group">       
    <label>Código Siniestrado</label>

       <v-select v-model="fd.Sinestercode" :options="fd.Sinestercodelist"></v-select>
  </div>

  <div class="form-group">       
    <label>Nivel de Estudios</label>

       <v-select v-model="fd.Levelstudies" :options="fd.Levelstudieslist"></v-select>
  </div>


</div>




</div>
</div>
</div>
</div>

<!-- Nueva Tarjeta hija -->

<div class="card" >
  <div class="card-header">
   <div class="row">
    <div class="col-md-9">
     <h2>Grupos</h2>
   </div>
   <div class="col-md-3">
     @include('modal.ModalGroupEmployee')

   </div>
 </div>
</div>
<div class="card-block">
  <div class="row">
    <!-- Division del cuerpo en 3 columns -->
    <div class="col-md-12">

      <div class="form-group">


        <table id="TableEmployeeGroup" class="table table-striped table-bordered" cellspacing="0" width="100%">
          <thead width="100%">
            <tr>
              <th>Idn</th>
              <th>Nombre Grupo</th>
          

            </tr>
          </thead>             

        </table>
      </div>
    </div>
  </div>
</div>
</div>







