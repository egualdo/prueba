     <!DOCTYPE html>
  <html>
  @include('layouts.heads')
  <body>
    <!-- Side Navbar -->
    @include('layouts.sidebaradmin')
    <div class="page forms-page">
      <!-- navbar-->
      @include('layouts.navbar')
      <div id="vuedata">

<!-- Modal -->
      
       <!-- MODAL -->
       <div id="ModalConstant" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal text-left">
        <div role="document" class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 id="exampleModalLabel" class="modal-title">Editar Elemento</h5>
              <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
              <p>Indica los datos solicitados</p>
             
             <div class="row">
              <div class="col-md-6">
                  
                  
                 
                  <div class="form-group">       
                <label>Constantes</label>
                                  <select v-model="fd.Constant" class="form-control">
                                <option value="">Seleccione</option>
                               <option v-for="OneConstant in fd.Constantoptions" v-bind:value="OneConstant.idn">
                                @{{ OneConstant.name}}
                                 </option>
                                 </select>
              </div> 
                 
                    <div class="form-group">       
                 <label>Periodo</label>
                                  <select v-model="fd.Period" class="form-control">
                                <option value="">Seleccione</option>
                               <option v-for="OnePeriod in fd.Periodoptions" v-bind:value="OnePeriod.idn">
                                @{{ OnePeriod.title}}
                                 </option>
                                 </select>
                </div>
                  
                   <div class="form-group">       
                 <label>Sub Periodo</label>
                                  <select v-model="fd.Subperiod" class="form-control">
                                <option value="">Seleccione</option>
                               <option v-for="OneSubperiod in fd.Subperiodoptions" v-bind:value="OneSubperiod.idn">
                                @{{ OneSubperiod.name}}
                                 </option>
                                 </select>
                </div>
                 </div>
              <div class="col-md-6">
               
             
                  
             
          <div class="form-group">     
                     <label>Valor</label>
                         <br>
                <input v-model="fd.Value" class="form-control" type="text" id="value" name="value">
                </div>
                 
                   <div class="form-group">     
                     <label>Comentario</label>
                         <br>
                <input v-model="fd.Comments" type="text" id="comments" class="form-control" name="comments">
                </div>

            </div>
                 
            <div class="modal-footer">
              <div v-if="ff.save == true">
                <button v-on:click="guardarConstant" class="btn btn-sucess btn-fill pull-left">Guardar</button>
                <button type="button" class="btn btn-primary btn-fill pull-rigth" data-dismiss="modal">Cerrar</button>
              </div>

              <div v-else-if="ff.edit == true">
                <button v-on:click="editarConstant" class="btn btn-info btn-fill pull-left">Editar</button>
                <button v-on:click="borrarConstant" class="btn btn-warning btn-fill pull-rigth">Eliminar</button>
                <button type="button" class="btn btn-primary btn-fill pull-rigth" data-dismiss="modal">Cerrar</button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- FIN MODAL -->
    </div>
            </div>
       



  <div class="breadcrumb-holder fixed-top">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-2 border">
            </div>
            <div class="col-md-7 border">
              <!-- TITULO DE VENTANA -->
              <h1>  Constantes</h1>
            </div>
            <div class="col-md-3">
              <!--<p>Dato de Prueba</p>-->
               <button v-on:click="openmodaltonew" class="btn btn-info btn-fill pull-left">Nuevo Elemento</button>
            </div>

          </div>
        </div>
      </div>
              </div>
      <!-- FIN DE LA CABECERA GRIS -->


      <section class="forms">
        <div class="container-fluid">

          <!-- Tarjeta -->
          <div class="card" >
            <!-- Cabecera de Tarjeta  -->
           
           <!-- Cuerpo de Tarjeta -->
           <div class="card-body">  

            <div class="row">
              <!-- === PANEL DE TABLA === --> 
              <!-- ====== DATATABLE ======= -->
             <table id="TableConstant" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>Idn</th>
                    <th>Constante</th>
                      <th>Periodo</th>
                      <th>Legajo</th>
                      <th>Sub Periodo</th>
                      <th>Valor</th>
                      <th>Comentario</th>
                      <th>Activo</th>
                  </tr>
                </thead>
              </table>
              <!-- ====== FIN DATATABLE======= -->
            </div>
          </div>
        </div>
        <!-- FIN de Tarjeta Hija -->
      </div>
    </section>
                                 


    @include('layouts.footer')
  </div>
  @include('layouts.libraries')
 <script src="../js/employeejs/employeeconstant.js"></script>
</body>
</html>