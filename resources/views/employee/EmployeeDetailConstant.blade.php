


        <!-- Nueva Tarjeta hija -->
          <div class="card" >
            <div class="card-header">
             <div class="row">
              <div class="col-md-9">
               <h2>Constantes del Legajo</h2>
             </div>
             <div class="col-md-3">
                  @include('modal.ModalConstants')
             </div>
           </div>
         </div>
         <div class="card-body">
          <div class="row">
            <!-- Division del cuerpo en 3 columns -->
            <div class="col-md-12">

              <div class="form-group">


                <table id="TableEmployeeConstant" class="table table-striped table-bordered" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <th>Idn</th>
                      <th>Periodo</th>
                      <th>Tipo</th>
                     <th>Nombre</th>
                     <th>Valor</th>
                     <th>Comentarios</th>

                    </tr>
                  </thead>             

                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
 