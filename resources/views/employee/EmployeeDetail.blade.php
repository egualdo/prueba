<!DOCTYPE html>
<html>
@include('layouts.heads')

<style>
    .bar{
        background: #ffffff !important;
        height: 3px !important;
    }
    .bar.peg{
        background: #ffffff !important;
        height: 3px !important;
    }
    .spinner-icon{
        width: 30px !important;
        height: 30px !important;
        border: solid 4px transparent;
        border-top-color: #e9eef1 !important;
        border-left-color: #f4f6f7 !important;
        border-radius: 50%;
        -webkit-animation: nprogress-spinner 400ms linear infinite;
        animation: nprogress-spinner 400ms linear infinite;
    }
</style>
<body>
<!-- Side Navbar -->
@include('layouts.sidebaradmin')
<div class="page forms-page">
    <!-- navbar-->
@include('layouts.navbar')
<!-- CABECERA GRIS-->
    <div id="vue-EmployeeHead">
        @include('modal.ModalClone')
        <div class="breadcrumb-holder fixed-top">
            <div class="container-fluid">
                <div class="row headercolums">
                    <div class="colum1">
                        <h1 style="font-size: 25px;">Legajo: @{{fd.HeadCod}}</h1>
                        <h1 style="font-size: 15px;">@{{fd.HeadName}}</h1>
                    </div>
                    <div class="colum2">
                        {{--    <div class="row" style="font-size: 12px;">
                             <div class="colum21">
                              <label style= "margin-right:15px;"><strong>Cuil:</strong> @{{fd.HeadCuil}}</label>
                               <label style= "margin-right:15px;"><strong>Ingreso:</strong> @{{fd.HeadStartdate}}</label>
                             </div>
                             <div class="colum22">
                                <label style= "margin-right:15px;"><strong>Egreso:</strong> @{{fd.HeadStartdate}}</label>
                               <label style= "margin-right:15px;"><strong>Salario:</strong> @{{fd.HeadSalary}}</label>
                             </div>
                              </div>
                              <div class="row" style="font-size: 12px;">
                              <div class="colum23">
                               <label style= "margin-right:15px;"><strong>Nómina:</strong> @{{fd.HeadRoster}}</label>
                               <label style= "margin-right:15px;"><strong>Situación: </strong>@{{fd.HeadSitrevision}}</label>
                             </div>
                              <div class="colum24">
                                <label style= "margin-right:15px;"><strong>Contrato: </strong>@{{fd.HeadModcontract}}</label>
                               <label style= "margin-right:15px;"><strong>CCT: </strong>@{{fd.Headcct}}</label>
                             </div>
                          </div>

                          --}}

                        <table cellpadding="3">
                            <tbody>
                            <tr style="padding-right: 10px;">
                                <td><strong>Cuil:</strong></td>
                                <td>@{{fd.HeadCuil}}</td>
                                <td><strong>Ingreso:</strong></td>
                                <td>@{{fd.HeadStartDate}}</td>
                                <td><strong>Egreso:</strong></td>
                                <td>@{{fd.HeadFinishDate}}</td>
                            </tr>
                            <tr>
                                <td><strong>Nómina:</strong></td>
                                <td>@{{fd.HeadRoster}}</td>
                                <td><strong>Situación: </strong></td>
                                <td>@{{fd.HeadSitRevision}}</td>
                                <td><strong>CCT: </strong></td>
                                <td>@{{fd.Headcct}}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="colum3">
                        <div class="row labelsup" style="margin-left: 10px">
                            <button type="button" onclick="abrirnuevolegajo();" class="btn btn-primary btn-sm">Nuevo
                            </button>
                            </br>
                            <button type="button" v-on:click="openmodalclonar" class="btn btn-warning btn-sm">Clonar</button>
                            </br>
                            <button style="display: none" v-show="fd.Active === 1" type="button" v-on:click="inactivar" class="btn btn-danger btn-sm">Inactivar
                            </button>
                            <button style="display: none" v-show="fd.Active === 0" type="button" v-on:click="activar" class="btn btn-success btn-sm">Activar
                            </button>
                            </br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <!-- FIN DE LA CABECERA GRIS -->
    <section class="forms">
        <div class="container-fluid">
            <!-- TARJETA PADRE -->
            <div class="card">
                <!-- Titulo Tarjeta Padre-->
                <!-- Tabs de Navegacion -->
                <ul class="nav nav-tabs flex-wrap" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link" href="#Information" role="tab" data-toggle="tab">Info.Basica</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#WorkRelation" role="tab" data-toggle="tab">Relacion Laboral</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#ManualAsig" role="tab" data-toggle="tab">Asignaciones Manuales</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#News" role="tab" data-toggle="tab">Novedades</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#Vacation" role="tab" data-toggle="tab">Vacaciones</a>
                    </li>
                   
                    <li class="nav-item">
                        <a class="nav-link" href="#Constant" role="tab" data-toggle="tab">Constantes</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="#Payrun" role="tab" data-toggle="tab">Corridas</a>
                    </li>
                </ul>
                <!--Cuerpo de Tarjeta Padre-->
                <div class="card-block">
                    <!-- Contenido Total de La navegacion-->
                    <div class="tab-content">
                        <!-- Primera Pestaña -->
                        <div role="tabpanel" class="tab-pane" id="Information">
                            @include('employee.EmployeeDetailInfo')
                        </div>
                        <!--PESTAÑA-->
                        <div role="tabpanel" class="tab-pane fade" id="WorkRelation">
                        @include('employee.EmployeeDetailWorkRelation')
                        <!-- FIN de Tarjeta Hija -->
                        </div>
                        <!-- FIN PESTAÑA -->
                        <!--PESTAÑA-->
                        <div role="tabpanel" class="tab-pane fade" id="ManualAsig">
                        @include('employee.EmployeeDetailManualAsig')
                        <!-- FIN de Tarjeta Hija -->
                        </div>
                        <!-- FIN PESTAÑA -->
                        <!--PESTAÑA-->
                        <div role="tabpanel" class="tab-pane fade" id="News">
                        @include('employee.EmployeeDetailNews')
                        <!-- FIN de Tarjeta Hija -->
                        </div>
                        <!-- FIN PESTAÑA -->
                        <!--PESTAÑA-->
                        <div role="tabpanel" class="tab-pane fade" id="Vacation">
                        @include('employee.EmployeeDetailVacation')
                        <!-- FIN de Tarjeta Hija -->
                        </div>
                        <!-- FIN PESTAÑA -->
                        <!--PESTAÑA-->
                     
                        <!-- FIN PESTAÑA -->
                        <!--PESTAÑA-->
                        <div role="tabpanel" class="tab-pane fade" id="Constant">
                        @include('employee.EmployeeDetailConstant')
                        <!-- FIN de Tarjeta Hija -->
                        </div>
                        <!-- FIN PESTAÑA -->
                        <!--PESTAÑA-->
                        <div role="tabpanel " class="tab-pane active" id="Payrun">
                        @include('employee.EmployeeDetailPayrun')
                        <!-- FIN de Tarjeta Hija -->
                        </div>
                        <!-- FIN PESTAÑA -->
                        <!-- FIN PESTAÑA -->
                    </div>
                    <br>
                </div>
            </div>
            <!-- FIN DE PRIMERA PESTAÑA -->
            <br>
            <br>
            <br>
            <br>
        </div>
    </section>
    @include('layouts.footer')
</div>
@include('layouts.libraries')
</body>
</html>
<script src="../js/librariesjs/moment-with-locales.min.js"></script>
<script src="../js/librariesjs/bootstrap-datetimepicker.min.js"></script>

<script src="../js/generic/generic.js" type="text/javascript"></script>
<script src="../js/generic/loadtables.js"></script>
<script src="../js/generic/loadcombos.js"></script>
<script src="../js/employeejs/employeeRun.js"></script>
<script src="../js/employeejs/employeedetail.js"></script>
<!--<script src="../js/employeejs/employeeAsigDeduc.js"></script>-->