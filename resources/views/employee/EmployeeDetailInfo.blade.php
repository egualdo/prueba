    
 
    <div id="vue-DataPerson">
     <!-- Tarjeta hija -->
                <div class="card" >
                  <!-- Cabecera de Tarjeta Hija -->
                  <div class="card-header">
                    <div class="row">
                    <div class="col-md-9">
                      <h2> Datos Personales</h2>
                    </div>
                    <div class="col-md-3">
                      <button type="button" v-on:click="updateBasicInfo" class="btn btn-success">Actualizar Datos</button>
                    </div>
                  </div>
                    
                    
                  </div>
                  <!-- Cuerpo de Tarjeta Hija-->
                  <div class="card-body">  
                    <div class="row">
                      <!-- Division del cuerpo en 3 columns -->
                    <div class="col">
                            <div class="form-group">
                              <label>Nro Legajo</label>
                              <input type="input" v-model="fd.Cod" placeholder="Código de Legajo" class="form-control">
                            </div>
                            <div class="form-group">       
                              <label>Primer Nombre</label>
                              <input type="input" v-model="fd.Firstname" placeholder="Primer Nombre" class="form-control">
                            </div>
                            <div class="form-group">       
                              <label>Segundo Nombre</label>
                              <input type="input" v-model="fd.Lastname" placeholder="Segundo Nombre" class="form-control">
                            </div>
                      </div>
                    <div class="col">
                             <div class="form-group">       
                              <label>DNI</label>
                              <input type="input" v-model="fd.Dni" placeholder="DNI del legajo" class="form-control">
                            </div>

                            <div class="form-group">       
                              <label>Cuil</label>
                              <input type="input" v-mask="'##-########-#'" v-model="fd.Cuil" placeholder="CUIL del legajo" class="form-control">
                            </div>

                            <div class="form-group">       
                              <label>Fecha de Nacimiento</label>
                              <!--<input type="input" v-model="fd.Birthdate" placeholder="Fecha de Nacimiento" class="form-control">-->
                              <input placeholder="Fecha de Nacimiento" class="form-control" type="text" id="DPickerBirthdate" v-model="fd.Birthdate">
                               <!--<calendar value="fd.Birthdate" :disabled-days-of-week="fd.disabled" :format="fd.format" :clear-button="fd.clear" :placeholder="fd.placeholder" ></calendar>-->
                            </div>

                      </div>
                    <div class="col">
                          <div class="form-group">       
                            <label>Genero</label>
                             <select v-model="fd.Gender" class="form-control">                           
                                <option value="1">Masculino</option>
                                <option value="0">Femenino</option>
                              </select>
                          </div>
                          <div class="form-group">
                            <label>Email Personal</label>
                            <input type="input" v-model="fd.Personemail" placeholder="Email personal" class="form-control">
                          </div>
                          <div class="form-group">
                            <label>Email de Trabajo</label>
                            <input type="input" v-model="fd.Workemail" placeholder="Email de trabajo" class="form-control">
                          </div>
                    </div>
                    <div class="col">
                          <div class="form-group">       
                            <label>Teléfono</label>
                            <input type="input" v-model="fd.Telephone" placeholder="Teléfono" class="form-control">
                          </div>
                          <div class="form-group">
                            <label>Celular</label>
                            <input type="input" v-model="fd.Cellphone" placeholder="Celular" class="form-control">
                          </div>
                          <div class="form-group">
                            <label>Estado Civil</label>
                             <!--<select v-model="fd.Civilstatus" class="form-control">
                                <option v-for="Civilsta in fd.Civilstatuslist" v-bind:value="Civilsta.idn">
                                  @{{ Civilsta.name }}
                                </option>                 
                              </select>-->

                               <v-select v-model="fd.Civilstatus" :options="fd.Civilstatuslist"></v-select>

                          </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- FIN de Tarjeta Hija -->
      </div>
       <div id="app">
      
          
         </div>
<div id="vue-Residence">
              <!-- Nueva Tarjeta hija -->
              <div class="card" >
                <div class="card-header">
                  <div class="row">
                    <div class="col-md-9">
                      <h2>Datos de Residencia</h2>
                    </div>
                    <div class="col-md-3">
                      <button type="button" v-on:click="updateResidence" class="btn btn-success">Actualizar Residencia</button>
                    </div>
                  </div>
               </div>
               <div class="card-body">
                <div class="row">
                  <!-- Division del cuerpo en 3 columns -->
                  <div class="col-md-3">

                    <div class="form-group">
                      <label>Nacionalidad</label>
                       {{--  <select v-model="fd.Nationality" class="form-control">
                           <option v-for="Natio in fd.Nationalitylist" v-bind:value="Natio.idn">
                                  @{{ Natio.name }} 
                          </select> --}}
                            <v-select v-model="fd.Nationality" :options="fd.Nationalitylist"></v-select>
                    </div>
                    <div class="form-group">       
                      <label>Ciudad</label>
                      <input type="input" v-model="fd.City" placeholder="Ciudad de origen" class="form-control">
                    </div>



                  </div>
                  <div class="col-md-3">
                   <div class="form-group">       
                    <label>Provincia</label>
                    <input type="input" v-model="fd.Province" placeholder="Provincia" class="form-control">
                  </div>

                  <div class="form-group">       
                    <label>Código Postal</label>
                    <input type="input" v-model="fd.Postalcode" placeholder="Código Postal" class="form-control">
                  </div>


                </div>

                <div class="col-md-3">


                  <div class="form-group">       
                    <label>Calle</label>
                    <input v-model="fd.Street" type="input" placeholder="Calle" class="form-control">
                  </div>

                  <div class="form-group">
                    <label>Número</label>
                    <input v-model="fd.Number" type="input" placeholder="Número" class="form-control">
                  </div>
                </div>
                <div class="col-md-3">


                  <div class="form-group">       
                    <label>Piso</label>
                    <input v-model="fd.Floor" type="input" placeholder="Piso" class="form-control">
                  </div>

                  <div class="form-group">
                    <label>Departamento</label>
                    <input v-model="fd.Department" type="input" placeholder="Departamento" class="form-control">

                  </div>
                </div>
              </div>
            </div>
          </div>

          <!-- Fin Nueva Tarjeta Hija -->

</div>


          <!-- Nueva Tarjeta hija -->
          <div class="card" >
            <div class="card-header">
             <div class="row">
              <div class="col-md-9">
               <h2>Familiares</h2>
             </div>
             <div class="col-md-3">
              

               @include('modal.ModalFamilyEmployee')
             </div>
           </div>
         </div>
         <div class="card-body">
          <div class="row">
            <!-- Division del cuerpo en 3 columns -->
            <div class="col-md-12">

              <div class="form-group">


                <table id="TableEmployeeFamily" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    
                  <thead>
                    <tr>
                       <th>Idn</th>
                      <th>Tipo</th>
                      <th>Nombre</th>
                      <th>Apellido</th>
                      <th>Genero</th>

                    </tr>
                  </thead>             

                </table>
              </div>
            </div>
          </div>
        </div>
      </div>


      <!-- Nueva Tarjeta hija -->
      <div class="card" >
        <div class="card-header">
          <div class="row">
            <div class="col-md-9">
             <h2>Cuentas Bancarias</h2>
           </div>
           <div class="col-md-3">
             @include('modal.ModalBankAccountEmployee')
           </div>
         </div>

       </div>
       <div class="card-body">
        <div class="row">
          <!-- Division del cuerpo en 3 columns -->
          <div class="col-md-12">

            <div class="form-group">


              <table id="TableEmployeeBankAccount" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                  <tr>
                     <th>Idn</th>
                    <th>Metodo de pago</th>                   
                    <th>Banco</th>
                    <th>Numero</th>
                 
                  </tr>
                </thead>





              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <br>

    <!-- Nueva Tarjeta hija -->
    <div class="card" >
      <div class="card-header">
        <div class="row">
          <div class="col-md-9">
           <h2>Carreras</h2>
         </div>
         <div class="col-md-3">
            @include('modal.ModalCareerEmployee')
         </div>
       </div>

     </div>
     <div class="card-body">
      <div class="row">
        <!-- Division del cuerpo en 3 columns -->
        <div class="col-md-12">

          <div class="form-group">


            <table id="TableEmployeeCareer" class="table table-striped table-bordered" cellspacing="0" width="100%">
              <thead>
                <tr>
                   <th>Idn</th>
                  <th>Carrera</th>
                  <th>Título</th> 
                   <th>Inicio</th>
                    <th>Fin</th   

                </tr>
              </thead>





            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <br>

  <!-- Nueva Tarjeta hija -->
  <div class="card" >
    <div class="card-header">
      <div class="row">
        <div class="col-md-9">
         <h2>Idiomas</h2>
       </div>
       <div class="col-md-3">
          @include('modal.ModalLanguageEmployee')
       </div>
     </div>
     
   </div>
   <div class="card-body">
    <div class="row">
      <!-- Division del cuerpo en 3 columns -->
      <div class="col-md-12">

        <div class="form-group">


          <table id="TableEmployeeLanguage" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>Idn</th>
                <th>Idioma</th>
                <th>Nivel</th>

              </tr>
            </thead>





          </table>
        </div>
      </div>
    </div>
  </div>
</div>


