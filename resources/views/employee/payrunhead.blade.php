<!DOCTYPE html>
<html>
@include('layouts.heads')
<body>
<!-- Side Navbar -->
@include('layouts.sidebaradmin')
<div class="page forms-page">
    <!-- navbar-->
    @include('layouts.navbar')
    <div id="vuedata">

        <!-- Modal -->
        <!-- MODAL -->
        <div id="ModalPayRunHead" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"
             class="modal text-left">
            <div role="document" class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 id="exampleModalLabel" class="modal-title">Editar Elemento</h5>
                        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span
                                    aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <p>Indica los datos solicitados</p>

                        <div class="form-group">
                            <label>Periodo</label>
                            <option value="">Seleccione ...</option>
                            <select v-model="fd.Period" class="form-control">
                                <option v-for="Period in fd.Periodlist" v-bind:value="Period.idn">
                                    @{{ Period.title }}
                                </option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Empleado</label>
                            <option value="">Seleccione ...</option>
                            <select v-model="fd.Employee" class="form-control">
                                <option v-for="Employee in fd.Employeelist" v-bind:value="Employee.idn">
                                    @{{ Employee.firstname }}
                                </option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Lista</label>
                            <option value="">Seleccione ...</option>
                            <select v-model="fd.Roster" class="form-control">
                                <option v-for="Roster in fd.Rosterlist" v-bind:value="Roster.idn">
                                    @{{ Roster.name }}
                                </option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Metodo de pago</label>
                            <option value="">Seleccione ...</option>
                            <select v-model="fd.Paymentmethod" class="form-control">
                                <option v-for="Paymethod in fd.Paymethodlist" v-bind:value="Paymethod.idn">
                                    @{{ Paymethod.name }}
                                </option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Fecha de pago</label>
                            <input placeholder="Fecha de Pago" class="form-control" type="input" id="DPickerPaydate"
                                   v-model="fd.Paydate">
                        </div>


                        <div class="form-group">
                            <label>Fecha de pago de contribución</label>
                            <input placeholder="Fecha de Pago de contribución" class="form-control" type="input"
                                   id="DPickerPayContributiondate" v-model="fd.Paycontributiondate">
                        </div>

                        <div class="form-group">
                            <label>Pagos</label>
                            <input v-model="fd.Payments" type="text" placeholder="" class="form-control">
                        </div>

                        <div class="form-group">
                            <label>Deducciones</label>
                            <input v-model="fd.Deductions" type="text" placeholder="" class="form-control">
                        </div>

                        <div class="form-group">
                            <label>No remunerativos</label>
                            <input v-model="fd.Unremuns" type="text" placeholder="" class="form-control">
                        </div>

                        <div class="form-group">
                            <label>Excentos</label>
                            <input v-model="fd.Exempts" type="text" placeholder="" class="form-control">
                        </div>

                        <div class="form-group">
                            <label>Total Pago</label>
                            <input v-model="fd.Totalpay" type="text" placeholder="" class="form-control">
                        </div>

                        <div class="form-group">
                            <label>Comentario</label>
                            <input v-model="fd.Comment" type="text" placeholder="" class="form-control">
                        </div>


                    </div>
                    <div class="modal-footer">
                        <div v-if="ff.save == true">
                            <button v-on:click="guardar" class="btn btn-sucess btn-fill pull-left">Guardar</button>
                            <button type="button" class="btn btn-primary btn-fill pull-rigth" data-dismiss="modal">
                                Cerrar
                            </button>
                        </div>

                        <div v-else-if="ff.edit == true">
                            <button v-on:click="editar" class="btn btn-info btn-fill pull-left">Editar</button>
                            <button v-on:click="borrar" class="btn btn-warning btn-fill pull-rigth">Eliminar</button>
                            <button type="button" class="btn btn-primary btn-fill pull-rigth" data-dismiss="modal">
                                Cerrar
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- FIN MODAL -->
        <!-- FIN DE LA MODAL -->


        <!-- CABECERA GRIS-->
        <div class="breadcrumb-holder fixed-top">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-2 border">
                    </div>
                    <div class="col-md-7 border">
                        <!-- TITULO DE VENTANA -->
                        <h1> Ejecución de pagos anteriores</h1>
                    </div>
                    <div class="col-md-3">
                        <!--<p>Dato de Prueba</p>-->
                        <button v-on:click="openmodaltonew" class="btn btn-info btn-fill pull-left">Nuevo Elemento
                        </button>
                    </div>

                </div>
            </div>
        </div>
        <!-- FIN DE LA CABECERA GRIS -->
    </div>

    <section class="forms">
        <div class="container-fluid">

            <!-- Tarjeta -->
            <div class="card">
                <!-- Cabecera de Tarjeta  -->

                <!-- Cuerpo de Tarjeta -->
                <div class="card-body">

                    <div class="row">
                        <!-- === PANEL DE TABLA === -->
                        <!-- ====== DATATABLE ======= -->
                        <table id="TablePayRunHead" class="table table-striped table-bordered" cellspacing="0"
                               width="100%">
                            <thead>
                            <tr>
                                <th>Idn</th>
                                <th>Periodo</th>
                                <th>Fecha de pago</th>
                                <th>Pago</th>
                                <th>Deducciones</th>
                                <th>No remunerativos</th>
                                <th>Excentos</th>
                                <th>Total de Pago</th>
                            </tr>
                            </thead>
                        </table>
                        <!-- ====== FIN DATATABLE======= -->
                    </div>
                </div>
            </div>
            <!-- FIN de Tarjeta Hija -->
        </div>
    </section>


    @include('layouts.footer')
</div>
@include('layouts.libraries')
<script src="../js/generic/generic.js" type="text/javascript"></script>
<script src="../js/employeejs/payrunhead.js"></script>
</body>
</html>