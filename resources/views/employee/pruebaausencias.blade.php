     <!DOCTYPE html>
  <html>
  @include('layouts.heads')
  <body>
    <!-- Side Navbar -->
    @include('layouts.sidebaradmin')
    <div class="page forms-page">
      <!-- navbar-->
      @include('layouts.navbar')
      <div id="vuedata">

<!-- Modal -->
      
       <!-- MODAL -->
       <div id="ModalAbsences" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal text-left">
        <div role="document" class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 id="exampleModalLabel" class="modal-title">Editar Elemento</h5>
              <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
              <p>Indica los datos solicitados</p>
             
             <div class="row">
              <div class="col-md-6">
                 
                <div class="form-group">       
                  
                 <label>Tipo de Ausencia</label>
                  <select v-model="fd.AbsenceType" class="form-control">
                      <option value="">Seleccione</option>
                   <option v-for="OneAbsence in fd.Absencetypeoptions" v-bind:value="OneAbsence.idn">
                      @{{ OneAbsence.name }}
                   </option>
                </select>
             
                 <div class="form-group">     
                     <label>Cantidad</label>
                         <br>
                <input v-model="fd.Cant" type="text" id="cant" class="form-control" name="cant">
                </div>
                </div>
                  
                   
              </div> 
              <div class="col-md-6">
               
               <div class="form-group">     
                     <label>Fecha Desde</label>
                         <br>
                <input v-model="fd.Startdate" type="date" id="fechainicio" name="fecha">
                </div>
             
          <div class="form-group">     
                     <label>Fecha Hasta</label>
                         <br>
                <input v-model="fd.Finishdate" type="date" id="fechafin" name="fecha">
                </div>
                
                  
              </div>

            </div>
            <div class="modal-footer">
              <div v-if="ff.save == true">
                <button v-on:click="guardarAbsences" class="btn btn-sucess btn-fill pull-left">Guardar</button>
                <button type="button" class="btn btn-primary btn-fill pull-rigth" data-dismiss="modal">Cerrar</button>
              </div>

              <div v-else-if="ff.edit == true">
                <button v-on:click="editarAbsences" class="btn btn-info btn-fill pull-left">Editar</button>
                <button v-on:click="borrarAbsences" class="btn btn-warning btn-fill pull-rigth">Eliminar</button>
                <button type="button" class="btn btn-primary btn-fill pull-rigth" data-dismiss="modal">Cerrar</button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- FIN MODAL -->
    </div>



  <div class="breadcrumb-holder fixed-top">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-2 border">
            </div>
            <div class="col-md-7 border">
              <!-- TITULO DE VENTANA -->
              <h1>  Ausencias</h1>
            </div>
            <div class="col-md-3">
              <!--<p>Dato de Prueba</p>-->
               <button v-on:click="openmodaltonew" class="btn btn-info btn-fill pull-left">Nuevo Elemento</button>
            </div>

          </div>
        </div>
      </div>
      <!-- FIN DE LA CABECERA GRIS -->
</div>

      <section class="forms">
        <div class="container-fluid">

          <!-- Tarjeta -->
          <div class="card" >
            <!-- Cabecera de Tarjeta  -->
           
           <!-- Cuerpo de Tarjeta -->
           <div class="card-body">  

            <div class="row">
              <!-- === PANEL DE TABLA === --> 
              <!-- ====== DATATABLE ======= -->
             <table id="TableAbsences" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>Idn</th>
                    <th>Ausencia</th>
                      <th>Legajo</th>
                      <th>Cantidad</th>
                      <th>Fecha desde</th>
                      <th>Fecha hasta</th>
                      <th>Activo</th>
                  </tr>
                </thead>
              </table>
              <!-- ====== FIN DATATABLE======= -->
            </div>
          </div>
        </div>
        <!-- FIN de Tarjeta Hija -->
      </div>
    </section>
                                 


    @include('layouts.footer')
  </div>
  @include('layouts.libraries')
 <script src="../js/employeejs/absence.js"></script>
</body>
</html>