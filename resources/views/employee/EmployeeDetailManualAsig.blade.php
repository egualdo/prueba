
        <!-- Nueva Tarjeta hija -->

          <div class="card" >
            <div class="card-header">
             <div class="row">
              <div class="col-md-9">
               <h2>Asignaciones y Deducciones</h2>
             </div>
             <div class="col-md-3">
              @include('modal.ModalAsignationDeductions')
             </div>
            </div>
           </div>
       
      
         <div class="card-body">
          <div class="row">
            <!-- Division del cuerpo en 3 columns -->
            <div class="col-md-12">

              <div class="form-group">


                <table id="TableAsigDeduc" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                  <tr>
                      <th>Idn</th>
                      <th>Periodo</th>  
                      <th>Concepto</th>
                      <th>Tipo</th>
                       <th>Cantidad</th>               
                      <th>Monto</th>
                      <th>Fecha desde</th>
                      <th>Fecha hasta</th>
                     
                   
                    
                  </tr>
                </thead>
              </table>
              </div>
            </div>
          </div>
        </div>
       </div>
     

       <div class="card" >
            <div class="card-header">
             <div class="row">
              <div class="col-md-9">
               <h2>Exclusiones</h2>
             </div>
             <div class="col-md-3">
              @include('modal.ModalExclusions')
             </div>
            </div>
           </div>
       
      
         <div class="card-body">
          <div class="row">
            <!-- Division del cuerpo en 3 columns -->
            <div class="col-md-12">

              <div class="form-group">


                <table id="TableExclusions" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                  <tr>
                      <th>Idn</th>
                      <th>Periodo</th>  
                      <th>Tipo</th> 
                      <th>Concepto</th>                          
                     
                     
                   
                    
                  </tr>
                </thead>
              </table>
              </div>
            </div>
          </div>
        </div>
       </div>