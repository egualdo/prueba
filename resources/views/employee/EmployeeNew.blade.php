<!DOCTYPE html>
<html>
@include('layouts.heads')
<body>
<!-- Side Navbar -->
@include('layouts.sidebaradmin')
<div class="page forms-page">
    <!-- navbar-->
@include('layouts.navbar')

<!-- CABECERA GRIS-->
    <div id="vue-DataPerson">
        <div class="breadcrumb-holder fixed-top">
            <div class="container-fluid">
                <div class="row headercolums">

                    <div class="colum1">
                        <h1 style="font-size: 25px;">Nuevo Legajo</h1>
                        <h5>Formulario de registro para nuevo legajo</h5>
    <!--<h1 style="font-size: 15px;">@{{fd.HeadName}}</h1>-->

                    </div>
                    <div class="colum2">
                        <div class="row" style="font-size: 12px;">
                            <div class="colum21">
                                      </div>
                            <div class="colum22">
    
                            </div>
                        </div>
                        <div class="row" style="font-size: 12px;">
                            <div class="colum23">
              
                            </div>
                            <div class="colum24">
                      
                            </div>
                        </div>


                    </div>
                    <div class="colum3">
                        <div class="row labelsup" style="margin-left: 10px">


                            <button type="button" v-on:click="guardarlegajonew" class="btn btn-info">Guardar</button>
                            
                            

                        </div>

                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
        <!-- FIN DE LA CABECERA GRIS -->
        <section class="forms">
            <div class="container-fluid">

                <!-- TARJETA PADRE -->
                <div class="card">
                    <!-- Titulo Tarjeta Padre-->

                    <!-- Tabs de Navegacion -->
                    <ul class="nav nav-tabs flex-wrap" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link" href="#Information" role="tab" data-toggle="tab">Datos Obligatorios</a>

                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#WorkRelation" role="tab" data-toggle="tab">Datos Opcionales</a>
                        </li>
                    </ul>
                    <!--Cuerpo de Tarjeta Padre-->
                    <div class="card-block">
                        <!-- Contenido Total de La navegacion-->
                        <div class="tab-content">
                            <!-- Primera Pestaña -->

                            <div role="tabpanel" class="tab-pane active" id="Information">

                                <!-- Datos personales -->
                                <div class="card">
                                    <!-- Cabecera de Tarjeta Hija -->
                                    <div class="card-header">
                                        <div class="row">
                                            <div class="col-md-9">
                                                <h2> Datos Obligatorios</h2>

                                            </div>
                                            <div class="col-md-3">
                                            </div>
                                        </div>


                                    </div>
                                    <!-- Cuerpo de Tarjeta Hija-->
                                    <div class="card-body">
                                                <p>Por favor ingrese estos datos para guardar correctamente un nuevo legajo, los datos opcionales puede registrarlos luego, pero debe considerarlos para las corridas de nomina.</p>
                                                           
                                        <div class="row">
                                            <!-- Division del cuerpo en 3 columns -->
                                            <div class="col">
                                                <div class="form-group">
                                                    <label>Nro Legajo</label>
                                                    <input type="input" v-model="fd.Cod" placeholder="Código de Legajo"
                                                           class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>Primer Nombre</label>
                                                    <input required type="input" v-model="fd.Firstname"
                                                           placeholder="Primer Nombre" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>Apellido</label>
                                                    <input required type="input" v-model="fd.Lastname"
                                                           placeholder="Segundo Nombre" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col">
                                                    <div class="form-group">

                                                    <label>Genero</label>
                                                    <select required v-model="fd.Gender"  @change="calcularcuil" class="form-control">
                                                        <option value="1">Masculino</option>
                                                        <option value="0">Femenino</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>DNI</label>
                                                    <input required type="input"{{-- v-mask="'##.###.###'"--}} v-model="fd.Dni"
                                                           @input="calcularcuil"   placeholder="DNI del legajo" class="form-control">
                                                </div>

                                                <div class="form-group">
                                                    <label>Cuil</label>
                                                    <input required type="input"{{-- v-mask="'##-########-#'"--}} v-model="fd.Cuil"
                                                           placeholder="CUIL del legajo" class="form-control">
                                                </div>

                                             

                                            </div>

                                            <div class="col">
                                                                <div class="form-group">
                                                    <label>Fecha de Nacimiento</label>
                                                    <!--<input type="input" v-model="fd.Birthdate" placeholder="Fecha de Nacimiento" class="form-control">-->
                                                    <input required placeholder="Fecha de Nacimiento"
                                                           class="form-control" type="text" id="DPickerBirthdate"
                                                           v-model="fd.Birthdate">
                                                    <!--<calendar value="fd.Birthdate" :disabled-days-of-week="fd.disabled" :format="fd.format" :clear-button="fd.clear" :placeholder="fd.placeholder" ></calendar>-->
                                                </div>
                                                 <div class="form-group">
                                                    <label>Nacionalidad</label>
                                                    <select required v-model="fd.Nationality" class="form-control">
                                                        <option v-for="Natio in fd.Nationalitylist"
                                                                v-bind:value="Natio.idn">
                                                            @{{ Natio.name }}
                                                        </option>
                                                    </select>
                                                </div>
                                                  <div class="form-group">
                                                    <label>Estado Civil</label>
                                                    <select required v-model="fd.Civilstatus" class="form-control">
                                                        <option v-for="Civilsta in fd.Civilstatuslist"
                                                                v-bind:value="Civilsta.idn">
                                                            @{{ Civilsta.name }}
                                                        </option>
                                                    </select>
                                                </div>
                                            
                                            </div>
                                            <div class="col">
                                                       <div class="form-group">
                                                    <label>Código Siniestrado</label>
                                                    <select required v-model="fd.Sinestercode" class="form-control">
                                                        <option v-for="sinester in fd.Sinestercodelist"
                                                                v-bind:value="sinester.idn">
                                                            @{{ sinester.name }}
                                                        </option>
                                                    </select>
                                                </div>
                                                   <div class="form-group">
                                                    <label>Nómina</label>
                                                    <select required v-model="fd.Roster" class="form-control">
                                                        <option v-for="rost in fd.Rosterlist" v-bind:value="rost.idn">
                                                            @{{ rost.name }}
                                                        </option>
                                                    </select>
                                                </div>
                                              
                                              
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- FIN de Tarjeta Hija -->


                           


                                <!-- Familiares -->

                                <!-- Cuentas bancarias -->

                                <!-- Carreras -->
                            </div>
                            <!--2 PESTAÑA-->
                            <div role="tabpanel" class="tab-pane" id="WorkRelation">

                                <!-- Relacion laboral -->
                                     <!-- Datos de Residencia -->
                                <div class="card">
                                    <div class="card-header">
                                        <div class="row">
                                            <div class="col-md-9">
                                                <h2>Datos de Residencia</h2>
                                            </div>
                                            <div class="col-md-3">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <!-- Division del cuerpo en 3 columns -->
                                            <div class="col-md-3">

                                               
                                                <div class="form-group">
                                                    <label>Ciudad</label>
                                                    <input required type="input" v-model="fd.City"
                                                           placeholder="Ciudad de origen" class="form-control">
                                                </div>


                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Provincia</label>
                                                    <input required type="input" v-model="fd.Province"
                                                           placeholder="Provincia" class="form-control">
                                                </div>

                                                <div class="form-group">
                                                    <label>Código Postal</label>
                                                    <input type="input" v-model="fd.Postalcode"
                                                           placeholder="Código Postal" class="form-control">
                                                </div>


                                            </div>

                                            <div class="col-md-3">


                                                <div class="form-group">
                                                    <label>Calle</label>
                                                    <input required v-model="fd.Street" type="input" placeholder="Calle"
                                                           class="form-control">
                                                </div>

                                                <div class="form-group">
                                                    <label>Número</label>
                                                    <input v-model="fd.Number" type="input" placeholder="Número"
                                                           class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-3">


                                                <div class="form-group">
                                                    <label>Piso</label>
                                                    <input v-model="fd.Floor" type="input" placeholder="Piso"
                                                           class="form-control">
                                                </div>

                                                <div class="form-group">
                                                    <label>Departamento</label>
                                                    <input v-model="fd.Department" type="input"
                                                           placeholder="Departamento" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <!-- Relacion laboral -->
                                    <div class="card-header">
                                        <div class="row">
                                            <div class="col-md-9">
                                                <h2> Relacion Laboral</h2>
                                            </div>
                                            <div class="col-md-3">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Cuerpo de Tarjeta Hija-->
                                    <div class="card-body">

                                        <div class="row">
                                            <!-- Division del cuerpo en 3 columns -->
                                            <div class="col-md-3">

                                                <div class="form-group">
                                                    <label>Centro de Costo</label>
                                                    <select required v-model="fd.Costcenter" class="form-control">
                                                        <option v-for="Cost in fd.Costcenterlist"
                                                                v-bind:value="Cost.idn">
                                                            @{{ Cost.name }}
                                                        </option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Puesto</label>
                                                    <select required v-model="fd.Positionjob" class="form-control">
                                                        <option v-for="Position in fd.Positionjoblist"
                                                                v-bind:value="Position.idn">
                                                            @{{ Position.name }}
                                                        </option>
                                                    </select>
                                                </div>


                                            </div>
                                            <div class="col-md-3">
                                                  <div class="form-group">
                                                    <label>Departamento</label>
                                                    <select required v-model="fd.Idndepartment" class="form-control">
                                                        <option v-for="Dep in fd.Departmentlist" v-bind:value="Dep.idn">
                                                            @{{ Dep.name }}
                                                        </option>
                                                    </select>
                                                </div>
                                               

                                                <div class="form-group">
                                                    <label>Calendario Laboral</label>
                                                    <select required v-model="fd.Workcalendar" class="form-control">
                                                        <option v-for="Wor in fd.Workcalendarlist"
                                                                v-bind:value="Wor.idn">
                                                            @{{ Wor.name }}
                                                        </option>
                                                    </select>
                                                </div>

                                            </div>

                                            <div class="col-md-3">


                                               <div class="form-group">
                                                    <label>Categoria</label>
                                                    <select required v-model="fd.Categorysalary" class="form-control" v-on:change="UpdateSalary">
                                                        <option v-for="Category in fd.Categorysalarylist"
                                                                v-bind:value="Category.idn">
                                                            @{{ Category.name }}
                                                        </option>
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <label>Convenio Colectivo</label>
                                                    <select required v-model="fd.Cct" class="form-control">
                                                        <option v-for="ccts in fd.Cctlist" v-bind:value="ccts.idn">
                                                            @{{ ccts.name }}
                                                        </option>
                                                    </select>
                                                </div>


                                            </div>
                                            <div class="col-md-3">

                                                <div class="form-group">
                                                    <label>Salario Según Categoria</label>
                                                    <input type="input" v-model="fd.CategorySalaryAmount" placeholder=""
                                                           class="form-control" disabled>
                                                </div>

                                                <div class="form-group">
                                                    <label>Salario Básico</label>
                                                    <input type="input" v-model="fd.Basicsalary" placeholder=""
                                                           class="form-control">
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- FIN de Tarjeta Hija -->


                                <!-- actividad en la empresa -->
                                <div class="card">
                                    <div class="card-header">
                                        <div class="row">
                                            <div class="col-md-9">
                                                <h2> Actividad en la Empresa</h2>
                                            </div>
                                            <div class="col-md-3">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <!-- Division del cuerpo en 3 columns -->
                                            <div class="col-md-3">

                                                <div class="form-group">
                                                    <label>Situación</label>
                                                    <select required v-model="fd.Situation" class="form-control">
                                                        <option value="1">Activo</option>
                                                        <option value="2">Inactivo</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Condición</label>
                                                    <select required v-model="fd.Condition" class="form-control">
                                                        <option v-for="cond in fd.Conditionlist"
                                                                v-bind:value="cond.idn">
                                                            @{{ cond.name }}
                                                        </option>
                                                    </select>
                                                </div>


                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Fecha de Ingreso</label>
                                                    <input placeholder="Fecha de Ingreso" class="form-control"
                                                           type="text" id="DPDateentry" v-model="fd.Dateentry">
                                                </div>

                                                <div class="form-group">
                                                    <label>Fecha Ingreso Reconocida</label>
                                                    <input placeholder="Fecha de Ingreso Reconocida"
                                                           class="form-control" type="text" id="DPDateentryrecognized"
                                                           v-model="fd.Dateentryrecognized"
                                                           @change="calcularantiguedad">
                                                </div>


                                            </div>

                                            <div class="col-md-3">


                                                <div class="form-group">
                                                    <label>Fecha de Egreso</label>
                                                    <input id="DPDateexit" v-model="fd.Dateexit" type="input"
                                                           placeholder="Fecha de Egreso" class="form-control">
                                                </div>

                                                <div class="form-group">
                                                    <label>Motivo de Egreso</label>
                                                    <select required v-model="fd.Exitreason" class="form-control">
                                                        <option v-for="ext in fd.Exitreasonlist" v-bind:value="ext.idn">
                                                            @{{ ext.name }}
                                                        </option>
                                                    </select>
                                                </div>


                                            </div>
                                            <div class="col-md-3">


                                                <div class="form-group">
                                                    <label>Modalidad de Contratacion</label>
                                                    <select required v-model="fd.Modcontract" class="form-control">
                                                        <option v-for="mcont in fd.Modcontractlist"
                                                                v-bind:value="mcont.idn">
                                                            @{{ mcont.name }}
                                                        </option>
                                                    </select>
                                                </div>

                                             


                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- obra social -->


                                <!-- Dtos especiales -->
                                <div class="card">
                                    <div class="card-header">
                                        <div class="row">
                                            <div class="col-md-9">
                                                <h2> Datos Especiales</h2>
                                            </div>
                                            <div class="col-md-3">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <!-- Division del cuerpo en 3 columns -->
                                            <div class="col-md-3">

                                                <div class="form-group">
                                                    <label>Obra Social</label>
                                                    <select required v-model="fd.Obsocial" class="form-control">
                                                        <option v-for="obsoc in fd.Obsociallist"
                                                                v-bind:value="obsoc.idn">
                                                            @{{ obsoc.name }}
                                                        </option>
                                                    </select>
                                                </div>


                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Adherentes</label>
                                                    <input type="input" placeholder="" v-model="fd.Adherents"
                                                           class="form-control">
                                                </div>


                                            </div>

                                            <div class="col-md-3">

                                                <div class="form-group">
                                                    <label>Plan Médico</label>
                                                    <select required v-model="fd.Medicplan" class="form-control">
                                                        <option v-for="medic in fd.Medicplanlist"
                                                                v-bind:value="medic.idn">
                                                            @{{ medic.name }}
                                                        </option>
                                                    </select>
                                                </div>

                                            </div>
                                            <div class="col-md-3">


                                                <div class="form-group">
                                                    <label>Cantidad Cápitas</label>
                                                    <input type="input" placeholder="" v-model="fd.Numberofcap"
                                                           class="form-control">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Fin Nueva Tarjeta Hija -->

                                <!-- otros datos -->
                                <div class="card">
                                    <div class="card-header">
                                        <div class="row">
                                            <div class="col-md-9">
                                                <h2>Otros Datos</h2>
                                            </div>
                                            <div class="col-md-3">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <!-- Division del cuerpo en 3 columns -->
                                            <div class="col-md-3">
                                                    <div class="form-group">
                                                    <label>Email Personal</label>
                                                    <input type="input" v-model="fd.Personemail"
                                                           placeholder="Email personal" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>Email de Trabajo</label>
                                                    <input type="input" v-model="fd.Workemail"
                                                           placeholder="Email de trabajo" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>Situación Revista</label>
                                                    <select required v-model="fd.Sitrevision" class="form-control">
                                                        <option v-for="sit in fd.Sitrevisionlist"
                                                                v-bind:value="sit.idn">
                                                            @{{ sit.name }}
                                                        </option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Lugar de Trabajo</label>
                                                    <select required v-model="fd.Workplace" class="form-control">
                                                        <option v-for="work in fd.Workplacelist"
                                                                v-bind:value="work.idn">
                                                            @{{ work.name }}
                                                        </option>
                                                    </select>
                                                </div>


                                            </div>
                                            <div class="col-md-3">

                                                <div class="form-group">
                                                    <label>Sindicato</label>
                                                    <select required v-model="fd.Sindicate" class="form-control">
                                                        <option v-for="sind in fd.Sindicatelist"
                                                                v-bind:value="sind.idn">
                                                            @{{ sind.name }}
                                                        </option>
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <label>Afiliado</label>
                                                    <select required v-model="fd.Affiliate" class="form-control">
                                                        <option v-bind:value="1">Si</option>
                                                        <option v-bind:value="2">No</option>
                                                    </select>
                                                </div>
                                              
                                                     <div class="form-group">
                                                    <label>Nivel de Estudios</label>
                                                    <select  v-model="fd.Levelstudies" class="form-control">
                                                        <option v-for="levels in fd.Levelstudieslist"
                                                                v-bind:value="levels.idn">
                                                            @{{ levels.name }}
                                                        </option>
                                                    </select>
                                                </div>


                                            </div>

                                            <div class="col-md-3">


                                                <div class="form-group">
                                                    <label>Régimen previsional</label>
                                                    <select required v-model="fd.Regprevisional" class="form-control">
                                                        <option v-for="reg in fd.Regprevisionallist"
                                                                v-bind:value="reg.idn">
                                                            @{{ reg.name }}
                                                        </option>
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <label>Seguro Colec. de Vida Oblig.</label>
                                                    <select required v-model="fd.Lifesecure" class="form-control">
                                                        <option v-bind:value="1">Si</option>
                                                        <option v-bind:value="0">No</option>
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <label>Beneficio de Red. de Cargas Soc.</label>
                                                    <select required v-model="fd.Benefitsoc" class="form-control">
                                                        <option v-bind:value="1">Si</option>
                                                        <option v-bind:value="0">No</option>
                                                    </select>
                                                </div>


                                            </div>
                                            <div class="col-md-3">

                                                  <div class="form-group">
                                                    <label>Teléfono</label>
                                                    <input type="input" v-model="fd.Telephone" placeholder="Teléfono"
                                                           class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>Celular</label>
                                                    <input type="input" v-model="fd.Cellphone" placeholder="Celular"
                                                           class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>Convencionado</label>
                                                    <select required v-model="fd.Agreed" class="form-control">
                                                        <option v-bind:value="1">Si</option>
                                                        <option v-bind:value="2">No</option>
                                                    </select>
                                                </div>

                                         

                                           


                                            </div>


                                        </div>
                                    </div>
                                </div>


                                <!-- Grupos -->
                            </div>

                        </div>
                    </div>
                    <!-- FIN PESTAÑA -->
                </div>
                <br>
            </div>

            <br>
            <br>
            <br>
            <br>
        </section>
    </div>
    @include('layouts.footer')
</div>
@include('layouts.libraries')
</body>
</html>
<script src="../js/generic/generic.js" type="text/javascript"></script>

<!--<script src="../js/generic/loadtables.js"></script>-->
<script src="../js/generic/loadcombosnew.js"></script>
<script src="../js/employeejs/employeenew.js"></script>


<!--<script src="../js/employeejs/employeeAsigDeduc.js"></script>-->


