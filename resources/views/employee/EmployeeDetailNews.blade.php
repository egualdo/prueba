<!-- Nueva Tarjeta hija -->
<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-md-9">
                <h2>Ausencias</h2>
            </div>
            <div class="col-md-3">
                @include('modal.ModalAbsences')
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="row">
            <!-- Division del cuerpo en 3 columns -->
            <div class="col-md-12">

                <div class="form-group">


                    <table id="TableEmployeeAbsences" class="table table-striped table-bordered" cellspacing="0"
                           width="100%">
                        <thead>
                        <tr>
                            <th>Idn</th>
                            <th>Ausencia</th>
                            <th>Cantidad</th>
                            <th>Fecha desde</th>
                            <th>Fecha hasta</th>

                        </tr>
                        </thead>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Nueva Tarjeta hija -->
<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-md-9">
                <h2>Horas Trabajadas</h2>
            </div>
            <div class="col-md-3">
                @include('modal.ModalHoursWorked')
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="row">
            <!-- Division del cuerpo en 3 columns -->
            <div class="col-md-12">
                <div class="row">


                </div>
                <div class="form-group">


                    <table id="TableHoursworked" class="table table-striped table-bordered" cellspacing="0"
                           width="100%">
                        <thead>
                        <tr>
                            <th>Idn</th>
                            <th>Fecha de Inicio</th>
                            <th>Hora de Inicio</th>
                            <th>Fecha de Fin</th>
                            <th>Hora de Inicio</th>
                            <th>Horas</th>
                            <th>Observacion</th>

                        </tr>
                        </thead>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>