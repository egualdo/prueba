   
      <div id="FamilyEmployee"> 

 <button type="button" v-on:click="openmodaltonew" class="btn btn-primary">Agregar Familiar</button>
   
       <!-- MODAL -->
       <div id="ModalFamilyEmployee" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal text-left">
        <div role="document" class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 id="exampleModalLabel" class="modal-title">Editar Elemento</h5>
              <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
              <p>Indica los datos solicitados</p>
             
             <div class="row">
              <div class="col-md-6">
                 <div class="form-group">
                <label>Tipo</label>
          
                  <select v-model="fd.Familytype" class="form-control">
                  <option selected="selected" value="Hijo">Hijo</option>
                  <option value="Madre">Madre</option>
                  <option value="Padre">Padre</option>
                  <option value="Abuelo">Abuelo(a)</option>
                  <option value="Nieto">Nieto(a)</option>
                  <option value="Tio">Tio</option>
                </select>
              </div>
              <div class="form-group">       
                <label>CUIL</label>
                <input v-model="fd.Cuil" v-mask="'##-########-#'" type="text" placeholder="" class="form-control">
              </div>
                <div class="form-group">       
                <label>Género</label>
                <select v-model="fd.Gender" class="form-control">
                  <option value="M">Masculino</option>
                  <option value="F">Femenino</option>
                </select>
              </div>
              </div>
              <div class="col-md-6">
                 <div class="form-group">
                <label>Nombre</label>
                <input v-model="fd.Firstname" type="text" placeholder="" class="form-control">
              </div>
              <div class="form-group">       
                <label>Apellido</label>
                <input v-model="fd.Lastname" type="text" placeholder="" class="form-control">
              </div>
              <div class="form-group">       
                <label>Nacimiento</label>
                <input type="text" placeholder="" id="DPickerBirthdateFamily" v-model="fd.Birthdate" class="form-control">
              </div>
         

              </div>

             </div> 
             
              
              
            </div>
            <div class="modal-footer">
              <div v-if="ff.save == true">
                <button v-on:click="guardarFamily" class="btn btn-sucess btn-fill pull-left">Guardar</button>
                <button type="button" class="btn btn-primary btn-fill pull-rigth" data-dismiss="modal">Cerrar</button>
              </div>

              <div v-else-if="ff.edit == true">
                <button v-on:click="editarFamily" class="btn btn-info btn-fill pull-left">Editar</button>
                <button v-on:click="borrarFamily" class="btn btn-warning btn-fill pull-rigth">Eliminar</button>
                <button type="button" class="btn btn-primary btn-fill pull-rigth" data-dismiss="modal">Cerrar</button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- FIN MODAL -->
    </div>



