<!-- MODAL -->
<div id="ModalPayrunDetails" tabindex="55" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"
     class="modal fade text-left">
    <div role="document" class="modal-dialog" style="min-width: 900px">
        <div class="modal-content">
            <div class="modal-header">
                <h4 id="exampleModalLabel" class="modal-title">Detalles de la Corrida de Nómina de: @{{ fd.HeadName }}
                    - @{{ fd.HeadCod }} en el Periodo @{{fd.DetNamePeriod}} - @{{fd.DetNameTyperun}}</h4>

                <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span
                            aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-sm" style="font-size: 11px;">
                    <thead>
                    <tr class="">
                        <th scope="col">CUIL</th>
                        <th scope="col">Salario</th>
                        <th scope="col">CCT</th>
                        <th scope="col">Nómina</th>

                        <th scope="col">Condición</th>
                        <th scope="col">Ingreso</th>
                        <th scope="col">Egreso</th>

                        <th scope="col">Antiguedad</th>
                        <th scope="col">Dias</th>
                        <th scope="col">Jornada</th>

                        <th scope="col">Situacion</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th style="font-weight: lighter;">@{{ fd.HeadCuil }}</th>
                        <th style="font-weight: lighter;">@{{ fd.HeadSalary }}</th>
                        <th style="font-weight: lighter;">@{{ fd.Headcct }}</th>
                        <th style="font-weight: lighter;">@{{ fd.HeadRoster }}</th>

                        <th style="font-weight: lighter;">@{{ fd.HeadCondition }}</th>
                        <th style="font-weight: lighter;">@{{ fd.HeadStartDate }}</th>
                        <th style="font-weight: lighter;">@{{ fd.HeadFinishDate }}</th>

                        <th style="font-weight: lighter;width: 60px">@{{ fd.HeadAntiquity }}</th>
                        <th style="font-weight: lighter; width: 60px">30</th>
                        <th style="font-weight: lighter; width: 30px">@{{ fd.HeadJournalType }}</th>
                        <th style="font-weight: lighter; width: 30px">@{{ fd.HeadSitRevision }}</th>
                    </tr>
                    </tbody>
                </table>
                <h4></h4>

                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#Conceptos" role="tab"
                           aria-controls="home" aria-selected="true">Conceptos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#Sicoss" role="tab"
                           aria-controls="profile" aria-selected="false">Sicoss</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#Depuracion" role="tab"
                           aria-controls="contact" aria-selected="false">Datos Adicionales</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="constant-tab" data-toggle="tab" href="#constant" role="tab"
                           aria-controls="constant" aria-selected="false">Constantes</a>
                    </li>
                </ul>
                <div class="tab-content" id="">
                    <!--PRIMER TAB-->
                    <div class="tab-pane fade show active" id="Conceptos" role="tabpanel" aria-labelledby="home-tab">
                        <div style="padding:10px;">
                            <table class="table table-bordered table-hover table-sm">
                                <thead>
                                <tr class="table-primary">
                                    <th width="50px" scope="col">Código</th>
                                    <th scope="col">Concepto</th>
                                    <th scope="col">Cantidad</th>
                                    <th width="120px" scope="col">Pagos</th>
                                    <th width="120px" scope="col">No Remun</th>
                                    <th width="120px" scope="col">Deducciones</th>
                                    <th width="120px" scope="col">Exentos</th>
                                    <th width="20px" scope="col"></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="bodyrun in fd.Payrunbodylist">
                                    <th>@{{bodyrun.codconcept}}</th>
                                    <th style="font-weight:lighter;">@{{bodyrun.nameconcept}}</th>

                                    <th style="font-weight:lighter;">@{{bodyrun.cant}}</th>
                                    <th v-if="bodyrun.columnconcept===1"
                                        style="font-weight:lighter;">@{{bodyrun.amount.toFixed(2)}}</th>
                                    <th v-else></th>

                                    <th v-if="bodyrun.columnconcept===2"
                                        style="font-weight:lighter;">@{{bodyrun.amount.toFixed(2)}}</th>
                                    <th v-else></th>
                                    <th v-if="bodyrun.columnconcept===3"
                                        style="font-weight:lighter;">@{{bodyrun.amount.toFixed(2)}}</th>
                                    <th v-else></th>
                                    <th v-if="bodyrun.columnconcept===4"
                                        style="font-weight:lighter;">@{{bodyrun.amount.toFixed(2)}}</th>
                                    <th v-else></th>
                                    <th><button type="button" v-on:click="editreceive(bodyrun.idn)" class="btn btn-primary btn-xs">+</button></th>
                                    <!--COLUMNAS DE PAGO -->


                                </tr>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th>Totales</th>
                                    <th>@{{fd.ColTotPays}}</th>
                                    <th>@{{fd.ColTotNorem}}</th>
                                    <th>@{{fd.ColTotDeduc}}</th>
                                    <th>@{{fd.ColTotExen}}</th>


                                </tr>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th>Pago Neto:</th>
                                    <th>@{{fd.NetPay}}</th>


                                </tr>
                                </tbody>
                            </table>
                        </div>


                    </div>
                    <!--FIN DEL PRIMER TAB-->
                    <div class="tab-pane fade" id="Sicoss" role="tabpanel" aria-labelledby="profile-tab">
                        <br>
                        <div class="row">
                            <div class="col">
                                <table class="table table-sm table-bordered tablesicoss">
                                    <thead>

                                    <tr>
                                        <th width="10px">Col</th>
                                        <th width="100px">Descripcion</th>
                                        <th>Valor</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>CUIL</td>
                                        <td>@{{fd.Sicossdata[0]}}</td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Apellido y Nombre</td>
                                        <td>@{{fd.Sicossdata[1]}}</td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Conyuge</td>
                                        <td>@{{fd.Sicossdata[2]}}</td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>Hijos</td>
                                        <td>@{{fd.Sicossdata[3]}}</td>
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td>Sit.Revista</td>
                                        <td>@{{fd.Sicossdata[4]}}</td>
                                    </tr>
                                    <tr>
                                        <td>6</td>
                                        <td>Condicion</td>
                                        <td>@{{fd.Sicossdata[5]}}</td>
                                    </tr>
                                    <tr>
                                        <td>7</td>
                                        <td>Actividad</td>
                                        <td>@{{fd.Sicossdata[6]}}</td>
                                    </tr>
                                    <tr>
                                        <td>8</td>
                                        <td>Zona</td>
                                        <td>@{{fd.Sicossdata[7]}}</td>
                                    </tr>
                                    <tr>
                                        <td>9</td>
                                        <td>% Ap. Ad. S.S.</td>
                                        <td>@{{fd.Sicossdata[8]}}</td>
                                    </tr>
                                    <tr>
                                        <td>10</td>
                                        <td>Mod. de Contrat.</td>
                                        <td>@{{fd.Sicossdata[9]}}</td>
                                    </tr>
                                    <tr>
                                        <td>11</td>
                                        <td>Obra Social</td>
                                        <td>@{{fd.Sicossdata[10]}}</td>
                                    </tr>
                                    <tr>
                                        <td>12</td>
                                        <td>Aderentes</td>
                                        <td>@{{fd.Sicossdata[11]}}</td>
                                    </tr>
                                    <tr>
                                        <td>13</td>
                                        <td>Total Bruto</td>
                                        <td>@{{fd.Sicossdata[12]}}</td>
                                    </tr>
                                    <tr>
                                        <td>14</td>
                                        <td>Rem.Imp.1</td>
                                        <td>@{{fd.Sicossdata[13]}}</td>
                                    </tr>
                                    <tr>
                                        <td>15</td>
                                        <td>Asig.Fam.</td>
                                        <td>@{{fd.Sicossdata[14]}}</td>
                                    </tr>
                                    <tr>
                                        <td>16</td>
                                        <td>Aporte Vol.</td>
                                        <td>@{{fd.Sicossdata[15]}}</td>
                                    </tr>
                                    <tr>
                                        <td>17</td>
                                        <td>Adic. O.S.</td>
                                        <td>@{{fd.Sicossdata[16]}}</td>
                                    </tr>
                                    <tr>
                                        <td>18</td>
                                        <td>Exced. S.S.</td>
                                        <td>@{{fd.Sicossdata[17]}}</td>
                                    </tr>
                                    <tr>
                                        <td>19</td>
                                        <td>Adic .O.S.</td>
                                        <td>@{{fd.Sicossdata[18]}}</td>
                                    </tr>
                                    <tr>
                                        <td>20</td>
                                        <td>Provincia</td>
                                        <td>@{{fd.Sicossdata[19]}}</td>
                                    </tr>

                                    </tbody>
                                </table>

                            </div>
                            <div class="col">
                                <table class="table table-sm table-bordered tablesicoss">
                                    <thead>

                                    <tr>
                                        <th width="10px">Col</th>
                                        <th width="100px">Descripcion</th>
                                        <th>Valor</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>21</td>
                                        <td>Rem.imp.2</td>
                                        <td>@{{fd.Sicossdata[20]}}</td>
                                    </tr>
                                    <tr>
                                        <td>22</td>
                                        <td>Rem.imp.3</td>
                                        <td>@{{fd.Sicossdata[21]}}</td>
                                    </tr>
                                    <tr>
                                        <td>23</td>
                                        <td>Rem.imp.4</td>
                                        <td>@{{fd.Sicossdata[22]}}</td>
                                    </tr>
                                    <tr>
                                        <td>24</td>
                                        <td>Cod.de Siniestro</td>
                                        <td>@{{fd.Sicossdata[23]}}</td>
                                    </tr>
                                    <tr>
                                        <td>25</td>
                                        <td>Marca reduc.</td>
                                        <td>@{{fd.Sicossdata[24]}}</td>
                                    </tr>
                                    <tr>
                                        <td>26</td>
                                        <td>Recomp. LRT</td>
                                        <td>@{{fd.Sicossdata[25]}}</td>
                                    </tr>
                                    <tr>
                                        <td>27</td>
                                        <td>Tipo Empresa</td>
                                        <td>@{{fd.Sicossdata[26]}}</td>
                                    </tr>
                                    <tr>
                                        <td>28</td>
                                        <td>Adic. O.S.</td>
                                        <td>@{{fd.Sicossdata[27]}}</td>
                                    </tr>
                                    <tr>
                                        <td>29</td>
                                        <td>Reg. previsional</td>
                                        <td>@{{fd.Sicossdata[28]}}</td>
                                    </tr>
                                    <tr>
                                        <td>30</td>
                                        <td>Cod. Revista 1</td>
                                        <td>@{{fd.Sicossdata[29]}}</td>
                                    </tr>
                                    <tr>
                                        <td>31</td>
                                        <td>Dia Revista 1</td>
                                        <td>@{{fd.Sicossdata[30]}}</td>
                                    </tr>
                                    <tr>
                                        <td>32</td>
                                        <td>Cod Revista 2</td>
                                        <td>@{{fd.Sicossdata[31]}}</td>
                                    </tr>
                                    <tr>
                                        <td>33</td>
                                        <td>Dia Revista 2</td>
                                        <td>@{{fd.Sicossdata[32]}}</td>
                                    </tr>
                                    <tr>
                                        <td>34</td>
                                        <td>Cod Revista 3</td>
                                        <td>@{{fd.Sicossdata[33]}}</td>
                                    </tr>
                                    <tr>
                                        <td>35</td>
                                        <td>Dia Revista 3</td>
                                        <td>@{{fd.Sicossdata[34]}}</td>
                                    </tr>
                                    <tr>
                                        <td>36</td>
                                        <td>Sueldos y Adicionales</td>
                                        <td>@{{fd.Sicossdata[35]}}</td>
                                    </tr>
                                    <tr>
                                        <td>37</td>
                                        <td>SAC</td>
                                        <td>@{{fd.Sicossdata[36]}}</td>
                                    </tr>
                                    <tr>
                                        <td>38</td>
                                        <td>Horas extras monto</td>
                                        <td>@{{fd.Sicossdata[37]}}</td>
                                    </tr>
                                    <tr>
                                        <td>39</td>
                                        <td>Zona desfav.</td>
                                        <td>@{{fd.Sicossdata[38]}}</td>
                                    </tr>
                                    <tr>
                                        <td>40</td>
                                        <td>Vacaciones</td>
                                        <td>@{{fd.Sicossdata[39]}}</td>
                                    </tr>


                                    </tbody>
                                </table>

                            </div>
                            <div class="col">
                                <table class="table table-sm table-bordered table-condensed tablesicoss">
                                    <thead>

                                    <tr>
                                        <th width="10px">Col</th>
                                        <th width="100px">Descripcion</th>
                                        <th>Valor</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>41</td>
                                        <td>Dias Trabajados</td>
                                        <td>@{{fd.Sicossdata[40]}}</td>
                                    </tr>
                                    <tr>
                                        <td>42</td>
                                        <td>Rem.imp.5</td>
                                        <td>@{{fd.Sicossdata[41]}}</td>
                                    </tr>
                                    <tr>
                                        <td>43</td>
                                        <td>Acuerdo S.S.</td>
                                        <td>@{{fd.Sicossdata[42]}}</td>
                                    </tr>
                                    <tr>
                                        <td>44</td>
                                        <td>Rem. imp. 6</td>
                                        <td>@{{fd.Sicossdata[43]}}</td>
                                    </tr>
                                    <tr>
                                        <td>45</td>
                                        <td>Tipo operacion</td>
                                        <td>@{{fd.Sicossdata[44]}}</td>
                                    </tr>
                                    <tr>
                                        <td>46</td>
                                        <td>Adicionales</td>
                                        <td>@{{fd.Sicossdata[45]}}</td>
                                    </tr>
                                    <tr>
                                        <td>47</td>
                                        <td>Premios</td>
                                        <td>@{{fd.Sicossdata[46]}}</td>
                                    </tr>
                                    <tr>
                                        <td>48</td>
                                        <td>Rem.imp.8</td>
                                        <td>@{{fd.Sicossdata[47]}}</td>
                                    </tr>
                                    <tr>
                                        <td>49</td>
                                        <td>Rem.imp.7</td>
                                        <td>@{{fd.Sicossdata[48]}}</td>
                                    </tr>
                                    <tr>
                                        <td>50</td>
                                        <td>Horas extras</td>
                                        <td>@{{fd.Sicossdata[49]}}</td>
                                    </tr>
                                    <tr>
                                        <td>51</td>
                                        <td>Tot.no remun.</td>
                                        <td>@{{fd.Sicossdata[50]}}</td>
                                    </tr>
                                    <tr>
                                        <td>52</td>
                                        <td>Maternidad</td>
                                        <td>@{{fd.Sicossdata[51]}}</td>
                                    </tr>
                                    <tr>
                                        <td>53</td>
                                        <td>Rect.Remun.</td>
                                        <td>@{{fd.Sicossdata[52]}}</td>
                                    </tr>
                                    <tr>
                                        <td>54</td>
                                        <td>Rem.imp.9</td>
                                        <td>@{{fd.Sicossdata[53]}}</td>
                                    </tr>
                                    <tr>
                                        <td>55</td>
                                        <td>Tarea dif.%</td>
                                        <td>@{{fd.Sicossdata[54]}}</td>
                                    </tr>
                                    <tr>
                                        <td>56</td>
                                        <td>Horas Trabajadas</td>
                                        <td>@{{fd.Sicossdata[55]}}</td>
                                    </tr>
                                    <tr>
                                        <td>57</td>
                                        <td>Seguro Oblig.</td>
                                        <td>@{{fd.Sicossdata[56]}}</td>
                                    </tr>


                                    </tbody>
                                </table>

                            </div>
                        </div>


                    </div>
                    <div class="tab-pane fade" id="Depuracion" role="tabpanel" aria-labelledby="contact-tab">
                        <br>
                        <p>En este lugar puedes visualizar algunos datos, que te permiten conocer el comportamiento de
                            la corrida</p>
                        <div class="row">
                            <!-- FIRST ROW -->
                            <div class="col-md-4">
                               <!-- <h4> Constantes</h4>
                                <table class="table table-sm table-bordered">
                                    <thead>
                                    <tr>
                                        <td scope="col">Nombre</td>
                                        <td scope="col">Valor</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr v-for="asig in fd.Constantfooterlist">
                                        <td>@{{asig.variable}}</td>
                                        <td>@{{asig.value}}</td>
                                    </tr>
                                    </tbody>
                                </table>-->
                                <div class="form-group">
                                  <h4> Asignaciones</h4>
                                <table class="table table-sm table-bordered">
                                    <thead>

                                    <tr>
                                        <th scope="col">Concepto</th>
                                        <th scope="col">Valor</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr v-for="asig in fd.Asignationfooterlist">
                                        <td>@{{asig.variable}}</td>
                                        <td>@{{asig.value}}</td>
                                    </tr>

                                    </tbody>
                                </table>
                                </div>
                            </div>
                            <div class="col-md-4">
                            <div class="form-group">
                                <h2> Exclusiones</h2>
                                    <table class="table table-sm table-bordered">
                                        <thead>
                                        <tr>
                                            <td scope="col">Concepto</td>
                                            <td scope="col">Idn</td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr v-for="exclu in fd.Exclusionfooterlist">
                                            <td>@{{exclu.variable}}</td>
                                            <td>@{{exclu.value}}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="form-group">
                                <h2> Novedades</h2>
                                <table class="table table-sm table-bordered">
                                    <thead>
                                    <tr>
                                        <td scope="col">Ausencia</td>
                                        <td scope="col">Cantidad</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr v-for="asig in fd.Newfooterlist">
                                        <td>@{{asig.variable}}</td>
                                        <td>@{{asig.value}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                                </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                              <h2> Vacaciones</h2>
                                <table class="table table-sm table-bordered">
                                    <thead>
                                    <tr>
                                        <td scope="col">Variable</td>
                                        <td scope="col">Valor</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr v-for="asig in fd.Vacationfooterlist">
                                        <td>@{{asig.variable}}</td>
                                        <td>@{{asig.value}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                              </div>
                              <div class="form-group">
                              <h2> Otros</h2>
                                <table class="table table-sm table-bordered">
                                    <thead>
                                    <tr>
                                        <td scope="col">Variable</td>
                                        <td scope="col">Valor</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr v-for="asig in fd.Otherfooterlist">
                                        <td>@{{asig.variable}}</td>
                                        <td>@{{asig.value}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                              </div>
                                
                            </div>
                            <!-- FIRST ROW -->
                        </div>
                       


                    </div>
                    <div class="tab-pane fade" id="constant" role="tabpanel" aria-labelledby="constant-tab">
                        <br>
                        <p>En este panel, puedes ver las constantes que estan siendo usadas en el Periodo: <strong>@{{fd.DetNamePeriod}}</strong> y en el tipo de corrida: <strong>@{{fd.DetNameTyperun}}</strong> y que se aplican en esta corrida</p>
                                 
                                 <div class="row">
                                 <div class="col-md-3">
                                 <h4>Constante de LCT</h4>
                                 <table class="table table-sm table-bordered">
                                    <thead>
                                    <tr>
                                        <td scope="col">Nombre</td>
                                        <td scope="col">Valor</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr v-for="asigLCT in fd.ConstantfooterLCTlist">
                                        <td>@{{asigLCT.variable}}</td>
                                        <td>@{{asigLCT.value}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                                </div>
                                <div class="col-md-3">
                                <h4>Constantes del CCT</h4>
                                 <table class="table table-sm table-bordered">
                                    <thead>
                                    <tr>
                                        <td scope="col">Nombre</td>
                                        <td scope="col">Valor</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr v-for="asigCCT in fd.ConstantfooterCCTlist">
                                        <td>@{{asigCCT.variable}}</td>
                                        <td>@{{asigCCT.value}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                                </div>
                                <div class="col-md-3">
                                <h4>Constantes de Empresa</h4>
                                 <table class="table table-sm table-bordered">
                                    <thead>
                                    <tr>
                                        <td scope="col">Nombre</td>
                                        <td scope="col">Valor</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr v-for="asigComp in fd.ConstantfooterCompanylist">
                                        <td>@{{asigComp.variable}}</td>
                                        <td>@{{asigComp.value}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                                </div>
                                <div class="col-md-3">
                                <h4>Constantes de Legajo</h4>
                                 <table class="table table-sm table-bordered">
                                    <thead>
                                    <tr>
                                        <td scope="col">Nombre</td>
                                        <td scope="col">Valor</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr v-for="asigEmp in fd.ConstantfooterEmployeelist">
                                        <td>@{{asigEmp.variable}}</td>
                                        <td>@{{asigEmp.value}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                                </div>
                                </div>
                    
                       


                    </div>
                
                </div>


            </div>
            <div class="modal-footer">
                <button v-on:click="remakepayrun" v-if="ff.closedPayruns==false"
                        class="btn btn-default">Correr de Nuevo
                </button>
                <button v-on:click="closepayrun" v-if="ff.openPayruns == true & ff.closedPayruns==false"
                        class="btn btn-warning">Cerrar Corrida
                </button>
                <button v-on:click="deletepayrun" v-if="ff.openPayruns == true & ff.closedPayruns==false"
                        class="btn btn-danger">Eliminar Corrida
                </button>
                <button v-on:click="openpayrun" v-if="ff.openPayruns == false & ff.closedPayruns==true"
                        class="btn btn-success">Reabrir Corrida
                </button>
                <button v-on:click="downloadreceive" class="btn btn-success">Descargar Recibo
                </button>         

                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar Ventana</button>

            </div>
        </div>
    </div>
</div>
<!-- FIN MODAL -->

<!-- Button to Open the Modal -->
<!-- The Modal -->
<div class="modal fade" id="ManualEdition">
  <div class="modal-dialog" width="500px">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Edición de Montos</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <p>Puede editar las cantidades y montos de una corrida manualmente para luego cerrarla, pero si realiza la acción "Correr de Nuevo", estos cambios podrían modificarse y perderia el monto almacenado en esta ventana.</p>
        <br>
                   <table class="table table-bordered table-hover table-sm">
                                <thead>
                                <tr class="table-primary">
                                    <th width="50px" scope="col">Código</th>
                                    <th scope="col">Concepto</th>
                                    <th scope="col">Cantidad Actual</th>

                                    <th width="120px" scope="col">Monto Actual</th>
                                     <th scope="col">Nueva Cantidad</th>
                                    <th width="120px" scope="col">Nuevo Monto</th>
                                  
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="concept in fd.Conceptdetaillist">
                                    <th>@{{concept.codconcept}}</th>
                                    <th style="font-weight:lighter;">@{{concept.nameconcept}}</th>

                                    <th style="font-weight:lighter;">@{{concept.cant}}</th>
                                    <th 
                                        style="font-weight:lighter;">@{{concept.amount.toFixed(2)}}</th>
                                   <th style="font-weight:lighter;"> <input type="input" v-model="fd.ConceptDetailCant" placeholder="Nueva Cantidad" class="form-control"></th>
                                    <th style="font-weight:lighter;"> <input type="input" v-model="fd.ConceptDetailAmount" placeholder="Nuevo Monto" class="form-control"></th>
                                    <!--COLUMNAS DE PAGO -->


                                </tr>
                       
                                </tbody>
                            </table>
                            <p>Si desea mantener la misma cantidad o monto actual, por favor escribalo de nuevo</p>
      
      </div>


      <!-- Modal footer -->
      <div class="modal-footer">
            <button v-on:click="saveconceptmanual" v-if="ff.closedPayruns==false"
                        class="btn btn-success">Guardar Nuevo Monto
                </button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>

    </div>
  </div>
</div>
