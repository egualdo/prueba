<div id="EmployeeAbsences">

    <button type="button" v-on:click="openmodaltonew" class="btn btn-primary">Agregar</button>

    <!-- MODAL -->
    <div id="ModalAbsences" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"
         class="modal text-left">
        <div role="document" class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 id="exampleModalLabel" class="modal-title">Editar Elemento</h5>
                    <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span
                                aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <p>Indica los datos solicitados</p>

                    <div class="row">
                        <div class="col-md-6">

                            <div class="form-group">

                                <label>Tipo de Ausencia</label>

                                {{--   <select v-model="fd.AbsenceType" class="form-control">
                                   <option v-for="AbsenceType in fd.AbsenceTypelist" v-bind:value="AbsenceType.idn">
                                      @{{ AbsenceType.name }}
                                   </option>

                                </select> --}}
                                <v-select v-model="fd.AbsenceType" :options="fd.AbsenceTypelist"></v-select>


                            </div>
                            <div class="form-group">
                                <label>Fecha Desde</label>
                                <input v-model="fd.Startdate" id="EABStarDate" type="text" placeholder=""
                                       class="form-control">
                            </div>

                            <div class="form-group">
                                <label>Fecha Hasta</label>
                                <input v-model="fd.Finishdate" id="EABFinishDate" type="text" placeholder=""
                                       class="form-control">
                            </div>


                        </div>
                        <div class="col-md-6">

                            <div class="form-group">
                                <label>Cantidad</label>
                                <input v-model="fd.Cant" v-on:click="calculardiasausencias" type="text" placeholder=""
                                       class="form-control">
                            </div>

                            <div class="form-group">
                                <label>Observación</label>
                                <input v-model="fd.Observation" type="text" placeholder="" class="form-control">
                            </div>


                        </div>

                    </div>
                    <div class="modal-footer">
                        <div v-if="ff.save == true">
                            <button v-on:click="saveAbsences" class="btn btn-sucess btn-fill pull-left">Guardar</button>
                            <button type="button" class="btn btn-primary btn-fill pull-rigth" data-dismiss="modal">
                                Cerrar
                            </button>
                        </div>

                        <div v-else-if="ff.edit == true">
                            <button v-on:click="updateAbsences" class="btn btn-info btn-fill pull-left">Editar</button>
                            <button v-on:click="deleteAbsences" class="btn btn-warning btn-fill pull-rigth">Eliminar
                            </button>
                            <button type="button" class="btn btn-primary btn-fill pull-rigth" data-dismiss="modal">
                                Cerrar
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- FIN MODAL -->
    </div>
</div>

