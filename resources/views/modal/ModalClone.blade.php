<div id="ModalClone" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"
     class="modal text-left">
    <div role="document" class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="exampleModalLabel" class="modal-title">Clonar Legajo </h5>
                <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span
                            aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <p>Por favor Indica los datos solicitados</p>

                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label>Nro Legajo</label>
                            <input type="input" v-model="fd.cloCod" placeholder="Código de Legajo"
                                   class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Primer Nombre</label>
                            <input required type="input" v-model="fd.cloFirstname"
                                   placeholder="Primer Nombre" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Apellido</label>
                            <input required type="input" v-model="fd.cloLastname"
                                   placeholder="Segundo Nombre" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Fecha de Nacimiento</label>
                            <!--<input type="input" v-model="fd.Birthdate" placeholder="Fecha de Nacimiento" class="form-control">-->
                            <input required placeholder="Fecha de Nacimiento"
                                   class="form-control" type="text" id="cloDPickerBirthdate"
                                   v-model="fd.cloBirthdate">
                            <!--<calendar value="fd.Birthdate" :disabled-days-of-week="fd.disabled" :format="fd.format" :clear-button="fd.clear" :placeholder="fd.placeholder" ></calendar>-->
                        </div>
                        <div class="form-group">
                            <label>Estado Civil</label>
                            <select required v-model="fd.cloCivilstatus" class="form-control">
                                <option v-for="Civilsta in fd.Civilstatuslist"
                                        v-bind:value="Civilsta.id">
                                    @{{ Civilsta.label }}
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">

                            <label>Genero</label>
                            <select required v-model="fd.cloGender" @change="calcularcuil" class="form-control">
                                <option value="1">Masculino</option>
                                <option value="0">Femenino</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>DNI</label>
                            <input required type="input" {{-- v-mask="'##.###.###'"--}} v-model="fd.cloDni"
                                   @input="calcularcuil" placeholder="DNI del legajo" class="form-control">
                        </div>

                        <div class="form-group">
                            <label>Cuil</label>
                            <input required type="input" {{-- v-mask="'##-########-#'"--}} v-model="fd.cloCuil"
                                   placeholder="CUIL del legajo" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Nacionalidad</label>
                            <select required v-model="fd.cloNationality" class="form-control">
                                <option v-for="Natio in fd.Nationalitylist"
                                        v-bind:value="Natio.id">
                                    @{{ Natio.label }}
                                </option>
                            </select>
                        </div>

                    </div>


                </div>
                <div class="modal-footer">
                    <div>
                        <button class="btn btn-success btn-fill pull-left" v-on:click="clonar">Clonar</button>
                        <button type="button" class="btn btn-primary btn-fill pull-rigth" data-dismiss="modal">Cerrar
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- FIN MODAL -->
</div>
</>

