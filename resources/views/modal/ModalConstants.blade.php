   
<div id="EmployeeConstants">

 <button type="button" v-on:click="openmodaltonew" class="btn btn-primary">Agregar</button>

 <!-- MODAL -->
 <div id="ModalConstants" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal text-left">
  <div role="document" class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 id="exampleModalLabel" class="modal-title">Editar Elemento</h5>
        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
      </div>
      <div class="modal-body">
        <p>Indica los datos solicitados</p>

        <div class="row">
          <div class="col-md-6">

           <div class="form-group">       

            <label>Periodo</label>             
            {{--  <select v-model="fd.Period" class="form-control">
               <option v-for="Period in fd.Periodlist" v-bind:value="Period.idn">
                @{{ Period.title }}
              </option>
            </select> --}}
            <v-select v-model="fd.Period" :options="fd.Periodlist"></v-select>
          </div>
              <div class="form-group">       

            <label>Tipo</label>             
            {{--  <select v-model="fd.Period" class="form-control">
               <option v-for="Period in fd.Periodlist" v-bind:value="Period.idn">
                @{{ Period.title }}
              </option>
            </select> --}}
            <v-select v-model="fd.Typerun" :options="fd.Typerunlist"></v-select>
          </div>
          <div class="form-group">
           <label>Constante</label>          
       {{--     <select v-model="fd.Constant" class="form-control">
             <option v-for="Constant in fd.Constantlist" v-bind:value="Constant.idn">
              @{{ Constant.name }}
            </option>               
          </select>  --}}
           <v-select v-model="fd.Constant" :options="fd.Constantlist"></v-select>
        </div>


      </div> 
      <div class="col-md-6">

       <div class="form-group">       
        <label>Valor</label>
        <input v-model="fd.Value" type="text" placeholder="" class="form-control">
      </div>

      <div class="form-group">       
        <label>Comentario</label>
        <input v-model="fd.Comment" type="text" placeholder="" class="form-control">
      </div>




    </div>

  </div>
  <div class="modal-footer">
    <div v-if="ff.save == true">
      <button v-on:click="saveConstant" class="btn btn-sucess btn-fill pull-left">Guardar</button>
      <button type="button" class="btn btn-primary btn-fill pull-rigth" data-dismiss="modal">Cerrar</button>
    </div>

    <div v-else-if="ff.edit == true">
      <button v-on:click="updateConstant" class="btn btn-info btn-fill pull-left">Editar</button>
      <button v-on:click="deleteConstant" class="btn btn-warning btn-fill pull-rigth">Eliminar</button>
      <button type="button" class="btn btn-primary btn-fill pull-rigth" data-dismiss="modal">Cerrar</button>
    </div>
  </div>
</div>
</div>
</div>
<!-- FIN MODAL -->
</div>
</div>

