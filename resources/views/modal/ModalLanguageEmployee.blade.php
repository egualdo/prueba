   
      <div id="LanguageEmployee">

 <button type="button" v-on:click="openmodaltonew" class="btn btn-primary">Agregar Lenguaje</button>
   
       <!-- MODAL -->
       <div id="ModalLanguageEmployee" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal text-left">
        <div role="document" class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 id="exampleModalLabel" class="modal-title">Editar Elemento</h5>
              <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
              <p>Indica los datos solicitados</p>
             
             <div class="row">
              <div class="col-md-6">
                 <div class="form-group">
                <label>Languaje</label>
          
                   {{-- <select v-model="fd.Language" class="form-control">
                      <option v-for="Lang in fd.Languagelist" v-bind:value="Lang.idn">
                      @{{ Lang.name }}
                      </option>                 
                   </select> --}}
                    <v-select v-model="fd.Language" :options="fd.Languagelist"></v-select>
              </div>
          
              </div>
              <div class="col-md-6">
                 <div class="form-group">
                <label>Nivel</label>
                <select v-model="fd.Level" class="form-control">
                  <option value="1">Bajo</option>
                  <option value="2">Medio</option>
                  <option value="3">Alto</option>
                </select>
              </div>
             

              </div>

             </div> 
             
              
              
            </div>
            <div class="modal-footer">
              <div v-if="ff.save == true">
                <button v-on:click="guardarLanguage" class="btn btn-sucess btn-fill pull-left">Guardar</button>
                <button type="button" class="btn btn-primary btn-fill pull-rigth" data-dismiss="modal">Cerrar</button>
              </div>

              <div v-else-if="ff.edit == true">
                <button v-on:click="editarLanguage" class="btn btn-info btn-fill pull-left">Editar</button>
                <button v-on:click="borrarLanguage" class="btn btn-warning btn-fill pull-rigth">Eliminar</button>
                <button type="button" class="btn btn-primary btn-fill pull-rigth" data-dismiss="modal">Cerrar</button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- FIN MODAL -->
    </div>



