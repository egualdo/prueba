   
      <div id="CareerEmployees">

 <button type="button" v-on:click="openmodaltonew" class="btn btn-primary">Agregar Cuenta</button>
   
       <!-- MODAL -->
       <div id="ModalCareerEmployee" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal text-left">
        <div role="document" class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 id="exampleModalLabel" class="modal-title">Editar Elemento</h5>
              <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
              <p>Indica los datos solicitados</p>
             
             <div class="row">
              <div class="col-md-6">
                 <div class="form-group">
                <label>Carrera</label>
          
                
                 

                 {{--   <select v-model="fd.Career" class="form-control">
                                <option v-for="Care in fd.Careerlist" v-bind:value="Care.idn">
                                  @{{ Care.name }}
                                </option>                 
                   </select> --}}
                    <v-select v-model="fd.Career" :options="fd.Careerlist"></v-select>
                  
                </select>
              </div>
              <div class="form-group">       
                <label>Título</label>
                <input v-model="fd.Title" type="text" placeholder="" class="form-control">
              </div>
              
              </div>
              <div class="col-md-6">
                 <div class="form-group">
                <label>Fecha de Inicio</label>
                <input v-model="fd.StartDate" id="DPickerStartdate" type="text" placeholder="" class="form-control">
              </div>
              <div class="form-group">       
                <label>Fecha de Fin</label>
                <input v-model="fd.FinishDate" id="DPickerFinishdate" type="text" placeholder="" class="form-control">
              </div>         
         

              </div>

             </div> 
             
              
              
            </div>
            <div class="modal-footer">
              <div v-if="ff.save == true">
                <button v-on:click="guardarCareer" class="btn btn-sucess btn-fill pull-left">Guardar</button>
                <button type="button" class="btn btn-primary btn-fill pull-rigth" data-dismiss="modal">Cerrar</button>
              </div>

              <div v-else-if="ff.edit == true">
                <button v-on:click="editarCareer" class="btn btn-info btn-fill pull-left">Editar</button>
                <button v-on:click="borrarCareer" class="btn btn-warning btn-fill pull-rigth">Eliminar</button>
                <button type="button" class="btn btn-primary btn-fill pull-rigth" data-dismiss="modal">Cerrar</button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- FIN MODAL -->
    </div>



