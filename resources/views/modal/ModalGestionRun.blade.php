<div id="ModalGestionRun" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"
     class="modal ">
    <div role="document" class="modal-dialog">
        <div class="modal-content">
            <div class="loader" style="display: none">
                <div class="spinner">
                    <div class="bounce1"></div>
                    <div class="bounce2"></div>
                    <div class="bounce3"></div>
                </div>
            </div>
            <div class="modal-header">
                <h5 v-show="ff.close === true" class="modal-title">Cerrar Corridas</h5>
                <h5 v-show="ff.reopen === true" class="modal-title">Reabrir Corridas</h5>
                <h5 v-show="ff.delete === true" class="modal-title">Borrar Corridas</h5>
                <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span
                            aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Tipo de Periodo</label>
                            <select v-model="fd.Typeperiod" v-on:change="TypePeriodOnChange"
                                    class="form-control">
                                <option value="1">Mensual</option>
                                <option value="2">Quincenal</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Periodo</label>
                            <select v-model="fd.Period" class="form-control">
                                <option value="">Seleccione ...</option>
                                <option v-for="Perio in fd.Periodlist" v-bind:selected="fd.Period"
                                        v-bind:value="Perio.idn">
                                    @{{ Perio.title }}
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Tipo de Corrida</label>
                            <select v-model="fd.Typerun" class="form-control">
                                <option value="">Seleccione ...</option>
                                <option v-for="Type in fd.Typerunlist" v-bind:selected="fd.TypeRun"
                                        v-bind:value="Type.id">
                                    @{{ Type.label }}
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" v-show="ff.delete === true" v-on:click="gestionRun('delete')"
                        class="btn btn-warning btn-fill pull-rigth">Eliminar
                </button>
                <button type="button" v-show="ff.close === true" v-on:click="gestionRun('close')"
                        class="btn btn-primary btn-fill pull-rigth">Cerrar
                </button>
                <button type="button" v-show="ff.reopen === true" v-on:click="gestionRun('reopen')"
                        class="btn btn-info btn-fill pull-left">Reabrir
                </button>
            </div>
        </div>
    </div>
    <!-- FIN MODAL -->
</div>