<div id="BankAccountEmployee">

    <button type="button" v-on:click="openmodaltonew" class="btn btn-primary">Agregar Cuenta Bancaria</button>

    <!-- MODAL -->
    <div id="ModalBankAccountEmployee" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true" class="modal text-left">
        <div role="document" class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 id="exampleModalLabel" class="modal-title">Editar Elemento</h5>
                    <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span
                                aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <p>Indica los datos solicitados</p>
                    <div class="row">

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Tipo de Pago</label>

                                    <select v-model="fd.Paymentmethod" class="form-control">
                                        <option v-for="Payment in fd.Paymentlist" v-bind:value="Payment.idn">
                                            @{{ Payment.name }}
                                        </option>
                                    </select>
                                </div>
                            </div>

                        <div class="col-md-6"  v-show="fd.Paymentmethod === 3 || fd.Paymentmethod === 4">
                            <div class="form-group">
                                <label>Sucursal <span style="color: red;">*</span></label>
                                <input v-model="fd.Branch" type="text" placeholder="" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Tipo de cuenta <span style="color: red;">*</span></label>

                                <select v-model="fd.Accounttype" class="form-control">
                                    <option value="1">Caja de ahorro</option>
                                    <option value="2">Cuenta Corriente</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6" v-show="fd.Paymentmethod === 3 || fd.Paymentmethod === 4">
                            <div class="form-group">
                                <label>Banco <span style="color: red;">*</span></label>

                                <select v-model="fd.Bank" class="form-control">
                                    <option v-for="Bank in fd.Banklist" v-bind:value="Bank.idn">
                                        @{{ Bank.name }}
                                    </option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Numero de Cuenta <span style="color: red;">*</span></label>
                                <input v-model="fd.Numberaccount" v-mask="'#####################'" type="text"
                                       placeholder="" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>CBU <span style="color: red;">*</span></label>
                                <input v-model="fd.Cbu" type="text" placeholder="" class="form-control">
                            </div>

                        </div>
                        <div class="col-md-6" v-show="fd.Paymentmethod === 2 || fd.Paymentmethod === 3 || fd.Paymentmethod === 4 || fd.Paymentmethod === 5">
                            <div class="form-group">
                                <label>Porcentaje </label>
                                <h6>si no se ingresa se considera el 100%</h6>
                                <input v-model="fd.Percentage" v-mask="'###'" v-on:input="validar_cien" type="text" placeholder=""
                                       class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12" v-show="fd.Paymentmethod === '' || fd.Paymentmethod === 1">
                            <h2>Por Favor seleccione un método de pago</h2>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <div v-if="ff.save == true">
                            <button v-on:click="guardarBankAccount" class="btn btn-sucess btn-fill pull-left">Guardar
                            </button>
                            <button type="button" class="btn btn-primary btn-fill pull-rigth" data-dismiss="modal">
                                Cerrar
                            </button>
                        </div>

                        <div v-else-if="ff.edit == true">
                            <button v-on:click="editarBankAccount" class="btn btn-info btn-fill pull-left">Editar
                            </button>
                            <button v-on:click="borrarBankAccount" class="btn btn-warning btn-fill pull-rigth">
                                Eliminar
                            </button>
                            <button type="button" class="btn btn-primary btn-fill pull-rigth" data-dismiss="modal">
                                Cerrar
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- FIN MODAL -->
    </div>

</div>

