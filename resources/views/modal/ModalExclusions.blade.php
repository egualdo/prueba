   
      <div id="EmployeeExclusions">

 <button type="button" v-on:click="openmodaltonew" class="btn btn-primary">Agregar</button>

       <!-- MODAL -->
       <div id="ModalExclusions" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal text-left">
        <div role="document" class="modal-dialog" style="min-width: 700px">
          <div class="modal-content">
            <div class="modal-header">
              <h5 id="exampleModalLabel" class="modal-title">Editar Elemento</h5>
              <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
              <p>Indica los datos solicitados</p>
             
             <div class="row">
              <div class="col-md-4">
                 
                     
                  
                 <label>Periodo</label>
          
                 {{--  <select v-model="fd.Period" class="form-control">
                   <option v-for="Perio in fd.Periodlist" v-bind:value="Perio.idn">
                      @{{ Perio.title }}
                   </option>
               
                </select> --}}
                 <v-select v-model="fd.Period" :options="fd.Periodlist"></v-select>
             
                
                
           
                  
                   
              </div> 
                <div class="col-md-4">
                 
                   
                  
                 <label>Tipo de corrida</label>
          
                 {{--  <select v-model="fd.Period" class="form-control">
                   <option v-for="Perio in fd.Periodlist" v-bind:value="Perio.idn">
                      @{{ Perio.title }}
                   </option>
               
                </select> --}}
                <v-select v-model="fd.Typerun" :options="fd.Typerunlist"></v-select>
             
                
               
           
                  
                   
              </div>
              <div class="col-md-4">
               
                        
                  
                 <label>Concepto</label>
          
                  {{-- <select v-model="fd.Concept" class="form-control">
                   <option v-for="concept in fd.Conceptlist" v-bind:value="concept.idn">
                    @{{ concept.name }}
                   </option>
               
                </select> --}}
                 <v-select v-model="fd.Concept" :options="fd.Conceptlist"></v-select>
             
                
                
                  
             
             
         
               
                  
              </div>

            </div>
            <div class="modal-footer">
              <div v-if="ff.save == true">
                <button v-on:click="saveExclusion" class="btn btn-sucess btn-fill pull-left">Guardar</button>
                <button type="button" class="btn btn-primary btn-fill pull-rigth" data-dismiss="modal">Cerrar</button>
              </div>

              <div v-else-if="ff.edit == true">
                <button v-on:click="updateExclusion" class="btn btn-info btn-fill pull-left">Editar</button>
                <button v-on:click="deleteExclusion" class="btn btn-warning btn-fill pull-rigth">Eliminar</button>
                <button type="button" class="btn btn-primary btn-fill pull-rigth" data-dismiss="modal">Cerrar</button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- FIN MODAL -->
    </div>
</div>

