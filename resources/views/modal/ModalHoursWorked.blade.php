<div id="vue-HoursWorked">

    <button type="button" v-on:click="openmodaltonew" class="btn btn-primary">Agregar</button>

    <!-- MODAL -->
    <div id="ModalHoursWorked" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"
         class="modal text-left">
        <div role="document" class="modal-dialog" style="min-width: 800px">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 id="exampleModalLabel" class="modal-title">Agregar Horas Trabajadas</h5>
                    <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span
                                aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <p>Indica los datos solicitados</p>

                    <div class="row">
                        <div class="col-md-4">

                            <div class="form-group">
                                <label>Fecha Desde</label>
                                <input v-model="fd.Startdate" id="HoursWorkedStartdate"  type="text" value="" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Hora Desde</label>
                                <input v-model="fd.StartHour" id="HoursWorkedStartdatehour" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-4">

                            <div class="form-group">
                                <label>Fecha Hasta</label>
                                <input v-model="fd.Finishdate" id="HoursWorkedFinishdate" type="text" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>Hora Hasta</label>
                                <input v-model="fd.FinishHour" id="HoursWorkedFinishdatehour" type="text" class="form-control">
                            </div>


                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Cantidad de Horas</label>
                                <input v-model="fd.Cant" type="number"
                                       value="1" class="form-control">
                            </div>
                            <div class="form-group">

                                <label>Observación (Opcional)</label>
                                <textarea v-model="fd.Observation" class="form-control"></textarea>

                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <div v-if="ff.save == true">
                            <button v-on:click="guardarHorasTrabajadas" class="btn btn-success btn-fill pull-left">
                                Guardar
                            </button>
                            <button type="button" class="btn btn-primary btn-fill pull-rigth" data-dismiss="modal">
                                Cerrar
                            </button>
                        </div>

                        <div v-else-if="ff.edit == true">
                            <button v-on:click="editarHorasTrabajadas" class="btn btn-info btn-fill pull-left">Editar
                            </button>
                            <button v-on:click="borrarHorasTrabajadas" class="btn btn-warning btn-fill pull-rigth">
                                Eliminar
                            </button>
                            <button type="button" class="btn btn-primary btn-fill pull-rigth" data-dismiss="modal">
                                Cerrar
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- FIN MODAL -->
    </div>
</div>

