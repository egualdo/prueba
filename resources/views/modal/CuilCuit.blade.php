  <!DOCTYPE html>
  <html>
  @include('layouts.heads')
  <body>
    <!-- Side Navbar -->
    @include('layouts.sidebaradmin')
    <div class="page forms-page">
      <!-- navbar-->
      @include('layouts.navbar')
      <div id="vuedata">
       <!-- Modal -->
       <!-- MODAL -->
       <div id="ModalCuilCuit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal text-left">
        <div role="document" class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 id="exampleModalLabel" class="modal-title">Editar Elemento</h5>
              <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
              <p>Indica los datos solicitados</p>
              
             
              <div class="form-group">       
                <label>DNI</label>
                <input v-model="fd.Dni" type="text" placeholder="" class="form-control">
              </div>
              
               <div class="form-group">
                <label>Cuil o Cuit</label>
                <input v-model="fd.CuilCuit" type="text" placeholder="" class="form-control" >
                   <span>CuilCuit: {{ msg }}</span>
              </div>
            </div>
            <div class="modal-footer">
              
                <button v-on:click="calcular" class="btn btn-sucess btn-fill pull-left">Calcular</button>
                <button type="button" class="btn btn-primary btn-fill pull-rigth" data-dismiss="modal">Cerrar</button>
            

             
            </div>
          </div>
        </div>
      </div>
      <!-- FIN MODAL -->
      <!-- FIN DE LA MODAL -->

      <!-- CABECERA GRIS-->
      <div class="breadcrumb-holder fixed-top">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-2 border">
            </div>
            <div class="col-md-7 border">
              <!-- TITULO DE VENTANA -->
              <h1> Cuil/Cuit</h1>
            </div>
            <div class="col-md-3">
              <!--<p>Dato de Prueba</p>-->
              <button v-on:click="openmodaltonew" class="btn btn-info btn-fill pull-left">Nuevo Elemento</button>
            </div>

          </div>
        </div>
      </div>
      <!-- FIN DE LA CABECERA GRIS -->
    </div>




    @include('layouts.footer')
  </div>
  @include('layouts.libraries')
  <script src="../js/generic/CuilCuit.js"></script>
</body>
</html>