  <div id="GroupiEmployee">

 <button type="button" v-on:click="openmodaltonew" class="btn btn-primary">Agregar Grupo</button>
   
       <!-- MODAL -->
       <div id="ModalGroupEmployee" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal text-left">
        <div role="document" class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 id="exampleModalLabel" class="modal-title">Editar Elemento</h5>
              <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
            </div>
              
            <div class="modal-body">
              <p>Indica los datos solicitados</p>
             
             <div class="row">
                 
              <div class="col-md-6">
                 <div class="form-group">
                <label>Grupos</label>
          
               {{--     <select v-model="fd.Group" class="form-control">
                                <option v-for="Group in fd.Grouplist" v-bind:value="Group.idn">
                                  @{{ Group.name }}
                                </option>                 
                   </select> --}}
                    <v-select v-model="fd.Group" :options="fd.Grouplist"></v-select>
              </div>
              </div>
                 
              <div class="col-md-6">
                
              </div>

             </div> 
             
            </div>
              
            <div class="modal-footer">
              <div v-if="ff.save == true">
                <button v-on:click="guardarGroup" class="btn btn-sucess btn-fill pull-left">Guardar</button>
                <button type="button" class="btn btn-primary btn-fill pull-rigth" data-dismiss="modal">Cerrar</button>
              </div>

              <div v-else-if="ff.edit == true">
                <button v-on:click="editarGroup" class="btn btn-info btn-fill pull-left">Editar</button>
                <button v-on:click="borrarGroup" class="btn btn-warning btn-fill pull-rigth">Eliminar</button>
                <button type="button" class="btn btn-primary btn-fill pull-rigth" data-dismiss="modal">Cerrar</button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- FIN MODAL -->
    </div>
      
