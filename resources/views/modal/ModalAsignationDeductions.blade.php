   
<div id="vue-Asignations">

 <button type="button" v-on:click="openmodaltonew" class="btn btn-primary">Agregar</button>

 <!-- MODAL -->
 <div id="ModalAsignationDeductions" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal text-left">
  <div role="document" class="modal-dialog" style="min-width: 800px">
    <div class="modal-content">
      <div class="modal-header">
        <h5 id="exampleModalLabel" class="modal-title">Gestionar Asignaciones Manuales</h5>
        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
      </div>
      <div class="modal-body">
        <p>Indica los datos solicitados</p>

        <div class="row">
          <div class="col-md-4">

            <div class="form-group">       

             <label>Concepto</label>

             <v-select v-model="fd.Concept" :options="fd.Conceptlist"></v-select>


          </div>
           <div class="form-group">       
         
         <label>Periodo</label>
          <v-select v-model="fd.Period" :options="fd.Periodlist"></v-select>
        
      </div>
        <div class="form-group">       
         
             <div class="form-group">       
                      <label>Tipo de Corrida</label>
                   {{--    <select v-model="fd.Typerun" class="form-control">
                        <option value="">Seleccione ...</option>
                           <option v-for="trun in fd.Typerunlist" v-bind:value="trun.idn">
                                  @{{ trun.name }}
                                </option>   
                          </select> --}}
                           <v-select v-model="fd.Typerun" :options="fd.Typerunlist"></v-select>
                    </div>


      
       </div> 
     </div>
       <div class="col-md-4">




      <div class="form-group">       
        <label>Fecha Desde</label>
        <input v-model="fd.Startdate" id="DPStardate" type="text" placeholder="" class="form-control">
      </div>

      <div class="form-group">       
        <label>Fecha Hasta</label>
        <input v-model="fd.Finishdate" id="DPFinishdate" type="text" placeholder="" class="form-control">
      </div>
      <div class="form-group">       

        <label>Tipo Especial</label>

      {{--   <select v-model="fd.Specialtype" class="form-control">
         <option v-for="special in fd.Specialtypelist" v-bind:value="special.idn">
          @{{ special.name }}
        </option>

      </select> --}}
      <v-select v-model="fd.Specialtype" :options="fd.Specialtypelist"></v-select>

    </div>

  </div>
  <div class="col-md-4">
        <div class="form-group">       

           <label>Descripción (Opcional)</label>
           <input v-model="fd.Description" type="text" placeholder="" class="form-control">

         </div>
         <div class="form-group">       

           <label>Cantidad</label>
           <input v-model="fd.Cant" v-on:click="calculardiasasignaciones" type="text" placeholder="" class="form-control">

         </div>
         <div class="form-group">       

           <label>Monto</label>
           <input v-model="fd.Amount" type="text" placeholder="" class="form-control">

         </div>
  </div>

</div>
<div class="modal-footer">
  <div v-if="ff.save == true">
    <button v-on:click="guardarAsigDeduc" class="btn btn-sucess btn-fill pull-left">Guardar</button>
    <button type="button" class="btn btn-primary btn-fill pull-rigth" data-dismiss="modal">Cerrar</button>
  </div>

  <div v-else-if="ff.edit == true">
    <button v-on:click="editarAsigDeduc" class="btn btn-info btn-fill pull-left">Editar</button>
    <button v-on:click="borrarAsigDeduc" class="btn btn-warning btn-fill pull-rigth">Eliminar</button>
    <button type="button" class="btn btn-primary btn-fill pull-rigth" data-dismiss="modal">Cerrar</button>
  </div>
</div>
</div>
</div>
</div>
<!-- FIN MODAL -->
</div>
</div>

