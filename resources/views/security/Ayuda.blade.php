<!DOCTYPE html>
<html>
@include('layouts.heads')
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
 
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
</head>
<body>
  <!-- Side Navbar -->
  @include('layouts.sidebaradmin')
  <div class="page forms-page">a
    <!-- navbar-->
     <div id="vuedata">
    @include('layouts.navbar')
    <!-- CABECERA GRIS-->
    <div class="breadcrumb-holder fixed-top">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-2 border">
          </div>
          <div class="col-md-3 border">

            <h1><strong><i class="icon-user"></i></i>  Ayuda<strong></h1>
          </div>
          <div class="col-md-2">
            <p></p>
          </div>
         
            
         
           

        </div>
      </div>
    </div>
    <!-- FIN DE LA CABECERA GRIS -->
    <section class="forms">
      <div class="container-fluid">

        <!-- TARJETA PADRE -->
        <div class="card">
          <!-- Titulo Tarjeta Padre-->

          <!-- Tabs de Navegacion -->
           <i class="fa fa-question-circle" aria-hidden="true"></i>
          <ul class="nav nav-tabs flex-wrap" role="tablist">
            <li class="nav-item">

             
            </li>        
          </ul>


          <!--Cuerpo de Tarjeta Padre-->
          <div class="card-body">
            <!-- Contenido Total de La navegacion-->
            <div class="tab-content">
              <!-- Primera Pestaña -->
              <div role="tabpanel" class="tab-pane active" id="Information">
             <div class="card" >
                  <!-- Cabecera de Tarjeta Hija -->
  <div class="card-header">
    <h2> Soporte</h2>
  </div>
                  <!-- Cuerpo de Tarjeta Hija-->
  <div class="card-body">  
    <div class="row">
                      <!-- Division del cuerpo en 3 columns -->
     <div id="TablaAyuda" class="container">
  <p>Aqui encontraras información de ayuda en cuanto al sistema</p>            
  <table class="table table-striped">
    <thead>
      <tr>
        <th>Descripción</th>
        <th>Link</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Video Tutoriales</td>
        <td>  <li> <a href="../master/accountingaccount">Link para ver video tutorial</a></li>  </td>
      </tr>
      <tr>
        <td>Manual de usuario</td>
        <td> <li> <a href="../master/accountingaccount">Link para ver manual de usuario</a></li>  </td>
      </tr>
      <tr>
        <td>Documentación</td>
        <td> <li> <a href="../master/accountingaccount">Link para ver documentacion del sistema</a></li>  </td>
      </tr>
        <tr>
        <td>Contacto con soporte tecnico</td>
        <td> <li> <a href="../master/accountingaccount">Link para establecer contacto con soporte tecnico</a></li>  </td>
      </tr>
    </tbody>
  </table>
</div>
    </div>
  </div>
</div>



              <!-- FIN de Tarjeta Hija -->

              <!-- Nueva Tarjeta hija -->

                                              
          <!-- FIN DE LA MODAL -->
              </div>
            </div>
          <br>
          </div>
          <!-- FIN DE PRIMERA PESTAÑA -->
        </div>
      </div>
        <br>
        
        </section>
<br>
<br>
<br>


@include('layouts.footer')
</div>
</div>
@include('layouts.libraries')

<script src="../js/generic/generic.js" type="text/javascript"></script>
<script src="../js/securityjs/ayuda.js"></script>
</body>
</html>
