<!DOCTYPE html>
<html>
@include('layouts.heads')
<body>
<!-- Side Navbar -->
@include('layouts.sidebaradmin')
<div class="page forms-page">a
    <!-- navbar-->
    <div id="vuedata">
    @include('layouts.navbar')
    <!-- CABECERA GRIS-->
        <div class="breadcrumb-holder fixed-top">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-2 border">
                    </div>
                    <div class="col-md-5 border">
                        <h1>Perfil</h1>
                    </div>
                    <div class="col-md-2">
                        <p></p>
                    </div>


                    <div class="col-md-3">
                        <button v-on:click="editar" class="btn btn-info btn-fill pull-left">Actualizar</button>
                    </div>

                </div>
            </div>
        </div>
        <!-- FIN DE LA CABECERA GRIS -->
        <section class="forms">
            <div class="container-fluid">

                <!-- TARJETA PADRE -->
                <div class="card">
                    <!-- Titulo Tarjeta Padre-->

                    <!-- Tabs de Navegacion -->
                    <ul class="nav nav-tabs flex-wrap" role="tablist">
                        <li class="nav-item active">
                            <a class="nav-link active" href="#Information" role="tab" data-toggle="tab">Perfil</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " href="#Image" role="tab" data-toggle="tab">Imagen de Perfil</a>
                        </li>
                    </ul>


                    <!--Cuerpo de Tarjeta Padre-->
                    <div class="card-body">
                        <!-- Contenido Total de La navegacion-->
                        <div class="tab-content">
                            <!-- Primera Pestaña -->
                            <div role="tabpanel" class="tab-pane active" id="Information">
                                <div class="card">
                                    <!-- Cabecera de Tarjeta Hija -->
                                    <div class="card-header">
                                        <h2> Datos basica</h2>
                                    </div>
                                    <!-- Cuerpo de Tarjeta Hija-->
                                    <div class="card-body">
                                        <div class="row">
                                            <!-- Division del cuerpo en 3 columns -->
                                            <div class="col-md-7">
                                                <div class="form-group">
                                                    <label>Username</label>
                                                    <input type="input" placeholder="Nombre" v-model="fd.Username"
                                                           class="form-control" readonly="readonly">
                                                </div>
                                                <div class="form-group">
                                                    <label>Rol</label>
                                                    <input type="input" placeholder="Rol" v-model="fd.Rol"
                                                           class="form-control" readonly="readonly">
                                                </div>
                                                <div class="form-group">
                                                    <label>Contraseña</label>
                                                    <input type="input" placeholder="Contraseña" v-model="fd.Password"
                                                           class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>Repetir Contraseña</label>
                                                    <input type="input" placeholder="Contraseña" v-model="fd.Password1"
                                                           class="form-control">
                                                </div>

                                                <div class="form-group">

                                                    <input type="input" hidden="true" enabled="false"
                                                           v-model="fd.IdnRol" class="form-control">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- FIN de Tarjeta Hija -->

                                <!-- Nueva Tarjeta hija -->


                                <!-- FIN DE LA MODAL -->
                            </div>

                            <div role="tabpanel" class="tab-pane" id="Image">
                                <div class="card">
                                    <!-- Cabecera de Tarjeta Hija -->
                                    <div class="card-header">
                                        <h2> Avatar</h2>
                                    </div>

                                    <div class="card-body">
                                        <div class="row">

                                            <div class="col-md-12">
                                                <center><img v-show="fd.AvatarImg" :src="fd.AvatarImg"  alt="" style="display: none; width: 30%;    border-radius: 50%;" id="ImgAvatar"></center>
                                                <center><img v-show="!fd.AvatarImg" src="../img/default-avatar.png"  alt="" style="display: none;    border-radius: 50%; width: 30%" id="FakeAvatar"></center>
                                                <center>
                                                    <label for="AvatarInput" class="btn btn-success">Buscar Imagen</label>
                                                    <input type="file" style="display: none;" id="AvatarInput">
                                                </center>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- FIN de Tarjeta Hija -->

                                <!-- Nueva Tarjeta hija -->


                                <!-- FIN DE LA MODAL -->
                            </div>
                        </div>
                        <br>
                    </div>
                    <!-- FIN DE PRIMERA PESTAÑA -->
                </div>
            </div>
            <br>

        </section>
        <br>
        <br>
        <br>


        @include('layouts.footer')
    </div>
</div>
@include('layouts.libraries')

<script src="../js/generic/generic.js" type="text/javascript"></script>
<script src="../js/securityjs/perfil.js"></script>
</body>
</html>
