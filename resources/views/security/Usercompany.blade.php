@section('FatherSeguridad','true')
@section('ChildSeguridad','show')
<!DOCTYPE html>
  <html>
  @include('layouts.heads')
  <body>
    <!-- Side Navbar -->
    @include('layouts.sidebaradmin')
    <div class="page forms-page">
      <!-- navbar-->
      @include('layouts.navbar')
      <div id="vuedata">
       <!-- Modal -->
       
       <!-- MODAL -->
       <div id="ModalEdicion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal text-left">
        <div role="document" class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 id="exampleModalLabel" class="modal-title">Editar Elemento</h5>
              <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true"></span></button>
            </div>
            <div class="modal-body">
              <p>Indica los datos solicitados</p>

              <div class="form-group">       
                <label>Usuario</label>
                <select v-model="fd.User" class="form-control">
                  <option value="">Seleccione un Usuario</option>
                  <option v-for="Useroptions in Useroptions" v-bind:value="Useroptions.idn">
                    @{{ Useroptions.username }}
                  </option>
                </select>
              </div>

              <div v-show="fd.Asignation != 2" class="form-group">       
                <label>Empresa</label>
               {{--  <select v-model="fd.Company" class="form-control">
                  <option value="">Seleccione una Empresa</option>
                  <option v-for="Companyoptions in Companyoptions" v-bind:value="Companyoptions.idn">
                    @{{ Companyoptions.name }}
                  </option>
                </select> --}}
                <v-select v-model="fd.Company" :options="Companyoptions"></v-select>
               
              </div>

              <div class="form-group">       
                <label>Tipo de Asignación</label>
                 <select v-model="fd.Asignation" class="form-control">
                  <option v-bind:value="1">Asignación Individual</option>  
                  <option v-bind:value="2">Asignación Masiva</option>               
                </select>             
               
              </div>
              <div v-show="fd.Asignation === 2" class="form-group">       
                <label>La asignación masiva le dara acceso al usuario a todas las empresas, ten cuidado con esta acción.</label>                        
               
              </div>

              
            </div>
            <div class="modal-footer">
              <div v-if="ff.save == true">
                <button v-on:click="guardar" class="btn btn-sucess btn-fill pull-left">Guardar</button>
                <button type="button" class="btn btn-primary btn-fill pull-rigth" data-dismiss="modal">Cerrar</button>
              </div>

              <div v-else-if="ff.edit == true">
                <button v-on:click="editar" class="btn btn-info btn-fill pull-left">Editar</button>
                <button v-on:click="borrar" class="btn btn-warning btn-fill pull-rigth">Eliminar</button>
                <button type="button" class="btn btn-primary btn-fill pull-rigth" data-dismiss="modal">Cerrar</button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- FIN MODAL -->
           <!-- MODAL -->
           <div id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal text-left">
        <div role="document" class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 id="exampleModalLabel" class="modal-title">Eliminación de Asignaciones</h5>
              <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true"></span></button>
            </div>
            <div class="modal-body">
              <p>Eliminar todas las asignaciones para el Usuario</p>

              <div class="form-group">       
                <label>Usuario</label>
                <select v-model="fd.User" class="form-control">
                  <option value="">Seleccione un Usuario</option>
                  <option v-for="Useroptions in Useroptions" v-bind:value="Useroptions.idn">
                    @{{ Useroptions.username }}
                  </option>
                </select>
              </div>
              <p> Con esta se revocara el acceso del usuario a todas las empresas que tenga asignadas</p>
              
            </div>
            <div class="modal-footer">
              
                <button v-on:click="deleteasignations" class="btn btn-sucess btn-fill pull-left">Eliminar Asignacion</button>
                <button type="button" class="btn btn-primary btn-fill pull-rigth" data-dismiss="modal">Cerrar</button>
             

             
            </div>
          </div>
        </div>
      </div>
      <!-- FIN MODAL -->
      
      <!-- FIN DE LA MODAL -->                                          
      <!-- FIN DE LA MODAL -->


      <!-- CABECERA GRIS-->
      <div class="breadcrumb-holder fixed-top">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-2 border">
            </div>
            <div class="col-md-6 border">
              <!-- TITULO DE VENTANA -->
              <h1>Asignacion de usuarios y empresas</h1>
            </div>
            <div class="col-md-2">
              <!--<p>Dato de Prueba</p>-->
              <button v-on:click="openmodaltonew" class="btn btn-info btn-fill pull-left">Nueva Asignacion</button>
           
            </div>
            <div class="col-md-2">
            <button v-on:click="openmodaltodelete" class="btn btn-warning btn-fill pull-left">Eliminar Asignaciones</button>
            
            </div>
          </div>
        </div>
      </div>
      <!-- FIN DE LA CABECERA GRIS -->
    </div>

    <section class="forms">
      <div class="container-fluid">

        <!-- Tarjeta -->
        <div class="card" >
          <!-- Cabecera de Tarjeta  -->
          
          <!-- Cuerpo de Tarjeta -->
          <div class="card-body">  

            <div class="row">

              <table id="TablaUserCompany" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>Idn</th>
                    <th>Usuario</th>
                    <th>Empresa</th>

                  </tr>
                </thead>
              </table>
              <!-- === PANEL DE TABLA === --> 
              <!-- ====== DATATABLE ======= -->
              
              <!-- ====== FIN DATATABLE======= -->
            </div>
          </div>
        </div>
        <!-- FIN de Tarjeta Hija -->
      </div>
    </section>



    @include('layouts.footer')
  </div>
  @include('layouts.libraries')
  <script src="../js/securityjs/usercompany.js"></script>
</body>
</html>