Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');

//FUNCIONES GENERICAS PARA INICIO DE LA PAGINA
 var vm = new Vue({
  methods: 
      {
        cargartabla:function()
        {
          CargarTabla();
        }
      }
    });

vm.cargartabla();


//FUNCIONES EN LA MODAL DE EDICION
var mod = new Vue({
  el: '#vuedata',
  data: {
    globalurl:'../api/v1/concept',
      
    fd:
    {
      Columnoptions: [
          {id: 1, label: 'Pagos'},
          {id: 2, label: 'No Remunerativos'},
          {id: 3, label: 'Deducciones'},
          {id: 4, label: 'Excentos'}
      ],
      Idn:'',
      Code:'',
      Name:'',
      Column:'',
      Order:'',
    },
    ff:
    {
      save:false,
      edit:true,
    }    
    
  },
  methods: {
    guardar: function (event) 
    {
            //Items para ser chequeados si estan nulos
            var items = [mod.fd.Code, mod.fd.Name, mod.fd.Column, mod.fd.Order];
            //Mensajes que se mostraran para cada objeto nulo
            var strings = ['Codigo', 'Nombre', 'una Columna', 'Numero de Orden'];
            //Funcion para chequear estos elementos
            var checked = CheckNulls(items, strings);
            //Si all is fine
            if (checked != 0) {

       swal({
                title: "Confirmación de guardado",
                text: "Estas seguro de guardar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {
                    code:mod.fd.Code,
                    name:mod.fd.Name,
                    column:mod.fd.Column,
                    order:mod.fd.Order                 
                  };
                  //VUE POST
                Vue.http.post(mod.globalurl+"/save",arr).then(response => {
                    // Si todo sale correcto              
                    swal('Guardado Correctamente','','success');
                    $('#ModalEdicion').modal('hide');
                    mod.cleanform();
                   

                    }, response => {
                      //Si no sale correcto
                            swal('Ocurrio un error al guardar','','error')
                            mod.cleanform();
                           
                        });
                       
                    })
                    //FIN VUE POST
           
       }
    },
    editar: function (event) 
    {

      //Items para ser chequeados si estan nulos
            var items = [mod.fd.Code, mod.fd.Name, mod.fd.Column, mod.fd.Order];
            //Mensajes que se mostraran para cada objeto nulo
            var strings = ['Codigo', 'Nombre', 'una Columna', 'Numero de Orden'];
            //Funcion para chequear estos elementos
            var checked = CheckNulls(items, strings);
            //Si all is fine
            if (checked != 0) {
        swal({
                title: "Confirmación de editado",
                text: "Estas seguro de editar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {
                    code:mod.fd.Code,
                    name:mod.fd.Name,
                    column:mod.fd.Column,
                    order:mod.fd.Order                 
                  };
                  //VUE POST
                Vue.http.put(mod.globalurl+"/update/"+ mod.fd.Idn,arr).then(response => {
                    // Si todo sale correcto              
                    swal('Modificada Correctamente','','success');
                    $('#ModalEdicion').modal('hide');
                    mod.cleanform();

                    }, response => {
                      //Si no sale correcto
                            swal('Ocurrio un error al modificar','','error')
                            mod.cleanform();
                           
                        });
                        
                    })
                    //FIN VUE POST
          }    
    },   
    borrar: function (event)
    {
       swal({
                title: "Confirmar Eliminación",
                text: "Estas seguro de eliminar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {name:mod.fd.Name};
                Vue.http.put(mod.globalurl+"/delete/"+ mod.fd.Idn,arr).then(response => {
                    // success callback
                    swal('Eliminado Correctamente','','success');
                $('#ModalEdicion').modal('hide');
                mod.cleanform();

               

            }, response => {
                    swal('Ocurrio un error al eliminar','','error');
                    // error callback
                });
            })
    },
    cleanform: function()
    {
      mod.fd.Idn='';
      mod.fd.Code='';
      mod.fd.Name='';
      mod.fd.Column='';
      mod.fd.Order=0;
       $('#ModalEdicion').modal('hide');
      var table = $('#TablaConcept').dataTable();
      //RECARGA LOS DATOS DE LA TABLA
     table.fnReloadAjax();
     
    },
 
     openmodaltonew:function()
        {
          mod.cleanform();
          $('#ModalEdicion').modal('toggle');
          //OCULTAR BOTONES O MOSTRAR
          mod.ff.save = true;
          mod.ff.edit = false;
        },
        openmodaltoedit:function()
        {
           $('#ModalEdicion').modal('toggle');
          //OCULTAR BOTONES O MOSTRAR
          mod.ff.save = false;
          mod.ff.edit = true;
        }
  }
 
});

   //======INICIO DE FUNCIONES ============
    function CargarTabla()
    {   
      var table = $("#TablaConcept").DataTable({
        //COMPROBACION PARA PINTAR Y CAMBIAR TEXTO DE TABLA
           //Especificacion de Ancho y Alto de la tabla
         "rowCallback": function( row, data, index ) {
             if ( data.idncolumn == "1" ) 
            {
            $('td:eq(3)', row).html( '<b>Pagos</b>' );

            }
            else if (data.idncolumn =="2")
            {
            $('td:eq(3)', row).html( '<b>No Remunerativo</b>' );
         
            }
              else if (data.idncolumn =="3")
            {
            $('td:eq(3)', row).html( '<b>Deducciones</b>' );
           
            }
               else if (data.idncolumn =="4")
            {
            $('td:eq(3)', row).html( '<b>Orden en el Recibo</b>' );
           
            }
      
          },

        "destroy": true,
        "scrollY":        "200px",
        "scrollX":        "1000px",
        "language": {
            "url": "/../Spanish.json"
        },
        //Especificaciones de las Columnas que vienen y deben mostrarse
        "columns" : [
            { data : 'idn' },
            { data : 'cod'},
            { data : 'name'},
            { data : 'idncolumn'},
            { data : 'order'}
                
           
        ],
        //Especificaciones de la URL del servicio
        "ajax": {
            url: '../api/v1/concept',
            dataSrc : ''
        }

    });


      $('#TablaConcept tbody').on( 'click', 'tr', function () {
        
        //OBTENGO LOS VARLOES DE LA TABLA
        mod.fd.Idn = table.row( this ).data().idn;
        mod.fd.Code = table.row( this ).data().cod;
        mod.fd.Name = table.row( this ).data().name;
        mod.fd.Column = mod.fd.Columnoptions[parseInt(table.row( this ).data().idncolumn)-1];
       
        mod.fd.Order = table.row( this ).data().order;   
      
      //ABRO LA MODAL
        mod.openmodaltoedit();

       });

     } 




