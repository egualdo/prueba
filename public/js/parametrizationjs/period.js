Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');
//FUNCIONES GENERICAS PARA INICIO DE LA PAGINA
var vm = new Vue({
    methods: {
        cargarcombocalendar: function() {
            // GET /someUrl
            this.$http.get('../api/v1/calendar').then(response => {
                $.each(response.body, function(i, v) {
                    mod.fd.Calendaroptions.push({
                        id: v.idn,
                        label: v.name
                    });
                });
            }, response => {
                // error callback
            });
        },
        cargarcombotypeperiod: function() {
            // GET /someUrl
            this.$http.get('../api/v1/typeperiod').then(response => {
                $.each(response.body, function(i, v) {
                    mod.fd.Typeperiodoptions.push({
                        id: v.idn,
                        label: v.name
                    });
                });
                // get body data
            }, response => {
                // error callback
            });
        },
     
        cargartabla: function() {
            CargarTabla();
        }
    }
});
vm.cargartabla();
vm.cargarcombocalendar();
vm.cargarcombotypeperiod();
//FUNCIONES EN LA MODAL DE EDICION
var mod = new Vue({
    el: '#vuedata',
    data: {
        globalurl: '../api/v1/period',
        fd: {
            Idn: '',
            Title: '',
            IdnCalendar: '',
            StartDate: '',
            FinishDate: '',
            Year: '',
            Month: '',          
            Sicoss: '',
            Special: '',
            Numperiodmonth: '',
            Namecalendar: '',
            Calendar: '',
            Calendaroptions: [],
            Typeperiodoptions: [],
            Typeperiod: '',
            Nameperiod: '',
            Dias: '',
            SiccossOptions: [{
                id: 1,
                label: 'Activado'
            }, {
                id: 2,
                label: 'Desactivado'
            }],
            SpecialOptions: [{
                id: 1,
                label: 'Especial'
            }, {
                id: 2,
                label: 'No Especial'
            }],
            Monthoptions: [
            {id:1,label:'Enero'},
            {id:2,label:'Febrero'},
            {id:3,label:'Marzo'},
            {id:4,label:'Abril'},
            {id:5,label:'Mayo'},
            {id:6,label:'Junio'},
            {id:7,label:'Julio'},
            {id:8,label:'Agosto'},
            {id:9,label:'Septiembre'},
            {id:10,label:'Octubre'},
            {id:11,label:'Noviembre'},
            {id:12,label:'Diciembre'}
            ],
            NameMonth: '',
            Month1options: [],
            NameMonth1: ''
        },
        ff: {
            save: false,
            edit: true
        }
    },
    mounted() {
        //Formato del Datepicker
        $('#StartDate').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            language: 'es'
        });
        //Obtener Datos del Datepicker
        $("#StartDate").datepicker().on("changeDate", () => {
            //this.startDate = $('#DPickerBirthdate').val();
            mod.fd.StartDate = $('#StartDate').val();
        });
        //Formato del Datepicker
        $('#FinishDate').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            language: 'es'
        });
        //Obtener Datos del Datepicker
        $("#FinishDate").datepicker().on("changeDate", () => {
            //this.startDate = $('#DPickerBirthdate').val();
            mod.fd.FinishDate = $('#FinishDate').val();
        });
    },
    methods: {
        guardar: function(event) {           

             //Items para ser chequeados si estan nulos
            var items = [mod.fd.Title, mod.fd.Calendar, mod.fd.StartDate, mod.fd.FinishDate,
            mod.fd.Year,mod.fd.Typeperiod,mod.fd.Month,mod.fd.Sicoss,mod.fd.Special,mod.fd.Numperiodmonth];
            //Mensajes que se mostraran para cada objeto nulo
            var strings = ['un Titulo', 'un Calendario', 'una Fecha de Inicio', 'una Fecha Final',
            'el Año','el Tipo de Periodo','un Mes','Sicoss','Especial','Cantidad de Periodos'];
            //Funcion para chequear estos elementos
            var checked = CheckNulls(items, strings);
            //Si all is fine
            if (checked != 0) {

            swal({
                title: "Confirmación de guardado",
                text: "Estas seguro de guardar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function() {
                var arr = {
                    //DATOS DE FORMULARIO
                    idncalendar: mod.fd.Calendar,
                    idntypeperiod: mod.fd.Typeperiod,
                    title: mod.fd.Title,
                    startdate: FormatDateReverse(mod.fd.StartDate),
                    finishdate: FormatDateReverse(mod.fd.FinishDate),
                    year: mod.fd.Year,
                    month: mod.fd.Month,
                    sicossactive: mod.fd.Sicoss,
                    special: mod.fd.Special,
                    numperiod: mod.fd.Numperiodmonth
                };
                //VUE POST
                Vue.http.post(mod.globalurl + "/save", arr).then(response => {
                    // Si todo sale correcto              
                    swal('Guardado Correctamente', '', 'success');
                    $('#ModalEdicion').modal('hide');
                    mod.cleanform();
                }, response => {
                    //Si no sale correcto
                    swal('Ocurrio un error al guardar', '', 'error')
                    mod.cleanform();
                });
            })
          }
            //FIN VUE POST
        },
        editar: function(event) {
             //Items para ser chequeados si estan nulos
            var items = [mod.fd.Title, mod.fd.Calendar, mod.fd.StartDate, mod.fd.FinishDate,
            mod.fd.Year,mod.fd.Typeperiod,mod.fd.Month,mod.fd.Sicoss,mod.fd.Special,mod.fd.Numperiodmonth];
            //Mensajes que se mostraran para cada objeto nulo
            var strings = ['un Titulo', 'un Calendario', 'una Fecha de Inicio', 'una Fecha Final',
            'el Año','el Tipo de Periodo','un Mes','Sicoss','Especial','Cantidad de Periodos'];
            //Funcion para chequear estos elementos
            var checked = CheckNulls(items, strings);
            //Si all is fine
            if (checked != 0) {
            swal({
                title: "Confirmación de guardado",
                text: "Estas seguro de guardar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function() {
                var arr = {
                    //DATOS DE FORMULARIO
                    idncalendar: mod.fd.Calendar,
                    idntypeperiod: mod.fd.Typeperiod,
                    title: mod.fd.Title,
                    startdate: FormatDateReverse(mod.fd.StartDate),
                    finishdate: FormatDateReverse(mod.fd.FinishDate),
                    year: mod.fd.Year,
                    month: mod.fd.Month,
                    sicossactive: mod.fd.SicossActive,
                    special: mod.fd.Special,
                    numperiod: mod.fd.Numperiodmonth
                };
                //VUE POST
                Vue.http.put(mod.globalurl + "/update/" + mod.fd.Idn, arr).then(response => {
                    // Si todo sale correcto              
                    swal('Modificada Correctamente', '', 'success');
                    $('#ModalEdicion').modal('hide');
                    mod.cleanform();
                }, response => {
                    //Si no sale correcto
                    swal('Ocurrio un error al modificar', '', 'error')
                    mod.cleanform();
                });
            })
          }
            //FIN VUE POST
        },
        borrar: function(event) {
            swal({
                title: "Confirmar Eliminación",
                text: "Estas seguro de eliminar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function() {
                var arr = {
                    title: mod.fd.Title
                };
                Vue.http.put(mod.globalurl + "/delete/" + mod.fd.Idn, arr).then(response => {
                    // success callback
                    swal('Eliminado Correctamente', '', 'success');
                    $('#ModalEdicion').modal('hide');
                    mod.cleanform();
                }, response => {
                    swal('Ocurrio un error al eliminar', '', 'error');
                    // error callback
                });
            })
        },
        cleanform: function() {
            mod.fd.Idn = '';
            mod.fd.Title = '';
            mod.fd.Calendar = '';
            mod.fd.Typeperiod = '';
            mod.fd.StartDate = '';
            mod.fd.FinishDate = '';
            mod.fd.Year = '';
            mod.fd.Month = '';
            mod.fd.Month1 = '';
            mod.fd.SicossActive = '';
            mod.fd.Special = '';
            mod.fd.Numperiodmonth = '';
            $('#ModalEdicion').modal('hide');
            var table = $('#TablaPeriodo').dataTable();
            //RECARGA LOS DATOS DE LA TABLA
            table.fnReloadAjax();
        },
        calculardias: function() {
            CalcularDias();
        },
        openmodaltonew: function() {
            mod.cleanform();
            $('#ModalEdicion').modal('toggle');
            //OCULTAR BOTONES O MOSTRAR
            mod.ff.save = true;
            mod.ff.edit = false;
        },
        openmodaltoedit: function() {
            $('#ModalEdicion').modal('toggle');
            //OCULTAR BOTONES O MOSTRAR
            mod.ff.save = false;
            mod.ff.edit = true;
        },
        showtypeperiodmonth: function() {
            //OCULTAR BOTONES O MOSTRAR
            mod.fd.Month = true;
            mod.ff.Month1 = false;
        },
        showtypeperiodmonth1: function() {
            //OCULTAR BOTONES O MOSTRAR
            mod.fd.Month = false;
            mod.ff.Month1 = true;
        },
    }
});
//======INICIO DE FUNCIONES ============
function CargarTabla() {
    var table = $("#TablaPeriodo").DataTable({
        //COMPROBACION PARA PINTAR Y CAMBIAR TEXTO DE TABLA
        "destroy": true,
        "scrollY": "200px",
        "scrollX": "1000px",
        "language": {
            "url": "/../Spanish.json"
        },
        //Especificaciones de las Columnas que vienen y deben mostrarse
        "columns": [{
            data: 'idn'
        }, {
            data: 'title'
        }, {
            data: 'namecalendar'
        }, {
            data: 'nametypeperiod'
        }, {
            data: 'startdate'
        }, {
            data: 'finishdate'
        }],
        //Especificaciones de la URL del servicio
        "ajax": {
            url: '../api/v1/period',
            dataSrc: ''
        }
    });
    $('#TablaPeriodo tbody').on('click', 'tr', function() {
        //OBTENGO LOS VARLOES DE LA TABLA
        mod.fd.Idn = table.row(this).data().idn;
        mod.fd.Title = table.row(this).data().title;
        mod.fd.Calendar = mod.fd.Calendaroptions[parseInt(table.row(this).data().idncalendar) - 1];
        mod.fd.Typeperiod = mod.fd.Typeperiodoptions[parseInt(table.row(this).data().idntypeperiod) - 1];
        mod.fd.StartDate = FormatDate(table.row(this).data().startdate);
        mod.fd.FinishDate = FormatDate(table.row(this).data().finishdate);
        mod.fd.Year = table.row(this).data().year;
        mod.fd.Month = mod.fd.Monthoptions[parseInt(table.row(this).data().month) - 1];
        mod.fd.Monthl = mod.fd.Month1options[parseInt(table.row(this).data().month) - 1];
        mod.fd.SicossActive = mod.fd.SiccossOptions[parseInt(table.row(this).data().sicossactive) - 1];
        mod.fd.Special = mod.fd.SpecialOptions[parseInt(table.row(this).data().special) - 1];
        mod.fd.Numperiodmonth = table.row(this).data().numperiod;
        //ABRO LA MODAL
        mod.openmodaltoedit();
    });
}

function CalcularDias() {
    var start = $("#StartDate").datepicker("getDate");
    var end = $("#FinishDate").datepicker("getDate");
    var days = (end - start) / (3600 * 24 * 1000);
    var total = Math.round(days);
    if (mod.fd.StartDate == "") {
        swal('Por favor seleccione una fecha de inicio', '', 'error');
        return false;
        mod.fd.Dias = '';
    }
    if (mod.fd.FinishDate == "") {
        swal('Por favor seleccione una fecha Final', '', 'error');
        return false;
        mod.fd.Dias = '';
    }
    mod.fd.Dias = total;
}