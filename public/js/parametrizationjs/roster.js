Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');

//FUNCIONES GENERICAS PARA INICIO DE LA PAGINA
 var vm = new Vue({
  methods: 
      {
        cargartabla:function()
        {
          CargarTabla();
        }
      }
    });

vm.cargartabla();


//FUNCIONES EN LA MODAL DE EDICION
var mod = new Vue({
  el: '#vuedata',
  data: {
    globalurl:'../api/v1/roster',

    Salarytypeoptions:[],
    Vacationtypeoptions:[],
    Calendaroptions:[],

    fd:
    {
      Idn:'',
      Name:'',
      Description:'',
      Calendar:'',
      Salarytype:'',
      Vacationtype:'',
      Journalteoric:'',
      Journaltype:'',
      Numperiodmonth:''

    },
    ff:
    {
      save:false,
      edit:true,
    }    
    
  },
  methods: {
    guardar: function (event) 
    {
        if (mod.fd.Name === '') {
            swal({
                title: "Atencion",
                text: "Debes indicar Nombre",
                type: "warning",
                confirmButtonText: 'Aceptar'
            });
            return false;
        }
        if (mod.fd.Description === '') {
            swal({
                title: "Atencion",
                text: "Debes indicar Descripción",
                type: "warning",
                confirmButtonText: 'Aceptar'
            });
            return false;
        }
        if (mod.fd.Calendar === '') {
            swal({
                title: "Atencion",
                text: "Debes indicar Calendario",
                type: "warning",
                confirmButtonText: 'Aceptar'
            });
            return false;
        }
        if (mod.fd.Salarytype === '') {
            swal({
                title: "Atencion",
                text: "Debes indicar Tipo de Salario",
                type: "warning",
                confirmButtonText: 'Aceptar'
            });
            return false;
        }
        if (mod.fd.Vacationtype === '') {
            swal({
                title: "Atencion",
                text: "Debes indicar Tipo de Vacación",
                type: "warning",
                confirmButtonText: 'Aceptar'
            });
            return false;
        }
        if (mod.fd.Journalteoric === '') {
            swal({
                title: "Atencion",
                text: "Debes indicar Jornada Teorica",
                type: "warning",
                confirmButtonText: 'Aceptar'
            });
            return false;
        }
        if (mod.fd.Journaltype === '') {
            swal({
                title: "Atencion",
                text: "Debes indicar Tipo de Jornada",
                type: "warning",
                confirmButtonText: 'Aceptar'
            });
            return false;
        }
        if (mod.fd.Numperiodmonth === '') {
            swal({
                title: "Atencion",
                text: "Debes indicar Cantidad de Periodos",
                type: "warning",
                confirmButtonText: 'Aceptar'
            });
            return false;
        }
       swal({
                title: "Confirmación de guardado",
                text: "Estas seguro de guardar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {
                    //DATOS DE FORMULARIO
                    name:mod.fd.Name,
                    description:mod.fd.Description, 
                    calendar:mod.fd.Calendar,
                    salarytype:mod.fd.Salarytype, 
                    vacationtype:mod.fd.Vacationtype,
                    journalteoric:mod.fd.Journalteoric,  
                    journaltype:mod.fd.Journaltype,
                    numperiodmonth:mod.fd.Numperiodmonth          
                  };
                  //VUE POST
                Vue.http.post(mod.globalurl+"/save",arr).then(response => {
                    // Si todo sale correcto              
                    swal('Guardado Correctamente','','success');
                    $('#ModalEdicion').modal('hide');
                    mod.cleanform();
                   

                    }, response => {
                      //Si no sale correcto
                            swal('Ocurrio un error al guardar','','error')
                            mod.cleanform();
                           
                        });
                       
                    })
                    //FIN VUE POST
           
       
    },
    editar: function (event) 
    {
        if (mod.fd.Name === '') {
            swal({
                title: "Atencion",
                text: "Debes indicar Nombre",
                type: "warning",
                confirmButtonText: 'Aceptar'
            });
            return false;
        }
        if (mod.fd.Description === '') {
            swal({
                title: "Atencion",
                text: "Debes indicar Descripción",
                type: "warning",
                confirmButtonText: 'Aceptar'
            });
            return false;
        }
        if (mod.fd.Calendar === '') {
            swal({
                title: "Atencion",
                text: "Debes indicar Calendario",
                type: "warning",
                confirmButtonText: 'Aceptar'
            });
            return false;
        }
        if (mod.fd.Salarytype === '') {
            swal({
                title: "Atencion",
                text: "Debes indicar Tipo de Salario",
                type: "warning",
                confirmButtonText: 'Aceptar'
            });
            return false;
        }
        if (mod.fd.Vacationtype === '') {
            swal({
                title: "Atencion",
                text: "Debes indicar Tipo de Vacación",
                type: "warning",
                confirmButtonText: 'Aceptar'
            });
            return false;
        }
        if (mod.fd.Journalteoric === '') {
            swal({
                title: "Atencion",
                text: "Debes indicar Jornada Teorica",
                type: "warning",
                confirmButtonText: 'Aceptar'
            });
            return false;
        }
        if (mod.fd.Journaltype === '') {
            swal({
                title: "Atencion",
                text: "Debes indicar Tipo de Jornada",
                type: "warning",
                confirmButtonText: 'Aceptar'
            });
            return false;
        }
        if (mod.fd.Numperiodmonth === '') {
            swal({
                title: "Atencion",
                text: "Debes indicar Cantidad de Periodos",
                type: "warning",
                confirmButtonText: 'Aceptar'
            });
            return false;
        }
        swal({
                title: "Confirmación de editado",
                text: "Estas seguro de editar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {
                    //DATOS DE FORMULARIO
                    
                    name:mod.fd.Name,
                    description:mod.fd.Description, 
                    calendar:mod.fd.Calendar,
                    salarytype:mod.fd.Salarytype, 
                    vacationtype:mod.fd.Vacationtype,
                    journalteoric:mod.fd.Journalteoric,  
                    journaltype:mod.fd.Journaltype,
                    numperiodmonth:mod.fd.Numperiodmonth   
                                      
                  };
                  //VUE POST
                Vue.http.put(mod.globalurl+"/update/"+mod.fd.Idn,arr).then(response => {
                    // Si todo sale correcto              
                    swal('Modificada Correctamente','','success');
                    $('#ModalEdicion').modal('hide');
                    mod.cleanform();

                    }, response => {
                      //Si no sale correcto
                            swal('Ocurrio un error al modificar','','error')
                            mod.cleanform();
                           
                        });
                        
                    })
                    //FIN VUE POST
    },   
    borrar: function (event)
    {
       swal({
                title: "Confirmar Eliminación",
                text: "Estas seguro de eliminar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {name:mod.fd.Name,};
                Vue.http.put(mod.globalurl+"/delete/"+mod.fd.Idn,arr).then(response => {
                    // success callback
                    swal('Eliminado Correctamente','','success');
                $('#ModalEdicion').modal('hide');
                mod.cleanform();

               

            }, response => {
                    swal('Ocurrio un error al eliminar','','error');
                    // error callback
                });
            })
    },
    cleanform: function()
    {
      
      mod.fd.Idn ='';
      mod.fd.Name ='';
      mod.fd.Description ='';
      mod.fd.Calendar ='';
      mod.fd.Salarytype ='';
      mod.fd.Vacationtype ='';
      mod.fd.Journalteoric ='';
      mod.fd.Journaltype ='';
      mod.fd.Numperiodmonth ='';
       $('#ModalEdicion').modal('hide');
      var table = $('#TablaRoster').dataTable();
      //RECARGA LOS DATOS DE LA TABLA
     table.fnReloadAjax();
     
    },
 
     openmodaltonew:function()
        {
          mod.cleanform();
          $('#ModalEdicion').modal('toggle');
          //OCULTAR BOTONES O MOSTRAR
          mod.ff.save = true;
          mod.ff.edit = false;
        },
        openmodaltoedit:function()
        {
           $('#ModalEdicion').modal('toggle');
          //OCULTAR BOTONES O MOSTRAR
          mod.ff.save = false;
          mod.ff.edit = true;
        }
  }
 
});

   //======INICIO DE FUNCIONES ============
    function CargarTabla()
    {   
    
      var table = $("#TablaRoster").DataTable({
        //COMPROBACION PARA PINTAR Y CAMBIAR TEXTO DE TABLA


        "destroy": true,
        "scrollY":        "200px",
        "scrollX":        "1000px",
        "language": {
            "url": "/../Spanish.json"
        },
        //Especificaciones de las Columnas que vienen y deben mostrarse
        "columns" : [
            { data : 'idn' },
            { data : 'name'},
            { data : 'description'},
             { data : 'journaltype'},
              { data : 'numperiodmonth'}
           

                
           
        ],
        //Especificaciones de la URL del servicio
        "ajax": {
            url: '../api/v1/roster',
            dataSrc : ''
        }

    });


      $('#TablaRoster tbody').on( 'click', 'tr', function () {
        
        //OBTENGO LOS VARLOES DE LA TABLA 

      mod.fd.Idn = table.row( this ).data().idn;
      mod.fd.Name = table.row( this ).data().name;
      mod.fd.Description = table.row( this ).data().description;
      mod.fd.Calendar = mod.Calendaroptions[parseInt(table.row( this ).data().idncalendar) -1];
      mod.fd.Salarytype = mod.Salarytypeoptions[parseInt(table.row( this ).data().idnsalarytype) -1];
      mod.fd.Vacationtype = mod.Vacationtypeoptions[parseInt(table.row( this ).data().idnvacationtype) -1];
      mod.fd.Journalteoric = table.row( this ).data().journalteoric;
      mod.fd.Journaltype = table.row( this ).data().journaltype;
      mod.fd.Numperiodmonth = table.row( this ).data().numperiodmonth;
      
      //ABRO LA MODAL
        mod.openmodaltoedit();

       });

     } 


 var vm = new Vue({
  methods: 
       {
        cargarcombosalarytype:function()
        {
                   // GET /someUrl
            this.$http.get('../api/v1/salarytype').then(response => {
                $.each(response.body,function(i,v){
                mod.Salarytypeoptions.push({id:v.idn,label:v.name});
            });
            // get body data


          }, response => {
            // error callback
          });
        },

         cargarcombovacationtype:function()
        {
                   // GET /someUrl
            this.$http.get('../api/v1/vacationtype').then(response => {
                $.each(response.body,function(i,v){
                mod.Vacationtypeoptions.push({id:v.idn,label:v.name});
            });
            // get body data


          }, response => {
            // error callback
          });
        },
        cargarcombocalendar:function()
        {
                   // GET /someUrl
            this.$http.get('../api/v1/calendar').then(response => {
                $.each(response.body,function(i,v){
                mod.Calendaroptions.push({id:v.idn,label:v.name});
            });
            // get body data

          }, response => {
            // error callback
          });
        }
       } 
    });

vm.cargarcombosalarytype();
vm.cargarcombovacationtype();
vm.cargarcombocalendar();

