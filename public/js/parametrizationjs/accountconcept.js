Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');

//FUNCIONES GENERICAS PARA INICIO DE LA PAGINA
 var vm = new Vue({
  methods: 
      { 

       
        cargarcomboconceptos:function()
        {
                   // GET /someUrl
            this.$http.get('../api/v1/concept').then(response => {

            // get body data
            mod.fd.Conceptoptions = response.body;

          }, response => {
            // error callback
          });
        },
            cargarcombocuentascontables:function()
        {
                   // GET /someUrl
            this.$http.get('../api/v1/accountingaccount').then(response => {

            // get body data
            mod.fd.Accountingaccountoptions = response.body;

          }, response => {
            // error callback
          });
        },
          
        cargartabla:function()
        {
          CargarTabla();
        }
      }
    });

vm.cargarcombocuentascontables();
vm.cargarcomboconceptos();

vm.cargartabla();

//FUNCIONES EN LA MODAL DE EDICION
var mod = new Vue({
  el: '#vuedata',
  data: {
    globalurl:'../api/v1/accountconcept',
            
    fd:
    {
      Idn:'',

      IdnConcept:'',
      NameConcept:'',
      Concept:'',
      Conceptoptions:'',
           
         IdnAccountingAccount:'',
      NameAccountingAccount:'',
      AccountingAccount:'',
      Accountingaccountoptions:''
        
       

      
    },
    ff:
    {
      save:false,
      edit:true,
    }    
    
  },
  
  methods: {
   
    guardarAccountConcept: function (event) 
    {
        if (mod.fd.Concept === '') {
            swal({
                title: "Atencion",
                text: "Debes indicar un Concepto",
                type: "warning",
                confirmButtonText: 'Aceptar'
            });
            return false;
        }

        if (mod.fd.AccountingAccount === '') {
            swal({
                title: "Atencion",
                text: "Debes indicar un Concepto",
                type: "warning",
                confirmButtonText: 'Aceptar'
            });
            return false;
        }

       swal({
                title: "Confirmación de guardado",
                text: "Estas seguro de guardar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {
                    //DATOS DE FORMULARIO

                    
                    idnconcept:mod.fd.Concept,
                    idnaccountingaccount:mod.fd.AccountingAccount
                   
                                        
                  };
                  //VUE POST
                Vue.http.post(mod.globalurl+"/save",arr).then(response => {
                    // Si todo sale correcto              
                    swal('Guardado Correctamente','','success');
                    $('#ModalAccountConcept').modal('hide');
                    mod.cleanform();

                    }, response => {
                      //Si no sale correcto
                               swal('No se aceptan campos repetidos','','warning')
                            mod.cleanform();
                        }, response => {
                      //Si no sale correcto
                         
                    swal('Ocurrio un error al guardar','','error')
                            mod.cleanform();
                        });
           
                        
                    })
                    //FIN VUE POST
           
       
    },

    editarAccountConcept: function (event) 
    {
        if (mod.fd.Concept === '') {
            swal({
                title: "Atencion",
                text: "Debes indicar un Concepto",
                type: "warning",
                confirmButtonText: 'Aceptar'
            });
            return false;
        }

        if (mod.fd.AccountingAccount === '') {
            swal({
                title: "Atencion",
                text: "Debes indicar un Concepto",
                type: "warning",
                confirmButtonText: 'Aceptar'
            });
            return false;
        }

        swal({
                title: "Confirmación de editado",
                text: "Estas seguro de editar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {

                     
                         
                    idnconcept:mod.fd.Concept,
                    idnaccountingaccount:mod.fd.AccountingAccount
                                      
                  };
                  //VUE POST
                Vue.http.put(mod.globalurl+"/update/"+mod.fd.Idn,arr).then(response => {
                    // Si todo sale correcto              
                    swal('Modificada Correctamente','','success');
                    $('#ModalAccountConcept').modal('hide');
                    mod.cleanform();

                    }, response => {
                      //Si no sale correcto
                     swal('No se aceptan campos repetidos','','warning')
                            mod.cleanform();
                           
                        }, response => {
                      //Si no sale correcto
                            swal('Ocurrio un error al modificar','','error')
                            mod.cleanform();
                        });
                        
                    })
                    //FIN VUE POST
    },

    borrarAccountConcept: function (event)
    {
        swal({
                title: "Confirmar Eliminación",
                text: "Estas seguro de eliminar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {idnconcept:mod.fd.Concept};
                Vue.http.put(mod.globalurl+"/delete/"+mod.fd.Idn,arr).then(response => {
                    // success callback
                    swal('Eliminado Correctamente','','success');
                $('#ModalAccountConcept').modal('hide');
                mod.cleanform();


            }, response => {
                    swal('Ocurrio un error al eliminar','','error');
                    // error callback
                });
            })
    },

    cleanform: function()
    {
      mod.fd.Idn='';
      mod.fd.Concept='';
        mod.fd.AccountingAccount='';
       
        
      $('#ModalAccountConcept').modal('hide');
      var table = $('#TablaAccountConcept').dataTable();
      //RECARGA LOS DATOS DE LA TABLA
     table.fnReloadAjax();
     
    },
   
     openmodaltonew:function()
        {
          mod.cleanform();
          $('#ModalAccountConcept').modal('toggle');
          //OCULTAR BOTONES O MOSTRAR
          mod.ff.save = true;
          mod.ff.edit = false;
        },
        openmodaltoedit:function()
        {
           $('#ModalAccountConcept').modal('toggle');
          //OCULTAR BOTONES O MOSTRAR
          mod.ff.save = false;
          mod.ff.edit = true;
        }
  }
 
});



   //======INICIO DE FUNCIONES ============
    function CargarTabla()
    {   
      var table = $("#TablaAccountConcept").DataTable({
        //COMPROBACION PARA PINTAR Y CAMBIAR TEXTO DE TABLA


        "destroy": true,
        "scrollY":        "200px",
        "scrollX":        "1000px",
        "language": {
            "url": "/../Spanish.json"
        },
        //Especificaciones de las Columnas que vienen y deben mostrarse
        "columns" : [
            { data : 'idn' },
            { data : 'nameconcept' },
            { data : 'nameaccountingaccount' },
         
                
           
           
        ],
        //Especificaciones de la URL del servicio
        "ajax": {
            url: '../api/v1/accountconcept',
            dataSrc : ''
        }

    });

            $('#TablaAccountConcept tbody').on( 'click', 'tr', function () {
        
        //OBTENGO LOS VARLOES DE LA TABLA
        mod.fd.Idn = table.row( this ).data().idn;
        mod.fd.Concept = table.row( this ).data().idnconcept;
        mod.fd.AccountingAccount = table.row( this ).data().idnaccountingaccount;  
      
        //idnabsencetype=mod.fd.NameAbsenceType;
      //ABRO LA MODAL
        mod.openmodaltoedit();

       });

     } 
