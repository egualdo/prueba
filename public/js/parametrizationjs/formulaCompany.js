Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');

//FUNCIONES GENERICAS PARA INICIO DE LA PAGINA
//FUNCIONES GENERICAS PARA INICIO DE LA PAGINA
var editor = CodeMirror.fromTextArea(document.getElementById("code"), {
  lineNumbers: true,        
  mode: "text/x-php",             

  theme: "monokai",
  autofocus: true,
  lineWrapping: true,
  styleActiveLine: true,
  gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter", "CodeMirror-markergutter"],
  foldGutter: true,
  markerGutter: true,
});
//FUNCIONES EN LA MODAL DE EDICION
var mod = new Vue({
  el: '#ModalForm',
  data: {
    globalurl:'../api/v1/formulecompany',
    Conceptoptions:[],
    fd:
    {
      Idn:'',
      Calc:'',
      Concept:'',
      Idncompany:'',
    },
    ff:
    {
      save:false,
      edit:true,
    }    
    
  },
  methods: {
    guardar: function (event) 
    {
      
        if (mod.fd.Concept === '') {
            swal({
                title: "Atencion",
                text: "Debes indicar un Concepto",
                type: "warning",
                confirmButtonText: 'Aceptar'
            });
            return false;
        }
        if (mod.fd.Idncompany === '') {
            swal({
                title: "Atencion",
                text: "Debes indicar una Compañia",
                type: "warning",
                confirmButtonText: 'Aceptar'
            });
            return false;
        }
       swal({
                title: "Confirmación de guardado",
                text: "Estas seguro de guardar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {
                    //DATOS DE FORMULARIO
                     Calc:editor.getDoc().getValue(),
                    Concept:mod.fd.Concept
                    //Company:mod.fd.Idncompany           
                  };
                  //VUE POST
                Vue.http.post(mod.globalurl+"/save",arr).then(response => {
                    // Si todo sale correcto              
                    swal('Guardado Correctamente','','success');
                    $('#ModalEdicion').modal('hide');
                    mod.cleanform();
                   

                    }, response => {
                        if (response.status === 403)
                    {
                      swal('Elemento ya existente','Ya existe una Formula para este concepto','error')
                    }
                    if (response.status === 500)
                    {
                     swal('Error interno',response.body,'error')
                   }
                           
                        });
                       
                    })
                    //FIN VUE POST
           
       
    },
    editar: function (event) 
    {
        
        if (mod.fd.Concept === '') {
            swal({
                title: "Atencion",
                text: "Debes indicar un Concepto",
                type: "warning",
                confirmButtonText: 'Aceptar'
            });
            return false;
        }
        if (mod.fd.Idncompany === '') {
            swal({
                title: "Atencion",
                text: "Debes indicar una Compañia",
                type: "warning",
                confirmButtonText: 'Aceptar'
            });
            return false;
        }
        swal({
                title: "Confirmación de editado",
                text: "Estas seguro de editar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


 var arr = {
                    //DATOS DE FORMULARIO
                    Calc:editor.getDoc().getValue(),
                    Concept:mod.fd.Concept
                   // Company:mod.fd.Idncompany           
                  };
                  //VUE POST
                Vue.http.put(mod.globalurl+"/update/"+mod.fd.Idn,arr).then(response => {
                    // Si todo sale correcto              
                    swal('Modificada Correctamente','','success');
                    $('#ModalEdicion').modal('hide');
                    mod.cleanform();

                    }, response => {
                        if (response.status === 403)
                    {
                      swal('Elemento ya existente','Ya existe una Formula para este concepto','error')
                    }
                    if (response.status === 500)
                    {
                     swal('Error interno',response.body,'error')
                   }
                           
                        });
                        
                    })
                    //FIN VUE POST
    },   
    borrar: function (event)
    {
       swal({
                title: "Confirmar Eliminación",
                text: "Estas seguro de eliminar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {
                    //DATOS DE FORMULARIO
                    Calc:mod.fd.Calc,
                    Concept:mod.fd.Concept
                   // Company:mod.fd.Idncompany         
                  };
                Vue.http.put(mod.globalurl+"/delete/"+mod.fd.Idn,arr).then(response => {
                    // success callback
                    swal('Eliminado Correctamente','','success');
                $('#ModalEdicion').modal('hide');
                mod.cleanform();

               

            }, response => {
                    swal('Ocurrio un error al eliminar','','error');
                    // error callback
                });
            })
    },
    cleanform: function()
    {
      mod.fd.Idn='';
      mod.fd.Calc='';
      mod.fd.Concept='';
     // mod.fd.Cct="";
       $('#ModalEdicion').modal('hide');
      var table = $('#TablaFormulaCompany').dataTable();
      //RECARGA LOS DATOS DE LA TABLA
     table.fnReloadAjax();
     
    },
 
  openmodaltonew:function()
    {
    //  mod.cleanform();
         editor.getDoc().setValue("");
      $('#ModalEdicion').modal('toggle');
          //OCULTAR BOTONES O MOSTRAR
          mod.ff.save = true;
          mod.ff.edit = false;
        },
        openmodaltoedit:function()
        {
         $('#ModalEdicion').modal('toggle');
          //OCULTAR BOTONES O MOSTRAR
          mod.ff.save = false;
          mod.ff.edit = true;
        }
  }
 
});
   var actionform = new Vue({
      el: '#ActionForm',
      data:'',
    methods: 
    {
      openmodal()
      {
       
        // mod.cleanform();
      $('#ModalEdicion').modal('toggle');
          //OCULTAR BOTONES O MOSTRAR
           editor.getDoc().setValue("");
          mod.ff.save = true;
          mod.ff.edit = false;
      }
    }
      });
   //======INICIO DE FUNCIONES ============
    function CargarTabla()
    {   
      var table = $("#TablaFormulaCompany").DataTable({
        //COMPROBACION PARA PINTAR Y CAMBIAR TEXTO DE TABLA


        "destroy": true,
        "scrollY":        "800px",
        "scrollX":        "1000px",
        "language": {
            "url": "/../Spanish.json"
        },
        //Especificaciones de las Columnas que vienen y deben mostrarse
        "columns" : [
            { data : 'idn' },
            { data : 'concept'},
            { data : 'calc'},

            
                
           
        ],
        //Especificaciones de la URL del servicio
        "ajax": {
             url: '../api/v1/formulecompany/'+ mod.fd.Idncompany,
            dataSrc : ''
        }

    });


      $('#TablaFormulaCompany tbody').on( 'click', 'tr', function () {
        
         //OBTENGO LOS VARLOES DE LA TABLA
        mod.fd.Idn = table.row( this ).data().idn;
        mod.fd.Calc = table.row( this ).data().calc;

       // editor.getDoc().setValue(mod.fd.Calc);
       mod.fd.Concept = mod.Conceptoptions[parseInt(table.row( this ).data().idnconcept) -1 ];
        //SETEO EL VALOR AL CODEMIRROR
        editor.getDoc().setValue(mod.fd.Calc);

        var that = editor;
        //REFRESCO EL CODEMIRROR PARA SOLUCIONAR UN BUG 
        setTimeout(function() {
          editor.refresh();
        },1);
      //ABRO LA MODAL
      mod.openmodaltoedit();
      $('#code').trigger('click');

    });

     } 


 var vm = new Vue({
  methods: 
      {
        cargartabla:function()
        {
          CargarTabla();
        },
         cargarcomboconcept:function()
        {
                   // GET /someUrl
            this.$http.get('../api/v1/concept').then(response => {
                $.each(response.body,function(i,v){
                mod.Conceptoptions.push({id:v.idn,label:v.name});
            });

          }, response => {
            // error callback
          });
        },
        /* cargardatos:function()
        {
                   // GET /someUrl
            this.$http.get('../api/v1/ccts/'+ mod.fd.Idncct).then(response => {

            // get body data
            mod.fd.Idncct = response.body[0].idn;
          }, response => {
            // error callback
          });
        }*/


      }
    });
vm.cargarcomboconcept();
//vm.cargardatos();
  var vm = new Vue({
  methods: 
      {
        loadcct:function()
        {
       var query = window.location;
    var qs = getUrlParams(query);
    console.log("PARAMETROS",qs.id);
    mod.fd.Idncompany=qs.id;


        },
        cargartabla:function()
        {
          CargarTabla();
        }
      }
    });
vm.loadcct();

vm.cargartabla();


