Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');
//FUNCIONES GENERICAS PARA INICIO DE LA PAGINA
var vm = new Vue({
    methods: {
        cargarcombocct: function() {
            // GET /someUrl
            this.$http.get('../api/v1/ccts').then(response => {
                $.each(response.body, function(i, v) {
                    mod.fd.Cctoptions.push({
                        id: v.idn,
                        label: v.name
                    });
                });
            }, response => {
                // error callback
            });
        },
        cargarcomboperiod: function() {
            // GET /someUrl
            this.$http.get('../api/v1/period').then(response => {
                $.each(response.body, function(i, v) {
                    mod.fd.Periodoptions.push({
                        id: v.idn,
                        label: v.title
                    });
                });
            }, response => {
                // error callback
            });
        },
        cargarcomboruntype: function() {
            // GET /someUrl
            this.$http.get('../api/v1/runtype').then(response => {
                $.each(response.body, function(i, v) {
                    mod.fd.TypeRunoptions.push({
                        id: v.idn,
                        label: v.name
                    });
                });
            }, response => {
                // error callback
            });
        },
        cargarcomboconstant: function() {
            // GET /someUrl
            this.$http.get('../api/v1/constant').then(response => {
                $.each(response.body, function(i, v) {
                    mod.fd.Constantoptions.push({
                        id: v.idn,
                        label: v.name
                    });
                });
            }, response => {
                // error callback
            });
        },
        cargartabla: function() {
            CargarTabla();
        }
    }
});
vm.cargarcombocct();
vm.cargarcomboruntype();
vm.cargarcomboperiod();
vm.cargarcomboconstant();
vm.cargartabla();
//FUNCIONES EN LA MODAL DE EDICION
var mod = new Vue({
    el: '#vuedata',
    data: {
        globalurl: '../api/v1/cctconstant',
        fd: {
            Idn: '',
            IdnCct: '',
            NameCct: '',
            Cct: '',
            Cctoptions: [],
            IdnPeriod: '',
            NamePeriod: '',
            Period: '',
            Periodoptions: [],
            IdnConstant: '',
            NameConstant: '',
            Constant: '',
            Constantoptions: [],
            IdnTypeRun: '',
            NameTypeRun: '',
            TypeRun: '',
            TypeRunoptions: [],
            Value: '',
            Comment: ''
        },
        ff: {
            save: false,
            edit: true,
        }
    },
    methods: {
        guardarCctConstant: function(event) {
          
            //Items para ser chequeados si estan nulos
            var items = [mod.fd.Cct, mod.fd.Period, mod.fd.Constant, mod.fd.TypeRun,mod.fd.Value,mod.fd.Comment];
            //Mensajes que se mostraran para cada objeto nulo
            var strings = ['CCT', 'Periodo', 'una Constante', 'un Tipo de Corrida','un Valor','Comentario'];
            //Funcion para chequear estos elementos
            var checked = CheckNulls(items, strings);
            //Si all is fine
            if (checked != 0) {
            swal({
                title: "Confirmación de guardado",
                text: "Estas seguro de guardar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function() {
                var arr = {
                    //DATOS DE FORMULARIO
                    idncct: mod.fd.Cct,
                    idnperiod: mod.fd.Period,
                    idnconstant: mod.fd.Constant,
                    idntyperun: mod.fd.TypeRun,
                    value: mod.fd.Value,
                    comment: mod.fd.Comment
                };
                //VUE POST
                Vue.http.post(mod.globalurl + "/save", arr).then(response => {
                    // Si todo sale correcto              
                    swal('Guardado Correctamente', '', 'success');
                    $('#ModalCctConstant').modal('hide');
                    mod.cleanform();
                }, response => {
                    //Si no sale correcto
                    swal('Ocurrio un error al guardar', '', 'error')
                    mod.cleanform();
                });
            })
          }
            //FIN VUE POST
        },
        editarCctConstant: function(event) {
           
            //Items para ser chequeados si estan nulos
            var items = [mod.fd.Cct, mod.fd.Period, mod.fd.Constant, mod.fd.TypeRun,mod.fd.Value,mod.fd.Comment];
            //Mensajes que se mostraran para cada objeto nulo
            var strings = ['CCT', 'Periodo', 'una Constante', 'un Tipo de Corrida','un Valor','Comentario'];
            //Funcion para chequear estos elementos
            var checked = CheckNulls(items, strings);
            //Si all is fine
            if (checked != 0) {
            swal({
                title: "Confirmación de editado",
                text: "Estas seguro de editar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function() {
                var arr = {
                    idncct: mod.fd.Cct,
                    idnperiod: mod.fd.Period,
                    idnconstant: mod.fd.Constant,
                    idntyperun: mod.fd.TypeRun,
                    value: mod.fd.Value,
                    comment: mod.fd.Comment
                };
                //VUE POST
                Vue.http.put(mod.globalurl + "/update/" + mod.fd.Idn, arr).then(response => {
                    // Si todo sale correcto              
                    swal('Modificada Correctamente', '', 'success');
                    $('#ModalCctConstant').modal('hide');
                    mod.cleanform();
                }, response => {
                    //Si no sale correcto
                    swal('Ocurrio un error al modificar', '', 'error')
                    mod.cleanform();
                });
            })
          }
            //FIN VUE POST
        },
        borrarCctConstant: function(event) {
            swal({
                title: "Confirmar Eliminación",
                text: "Estas seguro de eliminar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function() {
                var arr = {
                    value: mod.fd.Value
                };
                Vue.http.put(mod.globalurl + "/delete/" + mod.fd.Idn, arr).then(response => {
                    // success callback
                    swal('Eliminado Correctamente', '', 'success');
                    $('#ModalCctConstant').modal('hide');
                    mod.cleanform();
                }, response => {
                    swal('Ocurrio un error al eliminar', '', 'error');
                    // error callback
                });
            })
        },
        cleanform: function() {
            mod.fd.Idn = '';
            mod.fd.IdnCct = '';
            mod.fd.IdnPeriod = '';
            mod.fd.IdnConstant = '';
            mod.fd.Value = '';
            mod.fd.Comment = '';
            $('#ModalCctConstant').modal('hide');
            var table = $('#TablaCctConstant').dataTable();
            //RECARGA LOS DATOS DE LA TABLA
            table.fnReloadAjax();
        },
        openmodaltonew: function() {
            mod.cleanform();
            $('#ModalCctConstant').modal('toggle');
            //OCULTAR BOTONES O MOSTRAR
            mod.ff.save = true;
            mod.ff.edit = false;
        },
        openmodaltoedit: function() {
            $('#ModalCctConstant').modal('toggle');
            //OCULTAR BOTONES O MOSTRAR
            mod.ff.save = false;
            mod.ff.edit = true;
        }
    }
});
//======INICIO DE FUNCIONES ============
function CargarTabla() {
    var table = $("#TablaCctConstant").DataTable({
        //COMPROBACION PARA PINTAR Y CAMBIAR TEXTO DE TABLA
        "destroy": true,
        "scrollY": "200px",
        "scrollX": "1000px",
        "language": {
            "url": "/../Spanish.json"
        },
        //Especificaciones de las Columnas que vienen y deben mostrarse
        "columns": [{
            data: 'idn'
        }, {
            data: 'periodname'
        }, {
            data: 'constantname'
        }, {
            data: 'namecct'
        }, {
            data: 'typerunname'
        }, {
            data: 'value'
        }, {
            data: 'comment'
        }, {
            data: 'active'
        }],
        //Especificaciones de la URL del servicio
        "ajax": {
            url: '../api/v1/cctconstant',
            dataSrc: ''
        }
    });
    $('#TablaCctConstant tbody').on('click', 'tr', function() {
        //OBTENGO LOS VARLOES DE LA TABLA
        mod.fd.Idn = table.row(this).data().idn;
        mod.fd.IdnCct = table.row(this).data().idncct;
        mod.fd.Cct = mod.fd.Cctoptions[parseInt(table.row(this).data().idncct) - 1];
        mod.fd.Period = mod.fd.Periodoptions[parseInt(table.row(this).data().idnperiod) - 1];
        mod.fd.Constant = mod.fd.Constantoptions[parseInt(table.row(this).data().idnconstant) - 1];
        mod.fd.TypeRun = mod.fd.TypeRunoptions[parseInt(table.row(this).data().idntyperun) - 1];
        mod.fd.Value = table.row(this).data().value;
        mod.fd.Comment = table.row(this).data().comment;
        //idnabsencetype=mod.fd.NameAbsenceType;
        //ABRO LA MODAL
        mod.openmodaltoedit();
    });
}