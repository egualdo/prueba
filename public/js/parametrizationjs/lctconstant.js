Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');

//FUNCIONES GENERICAS PARA INICIO DE LA PAGINA
 var vm = new Vue({
  methods: 
      { 

       /* cargarcombolct:function()
        {
                   // GET /someUrl
            this.$http.get('../api/v1/formulelct').then(response => {

            // get body data
            mod.fd.Lctoptions = response.body;

          }, response => {
            // error callback
          });
        },*/
        cargarcomboperiod:function()
        {
                   // GET /someUrl
            this.$http.get('../api/v1/period').then(response => {
                $.each(response.body,function(i,v){
                mod.fd.Periodoptions.push({id:v.idn,label:v.title});
            });


          }, response => {
            // error callback
          });
        },
     cargarcomboruntype:function()
        {
                   // GET /someUrl
            this.$http.get('../api/v1/runtype').then(response => {
                $.each(response.body,function(i,v){
                mod.fd.TypeRunoptions.push({id:v.idn,label:v.name});
            });


          }, response => {
            // error callback
          });
        },
        cargarcomboconstant:function()
        {
                   // GET /someUrl
            this.$http.get('../api/v1/constant').then(response => {

            $.each(response.body,function(i,v){
                mod.fd.Constantoptions.push({id:v.idn,label:v.name});
        });

          }, response => {
            // error callback
          });
        },
          
        cargartabla:function()
        {
          CargarTabla();
        }
      }
    });


vm.cargarcomboruntype();
vm.cargarcomboperiod();
vm.cargarcomboconstant();
vm.cargartabla();

//FUNCIONES EN LA MODAL DE EDICION
var mod = new Vue({
  el: '#vuedata',
  data: {
    globalurl:'../api/v1/lctconstant',
            
    fd:
    {
      Idn:'',

     /* IdnLct:'',
      NameLct:'',
      Lct:'',
      Lctoptions:'',*/
           
         IdnPeriod:'',
      NamePeriod:'',
      Period:'',
      Periodoptions:[],
        
         IdnTypeRun:'',
         NameTypeRun:'',
      TypeRun:'',
      TypeRunoptions:[],
      /*   IdnSubPeriod:'',
      NameSubPeriod:'',
      SubPeriod:'',
      SubPeriodoptions:'',*/
        
          IdnConstant:'',
      NameConstant:'',
      Constant:'',
      Constantoptions:[],
        
     Value:'',
      Comment:'',

      
    },
    ff:
    {
      save:false,
      edit:true,
    }    
    
  },
  
  methods: {
   
    guardarLctConstant: function (event) 
    {
      
        //Items para ser chequeados si estan nulos
            var items = [mod.fd.Constant, mod.fd.Period, mod.fd.TypeRun,mod.fd.Value,mod.fd.Comment];
            //Mensajes que se mostraran para cada objeto nulo
            var strings = ['una Constante', 'un Periodo', 'un Tipo de Corrida','un Valor','Comentario'];
            //Funcion para chequear estos elementos
            var checked = CheckNulls(items, strings);
            //Si all is fine
            if (checked != 0) {
       swal({
                title: "Confirmación de guardado",
                text: "Estas seguro de guardar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {
                    //DATOS DE FORMULARIO

                    
                  //  idnlct:mod.fd.Lct,
                    idnperiod:mod.fd.Period,
                  //  idnsubperiod:mod.fd.SubPeriod,
                    idnconstant:mod.fd.Constant,
                    idntyperun:mod.fd.TypeRun,
                    value:mod.fd.Value,
                    comment:mod.fd.Comment
                                        
                  };
                  //VUE POST
                Vue.http.post(mod.globalurl+"/save",arr).then(response => {
                    // Si todo sale correcto              
                    swal('Guardado Correctamente','','success');
                    $('#ModalLctConstant').modal('hide');
                    mod.cleanform();

                    }, response => {
                      //Si no sale correcto
                            swal('Ocurrio un error al guardar','','error')
                            mod.cleanform();
                        });
                        
                    })
                    //FIN VUE POST
           }
       
    },

    editarLctConstant: function (event) 
    {
        //Items para ser chequeados si estan nulos
            var items = [mod.fd.Constant, mod.fd.Period, mod.fd.TypeRun,mod.fd.Value,mod.fd.Comment];
            //Mensajes que se mostraran para cada objeto nulo
            var strings = ['una Constante', 'un Periodo', 'un Tipo de Corrida','un Valor','Comentario'];
            //Funcion para chequear estos elementos
            var checked = CheckNulls(items, strings);
            //Si all is fine
            if (checked != 0) {
       swal({
                title: "Confirmación de editado",
                text: "Estas seguro de editar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {

                     
                   // idnlct:mod.fd.Lct,
                    idnperiod:mod.fd.Period,
                    //idnsubperiod:mod.fd.SubPeriod,
                    idnconstant:mod.fd.Constant,
                    idntyperun:mod.fd.TypeRun,
                    value:mod.fd.Value,
                    comment:mod.fd.Comment
                                      
                  };
                  //VUE POST
                Vue.http.put(mod.globalurl+"/update/"+mod.fd.Idn,arr).then(response => {
                    // Si todo sale correcto              
                    swal('Modificada Correctamente','','success');
                    $('#ModalLctConstant').modal('hide');
                    mod.cleanform();

                    }, response => {
                      //Si no sale correcto
                            swal('Ocurrio un error al modificar','','error')
                            mod.cleanform();
                        });
                        
                    })
                    //FIN VUE POST
                  }
    },

    borrarLctConstant: function (event)
    {
        swal({
                title: "Confirmar Eliminación",
                text: "Estas seguro de eliminar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {value:mod.fd.Value};
                Vue.http.put(mod.globalurl+"/delete/"+mod.fd.Idn,arr).then(response => {
                    // success callback
                    swal('Eliminado Correctamente','','success');
                $('#ModalLctConstant').modal('hide');
                mod.cleanform();


            }, response => {
                    swal('Ocurrio un error al eliminar','','error');
                    // error callback
                });
            })
    },

    cleanform: function()
    {
      mod.fd.Idn='';
      //mod.fd.IdnLct='';
        mod.fd.IdnPeriod='';
       mod.fd.TypeRun='';
        mod.fd.IdnConstant='';
      mod.fd.Value='';
      mod.fd.Comment='';
        
      $('#ModalLctConstant').modal('hide');
      var table = $('#TablaLctConstant').dataTable();
      //RECARGA LOS DATOS DE LA TABLA
     table.fnReloadAjax();
     
    },
   
     openmodaltonew:function()
        {
          mod.cleanform();
          $('#ModalLctConstant').modal('toggle');
          //OCULTAR BOTONES O MOSTRAR
          mod.ff.save = true;
          mod.ff.edit = false;
        },
        openmodaltoedit:function()
        {
           $('#ModalLctConstant').modal('toggle');
          //OCULTAR BOTONES O MOSTRAR
          mod.ff.save = false;
          mod.ff.edit = true;
        }
  }
 
});

   //======INICIO DE FUNCIONES ============
    function CargarTabla()
    {   
      var table = $("#TablaLctConstant").DataTable({
        //COMPROBACION PARA PINTAR Y CAMBIAR TEXTO DE TABLA


        "destroy": true,
        "scrollY":        "200px",
        "scrollX":        "1000px",
        "language": {
            "url": "/../Spanish.json"
        },
        //Especificaciones de las Columnas que vienen y deben mostrarse
        "columns" : [
            { data : 'idn' },
            { data : 'periodname' },
            { data : 'typerunname' },
            { data : 'constantname'},
           // { data : 'namelct'},
            { data : 'value'},
            { data : 'comment'},
            { data : 'active'}
                
           
           
        ],
        //Especificaciones de la URL del servicio
        "ajax": {
            url: '../api/v1/lctconstant',
            dataSrc : ''
        }

    });

            $('#TablaLctConstant tbody').on( 'click', 'tr', function () {
        
        //OBTENGO LOS VARLOES DE LA TABLA
        mod.fd.Idn = table.row( this ).data().idn;
       // mod.fd.IdnLct = table.row( this ).data().idnlct;

        mod.fd.Lct = table.row( this ).data().idnlct;
                
        mod.fd.Period = mod.fd.Periodoptions[parseInt(table.row( this ).data().idnperiod) -1];
       // mod.fd.SubPeriod = table.row( this ).data().idnsubperiod;
        mod.fd.Constant = mod.fd.Constantoptions[parseInt(table.row( this ).data().idnconstant) -1];
         mod.fd.TypeRun =mod.fd.TypeRunoptions[ parseInt(table.row( this ).data().idntyperun) -1];
                
        mod.fd.Value = table.row( this ).data().value;
        mod.fd.Comment = table.row( this ).data().comment;    
      
        //idnabsencetype=mod.fd.NameAbsenceType;
      //ABRO LA MODAL
        mod.openmodaltoedit();

       });

     } 
