Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');

//FUNCIONES GENERICAS PARA INICIO DE LA PAGINA
 var vm = new Vue({
  methods: 
      { 

           cargarcomboabsencetype:function()
        {
                   // GET /someUrl
            this.$http.get('../api/v1/absencetype').then(response => {

            // get body data
            mod.fd.Absencetypeoptions = response.body;

          }, response => {
            // error callback
          });
        },

        


        cargartabla:function()
        {
          CargarTabla();
        }
      }
    });

vm.cargarcomboabsencetype();
vm.cargartabla();

//FUNCIONES EN LA MODAL DE EDICION
var mod = new Vue({
  el: '#vuedata',
  data: {
    globalurl:'../api/v1/absence',
            
    fd:
    {
      Idn:'',

      IdnAbsenceType:'',
             IdnEmployee:'',
             NameEmployee:'',
        Cant:'',
             
     Startdate:'',
      Finishdate:'',

       NameAbsenceType:'',
      AbsenceType:'',
      Absencetypeoptions:'',


     
      
    },
    ff:
    {
      save:false,
      edit:true,
    }    
    
  },
   mounted()
  {
     //Formato del Datepicker
       $('#StartDate').datepicker({
                format: 'dd-mm-yyyy',
                autoclose:true,
                language: 'es'                
            });
       //Obtener Datos del Datepicker
       $("#StartDate").datepicker().on(
        
        "changeDate", () => {
          //this.startDate = $('#DPickerBirthdate').val();
          mod.fd.StartDate = $('#StartDate').val();
            
            
        }
    );
        //Formato del Datepicker
       $('#FinishDate').datepicker({
                format: 'dd-mm-yyyy',
                autoclose:true,
                language: 'es'                
            });
       //Obtener Datos del Datepicker
       $("#FinishDate").datepicker().on(
        
        "changeDate", () => {
          //this.startDate = $('#DPickerBirthdate').val();
          mod.fd.FinishDate = $('#FinishDate').val();
        }
    );
  },
  methods: {
   
    guardarAbsences: function (event) 
    {
       
       swal({
                title: "Confirmación de guardado",
                text: "Estas seguro de guardar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {
                    //DATOS DE FORMULARIO

                    
                    idnabsencetype:mod.fd.AbsenceType,
                    idnemployee:1,
                    cant:mod.fd.Cant,
                    startdate:mod.fd.Startdate,
                    finishdate:mod.fd.Finishdate                    
                  };
                  //VUE POST
                Vue.http.post(mod.globalurl+"/save",arr).then(response => {
                    // Si todo sale correcto              
                    swal('Guardado Correctamente','','success');
                    $('#ModalAbsences').modal('hide');
                    mod.cleanform();

                    }, response => {
                      //Si no sale correcto
                            swal('Ocurrio un error al guardar','','error')
                            mod.cleanform();
                        });
                        
                    })
                    //FIN VUE POST
           
       
    },

    editarAbsences: function (event) 
    { 
       
       swal({
                title: "Confirmación de editado",
                text: "Estas seguro de editar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {

                    //DATOS DE FORMULARIO
                    idnabsencetype:mod.fd.IdnAbsenceType,

                    //DATOS DE FORMULARIO 
                    idnabsencetype:mod.fd.AbsenceType,
                   
                    startdate:mod.fd.Startdate,
                    idnemployee:1,
                    cant:mod.fd.Cant,
                    finishdate:mod.fd.Finishdate 
                                      
                  };
                  //VUE POST
                Vue.http.put(mod.globalurl+"/update/"+mod.fd.Idn,arr).then(response => {
                    // Si todo sale correcto              
                    swal('Modificada Correctamente','','success');
                    $('#ModalAbsences').modal('hide');
                    mod.cleanform();

                    }, response => {
                      //Si no sale correcto
                            swal('Ocurrio un error al modificar','','error')
                            mod.cleanform();
                        });
                        
                    })
                    //FIN VUE POST
    },

    borrarAbsences: function (event)
    {
        swal({
                title: "Confirmar Eliminación",
                text: "Estas seguro de eliminar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {startdate:mod.fd.Startdate};
                Vue.http.put(mod.globalurl+"/delete/"+mod.fd.Idn,arr).then(response => {
                    // success callback
                    swal('Eliminado Correctamente','','success');
                $('#ModalAbsences').modal('hide');
                mod.cleanform();


            }, response => {
                    swal('Ocurrio un error al eliminar','','error');
                    // error callback
                });
            })
    },

    cleanform: function()
    {
      mod.fd.Idn='';
      mod.fd.AbsenceType='';
      mod.fd.Startdate='';
      mod.fd.Finishdate='';
        mod.fd.Cant="";
        
      $('#ModalAbsences').modal('hide');
      var table = $('#TableAbsences').dataTable();
      //RECARGA LOS DATOS DE LA TABLA
     table.fnReloadAjax();
     
    },
    calculardias:function()
        {
           
            CalcularDias();       
        },
     openmodaltonew:function()
        {
          mod.cleanform();
          $('#ModalAbsences').modal('toggle');
          //OCULTAR BOTONES O MOSTRAR
          mod.ff.save = true;
          mod.ff.edit = false;
        },
        openmodaltoedit:function()
        {
           $('#ModalAbsences').modal('toggle');
          //OCULTAR BOTONES O MOSTRAR
          mod.ff.save = false;
          mod.ff.edit = true;
        }
  }
 
});

   //======INICIO DE FUNCIONES ============
    function CargarTabla()
    {   
      var table = $("#TableAbsences").DataTable({
        //COMPROBACION PARA PINTAR Y CAMBIAR TEXTO DE TABLA


        "destroy": true,
        "scrollY":        "200px",
        "scrollX":        "1000px",
        "language": {
            "url": "/../Spanish.json"
        },
        //Especificaciones de las Columnas que vienen y deben mostrarse
        "columns" : [
            { data : 'idn' },
            { data : 'nameabsencetype' },
            { data : 'employeename' },
            { data : 'cant' },
            { data : 'startdate'},
            { data : 'finishdate'},
            { data : 'active'}
                
           
        ],
        //Especificaciones de la URL del servicio
        "ajax": {
            url: '../api/v1/absence',
            dataSrc : ''
        }

    });

            $('#TableAbsences tbody').on( 'click', 'tr', function () {
        
        //OBTENGO LOS VARLOES DE LA TABLA
        mod.fd.Idn = table.row( this ).data().idn;
       // mod.fd.IdnAbsenceType = table.row( this ).data().idnabsencetype;

        mod.fd.AbsenceType = table.row( this ).data().idnabsencetype;
      
                 mod.fd.Cant = table.row( this ).data().cant;
                 mod.fd.IdnEmployee = table.row( this ).data().idnemployee;
                
        mod.fd.Startdate = table.row( this ).data().startdate;
        mod.fd.Finishdate = table.row( this ).data().finishdate;    
      
        //idnabsencetype=mod.fd.NameAbsenceType;
      //ABRO LA MODAL
        mod.openmodaltoedit();

       });

     } 


function CalcularDias()

{
    var start= $("#StartDate").datepicker("getDate");
var end= $("#FinishDate").datepicker("getDate");
var days = (end- start)/(3600 * 24 * 1000 ) ;
    
   
    var total = Math.round(days);
    
    if(mod.fd.StartDate == "")
      {
        swal('Por favor seleccione una fecha de inicio','','error');
        return false;
          mod.fd.Cant='';
          
      }
         if(mod.fd.FinishDate == "")
      {
        swal('Por favor seleccione una fecha Final','','error');
        return false;
                    mod.fd.Cant='';
          
      }
         mod.fd.Cant = total;    
        
    
}