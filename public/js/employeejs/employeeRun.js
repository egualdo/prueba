NProgress.configure({ easing: 'ease', speed: 3000 });
var empRun = new Vue({
  el: '#vue-Payrun',
  data: {
    globalurl:'../api/v1/payrunhead',    
    fd:
    {
      Idn:'',
      Month:'Enero',
      Period:'',
      Periodlist:[],  
      //DataCombos    
      Typeperiod:'',
      Paymentmethod:'',
      Paymentmethodlist:[],
      PaymentRundate:'05-05-2017',
      PayContributiondate:'04-08-2017',
      Typerun:'',
      //Detail - DataRemake
      DetPayrunHead:'',
      DetPeriod:'',
      DetNamePeriod:'',
      DetEmployee:'',
      DetRoster:'',      
      DetTyperun:'',
      DetNameTyperun:'',
      DetPaymentmethod:'',
      DetPaymentRundate:'',
      DetPayContributiondate:'',

      Typerunlist:[],
      Payrunbodylist:[],
      Typeperiodlist:
      [{id:1,label:'Mensual'},{id:2,label:'Quincenal'}],
      HeadRoster:'',
      HeadSalary:'',
      HeadStartdate:'',
      HeadSitrevision:'',
      HeadModcontract:'',
      Headcct:'',
      HeadName:'',
      HeadCod:'',
      ColTotPays:0,
      ColTotNorem:0,
      ColTotDeduc:0,
      ColTotExen:0,
      NetPay:0,
      ConstantfooterLCTlist:[],
      ConstantfooterCCTlist:[],
      ConstantfooterCompanylist:[],
      ConstantfooterEmployeelist:[],



      Asignationfooterlist:[],
      Exclusionfooterlist:[],
      Newfooterlist:[],
      Vacationfooterlist:[],
      Otherfooterlist:[],
      Conceptdetaillist:[],
      ConceptDetailCant:'',
      ConceptDetailAmount:'',
        Sicossdata:[]
    },    
    ff:
    {
      save:false,
      edit:true,
      openPayruns:true,
      closedPayruns:false
    }    
  },
  mounted() {

        $('Typeperiod').on('change', function() {
          alert("Hola");
        });
        $('Typeperiod').on('change', function() {
          alert("Hola");
        });
        //Formato del Datepicker
        $('#DPickerPaymentdate').datepicker({
          format: 'dd-mm-yyyy',
          autoclose:true,
          language: 'es'                
        });
       //Obtener Datos del Datepicker
       $("#DPickerPaymentdate").datepicker().on(

        "changeDate", () => {
          //this.startDate = $('#DPickerBirthdate').val();
          empRun.fd.PaymentRundate = $('#DPickerPaymentdate').val();
        });
      //////
      $('#DPickerPayContributiondate').datepicker({
        format: 'dd-mm-yyyy',
        autoclose:true,
        language: 'es'                
      });
       //Obtener Datos del Datepicker
       $("#DPickerPayContributiondate").datepicker().on(

        "changeDate", () => {
          //this.startDate = $('#DPickerBirthdate').val();
          empRun.fd.PayContributiondate = $('#DPickerPayContributiondate').val();
        });

     },
     methods: {  
      loadMonth:function(val)
      {
        empRun.fd.Period = val;
        if (empRun.fd.Period.month === 1)
        {
          empRun.fd.Month = 'Diciembre';          
        }
        if (empRun.fd.Period.month === 2)
        {
          empRun.fd.Month = 'Enero';          
        }
        if (empRun.fd.Period.month === 3)
        {
          empRun.fd.Month = 'Febrero';          
        }
        if (empRun.fd.Period.month === 4)
        {
          empRun.fd.Month = 'Marzo';          
        }
        if (empRun.fd.Period.month === 5)
        {
          empRun.fd.Month = 'Abril';          
        }
        if (empRun.fd.Period.month === 6)
        {
          empRun.fd.Month = 'Mayo';          
        }
        if (empRun.fd.Period.month === 7)
        {
          empRun.fd.Month = 'Junio';          
        }
        if (empRun.fd.Period.month === 8)
        {
          empRun.fd.Month = 'Julio';          
        }
        if (empRun.fd.Period.month === 9)
        {
          empRun.fd.Month = 'Agosto';          
        }
        if (empRun.fd.Period.month === 10)
        {
          empRun.fd.Month = 'Septiembre';          
        }
        if (empRun.fd.Period.month === 11)
        {
          empRun.fd.Month = 'Octubre';          
        }
        if (empRun.fd.Period.month === 12)
        {
          empRun.fd.Month = 'Noviembre';          
        }
 
      },
      showClosed:function()
      {
        empRun.ff.openPayruns = false;
        empRun.ff.closedPayruns=true;
        $("#ListPayrunOpen").hide();
        $("#ListPayrunClosed").show();
      },
      showOpen:function()
      {
        empRun.ff.openPayruns = true;
        empRun.ff.closedPayruns=false;
        $("#ListPayrunOpen").show();
        $("#ListPayrunClosed").hide();
      },
      suma:function()
      {
      //Obtengo la lista de montos
      var amountList = empRun.fd.Payrunbodylist;
      var totpays = 0;
      var totnorem = 0;
      var totdeduc = 0;
      var totexen = 0;

      //recorro el objeto para sumar los totales
      for (var i in amountList) {
        var amountConcept = amountList[i].amount;
        var columnConcept = amountList[i].columnconcept;

        if (columnConcept === 1)
        {         
          totpays =totpays+ amountConcept;         
        }
        if (columnConcept === 2)
        {         
          totnorem =totnorem+ amountConcept;         
        }
        if (columnConcept === 3)
        {         

          totdeduc =totdeduc+ amountConcept;         
        }
        if (columnConcept === 4)
        {         
          totexen =totexen+ amountConcept;         
        }
     /*   alert(objeto[i].amount);
        console.log(objeto[i]);
        empRun.fd.Sumtotal =+ 
        ColTotPagos:'',
      ColTotNorem:'',
      ColTotDeduc:'',
      ColTotExen:''*/
    }

    var totpays = parseFloat(totpays.toFixed(2));
    var totnorem = parseFloat(totnorem.toFixed(2));
    var totdeduc = parseFloat(totdeduc.toFixed(2));
    var totexen = parseFloat(totexen.toFixed(2));
    empRun.fd.ColTotPays = totpays.toFixed(2);
    empRun.fd.ColTotNorem = totnorem.toFixed(2);
    empRun.fd.ColTotDeduc = totdeduc.toFixed(2);
    empRun.fd.ColTotExen = totexen.toFixed(2);

    empRun.fd.NetPay = (totpays + totnorem - totdeduc + totexen).toFixed(2);
    
  },
  remakepayrun:function()
  {
   swal({
    title: "Atención",
    text: "¿Estas seguro de correr de nuevo la nómina de "+empDP.fd.Firstname+" "+empDP.fd.Lastname+"?",
    type: "warning",
    showCancelButton: true,
    confirmButtonText: 'Correr',
    cancelButtonText: 'Cancelar'
  }).then(function () {
    
        console.log('Sending - Payrunhead:'+empRun.fd.DetPayrunHead);
        console.log('Sending - Typerun:'+empRun.fd.DetTyperun);
        console.log('Sending - Period:'+empRun.fd.DetPeriod);
        console.log('Sending - Employee:'+empRun.fd.DetEmployee);
        console.log('Sending - Roster:'+empRun.fd.DetRoster);
        console.log('Sending - Payment method:'+empRun.fd.DetPaymentmethod);
        console.log('Sending - Payment RunDate:'+empRun.fd.DetPaymentRundate);
        console.log('Sending - Contribution Date:'+empRun.fd.DetPayContributiondate);
    var arr = {
                //DATOS DE FORMULARIO                    
                Period:empRun.fd.DetPeriod,
                Employee:empRun.fd.DetEmployee, 
                Roster:empRun.fd.DetRoster,
                Paymentmethod:empRun.fd.DetPaymentmethod,
                Paydate:empRun.fd.DetPaymentmethod,
                Paycontributiondate:empRun.fd.DetPayContributiondate,
                Typerun:empRun.fd.DetTyperun, 
                TypeExecution:2,             
                idnpayrun:empRun.fd.DetPayrunHead                        
              };
                  //VUE POST
                  Vue.http.post('../api/v1/payrun/process',arr).then(response => {
                    // Si todo sale correcto              
                    swal('Corrida almacenada correctamente','','success'); 
                    empRun.GetPayrunBody();
                    empRun.getpayrunfooterbyhead();
                    empRun.GetPayrunSicoss(1);
                    empRun.cleanform();
                  }, response => {
                    if (response.status === 409)
                    {
                      swal('Elemento ya existente','Ya existe una corrida con estos parametros','error')
                    }
                    if (response.status === 500)
                    {
                     swal('Error interno',response.body,'error')
                   }
                   if (response.status === 404)
                   {
                    swal('No encontrado','No hay comunicación correcta con el servidor','error')
                  }
                      //Si no sale correcto


                    });

                })
                    //FIN VUE POST
                  },
                  closepayrun:function()
                  {
                    swal({
                      title: "Atención",
                      text: "¿Estas seguro de cerrar la corrida de "+empDP.fd.Firstname+" "+empDP.fd.Lastname+"?",
                      type: "warning",
                      showCancelButton: true,
                      confirmButtonText: 'Cerrar Corrida',
                      cancelButtonText: 'Cancelar'
                    }).then(function () {
                               //VUE POST
                               Vue.http.put('../api/v1/payrunhead/close/'+empRun.fd.DetPayrunHead).then(response => {
                    // Si todo sale correcto              
                    swal('Corrida cerrada correctamente','','success'); 
                    empRun.cleanform();
                    $('#ModalPayrunDetails').modal('toggle');

                  }, response => {
                    if (response.status === 409)
                    {
                      swal('Elemento ya existente','Ya existe una corrida con estos parametros','error')
                    }
                    if (response.status === 500)
                    {
                      swal('Error interno','Ocurrio un error interno','error')
                    }
                    if (response.status === 404)
                    {
                      swal('No encontrado','No hay comunicación correcta con el servidor','error')
                    }
                      //Si no sale correcto
                    });
                             })
                  },
                    deletepayrun:function()
                  {
                    swal({
                      title: "Atención",
                      text: "¿Estas seguro de Eliminar la corrida de "+empDP.fd.Firstname+" "+empDP.fd.Lastname+"?",
                      type: "warning",
                      showCancelButton: true,
                      confirmButtonText: 'Cerrar Corrida',
                      cancelButtonText: 'Cancelar'
                    }).then(function () {
                               //VUE POST
                               Vue.http.put('../api/v1/payrunheadt/delete/'+empRun.fd.DetPayrunHead).then(response => {
                    // Si todo sale correcto
                    swal('Corrida Eliminada correctamente','','success');
                    empRun.cleanform();
                    $('#ModalPayrunDetails').modal('toggle');

                  }, response => {
                    if (response.status === 500)
                    {
                      swal('Error interno','Ocurrio un error interno','error')
                    }
                    if (response.status === 404)
                    {
                      swal('No encontrado','No hay comunicación correcta con el servidor','error')
                    }
                      //Si no sale correcto
                    });
                             });
                  },
                  openpayrun:function()
                  {
                    swal({
                      title: "Cuidado",
                      text: "¿Estas seguro de reabrir la corrida de "+empDP.fd.Firstname+" "+empDP.fd.Lastname+"?, si reabre una corrida puede modificar datos históricos",
                      type: "warning",
                      showCancelButton: true,
                      confirmButtonText: 'Estoy seguro',
                      cancelButtonText: 'Cancelar'
                    }).then(function () {
                               //VUE POST
                               Vue.http.put('../api/v1/payrunhead/open/'+empRun.fd.DetPayrunHead).then(response => {
                    // Si todo sale correcto              
                    swal('Corrida abierta correctamente','','success'); 
                    empRun.cleanform();
                    $('#ModalPayrunDetails').modal('toggle');
                  }, response => {
                    if (response.status === 409)
                    {
                      swal('Elemento ya existente','Ya existe una corrida con estos parametros','error')
                    }
                    if (response.status === 500)
                    {
                      swal('Error interno','Ocurrio un error interno','error')
                    }
                    if (response.status === 404)
                    {
                      swal('No encontrado','No hay comunicación correcta con el servidor','error')
                    }
                      //Si no sale correcto
                    });
                             })
                  },
                  downloadreceive:function()
                  {
                    swal({
                      title: "Confirmación",
                      text: "¿Desea descargar el recibo de "+empDP.fd.Firstname+" "+empDP.fd.Lastname+"?, la información será mostrada en un documento PDF",
                      type: "warning",
                      showCancelButton: true,
                      confirmButtonText: 'Estoy seguro',
                      cancelButtonText: 'Cancelar'
            }).then(function () {                               //VUE POST
                  console.log('EMPLOYEE-'+empDP.fd.Idn);
                  console.log('PBody-'+empRun.fd.Idn);
                  console.log('PHead-'+empRun.fd.DetPayrunHead);
                    // Si todo sale correcto    
                    var arr = {
                      Employee:empDP.fd.Idn, 
                      PayrunBody:empRun.fd.Idn, 
                      PayrunHead:empRun.fd.DetPayrunHead,
                        Typedownload:1
                    };
                  //VUE POST
                  Vue.http.post(empRun.globalurl+"/validateDownloadPDF",arr).then(response => {
                    // Si todo sale correcto   
                    var Token = response.body;                        

                    window.open('../exportPDF_process/'+Token, '_blank');
                  }, response => {
                    if (response.status === 409)
                    {
                      swal('Elemento ya existente','Ya existe una corrida con estos parametros','error')
                    }
                    if (response.status === 500)
                    {
                      swal('Error interno','Ocurrio un error interno','error')
                    }
                    if (response.status === 404)
                    {
                      swal('No encontrado','No hay comunicación correcta con el servidor','error')
                    }
                      //Si no sale correcto
                    });             
                })
          },
          editreceive:function(idnpayrunbody)
          {
            empRun.fd.ConceptDetailCant = ''; 
            empRun.fd.ConceptDetailAmount = ''; 
            $('#ManualEdition').modal('toggle');
             // GET /someUrl
          this.$http.get('../api/v1/payrunbodyconceptbyid/'+idnpayrunbody).then(response => {

          // get body data
          empRun.fd.Conceptdetaillist = response.body;
          

        }, response => {
            // error callback
          });      
            
          },
          saveconceptmanual:function()
          {
             if (empRun.fd.ConceptDetailCant === '' || empRun.fd.ConceptDetailAmount === '')
             {
              swal('Indica ambos Campos','Por favor indica Cantidad y monto','error')
                   return false;
             }
              var idnpayrunbody = empRun.fd.Conceptdetaillist[0]['idn'];
                   swal({
                      title: "Confirmación",
                      text: "¿Esta seguro de editar manualmente el monto de este concepto?",
                      type: "warning",
                      showCancelButton: true,
                      confirmButtonText: 'Estoy seguro',
                      cancelButtonText: 'Cancelar'
            }).then(function () {                            //VUE POST
                
                    // Si todo sale correcto    
                    var arr = {
                      IdnPayrunBody:idnpayrunbody,
                        IdnPayrunHead:empRun.fd.DetPayrunHead,
                        CantConcept:empRun.fd.ConceptDetailCant,
                      AmountConcept:empRun.fd.ConceptDetailAmount                     
                    };
                  //VUE POST
                  Vue.http.put("../api/v1/payrunbodyconcept/update",arr).then(response => {
                    // Si todo sale correcto                                      

                   swal('Montos del concepto actualizado','Puede visualizar en la corrida el monto indicado manualmente','success')
                     empRun.GetPayrunBody();
                    empRun.getpayrunfooterbyhead();
                    empRun.cleanform();
                    $('#ManualEdition').modal('hide');
                  }, response => {
                    
                    if (response.status === 500)
                    {
                      swal('Error interno','Ocurrio un error interno','error')
                    }                  
                      //Si no sale correcto
                    });             
                })
          },
          TypePeriodOnChange:function(val)
          {    
                  
            empRun.fd.Typeperiod = val;

            combo.loadPeriodByType(val.id);
          }, 
          Run: function()
          {
            //Items para ser chequeados si estan nulos
            var items = [empRun.fd.Period,empRun.fd.Typerun,empRun.fd.Paymentmethod,empRun.fd.PayContributiondate,empRun.fd.PaymentRundate];
            //Mensajes que se mostraran para cada objeto nulo
            var strings = ['Periodo','Tipo de Corrida','Metodo de Pago','Fecha de pago de Contribuciones','Fecha de Pago'];
            //Funcion para chequear estos elementos
            var checked = CheckNulls(items,strings);
            //Si all is fine
            if (checked != 0)
            {
            swal({
            title: "Atención",
            text: "¿Estas seguro de correr la nómina de "+empDP.fd.Firstname+" "+empDP.fd.Lastname+"?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: 'Correr',
            cancelButtonText: 'Cancelar'
          }).then(function () {
              NProgress.start();
            var arr = {
                //DATOS DE FORMULARIO              
                Period:empRun.fd.Period.id,
                Employee:empDP.fd.Idn, 
                Roster:empDP.fd.Roster,
                Paymentmethod:empRun.fd.Paymentmethod.id,
                Paydate:FormatDateReverse(empRun.fd.PaymentRundate),
                Paycontributiondate:FormatDateReverse(empRun.fd.PayContributiondate),
                Typerun:empRun.fd.Typerun.id,
                TypeExecution:1                        
              };
                  //VUE POST
                  Vue.http.post('../api/v1/payrun/process',arr).then(response => {
                      NProgress.done(true);
                    // Si todo sale correcto              
                    swal('Corrida almacenada correctamente','','success'); 
                   // empRun.GetPayrunBody();
                   empRun.cleanform();
                    //$('#ModalPayrunDetails').modal('toggle');
                  }, response => {
                  NProgress.done(true);

                    if (response.status === 403)
                    {
                      swal('Corrida ya existente','Ya existe una corrida con estos parametros','error')
                    }
                    if (response.status === 500)
                    {
                      swal('Error interno','Una seleccion incorrecta de un dato para procesar, puede producir este error','error')
                    }
                    if (response.status === 404)
                    {
                      swal('No encontrado','No hay comunicación correcta con el servidor','error')
                    }
                      //Si no sale correcto
                    });
                })
            }  //FIN VUE POST
                  },  
                  GetPayrunBody:function()
                  {

          // GET /someUrl
          this.$http.get('../api/v1/payrunbodybyid/'+empRun.fd.DetPayrunHead).then(response => {

          // get body data
          empRun.fd.Payrunbodylist = response.body;
          empRun.suma();

        }, response => {
            // error callback
          });
        },
         GetPayrunSicoss:function(Type)
         {

    //GetSicoss($IdnEmployee,$IdnPeriod,$IdnTypeRun,$IdnCompany)
             // GET /someUrl
             this.$http.get('../api/v1/sicoss/employee/'+empRun.fd.DetEmployee+'/'+empRun.fd.DetPeriod+'/'+empRun.fd.DetTyperun+'/'+Type).then(response => {

                 // get body data
                 empRun.fd.Sicossdata = response.body[0];


         }, response => {
             // error callback
         });
         },
        getpayrunfooterbyhead:function()
        {
        
          // GET /someUrl
          this.$http.get('../api/v1/payrunfooterbyhead/'+empRun.fd.DetPayrunHead).then(response => {

            empRun.fd.ConstantfooterLCTlist=[];
            empRun.fd.ConstantfooterCCTlist=[];
            empRun.fd.ConstantfooterCompanylist=[];
            empRun.fd.ConstantfooterEmployeelist=[];
            empRun.fd.Asignationfooterlist=[];
            empRun.fd.Exclusionfooterlist=[];
            empRun.fd.Newfooterlist=[];
            empRun.fd.Vacationfooterlist=[];
            empRun.fd.Otherfooterlist=[];
          // get body data
           $.each(response.body,function(i,v)
             { 
             
              if (parseInt(v.type) === 2)
              {
                console.log('VARIABLE2:'+v.variable);
                empRun.fd.Asignationfooterlist.push(v);                
              }
              if (parseInt(v.type) === 3)
              {
                console.log('VARIABLE3:'+v.variable);
                empRun.fd.Exclusionfooterlist.push(v);              
              }
              if (parseInt(v.type) === 4)
              {
                console.log('VARIABLE4:'+v.variable);
                empRun.fd.Newfooterlist.push(v);                
              }
              if (parseInt(v.type) === 5)
              {
                console.log('VARIABLE5:'+v.variable);
                empRun.fd.Vacationfooterlist.push(v);              
              }
              if (parseInt(v.type) === 6)
              {
                console.log('VARIABLE6:'+v.variable);
                empRun.fd.Otherfooterlist.push(v);                
              }
              if (parseInt(v.type) === 7)
              {
                console.log('VARIABLE7:'+v.variable);
                empRun.fd.ConstantfooterLCTlist.push(v);              
              }
              if (parseInt(v.type) === 8)
              {
                console.log('VARIABLE8:'+v.variable);
                empRun.fd.ConstantfooterCCTlist.push(v);              
              }
              if (parseInt(v.type) === 9)
              {
                console.log('VARIABLE9:'+v.variable);
                empRun.fd.ConstantfooterCompanylist.push(v);              
              }
              if (parseInt(v.type) === 10)
              {
                console.log('VARIABLE10:'+v.variable);
                empRun.fd.ConstantfooterEmployeelist.push(v);              
              }
             });  
          
          empRun.suma();

        }, response => {
            // error callback
          });
        
        },
        cleanform:function()
        {
          empRun.fd.Month='';
          empRun.fd.Period='';

          empRun.fd.Paymentmethod='';

          empRun.fd.PaymentRundate='';
          empRun.fd.PayContributiondate='';   
          var table = $('#TablePayrunOpenByEmployee').dataTable();
          var table2 = $('#TablePayrunClosedByEmployee').dataTable();
      //RECARGA LOS DATOS DE LA TABLA
      table.fnReloadAjax();
      table2.fnReloadAjax();
    },
    openpayrundetail:function()
    {
      empRun.fd.Payrunbodylist = [];
      $('#ModalPayrunDetails').modal('toggle');
          //OCULTAR BOTONES O MOSTRAR
           setTimeout(
    function() {
       empRun.GetPayrunBody();
          empRun.getpayrunfooterbyhead();
        empRun.GetPayrunSicoss(2);
    }, 200);
         

        }

      }

    });


combo.loadPeriodByType(empRun.fd.Typeperiod.id);
combo.loadRunType();