Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');

//FUNCIONES GENERICAS PARA INICIO DE LA PAGINA
 var vm = new Vue({
  methods: 
      { 

           cargarcomboConstant:function()
        {
                   // GET /someUrl
            this.$http.get('../api/v1/constant').then(response => {

            // get body data
            mod.fd.Constantoptions = response.body;

          }, response => {
            // error callback
          });
        },
          cargarcomboPeriod:function()
        {
                   // GET /someUrl
            this.$http.get('../api/v1/period').then(response => {

            // get body data
            mod.fd.Periodoptions = response.body;

          }, response => {
            // error callback
          });
        },
        /*  cargarcomboSubperiod:function()
        {
                   // GET /someUrl
            this.$http.get('../api/v1/subperiod').then(response => {

            // get body data
            mod.fd.Subperiodoptions = response.body;

          }, response => {
            // error callback
          });
        },*/
        


        cargartabla:function()
        {
          CargarTabla();
        }
      }
    });

vm.cargarcomboConstant();
vm.cargarcomboPeriod();
//vm.cargarcomboSubperiod();
vm.cargartabla();

//FUNCIONES EN LA MODAL DE EDICION
var mod = new Vue({
  el: '#vuedata',
  data: {
    globalurl:'../api/v1/employee/constant',
            
    fd:
    {
      Idn:'',

      IdnPeriod:'',
          NamePeriod:'',
      Period:'',
      Periodoptions:'',
        
      IdnConstant:'',
          NameConstant:'',
      Constant:'',
      Constantoptions:'',
        

     /* IdnSubperiod:'',
         NameSubperiod:'',
      Subperiod:'',
      Subperiodoptions:'',*/
        
      IdnEmployee:'',
             NameEmployee:'',
      
             
     Value:'',
      Comments:'',
  
    },
    ff:
    {
      save:false,
      edit:true,
    }    
    
  },
  
  methods: {
   
    guardarConstant: function (event) 
    {
       
       swal({
                title: "Confirmación de guardado",
                text: "Estas seguro de guardar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {
                    //DATOS DE FORMULARIO

                    
                    idnconstant:mod.fd.Constant,
                    idnemployee:1,
                   idnperiod:mod.fd.Period,// idnsubperiod:mod.fd.Subperiod,
                    value:mod.fd.Value,
                    comment:mod.fd.Comments                    
                  };
                  //VUE POST
                Vue.http.post(mod.globalurl+"/save",arr).then(response => {
                    // Si todo sale correcto              
                    swal('Guardado Correctamente','','success');
                    $('#ModalConstant').modal('hide');
                    mod.cleanform();

                    }, response => {
                      //Si no sale correcto
                            swal('Ocurrio un error al guardar','','error')
                            mod.cleanform();
                        });
                        
                    })
                    //FIN VUE POST
           
       
    },

    editarConstant: function (event) 
    { 
       
       swal({
                title: "Confirmación de editado",
                text: "Estas seguro de editar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {

                     idnconstant:mod.fd.Constant,
                    idnemployee:1,
                   idnperiod:mod.fd.Period,// idnsubperiod:mod.fd.Subperiod,
                    value:mod.fd.Value,
                    comment:mod.fd.Comments  
                                      
                  };
                  //VUE POST
                Vue.http.put(mod.globalurl+"/update/"+mod.fd.Idn,arr).then(response => {
                    // Si todo sale correcto              
                    swal('Modificada Correctamente','','success');
                    $('#ModalConstant').modal('hide');
                    mod.cleanform();

                    }, response => {
                      //Si no sale correcto
                            swal('Ocurrio un error al modificar','','error')
                            mod.cleanform();
                        });
                        
                    })
                    //FIN VUE POST
    },

    borrarConstant: function (event)
    {
        swal({
                title: "Confirmar Eliminación",
                text: "Estas seguro de eliminar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {comment:mod.fd.Comments};
                Vue.http.put(mod.globalurl+"/delete/"+mod.fd.Idn,arr).then(response => {
                    // success callback
                    swal('Eliminado Correctamente','','success');
                $('#ModalConstant').modal('hide');
                mod.cleanform();


            }, response => {
                    swal('Ocurrio un error al eliminar','','error');
                    // error callback
                });
            })
    },

    cleanform: function()
    {
      mod.fd.Idn='';
      mod.fd.Constant='';
        mod.fd.Period='';
      //  mod.fd.Subperiod='';
    
      mod.fd.Value='';
        mod.fd.Comments='';
      $('#ModalConstant').modal('hide');
      var table = $('#TableConstant').dataTable();
      //RECARGA LOS DATOS DE LA TABLA
     table.fnReloadAjax();
     
    },
   
     openmodaltonew:function()
        {
          mod.cleanform();
          $('#ModalConstant').modal('toggle');
          //OCULTAR BOTONES O MOSTRAR
          mod.ff.save = true;
          mod.ff.edit = false;
        },
        openmodaltoedit:function()
        {
           $('#ModalConstant').modal('toggle');
          //OCULTAR BOTONES O MOSTRAR
          mod.ff.save = false;
          mod.ff.edit = true;
        }
  }
 
});

   //======INICIO DE FUNCIONES ============
    function CargarTabla()
    {   
      var table = $("#TableConstant").DataTable({
        //COMPROBACION PARA PINTAR Y CAMBIAR TEXTO DE TABLA


        "destroy": true,
        "scrollY":        "200px",
        "scrollX":        "1000px",
        "language": {
            "url": "/../Spanish.json"
        },
        //Especificaciones de las Columnas que vienen y deben mostrarse
        "columns" : [
            { data : 'idn' },
            { data : 'constantcod' },
            { data : 'periodname' },
            { data : 'employeename' },
           // { data : 'subperiodname' },
            { data : 'value'},
            { data : 'comment'},
            { data : 'active'}
                
           
        ],
        //Especificaciones de la URL del servicio
        "ajax": {
            url: '../api/v1/employee/constant/'+empDP.fd.Idn,
            dataSrc : ''
        }

    });

            $('#TableConstant tbody').on( 'click', 'tr', function () {
        
        //OBTENGO LOS VARLOES DE LA TABLA
                
        mod.fd.Idn = table.row( this ).data().idn;
                
        mod.fd.IdnPeriod = table.row( this ).data().idnperiod;
        
                mod.fd.Period = table.row( this ).data().periodname;
      
      mod.fd.IdnConstant = table.row( this ).data().idnConstant;
        mod.fd.Constant = table.row( this ).data().constantname;
      
      
      //  mod.fd.IdnSubperiod = table.row( this ).data().idnsubperiod;
        //mod.fd.Subperiod = table.row( this ).data().subperiod;
      
        mod.fd.IdnEmployee = table.row( this ).data().idnemployee;
                
        mod.fd.Value = table.row( this ).data().value;
        mod.fd.Comments = table.row( this ).data().comment;    
      
        //idnabsencetype=mod.fd.NameAbsenceType;
      //ABRO LA MODAL
        mod.openmodaltoedit();

       });

     } 
