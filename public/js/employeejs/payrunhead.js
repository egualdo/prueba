Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');

//FUNCIONES GENERICAS PARA INICIO DE LA PAGINA


//FUNCIONES EN LA MODAL DE EDICION
var mod = new Vue({
  el: '#vuedata',
  data: {
    globalurl:'../api/v1/payrunheadt',
            
    fd:
    {
      Idn:'',
      Period:'',
      Employee:'',
      Roster:'',
      Paymentmethod:'',
      Paydate:'',
      Paycontributiondate:'',
      Payments:'', 
      Deductions:'',
      Unremuns:'',
      Exempts:'',
      Totalpay:'', 
      Status:'', 
      Comment:'',
      Periodlist:'',
      Employeelist:'',
      Rosterlist:'',
      Paymethodlist:''
    },
    ff:
    {
      save:false,
      edit:true,
    }    
    
  },
  methods: {
   
    guardar: function (event) 
    {
       
       swal({
                title: "Confirmación de guardado",
                text: "Estas seguro de guardar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {
                    //DATOS DE FORMULARIO
                    Period:mod.fd.Period,
                    Employee:mod.fd.Employee,
                    Roster:mod.fd.Roster,
                    Paymentmethod:mod.fd.Paymentmethod,
                    Paydate:mod.fd.Paydate,
                    Paycontributiondate:mod.fd.Paycontributiondate,
                    Payments:mod.fd.Payments, 
                    Deductions:mod.fd.Deductions,
                    Unremuns:mod.fd.Unremuns,
                    Exempts:mod.fd.Exempts,
                    Totalpay:mod.fd.Totalpay, 
                    //Status:mod.fd.Status, 
                    Status:1,
                    Comment:mod.fd.Comment
                  };
                  //VUE POST
                Vue.http.post(mod.globalurl+"/save",arr).then(response => {
                    // Si todo sale correcto              
                    swal('Guardado Correctamente','','success');
                    $('#ModalPayRunHead').modal('hide');
                    mod.cleanform();

                    }, response => {
                      //Si no sale correcto
                            swal('Ocurrio un error al guardar','','error')
                            mod.cleanform();
                        });
                        
                    })
                    //FIN VUE POST
           
       
    },

    editar: function (event) 
    { 
       
       swal({
                title: "Confirmación de editado",
                text: "Estas seguro de editar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {

                    Period:mod.fd.Period,
                    Employee:mod.fd.Employee,
                    Roster:mod.fd.Roster,
                    Paymentmethod:mod.fd.Paymentmethod,
                    Paydate:mod.fd.Paydate,
                    Paycontributiondate:mod.fd.Paycontributiondate,
                    Payments:mod.fd.Payments, 
                    Deductions:mod.fd.Deductions,
                    Unremuns:mod.fd.Unremuns,
                    Exempts:mod.fd.Exempts,
                    Totalpay:mod.fd.Totalpay, 
                    Status:mod.fd.Status, 
                    Comment:mod.fd.Comment
                                      
                  };
                  //VUE POST
                Vue.http.put(mod.globalurl+"/update/"+mod.fd.Idn,arr).then(response => {
                    // Si todo sale correcto              
                    swal('Modificada Correctamente','','success');
                    $('#ModalPayRunHead').modal('hide');
                    mod.cleanform();

                    }, response => {
                      //Si no sale correcto
                            swal('Ocurrio un error al modificar','','error')
                            mod.cleanform();
                        });
                        
                    })
                    //FIN VUE POST
    },

    borrar: function (event)
    {
        swal({
                title: "Confirmar Eliminación",
                text: "Estas seguro de eliminar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {

                    Period:mod.fd.Period,
                    Employee:mod.fd.Employee,
                    Roster:mod.fd.Roster,
                    Paymentmethod:mod.fd.Paymentmethod,
                    Paydate:mod.fd.Paydate,
                    Paycontributiondate:mod.fd.Paycontributiondate,
                    Payments:mod.fd.Payments, 
                    Deductions:mod.fd.Deductions,
                    Unremuns:mod.fd.Unremuns,
                    Exempts:mod.fd.Exempts,
                    Totalpay:mod.fd.Totalpay, 
                    Status:mod.fd.Status, 
                    Comment:mod.fd.Comment
                     };
                Vue.http.put(mod.globalurl+"/delete/"+mod.fd.Idn,arr).then(response => {
                    // success callback
                    swal('Eliminado Correctamente','','success');
                $('#ModalPayRunHead').modal('hide');
                mod.cleanform();


            }, response => {
                    swal('Ocurrio un error al eliminar','','error');
                    // error callback
                });
            })
    },

    cleanform: function()
    {
        
        mod.fd.Period='';
        mod.fd.Employee='';
        mod.fd.Roster='';
        mod.fd.Paymentmethod='';
        mod.fd.Paydate='';
        mod.fd.Paycontributiondate='';
        mod.fd.Payments='';
        mod.fd.Deductions='';
        mod.fd.Unremuns='';
        mod.fd.Exempts='';
        mod.fd.Totalpay='';
        mod.fd.Status='';
        mod.fd.Comment='';

      $('#ModalPayRunHead').modal('hide');
      var table = $('#TablePayRunHead').dataTable();
      //RECARGA LOS DATOS DE LA TABLA
     table.fnReloadAjax();
     
    },
   
     openmodaltonew:function()
        {
          mod.cleanform();
          $('#ModalPayRunHead').modal('toggle');
          //OCULTAR BOTONES O MOSTRAR
          mod.ff.save = true;
          mod.ff.edit = false;
        },
        openmodaltoedit:function()
        {
           $('#ModalPayRunHead').modal('toggle');
          //OCULTAR BOTONES O MOSTRAR
          mod.ff.save = false;
          mod.ff.edit = true;
        }
  }
 
});

   //======INICIO DE FUNCIONES ============
    function CargarTabla()
    {   
      var table = $("#TablePayRunHead").DataTable({
        //COMPROBACION PARA PINTAR Y CAMBIAR TEXTO DE TABLA


        "destroy": true,
        "scrollY":        "200px",
        "scrollX":        "1000px",
        "language": {
            "url": "/../Spanish.json"
        },
        //Especificaciones de las Columnas que vienen y deben mostrarse
        "columns" : [
            { data : 'idn' },
            { data : 'idnperiod' },
            { data : 'paydate' },
            { data : 'payments'},
            { data : 'deductions'},
            { data : 'unremuns'},
            { data : 'exempts'},
            { data : 'totalpay'}
                
           
        ],
        //Especificaciones de la URL del servicio
        "ajax": {
            url: '../api/v1/payrunheadt',
            dataSrc : ''
        }

    });

            $('#TablePayRunHead tbody').on( 'click', 'tr', function () {
        
        //OBTENGO LOS VARLOES DE LA TABLA
        mod.fd.Idn = table.row( this ).data().idn;
        mod.fd.Period = table.row( this ).data().idnperiod;
        mod.fd.Employee = table.row( this ).data().idnemployee;
        mod.fd.Roster = table.row( this ).data().idnroster;
        mod.fd.Paymentmethod = table.row( this ).data().idnpaymentmethod;
        mod.fd.Paydate = table.row( this ).data().paydate;
        mod.fd.Paycontributiondate = table.row( this ).data().paycontributiondate;
        mod.fd.Payments = table.row( this ).data().payments;
        mod.fd.Deductions = table.row( this ).data().deductions;
        mod.fd.Unremuns = table.row( this ).data().unremuns;
        mod.fd.Exempts = table.row( this ).data().exempts;
        mod.fd.Totalpay = table.row( this ).data().totalpay;
        mod.fd.Status = table.row( this ).data().status;
        mod.fd.Comment = table.row( this ).data().comment;   
      
        //idnabsencetype=mod.fd.NameAbsenceType;
      //ABRO LA MODAL
        mod.openmodaltoedit();

       });

     } 
 var vm = new Vue({

    
  methods: 
      { 
        cargartabla:function()
        {
          CargarTabla();
        },
          cargarcomboperiod:function()
        {
                   // GET /someUrl
            this.$http.get('../api/v1/period').then(response => {

            // get body data
            mod.fd.Periodlist = response.body;

          }, response => {
            // error callback
          });
        },
          cargarcomboemployee:function()
        {
                   // GET /someUrl
            this.$http.get('../api/v1/employee').then(response => {

            // get body data
            mod.fd.Employeelist = response.body;

          }, response => {
            // error callback
          });
        },
          cargarcomboroster:function()
        {
                   // GET /someUrl
            this.$http.get('../api/v1/roster').then(response => {

            // get body data
            mod.fd.Rosterlist = response.body;

          }, response => {
            // error callback
          });
        },
          cargarcombopaymentmethod:function()
        {
                   // GET /someUrl
            this.$http.get('../api/v1/paymethod').then(response => {

            // get body data
            mod.fd.Paymethodlist = response.body;

          }, response => {
            // error callback
          });
        },
         mounted() {
        //Formato del Datepicker
       $('#DPickerPaydate').datepicker({
                format: 'dd-mm-yyyy',
                autoclose:true,
                language: 'es'                
            });
       //Obtener Datos del Datepicker
       $("#DPickerPaydate").datepicker().on(
        
        "changeDate", () => {
          //this.startDate = $('#DPickerBirthdate').val();
          mod.fd.Paydate = $('#DPickerPaydate').val();
        });
      //////
         $('#DPickerPayContributiondate').datepicker({
                format: 'dd-mm-yyyy',
                autoclose:true,
                language: 'es'                
            });
       //Obtener Datos del Datepicker
       $("#DPickerPayContributiondate").datepicker().on(
        
        "changeDate", () => {
          //this.startDate = $('#DPickerBirthdate').val();
          mod.fd.Paycontributiondate = $('#DPickerPayContributiondate').val();
        });
        
  }
      }
    });


vm.cargartabla();
vm.cargarcomboperiod();
vm.cargarcomboemployee();
vm.cargarcomboroster();
vm.cargarcombopaymentmethod();
vm.mounted();