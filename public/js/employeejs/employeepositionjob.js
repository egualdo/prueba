Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');

//FUNCIONES GENERICAS PARA INICIO DE LA PAGINA
 var vm = new Vue({
  methods: 
      { 

           cargarcombopositionjob:function()
        {
                   // GET /someUrl
            this.$http.get('../api/v1/positionjob').then(response => {

            // get body data
            mod.fd.PositionJoboptions = response.body;

          }, response => {
            // error callback
          });
        },
          cargarcombocategorysalary:function()
        {
                   // GET /someUrl
            this.$http.get('../api/v1/categorysalary').then(response => {

            // get body data
            mod.fd.CategorySalaryoptions = response.body;

          }, response => {
            // error callback
          });
        },
          cargarcombocostcenter:function()
        {
                   // GET /someUrl
            this.$http.get('../api/v1/costcenter').then(response => {

            // get body data
            mod.fd.CostCenteroptions = response.body;

          }, response => {
            // error callback
          });
        },
        


        cargartabla:function()
        {
          CargarTabla();
        }
      }
    });

vm.cargarcombocostcenter();
vm.cargarcombocategorysalary();
vm.cargarcombopositionjob();
vm.cargartabla();

//FUNCIONES EN LA MODAL DE EDICION
var mod = new Vue({
  el: '#vuedata',
  data: {
    globalurl:'../api/v1/employee/positionjob',
            
    fd:
    {
      Idn:'',

      IdnCostCenter:'',
          NameCostCenter:'',
      CostCenter:'',
      CostCenteroptions:'',
        
      IdnPositionJob:'',
          NamePositionJob:'',
      PositionJob:'',
      PositionJoboptions:'',
        

      IdnCategorySalary:'',
         NameCategorySalary:'',
      CategorySalary:'',
      CategorySalaryoptions:'',
        
      IdnEmployee:'',
             NameEmployee:'',
      
             
     Startdate:'',
      Finishdate:'',
  
    },
    ff:
    {
      save:false,
      edit:true,
    }    
    
  },
  
  methods: {
   
    guardarpositionjob: function (event) 
    {
       
       swal({
                title: "Confirmación de guardado",
                text: "Estas seguro de guardar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {
                    //DATOS DE FORMULARIO

                    
                    idnpositionjob:mod.fd.PositionJob,
                    idnemployee:1,
                   idncostcenter:mod.fd.CostCenter, idncategorysalary:mod.fd.CategorySalary,
                    startdate:mod.fd.Startdate,
                    finishdate:mod.fd.Finishdate                    
                  };
                  //VUE POST
                Vue.http.post(mod.globalurl+"/save",arr).then(response => {
                    // Si todo sale correcto              
                    swal('Guardado Correctamente','','success');
                    $('#ModalPositionjob').modal('hide');
                    mod.cleanform();

                    }, response => {
                      //Si no sale correcto
                            swal('Ocurrio un error al guardar','','error')
                            mod.cleanform();
                        });
                        
                    })
                    //FIN VUE POST
           
       
    },

    editarpositionjob: function (event) 
    { 
       
       swal({
                title: "Confirmación de editado",
                text: "Estas seguro de editar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {

                     idnpositionjob:mod.fd.PositionJob,
                    idnemployee:1,
                   idncostcenter:mod.fd.CostCenter,
                    idncategorysalary:mod.fd.CategorySalary,
                    startdate:mod.fd.Startdate,
                    finishdate:mod.fd.Finishdate
                                      
                  };
                  //VUE POST
                Vue.http.put(mod.globalurl+"/update/"+mod.fd.Idn,arr).then(response => {
                    // Si todo sale correcto              
                    swal('Modificada Correctamente','','success');
                    $('#ModalPositionjob').modal('hide');
                    mod.cleanform();

                    }, response => {
                      //Si no sale correcto
                            swal('Ocurrio un error al modificar','','error')
                            mod.cleanform();
                        });
                        
                    })
                    //FIN VUE POST
    },

    borrarpositionjob: function (event)
    {
        swal({
                title: "Confirmar Eliminación",
                text: "Estas seguro de eliminar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {startdate:mod.fd.Startdate};
                Vue.http.put(mod.globalurl+"/delete/"+mod.fd.Idn,arr).then(response => {
                    // success callback
                    swal('Eliminado Correctamente','','success');
                $('#ModalPositionjob').modal('hide');
                mod.cleanform();


            }, response => {
                    swal('Ocurrio un error al eliminar','','error');
                    // error callback
                });
            })
    },

    cleanform: function()
    {
      mod.fd.Idn='';
      mod.fd.IdnPositionJob='';
        mod.fd.IdnCostCenter='';
        mod.fd.IdnCategorySalary='';
    
      mod.fd.Startdate='';
        mod.fd.Finishdate='';
      $('#ModalPositionjob').modal('hide');
      var table = $('#TablePositionjob').dataTable();
      //RECARGA LOS DATOS DE LA TABLA
     table.fnReloadAjax();
     
    },
   
     openmodaltonew:function()
        {
          mod.cleanform();
          $('#ModalPositionjob').modal('toggle');
          //OCULTAR BOTONES O MOSTRAR
          mod.ff.save = true;
          mod.ff.edit = false;
        },
        openmodaltoedit:function()
        {
           $('#ModalPositionjob').modal('toggle');
          //OCULTAR BOTONES O MOSTRAR
          mod.ff.save = false;
          mod.ff.edit = true;
        }
  }
 
});

   //======INICIO DE FUNCIONES ============
    function CargarTabla()
    {   
      var table = $("#TablePositionjob").DataTable({
        //COMPROBACION PARA PINTAR Y CAMBIAR TEXTO DE TABLA


        "destroy": true,
        "scrollY":        "200px",
        "scrollX":        "1000px",
        "language": {
            "url": "/../Spanish.json"
        },
        //Especificaciones de las Columnas que vienen y deben mostrarse
        "columns" : [
            { data : 'idn' },
            { data : 'positionjobname' },
            { data : 'salaryname' },
            { data : 'employeename' },
            { data : 'costcentername' },
            { data : 'startdate'},
            { data : 'finishdate'},
            { data : 'active'}
                
           
        ],
        //Especificaciones de la URL del servicio
        "ajax": {
            url: '../api/v1/employee/positionjob',
            dataSrc : ''
        }

    });

            $('#TablePositionjob tbody').on( 'click', 'tr', function () {
        
        //OBTENGO LOS VARLOES DE LA TABLA
                
        mod.fd.Idn = table.row( this ).data().idn;
                
        mod.fd.IdnCostCenter = table.row( this ).data().idncostcenter;
        mod.fd.CostCenter = table.row( this ).data().idncostcenter;
      
      mod.fd.IdnPositionJob = table.row( this ).data().idnpositionjob;
        mod.fd.PositionJob = table.row( this ).data().idnpositionjob;
      
      
        mod.fd.IdnCategorySalary = table.row( this ).data().idncategorysalary;
        mod.fd.CategorySalary = table.row( this ).data().idncategorysalary;
      
        mod.fd.IdnEmployee = table.row( this ).data().idnemployee;
                
        mod.fd.Startdate = table.row( this ).data().startdate;
        mod.fd.Finishdate = table.row( this ).data().finishdate;    
      
        //idnabsencetype=mod.fd.NameAbsenceType;
      //ABRO LA MODAL
        mod.openmodaltoedit();

       });

     } 
