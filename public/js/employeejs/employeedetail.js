Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');
//FUNCIONES EN LA MODAL DE EDICION
combo.loadCivilStatus();
combo.loadNationality();
//RELACION LABORAL
combo.loadCostCenter();
combo.loadDepartment();
combo.loadPositionJob();
combo.loadWorkCalendar();
combo.loadCct();
combo.loadCategorySalary();
combo.loadCondition();
combo.loadExitReason();
combo.loadModContract();
combo.loadRoster();
combo.loadObsocial();
combo.loadMedicplan();
//Asignaciones
combo.loadPeriod();
combo.loadConcept();
combo.loadSpecialType();
//NEws- Ausencias
combo.loadAbsenceType();
//CORRIDA DE NOMINA
combo.loadPeriod();
combo.loadGroup();
combo.loadLanguage();
combo.loadPayMethodPayrun();
combo.loadCareer();
combo.loadConstant();
//Others
combo.loadSitRevision();
combo.loadWorkPlace();
combo.loadSindicate();
combo.loadRegPrevisional();
combo.loadSinestercode();
combo.loadLevelStudies();
var empHead = new Vue({
    el: '#vue-EmployeeHead',
    data: {
        globalurl: '../api/v1/employee',
        fd: {
            HeadCod: '',
            HeadRoster: '',
            HeadSalary: '',
            HeadStartdate: '',
            HeadFinishdate: '',
            HeadSitrevision: '',
            HeadModcontract: '',
            Headcct: '',
            Active: '',
            cloDni: '',
            cloCod: '',
            cloCuil: '',
            cloGender: '',
            cloFirstname: '',
            cloLastname: '',
            cloBirthdate: '',
            cloCivilstatus: '',
            cloNationality: '',
            Nationalitylist: [],
            Civilstatuslist: []
        },
        ff: {
            save: false,
            edit: true
        }
    },
    methods: {
        returndata: function() {
            alert("Volver al listado");
        },
        inactivar: function(event) {
            swal({
                title: "Confirmación de inactivación",
                text: "Estas seguro de inactivar este legajo",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function() {
                var arr = {
                    //DATOS DE FORMULARIO
                    active: empDP.fd.Active
                };
                //VUE POST
                Vue.http.put(empDP.globalurl + "/inactive/" + empDP.fd.Idn, arr).then(response => {
                    // Si todo sale correcto
                    swal('Inactivado Correctamente', '', 'success');
                    $('#ModalEdicion').modal('hide');
                    empHead.fd.Active = 0;
                }, response => {
                    //Si no sale correcto
                    swal('Ocurrio un error al inactivar', '', 'error')
                });
            })
            //FIN VUE POST
        },
        activar: function(event) {
            swal({
                title: "Confirmación de Activacion",
                text: "Estas seguro de activar este legajo",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function() {
                var arr = {
                    //DATOS DE FORMULARIO
                    active: empDP.fd.Active
                };
                //VUE POST
                Vue.http.put(empDP.globalurl + "/active/" + empDP.fd.Idn, arr).then(response => {
                    // Si todo sale correcto
                    swal('Activado Correctamente', '', 'success');
                    $('#ModalEdicion').modal('hide');
                    empHead.fd.Active = 1;
                }, response => {
                    //Si no sale correcto
                    swal('Ocurrio un error al activar', '', 'error')
                });
            })
            //FIN VUE POST
        },
        openmodalclonar: function() {
            $('#ModalClone').modal('show');
        },
        calcularcuil: function() {
            if (empHead.fd.cloDni !== '' && empHead.fd.cloDni.length === 8 && empHead.fd.cloGender !== '') {
                empHead.fd.cloCuil = get_cuil_cuit(empHead.fd.cloDni, empHead.fd.cloGender);
            } else {
                empHead.fd.cloCuil = '';
            }
        },
        clonar: function() {
            if (empHead.fd.cloCod === '' || empHead.fd.cloFirstname === '' || empHead.fd.cloLastname === '' || empHead.fd.cloGender === '' || empHead.fd.cloDni === '' || empHead.fd.cloCuil === '' || empHead.fd.cloBirthdate === '' || empHead.fd.cloCivilstatus === '' || empHead.fd.cloNationality === '') {
                swal('Por favor complete la información requerida', '', 'warning');
                return false;
            }
            swal({
                title: "Confirmación de Clonado",
                text: "¿Estas seguro de clonar el legajo?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function() {
                var arr = {
                    Idn: empDP.fd.Idn,
                    Cod: empHead.fd.cloCod,
                    Firstname: empHead.fd.cloFirstname,
                    Lastname: empHead.fd.cloLastname,
                    Gender: empHead.fd.cloGender,
                    Dni: empHead.fd.cloDni,
                    Cuil: empHead.fd.cloCuil,
                    Birthdate: FormatDateReverse(empHead.fd.cloBirthdate),
                    Civilstatus: empHead.fd.cloCivilstatus,
                    Nationality: empHead.fd.cloNationality
                };
                //VUE POST
                Vue.http.post(empHead.globalurl + "/clone/" + empDP.fd.Idn, arr).then(function(response) {
                    empHead.fd.cloCod = '';
                    empHead.fd.cloFirstname = '';
                    empHead.fd.cloLastname = '';
                    empHead.fd.cloGender = '';
                    empHead.fd.cloDni = '';
                    empHead.fd.cloCuil = '';
                    empHead.fd.cloBirthdate = '';
                    empHead.fd.cloCivilstatus = '';
                    empHead.fd.cloNationality = '';
                    $('#ModalClone').modal('hide');
                    swal('Se ha Clonado Correctamente correctamente', '', 'success');
                }).catch(function() {
                    swal('Ocurrio un error intentando clonar', '', 'error');
                });
            });
        },
        cleanform: function() {
            empHead.fd.cloCod = '';
            empHead.fd.cloFirstname = '';
            empHead.fd.cloLastname = '';
            empHead.fd.cloGender = '';
            empHead.fd.cloDni = '';
            empHead.fd.cloCuil = '';
            empHead.fd.cloBirthdate = '';
            empHead.fd.cloCivilstatus = '';
            empHead.fd.cloNationality = '';
            $('#ModalClone').modal('hide');
        }
    },
    mounted() {
        $('#cloDPickerBirthdate').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            language: 'es'
        });
        $("#cloDPickerBirthdate").datepicker().on("changeDate", function() {
            empHead.fd.cloBirthdate = $('#cloDPickerBirthdate').val();
        });
    },
});
//Datos personales del legajo
var empDP = new Vue({
    el: '#vue-DataPerson',
    data: {
        globalurl: '../api/v1/employee',
        fd: {
            Idn: '',
            Cod: '',
            Firstname: '',
            Lastname: '',
            Gender: '',
            Dni: '',
            Cuil: '',
            Birthdate: '',
            Workemail: '',
            Personemail: '',
            Telephone: '',
            Cellphone: '',
            Civilstatus: '',
            Civilstatuslist: [],
            startDate: '',
            Roster: '',
            Active: 1
        },
        ff: {
            save: false,
            edit: true,
        }
    },
    mounted() {
        //Formato del Datepicker
        $('#DPickerBirthdate').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            language: 'es'
        });
        //Obtener Datos del Datepicker
        $("#DPickerBirthdate").datepicker().on("changeDate", () => {
            //this.startDate = $('#DPickerBirthdate').val();
            empDP.fd.Birthdate = $('#DPickerBirthdate').val();
        });
    },
    methods: {
        updateBasicInfo: function() {
            //Items para ser chequeados si estan nulos
            var items = [empDP.fd.Cod, empDP.fd.Firstname, empDP.fd.Lastname, empDP.fd.Dni, empDP.fd.Cuil, empDP.fd.Birthdate, empDP.fd.Civilstatus];
            //Mensajes que se mostraran para cada objeto nulo
            var strings = ['un Nro de Legajo', 'un Nombre', 'un Apellido', 'un Dni', 'un Cuil', 'una Fecha de Nacimiento', 'un estado Civil'];
            //Funcion para chequear estos elementos
            var checked = CheckNulls(items, strings);
            //Si all is fine
            if (checked != 0) {
                swal({
                    title: "Confirmación de editado",
                    text: "¿Estas seguro de editar los datos personales?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: 'Aceptar',
                    cancelButtonText: 'Cancelar'
                }).then(function() {
                    var arr = {
                        //DATOS DE FORMULARIO
                        Cod: empDP.fd.Cod,
                        Firstname: empDP.fd.Firstname,
                        Lastname: empDP.fd.Lastname,
                        Gender: empDP.fd.Gender,
                        Dni: empDP.fd.Dni,
                        Cuil: empDP.fd.Cuil,
                        Birthdate: FormatDateReverse(empDP.fd.Birthdate),
                        Workemail: empDP.fd.Workemail,
                        Personemail: empDP.fd.Personemail,
                        Telephone: empDP.fd.Telephone,
                        Cellphone: empDP.fd.Cellphone,
                        Civilstatus: empDP.fd.Civilstatus.id
                    };
                    //VUE POST
                    Vue.http.put(empDP.globalurl + "/persondata/update/" + empDP.fd.Idn, arr).then(response => {
                        // Si todo sale correcto
                        swal('Datos modificados correctamente', '', 'success')
                    }, response => { //Si no sale correcto
                        swal('Ocurrio un error al modificar', '', 'error')
                    });
                })
            }
            //FIN VUE POST
        }
    }
});
var empFa = new Vue({
    el: '#FamilyEmployee',
    data: {
        globalurl: '../api/v1/employee',
        fd: {
            Idn: '',
            Familytype: 'Hijo',
            Cuil: '',
            Gender: 'M',
            Firstname: '',
            Lastname: '',
            Birthdate: ''
        },
        ff: {
            save: false,
            edit: true,
        }
    },
    mounted() {
        //Formato del Datepicker
        $('#DPickerBirthdateFamily').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            language: 'es'
        });
        //Obtener Datos del Datepicker
        $("#DPickerBirthdateFamily").datepicker().on("changeDate", () => {
            //this.startDate = $('#DPickerBirthdate').val();
            empFa.fd.Birthdate = $('#DPickerBirthdateFamily').val();
        });
    },
    methods: {
        guardarFamily: function(event) {
                        //Items para ser chequeados si estan nulos
            var items = [empFa.fd.Familytype, empFa.fd.Cuil, empFa.fd.Gender, empFa.fd.Firstname, empFa.fd.Lastname, empFa.fd.Birthdate];
            //Mensajes que se mostraran para cada objeto nulo
            var strings = ['un Parentezco', 'un Cuil', 'un Genero', 'un Nombre', 'un Apellido', 'una Fecha de Nacimiento'];
            //Funcion para chequear estos elementos
            var checked = CheckNulls(items, strings);
            //Si all is fine
            if (checked != 0) {
            swal({
                title: "Confirmación de guardado",
                text: "Estas seguro de guardar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function() {
                var arr = {
                    //DATOS DE FORMULARIO
                    Familytype: empFa.fd.Familytype,
                    Cuil: empFa.fd.Cuil,
                    Gender: empFa.fd.Gender,
                    Firstname: empFa.fd.Firstname,
                    Lastname: empFa.fd.Lastname,
                    Birthdate: FormatDateReverse(empFa.fd.Birthdate),
                    Employee: empDP.fd.Idn
                };
                //VUE POST
                Vue.http.post(empFa.globalurl + "/family/save", arr).then(response => {
                    // Si todo sale correcto
                    swal('Familiar Guardado Correctamente', '', 'success');
                    empFa.cleanform();
                }, response => {
                    //Si no sale correcto
                    swal('Ocurrio un error al guardar', '', 'error')
                    empFa.cleanform();
                });
            })
          }
            //FIN VUE POST
        },
        editarFamily: function(event) {
            if (empFa.fd.Familytype === '') {
                swal({
                    title: "Atención",
                    text: "Debes Indicar el Parentezco",
                    type: "warning",
                    confirmButtonText: 'Aceptar'
                });
            }
            if (empFa.fd.Cuil === '') {
                swal({
                    title: "Atención",
                    text: "Debes Indicar el Cuil",
                    type: "warning",
                    confirmButtonText: 'Aceptar'
                });
            }
            if (empFa.fd.Gender === '') {
                swal({
                    title: "Atención",
                    text: "Debes Indicar el Genero",
                    type: "warning",
                    confirmButtonText: 'Aceptar'
                });
            }
            if (empFa.fd.Firstname === '') {
                swal({
                    title: "Atención",
                    text: "Debes Indicar el Nombre",
                    type: "warning",
                    confirmButtonText: 'Aceptar'
                });
            }
            if (empFa.fd.Lastname === '') {
                swal({
                    title: "Atención",
                    text: "Debes Indicar el Apellido",
                    type: "warning",
                    confirmButtonText: 'Aceptar'
                });
            }
            if (empFa.fd.Birthdate === '') {
                swal({
                    title: "Atención",
                    text: "Debes Indicar la fecha de Nacimiento",
                    type: "warning",
                    confirmButtonText: 'Aceptar'
                });
            }
            swal({
                title: "Confirmación de editado",
                text: "Estas seguro de editar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function() {
                var arr = {
                    //DATOS DE FORMULARIO
                    Familytype: empFa.fd.Familytype,
                    Cuil: empFa.fd.Cuil,
                    Gender: empFa.fd.Gender,
                    Firstname: empFa.fd.Firstname,
                    Lastname: empFa.fd.Lastname,
                    Birthdate: FormatDateReverse(empFa.fd.Birthdate),
                    Employee: empDP.fd.Idn
                };
                //VUE POST
                Vue.http.put(empFa.globalurl + "/family/update/" + empFa.fd.Idn, arr).then(response => {
                    // Si todo sale correcto
                    swal('Modificada Correctamente', '', 'success');
                    empFa.cleanform();
                }, response => {
                    //Si no sale correcto
                    swal('Ocurrio un error al modificar', '', 'error')
                    empFa.cleanform();
                });
            })
            //FIN VUE POST
        },
        borrarFamily: function(event) {
            swal({
                title: "Confirmar Eliminación",
                text: "Estas seguro de eliminar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function() {
                Vue.http.put(empFa.globalurl + "/family/delete/" + empFa.fd.Idn).then(response => {
                    // success callback
                    swal('Eliminado Correctamente', '', 'success');
                    empFa.cleanform();
                }, response => {
                    swal('Ocurrio un error al eliminar', '', 'error');
                    // error callback
                });
            })
        },
        cleanform: function() {
            empFa.fd.Familytype = 'Hijo';
            empFa.fd.Cuil = '';
            empFa.fd.Gender = 'M';
            empFa.fd.Firstname = '';
            empFa.fd.Lastname = '';
            var date = new Date();
            var day = date.getDate();
            var monthIndex = date.getMonth();
            var year = date.getFullYear();
            // empFa.fd.Birthdate=day + '-' + monthIndex + '-' + year;
            $('#ModalFamilyEmployee').modal('hide');
            var table = $('#TableEmployeeFamily').dataTable();
            //RECARGA LOS DATOS DE LA TABLA
            table.fnReloadAjax();
        },
        openmodaltonew: function() {
            empFa.cleanform();
            $('#ModalFamilyEmployee').modal('toggle');
            //OCULTAR BOTONES O MOSTRAR
            empFa.ff.save = true;
            empFa.ff.edit = false;
        },
        openmodaltoedit: function() {
            $('#ModalFamilyEmployee').modal('toggle');
            //OCULTAR BOTONES O MOSTRAR
            empFa.ff.save = false;
            empFa.ff.edit = true;
        }
    }
});
var empBa = new Vue({
    el: '#BankAccountEmployee',
    data: {
        globalurl: '../api/v1/employee',
        fd: {
            Idn: '',
            Paymentmethod: '',
            Accounttype: '',
            Bank: '',
            Cbu: '',
            Branch: '',
            Percentage: '',
            Numberaccount: '',
            Banklist: [],
            Paymentlist: []
        },
        ff: {
            save: false,
            edit: true
        }
    },
    methods: {
        validar_cien: function(){
            console.log(parseInt(empBa.fd.Percentage));
            if(parseInt(empBa.fd.Percentage) > 100 || parseInt(empBa.fd.Percentage) < 0){
                toastr.warning('el valor del porcentaje debe estar entre 0 y 100');
                empBa.fd.Percentage = 0;
            }
        },
        guardarBankAccount: function(event) {
                checked = CheckNulls([empBa.fd.Paymentmethod], ['un Tipo de Pago']);
            if (checked !== 0) {
                if (empBa.fd.Paymentmethod === 3 || empBa.fd.Paymentmethod === 4) {
                    var items = [empBa.fd.Bank, empBa.fd.Numberaccount,empBa.fd.Cbu,empBa.fd.Branch,empBa.fd.Accounttype];
                    var strings = ['un Banco', 'un Numero de Cuenta','el CBU', 'La sucursal', 'el Tipo de Cuenta'];
                    checked = CheckNulls(items, strings);
                }
            }

            if (checked !== 0) {

            swal({
                title: "Confirmación de guardado",
                text: "Estas seguro de guardar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function() {
                var arr = {
                    //DATOS DE FORMULARIO
                    Paymentmethod: empBa.fd.Paymentmethod,
                    Accounttype: empBa.fd.Accounttype,
                    Bank: empBa.fd.Bank,
                    Cbu: empBa.fd.Cbu,
                    Branch: empBa.fd.Branch,
                    Percentage: empBa.fd.Percentage,
                    Numberaccount: empBa.fd.Numberaccount,
                    Employee: empDP.fd.Idn
                };
                //VUE POST
                Vue.http.post(empBa.globalurl + "/bankaccount/save", arr).then(response => {
                    // Si todo sale correcto
                    swal('Cuenta Guardada Correctamente', '', 'success');
                    $('#ModalBankAccountEmployee').modal('hide');
                    empBa.cleanform();
                }, response => {
                    //Si no sale correcto
                    swal('Ocurrio un error al guardar', '', 'error')
                    empBa.cleanform();
                });
            })
          }
            //FIN VUE POST
        },
        editarBankAccount: function(event) {
            checked = CheckNulls([empBa.fd.Paymentmethod], ['un Tipo de Pago']);
            if (checked !== 0) {
                if (empBa.fd.Paymentmethod === 3 || empBa.fd.Paymentmethod === 4) {
                    var items = [empBa.fd.Bank, empBa.fd.Numberaccount,empBa.fd.Cbu,empBa.fd.Branch,empBa.fd.Accounttype];
                    var strings = ['un Banco', 'un Numero de Cuenta','el CBU', 'La sucursal', 'el Tipo de Cuenta'];
                    checked = CheckNulls(items, strings);
                }
            }
            if (checked !== 0) {
            swal({
                title: "Confirmación de editado",
                text: "Estas seguro de editar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function() {
                var arr = {
                    Paymentmethod: empBa.fd.Paymentmethod,
                    Accounttype: empBa.fd.Accounttype,
                    Bank: empBa.fd.Bank,
                    Cbu: empBa.fd.Cbu,
                    Branch: empBa.fd.Branch,
                    Percentage: empBa.fd.Percentage,
                    Numberaccount: empBa.fd.Numberaccount,
                    Employee: empDP.fd.Idn
                };
                //VUE POST
                Vue.http.put(empBa.globalurl + "/bankaccount/update/" + empBa.fd.Idn, arr).then(response => {
                    // Si todo sale correcto
                    swal('Modificada Correctamente', '', 'success');
                    $('#ModalBankAccountEmployee').modal('hide');
                    empBa.cleanform();
                }, response => {
                    //Si no sale correcto
                    swal('Ocurrio un error al modificar', '', 'error')
                  ;
                });
            })
            //FIN VUE POST
              }
        },
        borrarBankAccount: function(event) {
            swal({
                title: "Confirmar Eliminación",
                text: "Estas seguro de eliminar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function() {
                Vue.http.put(empBa.globalurl + "/bankaccount/delete/" + empBa.fd.Idn).then(response => {
                    // success callback
                    swal('Eliminado Correctamente', '', 'success');
                    $('#ModalBankAccountEmployee').modal('hide');
                    empBa.cleanform();
                }, response => {
                    swal('Ocurrio un error al eliminar', '', 'error');
                    // error callback
                });
            })
        },
        cleanform: function() {
            empBa.fd.Paymentmethod = '';
            empBa.fd.Accounttype = '';
            empBa.fd.Bank = '';
            empBa.fd.Cbu = '';
            empBa.fd.Branch = '';
            empBa.fd.Percentage = '';
            empBa.fd.Numberaccount = '';
            $('#ModalBankAccountEmployee').modal('hide');
            var table = $('#TableEmployeeBankAccount').dataTable();
            //RECARGA LOS DATOS DE LA TABLA
            table.fnReloadAjax();
        },
        openmodaltonew: function() {
            empBa.cleanform();
            $('#ModalBankAccountEmployee').modal('toggle');
            //OCULTAR BOTONES O MOSTRAR
            empBa.ff.save = true;
            empBa.ff.edit = false;
        },
        openmodaltoedit: function() {
            $('#ModalBankAccountEmployee').modal('toggle');
            //OCULTAR BOTONES O MOSTRAR
            empBa.ff.save = false;
            empBa.ff.edit = true;
        },
        loadcomboBanks: function() {
            // GET /someUrl
            this.$http.get('../api/v1/bank').then(response => {
                // get body data
                empBa.fd.Banklist = response.body;
            }, response => {
                // error callback
            });
        },
        loadcomboPaymentMethods: function() {
            // GET /someUrl
            this.$http.get('../api/v1/paymethod').then(response => {
                // get body data
                empBa.fd.Paymentlist = response.body;
            }, response => {
                // error callback
            });
        }
    }
});
empBa.loadcomboPaymentMethods();
empBa.loadcomboBanks();
var empCa = new Vue({
    el: '#CareerEmployees',
    data: {
        globalurl: '../api/v1/employee',
        fd: {
            Idn: '',
            Career: '',
            Careerlist: [],
            Title: '',
            StartDate: '',
            FinishDate: ''
        },
        ff: {
            save: false,
            edit: true,
        }
    },
    mounted() {
        //Formato del Datepicker
        $('#DPickerStartdate').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            language: 'es'
        });
        //Obtener Datos del Datepicker
        $("#DPickerStartdate").datepicker().on("changeDate", () => {
            //this.startDate = $('#DPickerBirthdate').val();
            empCa.fd.StartDate = $('#DPickerStartdate').val();
        });
        //Formato del Datepicker
        $('#DPickerFinishdate').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            language: 'es'
        });
        //Obtener Datos del Datepicker
        $("#DPickerFinishdate").datepicker().on("changeDate", () => {
            //this.startDate = $('#DPickerBirthdate').val();
            empCa.fd.FinishDate = $('#DPickerFinishdate').val();
        });
    },
    methods: {
        guardarCareer: function(event) {
           var items = [empCa.fd.Career, empCa.fd.Title, empCa.fd.StartDate,empCa.fd.FinishDate];
            //Mensajes que se mostraran para cada objeto nulo
            var strings = ['una Carrera', 'un Titulo', 'una Fecha de inicio','una Fecha final'];
            //Funcion para chequear estos elementos
            var checked = CheckNulls(items, strings);
            //Si all is fine
            if (checked != 0) {

            swal({
                title: "Confirmación de guardado",
                text: "Estas seguro de guardar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function() {
                var arr = {
                    //DATOS DE FORMULARIO
                    Career: empCa.fd.Career.id,
                    Title: empCa.fd.Title,
                    Startdate: FormatDateReverse(empCa.fd.StartDate),
                    Finishdate: FormatDateReverse(empCa.fd.FinishDate),
                    Employee: empDP.fd.Idn
                };
                //VUE POST
                Vue.http.post(empCa.globalurl + "/career/save", arr).then(response => {
                    // Si todo sale correcto
                    swal('Familiar Guardado Correctamente', '', 'success');
                    empCa.cleanform();
                }, response => {
                    //Si no sale correcto
                    swal('Ocurrio un error al guardar', '', 'error')
                    empCa.cleanform();
                });
            })
          }
            //FIN VUE POST
        },
        editarCareer: function(event) {
            swal({
                title: "Confirmación de editado",
                text: "Estas seguro de editar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function() {
                var arr = {
                    //DATOS DE FORMULARIO
                    Career: empCa.fd.Career,
                    Title: empCa.fd.Title,
                    Startdate: FormatDateReverse(empCa.fd.StartDate),
                    Finishdate: FormatDateReverse(empCa.fd.FinishDate),
                    Employee: empDP.fd.Idn
                };
                //VUE POST
                Vue.http.put(empCa.globalurl + "/career/update/" + empCa.fd.Idn, arr).then(response => {
                    // Si todo sale correcto
                    swal('Modificada Correctamente', '', 'success');
                    empCa.cleanform();
                }, response => {
                    //Si no sale correcto
                    swal('Ocurrio un error al modificar', '', 'error')
                    empCa.cleanform();
                });
            })
            //FIN VUE POST
        },
        borrarCareer: function(event) {
            swal({
                title: "Confirmar Eliminación",
                text: "Estas seguro de eliminar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function() {
                Vue.http.put(empCa.globalurl + "/career/delete/" + empCa.fd.Idn).then(response => {
                    // success callback
                    swal('Eliminado Correctamente', '', 'success');
                    empCa.cleanform();
                }, response => {
                    swal('Ocurrio un error al eliminar', '', 'error');
                    // error callback
                });
            })
        },
        cleanform: function() {
            empCa.fd.Title = '';
            var date = new Date();
            var day = date.getDate();
            var monthIndex = date.getMonth();
            var year = date.getFullYear();
            // empCa.fd.StartDate=year + '-' + monthIndex + '-' + day;
            // empCa.fd.FinishDate=year + '-' + monthIndex + '-' + day;
            $('#ModalCareerEmployee').modal('hide');
            var table = $('#TableEmployeeCareer').dataTable();
            //RECARGA LOS DATOS DE LA TABLA
            table.fnReloadAjax();
        },
        openmodaltonew: function() {
            empCa.cleanform();
            $('#ModalCareerEmployee').modal('toggle');
            //OCULTAR BOTONES O MOSTRAR
            empCa.ff.save = true;
            empCa.ff.edit = false;
        },
        openmodaltoedit: function() {
            $('#ModalCareerEmployee').modal('toggle');
            //OCULTAR BOTONES O MOSTRAR
            empCa.ff.save = false;
            empCa.ff.edit = true;
        }
    }
});
var empLa = new Vue({
    el: '#LanguageEmployee',
    data: {
        globalurl: '../api/v1/employee',
        fd: {
            Idn: '',
            Language: '',
            Languagelist: [],
            Level: ''
        },
        ff: {
            save: false,
            edit: true,
        }
    },
    methods: {
        guardarLanguage: function(event) {
          var items = [empLa.fd.Language, empLa.fd.Level];
            //Mensajes que se mostraran para cada objeto nulo
            var strings = ['una Lenguaje', 'un Nivel'];
            //Funcion para chequear estos elementos
            var checked = CheckNulls(items, strings);
            //Si all is fine
            if (checked != 0) {

            swal({
                title: "Confirmación de guardado",
                text: "Estas seguro de guardar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function() {
                var arr = {
                    //DATOS DE FORMULARIO
                    Language: empLa.fd.Language.id,
                    Level: empLa.fd.Level,
                    Employee: empDP.fd.Idn
                };
                //VUE POST
                Vue.http.post(empLa.globalurl + "/language/save", arr).then(response => {
                    // Si todo sale correcto
                    swal('Lenguaje Guardado Correctamente', '', 'success');
                    empLa.cleanform();
                }, response => {
                    //Si no sale correcto
                    swal('Ocurrio un error al guardar', '', 'error')
                    empLa.cleanform();
                });
            })
          }
            //FIN VUE POST
        },
        editarLanguage: function(event) {
            swal({
                title: "Confirmación de editado",
                text: "Estas seguro de editar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function() {
                var arr = {
                    //DATOS DE FORMULARIO
                    Language: empLa.fd.Language,
                    Level: empLa.fd.Level,
                    Employee: empDP.fd.Idn
                };
                //VUE POST
                Vue.http.put(empLa.globalurl + "/language/update/" + empLa.fd.Idn, arr).then(response => {
                    // Si todo sale correcto
                    swal('Modificada Correctamente', '', 'success');
                    empLa.cleanform();
                }, response => {
                    //Si no sale correcto
                    swal('Ocurrio un error al modificar', '', 'error')
                    empLa.cleanform();
                });
            })
            //FIN VUE POST
        },
        borrarLanguage: function(event) {
            swal({
                title: "Confirmar Eliminación",
                text: "Estas seguro de eliminar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function() {
                Vue.http.put(empLa.globalurl + "/language/delete/" + empLa.fd.Idn).then(response => {
                    // success callback
                    swal('Eliminado Correctamente', '', 'success');
                    empLa.cleanform();
                }, response => {
                    swal('Ocurrio un error al eliminar', '', 'error');
                    // error callback
                });
            })
        },
        cleanform: function() {
            // empLa.fd.='';
            $('#ModalLanguageEmployee').modal('hide');
            var table = $('#TableEmployeeLanguage').dataTable();
            //RECARGA LOS DATOS DE LA TABLA
            table.fnReloadAjax();
        },
        openmodaltonew: function() {
            empLa.cleanform();
            $('#ModalLanguageEmployee').modal('toggle');
            //OCULTAR BOTONES O MOSTRAR
            empLa.ff.save = true;
            empLa.ff.edit = false;
        },
        openmodaltoedit: function() {
            $('#ModalLanguageEmployee').modal('toggle');
            //OCULTAR BOTONES O MOSTRAR
            empLa.ff.save = false;
            empLa.ff.edit = true;
        }
    }
});
var empRE = new Vue({
    el: '#vue-Residence',
    data: {
        globalurl: '../api/v1/employee',
        fd: {
            Nationality: '',
            City: '',
            Province: '',
            Postalcode: '',
            Street: '',
            Number: '',
            Floor: '',
            Department: '',
            Nationalitylist: [],
        },
        ff: {
            save: false,
            edit: true,
        }
    },
    methods: {
        updateResidence: function() {
           var items = [empRE.fd.Nationality, empRE.fd.City];
            //Mensajes que se mostraran para cada objeto nulo
            var strings = ['una Nacionalidad', 'una Ciudad'];
            //Funcion para chequear estos elementos
            var checked = CheckNulls(items, strings);
            //Si all is fine
            if (checked != 0) {

            swal({
                title: "Confirmación de editado",
                text: "¿Estas seguro de editar los datos de residencia?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function() {
                var arr = {
                    //DATOS DE FORMULARIO
                    Nationality: empRE.fd.Nationality.id,
                    City: empRE.fd.City,
                    Province: empRE.fd.Province,
                    Postalcode: empRE.fd.Postalcode,
                    Street: empRE.fd.Street,
                    Number: empRE.fd.Number,
                    Floor: empRE.fd.Floor,
                    Department: empRE.fd.Department
                };
                //VUE POST
                Vue.http.put(empDP.globalurl + "/residence/update/" + empDP.fd.Idn, arr).then(response => {
                    // Si todo sale correcto
                    swal('Datos de residencia modificados correctamente', '', 'success');
                }, response => {
                    //Si no sale correcto
                    swal('Ocurrio un error al modificar', '', 'error')
                });
            })
          }
            //FIN VUE POST
        },
    }
});
var empWR = new Vue({
    el: '#vue-WorkRelation',
    data: {
        globalurl: '../api/v1/employee',
        fd: {
            Costcenter: '',
            Department: '',
            Positionjob: '',
            Workcalendar: '',
            Cct: '',
            Group: '',
            Categorysalary: '',
            CategorySalaryAmount: '',
            BasicSalaryAmount: '',
            Basicsalary: '',
            //Listados de Combos
            Costcenterlist: [],
            Departmentlist: [],
            Workcalendarlist: [],
            Positionjoblist: [],
            Cctlist: [],
            Grouplist: [],
            Categorysalarylist: [],
        },
        ff: {
            save: false,
            edit: true,
        }
    },
    methods: {
        updateWorkRelation: function() {
            var items = [empWR.fd.Costcenter, empWR.fd.Department,empWR.fd.Positionjob,
            empWR.fd.Workcalendar,empWR.fd.Categorysalary,empWR.fd.Cct];
            //Mensajes que se mostraran para cada objeto nulo
            var strings = ['un Centro de Costo', 'un Departamento','nn Puesto',
            'un Calendario Laboral','una Categoria de Salario','un CCT'];
            //Funcion para chequear estos elementos
            var checked = CheckNulls(items, strings);
            //Si all is fine
            if (checked != 0) {           
          
            swal({
                title: "Confirmación de edición",
                text: "¿Estas seguro de editar los datos de residencia?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function() {
                var arr = {
                    //DATOS DE FORMULARIO
                    Costcenter: empWR.fd.Costcenter.id,
                    Department: empWR.fd.Department.id,
                    Positionjob: empWR.fd.Positionjob.id,
                    Workcalendar: empWR.fd.Workcalendar.id,
                    Cct: empWR.fd.Cct.id,
                    Categorysalary: empWR.fd.Categorysalary.id,
                    Basicsalary: empWR.fd.Basicsalary
                };
                //VUE POST
                
                Vue.http.put(empDP.globalurl + "/workrelation/update/" + empDP.fd.Idn, arr).then(response => {
                    // Si todo sale correcto
                    swal('Datos laborales modificados correctamente', '', 'success');
                }, response => {
                    //Si no sale correcto
                    swal('Ocurrio un error al modificar', '', 'error')
                });
            })
          }
            //FIN VUE POST
        },
    }
});
var empGroup = new Vue({
    el: '#GroupiEmployee',
    data: {
        globalurl: '../api/v1/employee/group',
        fd: {
            Idn: '',
            Group: '',
            Grouplist: [],
            Firstname: '',
            Employee: '',
            Name: ''
        },
        ff: {
            save: false,
            edit: true,
        }
    },
    methods: {
        guardarGroup: function(event) {
           var items = [empGroup.fd.Group];
            //Mensajes que se mostraran para cada objeto nulo
            var strings = ['un Grupo'];
            //Funcion para chequear estos elementos
            var checked = CheckNulls(items, strings);
            //Si all is fine
            if (checked != 0) {  

            swal({
                title: "Confirmación de guardado",
                text: "Estas seguro de guardar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function() {
                var arr = {
                    //DATOS DE FORMULARIO
                    Group: empGroup.fd.Group.id,
                    Name: empGroup.fd.Name,
                    // Startdate:empCa.fd.StartDate,
                    //Finishdate:empCa.fd.FinishDate,
                    Employee: empDP.fd.Idn
                };
                //VUE POST
                Vue.http.post(empGroup.globalurl + "/save", arr).then(response => {
                    // Si todo sale correcto
                    swal('Grupo Guardado Correctamente', '', 'success');
                    empGroup.cleanform();
                }, response => {
                    //Si no sale correcto
                    swal('Ocurrio un error al guardar', '', 'error')
                    empGroup.cleanform();
                });
            })
          }
            //FIN VUE POST
        },
        editarGroup: function(event) {
            swal({
                title: "Confirmación de editado",
                text: "Estas seguro de editar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function() {
                var arr = {
                    //DATOS DE FORMULARIO
                    Group: empGroup.fd.Group,
                    Name: empGroup.fd.Name,
                    //Startdate:empCa.fd.StartDate,
                    //Finishdate:empCa.fd.FinishDate,
                    Employee: empDP.fd.Idn
                };
                //VUE POST
                Vue.http.put(empGroup.globalurl + "/update/" + empGroup.fd.Idn, arr).then(response => {
                    // Si todo sale correcto
                    swal('Modificada Correctamente', '', 'success');
                    empGroup.cleanform();
                }, response => {
                    //Si no sale correcto
                    swal('Ocurrio un error al modificar', '', 'error')
                    empGroup.cleanform();
                });
            })
            //FIN VUE POST
        },
        borrarGroup: function(event) {
            swal({
                title: "Confirmar Eliminación",
                text: "Estas seguro de eliminar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function() {
                Vue.http.put(empGroup.globalurl + "/delete/" + empGroup.fd.Idn).then(response => {
                    // success callback
                    swal('Eliminado Correctamente', '', 'success');
                    empGroup.cleanform();
                }, response => {
                    swal('Ocurrio un error al eliminar', '', 'error');
                    // error callback
                });
            })
        },
        cleanform: function() {
            empGroup.fd.Name = '';
            var date = new Date();
            var day = date.getDate();
            var monthIndex = date.getMonth();
            var year = date.getFullYear();
            empCa.fd.StartDate = year + '-' + monthIndex + '-' + day;
            empCa.fd.FinishDate = year + '-' + monthIndex + '-' + day;
            $('#ModalGroupEmployee').modal('hide');
            var table = $('#TableEmployeeGroup').dataTable();
            //RECARGA LOS DATOS DE LA TABLA
            table.fnReloadAjax();
        },
        openmodaltonew: function() {
            empGroup.cleanform();
            $('#ModalGroupEmployee').modal('toggle');
            //OCULTAR BOTONES O MOSTRAR
            empGroup.ff.save = true;
            empGroup.ff.edit = false;
        },
        openmodaltoedit: function() {
            $('#ModalGroupEmployee').modal('toggle');
            //OCULTAR BOTONES O MOSTRAR
            empGroup.ff.save = false;
            empGroup.ff.edit = true;
        }
    }
});
var empCAC = new Vue({
    el: '#vue-CompanyActivity',
    data: {
        globalurl: '../api/v1/employee',
        fd: {
            Situation: '1',
            Employeecondition: '',
            Employeeconditionlist: [],
            Dateentry: '',
            Dateentryrecognized: '',
            Dateexit: '',
            Exitreason: '',
            Exitreasonlist: [],
            Modcontract: '',
            Modcontractlist: [],
            Roster: '',
            Rosterlist: []
            //Listados de Combos
        },
        ff: {
            save: false,
            edit: true,
        }
    },
    mounted() {
        //Formato del Datepicker
        $('#DPDateentry').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            language: 'es'
        });
        //Obtener Datos del Datepicker
        $("#DPDateentry").datepicker().on("changeDate", () => {
            //this.startDate = $('#DPickerBirthdate').val();
            empCAC.fd.Dateentry = $('#DPDateentry').val();
        });
        $('#DPDateentryrecognized').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            language: 'es'
        });
        //Obtener Datos del Datepicker
        $("#DPDateentryrecognized").datepicker().on("changeDate", () => {
            //this.startDate = $('#DPickerBirthdate').val();
            empCAC.fd.Dateentryrecognized = $('#DPDateentryrecognized').val();
        });
        $('#DPDateexit').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            language: 'es'
        });
        //Obtener Datos del Datepicker
        $("#DPDateexit").datepicker().on("changeDate", () => {
            //this.startDate = $('#DPickerBirthdate').val();
            empCAC.fd.Dateexit = $('#DPDateexit').val();
        });
    },
    methods: {


        updateWorkRelation: function() {

          var items = [empCAC.fd.Situation,empCAC.fd.Employeecondition,empCAC.fd.Dateentry,
      empCAC.fd.Dateentryrecognized,empCAC.fd.Modcontract,empCAC.fd.Roster];
            //Mensajes que se mostraran para cada objeto nulo
            var strings = ['Situacion','una Condicion','Fecha de Ingreso','Fecha de Ingreso reconocida',
            'Modo de Contratación','una Nómina'];
            //Funcion para chequear estos elementos
            var checked = CheckNulls(items, strings);
            //Si all is fine
            if (checked != 0) {  

            swal({
                title: "Confirmación de edición",
                text: "¿Estas seguro de editar la actividad de la empresa?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function() {
                var arr = {
                    //DATOS DE FORMULARIO
                    Situation: empCAC.fd.Situation,
                    Employeecondition: empCAC.fd.Employeecondition.id,
                    Dateentry: FormatDateReverse(empCAC.fd.Dateentry),
                    Dateentryrecognized: FormatDateReverse(empCAC.fd.Dateentryrecognized),
                    Dateexit: FormatDateReverse(empCAC.fd.Dateexit),
                    Exitreason: empCAC.fd.Exitreason.id,
                    Modcontract: empCAC.fd.Modcontract.id,
                    Roster: empCAC.fd.Roster.id
                };
                //VUE POST
                Vue.http.put(empDP.globalurl + "/companyactivity/update/" + empDP.fd.Idn, arr).then(response => {
                    // Si todo sale correcto
                    swal('Datos modificados correctamente', '', 'success');
                }, response => {
                    //Si no sale correcto
                    swal('Ocurrio un error al modificar', '', 'error');
                });
            })
          }
            //FIN VUE POST
        }
    }
});
var empOBS = new Vue({
    el: '#vue-Obsocial',
    data: {
        globalurl: '../api/v1/employee',
        fd: {
            Obsocial: '',
            Obsociallist: [],
            Adherents: '',
            Medicplan: '',
            Medicplanlist: [],
            Numberofcap: ''
            //Listados de Combos
        },
        ff: {
            save: false,
            edit: true,
        }
    },
    methods: {
        updateObsocial: function() {
            var items = [empOBS.fd.Obsocial,empOBS.fd.Medicplan];
            //Mensajes que se mostraran para cada objeto nulo
            var strings = ['Obra Social','Plan Médico'];
            //Funcion para chequear estos elementos
            var checked = CheckNulls(items, strings);
            //Si all is fine
            if (checked != 0) {  

            swal({
                title: "Confirmación de edición",
                text: "¿Estas seguro de editar estos datos?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function() {
                var arr = {
                    //DATOS DE FORMULARIO
                    Obsocial: empOBS.fd.Obsocial.id,
                    Adherents: empOBS.fd.Adherents,
                    Medicplan: empOBS.fd.Medicplan.id,
                    Numberofcap: empOBS.fd.Numberofcap
                };
                //VUE POST
                Vue.http.put(empDP.globalurl + "/obsocial/update/" + empDP.fd.Idn, arr).then(response => {
                    // Si todo sale correcto
                    swal('Datos modificados correctamente', '', 'success');
                }, response => {
                    //Si no sale correcto
                    swal('Ocurrio un error al modificar', '', 'error')
                });
            })
          }
            //FIN VUE POST
        },
    }
});
var empOTH = new Vue({
    el: '#vue-Othersdata',
    data: {
        globalurl: '../api/v1/employee',
        fd: {
            Sitrevision: '',
            Sitrevisionlist: [],
            Workplace: '',
            Workplacelist: [],
            Sindicate: '',
            Sindicatelist: [],
            Affiliate: '',
            Regprevisional: '',
            Regprevisionallist: [],
            Lifesecure: '',
            Benefitsoc: '',
            Agreed: '',
            Sinestercode: '',
            Sinestercodelist: [],
            Levelstudies: '',
            Levelstudieslist: []
            //Listados de Combos
        },
        ff: {
            save: false,
            edit: true,
        }
    },
    methods: {
        updateOthers: function() {
           var items = [empOTH.fd.Sitrevision,empOTH.fd.Workplace,empOTH.fd.Sindicate,
           empOTH.fd.Regprevisional,empOTH.fd.Sinestercode,empOTH.fd.Levelstudies];
            //Mensajes que se mostraran para cada objeto nulo
            var strings = ['Situacion Revista','un Lugar de Trabajo','un Sindicato',
            'un Regimen Previsional','un Codigo de Siniestro','el Nivel de Estudios'];
            //Funcion para chequear estos elementos
            var checked = CheckNulls(items, strings);
            //Si all is fine
            if (checked != 0) {  

            swal({
                title: "Confirmación de edición",
                text: "¿Estas seguro de editar estos datos?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function() {
                var arr = {
                    //DATOS DE FORMULARIO
                    Sitrevision: empOTH.fd.Sitrevision.id,
                    Workplace: empOTH.fd.Workplace.id,
                    Sindicate: empOTH.fd.Sindicate.id,
                    Affiliate: empOTH.fd.Affiliate,
                    Regprevisional: empOTH.fd.Regprevisional.id,
                    Lifesecure: empOTH.fd.Lifesecure,
                    Benefitsoc: empOTH.fd.Benefitsoc,
                    Agreed: empOTH.fd.Agreed,
                    Sinestercode: empOTH.fd.Sinestercode.id,
                    Levelstudies: empOTH.fd.Levelstudies.id
                };
                //VUE POST
                Vue.http.put(empDP.globalurl + "/othersdata/update/" + empDP.fd.Idn, arr).then(response => {
                    // Si todo sale correcto
                    swal('Datos modificados correctamente', '', 'success');
                }, response => {
                    //Si no sale correcto
                    swal('Ocurrio un error al modificar', '', 'error')
                });
            })
          }
            //FIN VUE POST
        },
    }
});
var empAsig = new Vue({
    el: '#vue-Asignations',
    data: {
        globalurl: '../api/v1/AsignationDeductions',
        fd: {
            Idn: '',
            Period: '',
            Typerun: '',
            Concept: '',
            Amount: 0,
            Startdate: "",
            Finishdate: "",
            Cant: '',
            Specialtype: '',
            Description: '',
            // Employee:'',
            Periodlist: [],
            Conceptlist: [],
            Specialtypelist: [],
            Typerunlist: []
        },
        ff: {
            save: false,
            edit: true,
        }
    },
    mounted() {
        //Formato del Datepicker
        $('#DPStardate').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            language: 'es'
        });
        //Obtener Datos del Datepicker
        $("#DPStardate").datepicker().on("changeDate", () => {
            //this.startDate = $('#DPickerBirthdate').val();
            empAsig.fd.Startdate = $('#DPStardate').val();
        });
        //////
        $('#DPFinishdate').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            language: 'es'
        });
        //Obtener Datos del Datepicker
        $("#DPFinishdate").datepicker().on("changeDate", () => {
            //this.startDate = $('#DPickerBirthdate').val();
            empAsig.fd.Finishdate = $('#DPFinishdate').val();
            CalcularDiasAsignaciones();
        });
    },
    methods: {
        guardarAsigDeduc: function(event) {
            var items = [empAsig.fd.Concept,empAsig.fd.Period,empAsig.fd.Typerun,
           empAsig.fd.Amount,empAsig.fd.Startdate,empAsig.fd.Finishdate,empAsig.fd.Cant,
           empAsig.fd.Specialtype];
            //Mensajes que se mostraran para cada objeto nulo
            var strings = ['un Concepto','un Periodo','un Tipo de corrida',
            'un Monto','una Fecha de inicio','una Fecha final','una Cantidad','Tipo Especial'];
            //Funcion para chequear estos elementos
            var checked = CheckNulls(items, strings);
            //Si all is fine
            if (checked != 0) {  


          
            swal({
                title: "Confirmación de guardado",
                text: "Estas seguro de guardar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function() {
                if (1 > empAsig.fd.Specialtype.length) {
                    empAsig.fd.Specialtype.id = '';
                }
                var arr = {
                    //DATOS DE FORMULARIO
                    Period: empAsig.fd.Period,
                    Typerun: empAsig.fd.Typerun,
                    Concept: empAsig.fd.Concept,
                    Amount: empAsig.fd.Amount,
                    Startdate: FormatDateReverse(empAsig.fd.Startdate),
                    Finishdate: FormatDateReverse(empAsig.fd.Finishdate),
                    Cant: empAsig.fd.Cant,
                    Specialtype: empAsig.fd.Specialtype,
                    Employee: empDP.fd.Idn,
                    Description: empAsig.fd.Description
                    //  Employee : empAsig.fd.Employee
                };
                //VUE POST
                Vue.http.post(empAsig.globalurl + "/save", arr).then(response => {
                    // Si todo sale correcto
                    swal('Guardado Correctamente', '', 'success');
                    $('#ModalAsigDeduc').modal('hide');
                    empAsig.cleanform();
                    empAsig.updatetable();
                }, response => {
                    //Si no sale correcto
                    swal('Ocurrio un error al guardar', '', 'error')
                    empAsig.cleanform();
                    empAsig.updatetable();
                });
            })
          }
            //FIN VUE POST
        },
        editarAsigDeduc: function(event) {
            if (empAsig.fd.Period === '') {
                swal({
                    title: "Atencion",
                    text: "Debes indicar Periodo",
                    type: "warning",
                    confirmButtonText: 'Aceptar'
                });
                return false;
            }
            if (empAsig.fd.Typerun === '') {
                swal({
                    title: "Atencion",
                    text: "Debes indicar Tipo de Corrida",
                    type: "warning",
                    confirmButtonText: 'Aceptar'
                });
                return false;
            }
            if (empAsig.fd.Concept === '') {
                swal({
                    title: "Atencion",
                    text: "Debes indicar Concepto",
                    type: "warning",
                    confirmButtonText: 'Aceptar'
                });
                return false;
            }
            if (empAsig.fd.Amount === '') {
                swal({
                    title: "Atencion",
                    text: "Debes indicar Monto",
                    type: "warning",
                    confirmButtonText: 'Aceptar'
                });
                return false;
            }
            if (empAsig.fd.Startdate === '') {
                swal({
                    title: "Atencion",
                    text: "Debes indicar Fecha Inicial",
                    type: "warning",
                    confirmButtonText: 'Aceptar'
                });
                return false;
            }
            if (empAsig.fd.Finishdate === '') {
                swal({
                    title: "Atencion",
                    text: "Debes indicar Fecha Final",
                    type: "warning",
                    confirmButtonText: 'Aceptar'
                });
                return false;
            }
            if (empAsig.fd.Cant === '') {
                swal({
                    title: "Atencion",
                    text: "Debes indicar Cantidad",
                    type: "warning",
                    confirmButtonText: 'Aceptar'
                });
                return false;
            }
            if (empAsig.fd.Specialtype === '') {
                swal({
                    title: "Atencion",
                    text: "Debes indicar si es especial o no",
                    type: "warning",
                    confirmButtonText: 'Aceptar'
                });
                return false;
            }
            swal({
                title: "Confirmación de editado",
                text: "Estas seguro de editar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function() {
                var arr = {
                    //DATOS DE FORMULARIO
                    Period: empAsig.fd.Period,
                    Typerun: empAsig.fd.Typerun,
                    Concept: empAsig.fd.Concept,
                    Amount: empAsig.fd.Amount,
                    Startdate: FormatDateReverse(empAsig.fd.Startdate),
                    Finishdate: FormatDateReverse(empAsig.fd.Finishdate),
                    Cant: empAsig.fd.Cant,
                    Specialtype: empAsig.fd.Specialtype,
                    Employee: empDP.fd.Idn,
                    Description: empAsig.fd.Description
                    // Employee : empAsig.fd.Employee
                };
                //VUE POST
                Vue.http.put(empAsig.globalurl + "/update/" + empAsig.fd.Idn, arr).then(response => {
                    // Si todo sale correcto
                    swal('Modificada Correctamente', '', 'success');
                    $('#ModalAsigDeduc').modal('hide');
                    empAsig.cleanform();
                    empAsig.updatetable();
                }, response => {
                    //Si no sale correcto
                    swal('Ocurrio un error al modificar', '', 'error')
                    empAsig.cleanform();
                    empAsig.updatetable();
                });
            })
            //FIN VUE POST
        },
        borrarAsigDeduc: function(event) {
            swal({
                title: "Confirmar Eliminación",
                text: "Estas seguro de eliminar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function() {
                Vue.http.put(empAsig.globalurl + "/delete/" + empAsig.fd.Idn).then(response => {
                    // success callback
                    swal('Eliminado Correctamente', '', 'success');
                    $('#ModalAsigDeduc').modal('hide');
                    empAsig.cleanform();
                    empAsig.updatetable();
                }, response => {
                    swal('Ocurrio un error al eliminar', '', 'error');
                    // error callback
                });
            })
        },
        cleanform: function() {
            empAsig.fd.Specialtype = '';
            empAsig.fd.Startdate = '';
            empAsig.fd.Finishdate = '';
            empAsig.fd.Concept = '';
            empAsig.fd.Period = '';
            empAsig.fd.Subperiod = '';
            empAsig.fd.Amount = '';
            empAsig.fd.Cant = '';
            empAsig.fd.Description = '';
        },
        updatetable: function() {
            $('#ModalAsignationDeductions').modal('hide');
            var table = $('#TableAsigDeduc').dataTable();
            //RECARGA LOS DATOS DE LA TABLA
            table.fnReloadAjax();
        },
        openmodaltonew: function() {
            empAsig.cleanform();
            $('#ModalAsignationDeductions').modal('toggle');
            //OCULTAR BOTONES O MOSTRAR
            empAsig.ff.save = true;
            empAsig.ff.edit = false;
        },
        openmodaltoedit: function() {
            $('#ModalAsignationDeductions').modal('toggle');
            //OCULTAR BOTONES O MOSTRAR
            empAsig.ff.save = false;
            empAsig.ff.edit = true;
        },
        calculardiasasignaciones: function() {
            CalcularDiasAsignaciones();
        }
    }
});
var empConst = new Vue({
    el: '#EmployeeConstants',
    data: {
        globalurl: '../api/v1/employee/constant',
        fd: {
            Idn: '',
            Period: '',
            Typerun: '',
            Constant: '',
            Value: '',
            Comment: '',
            // Employee:'',
            Periodlist: [],
            Constantlist: [],
            Typerunlist: []
        },
        ff: {
            save: false,
            edit: true,
        }
    },
    methods: {
        saveConstant: function(event) {

           var items = [empConst.fd.Period,empConst.fd.Typerun,empConst.fd.Constant,
           empConst.fd.Value];
            //Mensajes que se mostraran para cada objeto nulo
            var strings = ['un Periodo','un Tipo de corrida','un Tipo de constante',
            'un valor'];
            //Funcion para chequear estos elementos
            var checked = CheckNulls(items, strings);
            //Si all is fine
            if (checked != 0) {  

            swal({
                title: "Confirmación de guardado",
                text: "Estas seguro de guardar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function() {
                var arr = {
                    //DATOS DE FORMULARIO
                    Period: empConst.fd.Period.id,
                    Typerun: empConst.fd.Typerun.id,
                    Constant: empConst.fd.Constant.id,
                    Value: empConst.fd.Value,
                    Comment: empConst.fd.Comment,
                    Employee: empDP.fd.Idn,
                    //  Employee : empAsig.fd.Employee
                };
                //VUE POST
                Vue.http.post(empConst.globalurl + "/save", arr).then(response => {
                    // Si todo sale correcto
                    swal('Guardado Correctamente', '', 'success');
                    $('#ModalAsigDeduc').modal('hide');
                    empConst.cleanform();
                    empConst.updatetable();
                }, response => {
                    //Si no sale correcto
                    swal('Ocurrio un error al guardar', '', 'error')
                    empConst.cleanform();
                    empConst.updatetable();
                });
            })
          }
            //FIN VUE POST
        },
        updateConstant: function(event) {
            swal({
                title: "Confirmación de editado",
                text: "Estas seguro de editar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function() {
                var arr = {
                    //DATOS DE FORMULARIO
                    Period: empConst.fd.Period.id,
                    Typerun: empConst.fd.Typerun.id,
                    Constant: empConst.fd.Constant.id,
                    Value: empConst.fd.Value,
                    Comment: empConst.fd.Comment,
                    Employee: empDP.fd.Idn,
                    // Employee : empAsig.fd.Employee
                };
                //VUE POST
                Vue.http.put(empConst.globalurl + "/update/" + empConst.fd.Idn, arr).then(response => {
                    // Si todo sale correcto
                    swal('Modificada Correctamente', '', 'success');
                    $('#ModalAsigDeduc').modal('hide');
                    empConst.cleanform();
                    empConst.updatetable();
                }, response => {
                    //Si no sale correcto
                    swal('Ocurrio un error al modificar', '', 'error')
                    empAsig.cleanform();
                    empAsig.updatetable();
                });
            })
            //FIN VUE POST
        },
        deleteConstant: function(event) {
            swal({
                title: "Confirmar Eliminación",
                text: "Estas seguro de eliminar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function() {
                Vue.http.put(empConst.globalurl + "/delete/" + empConst.fd.Idn).then(response => {
                    // success callback
                    swal('Eliminado Correctamente', '', 'success');
                    $('#ModalAsigDeduc').modal('hide');
                    empConst.cleanform();
                    empConst.updatetable();
                }, response => {
                    swal('Ocurrio un error al eliminar', '', 'error');
                    // error callback
                });
            })
        },
        cleanform: function() {
            empConst.fd.Constant = '';
            empConst.fd.Period = '';
        },
        updatetable: function() {
            $('#ModalConstants').modal('hide');
            var table = $('#TableEmployeeConstant').dataTable();
            //RECARGA LOS DATOS DE LA TABLA
            table.fnReloadAjax();
        },
        openmodaltonew: function() {
            empConst.cleanform();
            $('#ModalConstants').modal('toggle');
            //OCULTAR BOTONES O MOSTRAR
            empConst.ff.save = true;
            empConst.ff.edit = false;
        },
        openmodaltoedit: function() {
            $('#ModalConstants').modal('toggle');
            //OCULTAR BOTONES O MOSTRAR
            empConst.ff.save = false;
            empConst.ff.edit = true;
        }
    }
});
var empExclu = new Vue({
    el: '#EmployeeExclusions',
    data: {
        globalurl: '../api/v1/employee/exclusion',
        fd: {
            Idn: '',
            Period: '',
            Typerun: '',
            Concept: '',
            // Employee:'',
            Periodlist: [],
            Conceptlist: [],
            Typerunlist: []
        },
        ff: {
            save: false,
            edit: true,
        }
    },
    methods: {
        saveExclusion: function(event) {
            var items = [empExclu.fd.Period,empExclu.fd.Typerun,empExclu.fd.Concept
           ];
            //Mensajes que se mostraran para cada objeto nulo
            var strings = ['un Periodo','un Tipo de corrida','un Concepto'];
            //Funcion para chequear estos elementos
            var checked = CheckNulls(items, strings);
            //Si all is fine
            if (checked != 0) {  

          
            swal({
                title: "Confirmación de guardado",
                text: "Estas seguro de guardar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function() {
                var arr = {
                    //DATOS DE FORMULARIO
                    Period: empExclu.fd.Period.id,
                    Typerun: empExclu.fd.Typerun.id,
                    Concept: empExclu.fd.Concept.id,
                    Employee: empDP.fd.Idn,
                    //  Employee : empAsig.fd.Employee
                };
                //VUE POST
                Vue.http.post(empExclu.globalurl + "/save", arr).then(response => {
                    // Si todo sale correcto
                    swal('Guardado Correctamente', '', 'success');
                    $('#ModalAsigDeduc').modal('hide');
                    empExclu.cleanform();
                    empExclu.updatetable();
                }, response => {
                    //Si no sale correcto
                    swal('Ocurrio un error al guardar', '', 'error')
                    empExclu.cleanform();
                    empExclu.updatetable();
                });
            })
          }
            //FIN VUE POST
        },
        updateExclusion: function(event) {
            if (empExclu.fd.Period === '') {
                swal({
                    title: "Atencion",
                    text: "Debes indicar Periodo",
                    type: "warning",
                    confirmButtonText: 'Aceptar'
                });
                return false;
            }
            if (empExclu.fd.Typerun === '') {
                swal({
                    title: "Atencion",
                    text: "Debes indicar Tipo de Corrida",
                    type: "warning",
                    confirmButtonText: 'Aceptar'
                });
                return false;
            }
            if (empExclu.fd.Concept === '') {
                swal({
                    title: "Atencion",
                    text: "Debes indicar Concepto",
                    type: "warning",
                    confirmButtonText: 'Aceptar'
                });
                return false;
            }
            swal({
                title: "Confirmación de editado",
                text: "Estas seguro de editar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function() {
                var arr = {
                    //DATOS DE FORMULARIO
                    Period: empExclu.fd.Period.id,
                    Typerun: empExclu.fd.Typerun.id,
                    Concept: empExclu.fd.Concept.id,
                    Employee: empDP.fd.Idn,
                    // Employee : empAsig.fd.Employee
                };
                //VUE POST
                Vue.http.put(empExclu.globalurl + "/update/" + empExclu.fd.Idn, arr).then(response => {
                    // Si todo sale correcto
                    swal('Modificada Correctamente', '', 'success');
                    $('#ModalAsigDeduc').modal('hide');
                    empExclu.cleanform();
                    empExclu.updatetable();
                }, response => {
                    //Si no sale correcto
                    swal('Ocurrio un error al modificar', '', 'error')
                    empAsig.cleanform();
                    empAsig.updatetable();
                });
            })
            //FIN VUE POST
        },
        deleteExclusion: function(event) {
            swal({
                title: "Confirmar Eliminación",
                text: "Estas seguro de eliminar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function() {
                Vue.http.put(empExclu.globalurl + "/delete/" + empExclu.fd.Idn).then(response => {
                    // success callback
                    swal('Eliminado Correctamente', '', 'success');
                    $('#ModalAsigDeduc').modal('hide');
                    empExclu.cleanform();
                    empExclu.updatetable();
                }, response => {
                    swal('Ocurrio un error al eliminar', '', 'error');
                    // error callback
                });
            })
        },
        cleanform: function() {
            empExclu.fd.Concept = '';
            empExclu.fd.Period = '';
        },
        updatetable: function() {
            $('#ModalExclusions').modal('hide');
            var table = $('#TableExclusions').dataTable();
            //RECARGA LOS DATOS DE LA TABLA
            table.fnReloadAjax();
        },
        openmodaltonew: function() {
            empExclu.cleanform();
            $('#ModalExclusions').modal('toggle');
            //OCULTAR BOTONES O MOSTRAR
            empExclu.ff.save = true;
            empExclu.ff.edit = false;
        },
        openmodaltoedit: function() {
            $('#ModalExclusions').modal('toggle');
            //OCULTAR BOTONES O MOSTRAR
            empExclu.ff.save = false;
            empExclu.ff.edit = true;
        }
    }
});
var empAbsen = new Vue({
    el: '#EmployeeAbsences',
    data: {
        globalurl: '../api/v1/employee/absence',
        fd: {
            Idn: '',
            AbsenceType: '',
            Startdate: '',
            Finishdate: '',
            Cant: '',
            Observation: '',
            // Employee:'',
            AbsenceTypelist: []
        },
        ff: {
            save: false,
            edit: true,
        }
    },
    mounted() {
        //Formato del Datepicker
        $('#EABStarDate').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            language: 'es'
        });
        //Obtener Datos del Datepicker
        $("#EABStarDate").datepicker().on("changeDate", () => {
            //this.startDate = $('#DPickerBirthdate').val();
            empAbsen.fd.Startdate = $('#EABStarDate').val();
        });
        //////
        $('#EABFinishDate').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            language: 'es'
        });
        //Obtener Datos del Datepicker
        $("#EABFinishDate").datepicker().on("changeDate", () => {
            //this.startDate = $('#DPickerBirthdate').val();
            empAbsen.fd.Finishdate = $('#EABFinishDate').val();
        });
    },
    methods: {
        saveAbsences: function(event) {
            var items = [empAbsen.fd.AbsenceType,empAbsen.fd.Startdate,empAbsen.fd.Finishdate,empAbsen.fd.Cant
           ];
            //Mensajes que se mostraran para cada objeto nulo
            var strings = ['un Tipo de Ausencia','Fecha de Inicio','Fecha Final','Cantidad'];
            //Funcion para chequear estos elementos
            var checked = CheckNulls(items, strings);
            //Si all is fine
            if (checked != 0) { 

            swal({
                title: "Confirmación de guardado",
                text: "Estas seguro de guardar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function() {
                var arr = {
                    //DATOS DE FORMULARIO
                    AbsenceType: empAbsen.fd.AbsenceType.id,
                    Startdate: FormatDateReverse(empAbsen.fd.Startdate),
                    Finishdate: FormatDateReverse(empAbsen.fd.Finishdate),
                    Employee: empDP.fd.Idn,
                    Cant: empAbsen.fd.Cant,
                    Observation: empAbsen.fd.Observation
                    //  Employee : empAbsen.fd.Employee
                };
                //VUE POST
                Vue.http.post(empAbsen.globalurl + "/save", arr).then(response => {
                    // Si todo sale correcto
                    swal('Guardado Correctamente', '', 'success');
                    $('#ModalAsigDeduc').modal('hide');
                    empAbsen.cleanform();
                    empAbsen.updatetable();
                }, response => {
                    //Si no sale correcto
                    swal('Ocurrio un error al guardar', '', 'error')
                    empAbsen.cleanform();
                    empAbsen.updatetable();
                });
            })
          }
            //FIN VUE POST
        },
        updateAbsences: function(event) {
            swal({
                title: "Confirmación de editado",
                text: "Estas seguro de editar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function() {
                var arr = {
                    //DATOS DE FORMULARIO
                    AbsenceType: empAbsen.fd.AbsenceType.id,
                    Startdate: FormatDateReverse(empAbsen.fd.Startdate),
                    Finishdate: FormatDateReverse(empAbsen.fd.Finishdate),
                    Employee: empDP.fd.Idn,
                    Cant: empAbsen.fd.Cant,
                    Observation: empAbsen.fd.Observation
                    // Employee : empAbsen.fd.Employee
                };
                //VUE POST
                Vue.http.put(empAbsen.globalurl + "/update/" + empAbsen.fd.Idn, arr).then(response => {
                    // Si todo sale correcto
                    swal('Modificada Correctamente', '', 'success');
                    $('#ModalAsigDeduc').modal('hide');
                    empAbsen.cleanform();
                    empAbsen.updatetable();
                }, response => {
                    //Si no sale correcto
                    swal('Ocurrio un error al modificar', '', 'error')
                    empAbsen.cleanform();
                    empAbsen.updatetable();
                });
            })
            //FIN VUE POST
        },
        deleteAbsences: function(event) {
            swal({
                title: "Confirmar Eliminación",
                text: "Estas seguro de eliminar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function() {
                Vue.http.put(empAbsen.globalurl + "/delete/" + empAbsen.fd.Idn).then(response => {
                    // success callback
                    swal('Eliminado Correctamente', '', 'success');
                    $('#ModalAsigDeduc').modal('hide');
                    empAbsen.cleanform();
                    empAbsen.updatetable();
                }, response => {
                    swal('Ocurrio un error al eliminar', '', 'error');
                    // error callback
                });
            })
        },
        cleanform: function() {
            empAbsen.fd.AbsenceType = '';
            empAbsen.fd.Startdate = '';
            empAbsen.fd.Finishdate = '';
            empAbsen.fd.Cant = '';
            empAbsen.fd.Observation = '';
        },
        updatetable: function() {
            $('#ModalAbsences').modal('hide');
            var table = $('#TableEmployeeAbsences').dataTable();
            //RECARGA LOS DATOS DE LA TABLA
            table.fnReloadAjax();
        },
        openmodaltonew: function() {
            empAbsen.cleanform();
            $('#ModalAbsences').modal('toggle');
            //OCULTAR BOTONES O MOSTRAR
            empAbsen.ff.save = true;
            empAbsen.ff.edit = false;
        },
        openmodaltoedit: function() {
            $('#ModalAbsences').modal('toggle');
            //OCULTAR BOTONES O MOSTRAR
            empAbsen.ff.save = false;
            empAbsen.ff.edit = true;
        },
        calculardiasausencias: function() {
            CalcularDiasAusencias();
        }
    }
});
var empVaca = new Vue({
    el: '#EmployeeVacations',
    data: {
        globalurl: '../api/v1/employee/vacation',
        fd: {
            Idn: '',
            Period: '',
            Typerun: '',
            Startdate: '',
            Finishdate: '',
            Year: '',
            Cant: '',
            // Employee:'',
            Periodlist: [],
            Typerunlist: []
        },
        ff: {
            save: false,
            edit: true,
        }
    },
    mounted() {
        //Formato del Datepicker
        $('#EVacStardate').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            language: 'es'
        });
        //Obtener Datos del Datepicker
        $("#EVacStardate").datepicker().on("changeDate", () => {
            //this.startDate = $('#DPickerBirthdate').val();
            empVaca.fd.Startdate = $('#EVacStardate').val();
        });
        //////
        $('#EVacFinishdate').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            language: 'es'
        });
        //Obtener Datos del Datepicker
        $("#EVacFinishdate").datepicker().on("changeDate", () => {
            //this.startDate = $('#DPickerBirthdate').val();
            empVaca.fd.Finishdate = $('#EVacFinishdate').val();
        });
    },
    methods: {
        saveAbsences: function(event) {

           var items = [empVaca.fd.Startdate,empVaca.fd.Finishdate,empVaca.fd.Cant];
            //Mensajes que se mostraran para cada objeto nulo
            var strings = ['Fecha de Inicio','Fecha Final','Cantidad'];
            //Funcion para chequear estos elementos
            var checked = CheckNulls(items, strings);
            //Si all is fine
            if (checked != 0) {  

         
            swal({
                title: "Confirmación de guardado",
                text: "Estas seguro de guardar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function() {
                var arr = {
                    //DATOS DE FORMULARIO
                    Period: empVaca.fd.Period,
                    Typerun: empVaca.fd.Typerun,
                    Startdate: FormatDateReverse(empVaca.fd.Startdate),
                    Finishdate: FormatDateReverse(empVaca.fd.Finishdate),
                    Employee: empDP.fd.Idn,
                    Year: empVaca.fd.Year,
                    Cant: empVaca.fd.Cant
                    //  Employee : empVaca.fd.Employee
                };
                //VUE POST
                Vue.http.post(empVaca.globalurl + "/save", arr).then(response => {
                    // Si todo sale correcto
                    swal('Guardado Correctamente', '', 'success');
                    $('#ModalAsigDeduc').modal('hide');
                    empVaca.cleanform();
                    empVaca.updatetable();
                }, response => {
                    //Si no sale correcto
                    swal('Ocurrio un error al guardar', '', 'error')
                    empVaca.cleanform();
                    empVaca.updatetable();
                });
            })
          }
            //FIN VUE POST
        },
        updateAbsences: function(event) {
            if (empVaca.fd.Startdate === '') {
                swal({
                    title: "Error",
                    text: "Debe indicar una fecha de inicio",
                    type: "error",
                    confirmButtonText: 'Aceptar'
                });
                return false;
            }
            if (empVaca.fd.Finishdate === '') {
                swal({
                    title: "Error",
                    text: "Debe indicar una fecha final",
                    type: "error",
                    confirmButtonText: 'Aceptar'
                });
                return false;
            }
            if (empVaca.fd.Cant === '') {
                swal({
                    title: "Error",
                    text: "Indique la cantidad",
                    type: "error",
                    confirmButtonText: 'Aceptar'
                });
                return false;
            }
            swal({
                title: "Confirmación de editado",
                text: "Estas seguro de editar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function() {
                var arr = {
                    Period: empVaca.fd.Period,
                    Typerun: empVaca.fd.Typerun,
                    Startdate: FormatDateReverse(empVaca.fd.Startdate),
                    Finishdate: FormatDateReverse(empVaca.fd.Finishdate),
                    Employee: empDP.fd.Idn,
                    Year: empVaca.fd.Year,
                    Cant: empVaca.fd.Cant
                };
                //VUE POST
                Vue.http.put(empVaca.globalurl + "/update/" + empVaca.fd.Idn, arr).then(response => {
                    // Si todo sale correcto
                    swal('Modificada Correctamente', '', 'success');
                    $('#ModalVacations').modal('hide');
                    empVaca.cleanform();
                    empVaca.updatetable();
                }, response => {
                    //Si no sale correcto
                    swal('Ocurrio un error al modificar', '', 'error')
                    empVaca.cleanform();
                    empVaca.updatetable();
                });
            })
            //FIN VUE POST
        },
        deleteAbsences: function(event) {
            swal({
                title: "Confirmar Eliminación",
                text: "Estas seguro de eliminar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function() {
                Vue.http.put(empVaca.globalurl + "/delete/" + empVaca.fd.Idn).then(response => {
                    // success callback
                    swal('Eliminado Correctamente', '', 'success');
                    $('#ModalVacations').modal('hide');
                    empVaca.cleanform();
                    empVaca.updatetable();
                }, response => {
                    swal('Ocurrio un error al eliminar', '', 'error');
                    // error callback
                });
            })
        },
        cleanform: function() {
            empVaca.fd.Period = '';
            empVaca.fd.Startdate = '';
            empVaca.fd.Finishdate = '';
            empVaca.fd.Cant = '';
            empVaca.fd.Year = '';
        },
        updatetable: function() {
            $('#ModalVacations').modal('hide');
            var table = $('#TableEmployeeVacations').dataTable();
            //RECARGA LOS DATOS DE LA TABLA
            table.fnReloadAjax();
        },
        openmodaltonew: function() {
            empVaca.cleanform();
            $('#ModalVacations').modal('toggle');
            //OCULTAR BOTONES O MOSTRAR
            empVaca.ff.save = true;
            empVaca.ff.edit = false;
        },
        openmodaltoedit: function() {
            $('#ModalVacations').modal('toggle');
            //OCULTAR BOTONES O MOSTRAR
            empVaca.ff.save = false;
            empVaca.ff.edit = true;
        },
        calculardiasvacaciones: function() {
            CalcularDiasVacaciones();
        }
    }
});
var empHours = new Vue({
    el: '#vue-HoursWorked',
    data: {
        globalurl: '../api/v1/employee/hoursworked',
        fd: {
            Idn: '',
            Startdate: moment().subtract(1, 'day').format('DD-MM-YYYY'),
            Finishdate: moment().format('DD-MM-YYYY'),
            StartHour: moment().subtract(1, 'hour').format('hh:mm A'),
            FinishHour: moment().format('hh:mm A'),
            StartHourMoment: '',
            FinishHourMoment: '',
            Observation: '',
            Cant: ''
        },
        ff: {
            save: false,
            edit: true
        }
    },
    mounted() {
        //Formato del Datepicker
        $('#HoursWorkedStartdate').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            language: 'es'
        });
        //Obtener Datos del Datepicker
        $("#HoursWorkedStartdate").datepicker().on("changeDate", () => {
            empHours.fd.Startdate = $('#HoursWorkedStartdate').val();
        });
        //////
        $('#HoursWorkedFinishdate').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            language: 'es'
        });
        //Obtener Datos del Datepicker
        $("#HoursWorkedFinishdate").datepicker().on("changeDate", () => {
            empHours.fd.Finishdate = $('#HoursWorkedFinishdate').val();
            calcularHoras()
        });
        $('#HoursWorkedStartdatehour').datetimepicker({
            format: 'LT',
            useCurrent: true,
            showClose: true
        }).on('dp.change', function() {
            empHours.fd.StartHour = $('#HoursWorkedStartdatehour').val();
            calcularHoras()
        });
        $('#HoursWorkedFinishdatehour').datetimepicker({
            format: 'LT',
            useCurrent: true,
            showClose: true
        }).on('dp.change', function() {
            empHours.fd.FinishHour = $('#HoursWorkedFinishdatehour').val();
            calcularHoras()
        });
    },
    methods: {
        guardarHorasTrabajadas: function(event) {
          var items = [empHours.fd.Startdate,empHours.fd.Finishdate,empHours.fd.StartHour,
           empHours.fd.FinishHour,empHours.fd.Cant];
            //Mensajes que se mostraran para cada objeto nulo
            var strings = ['Fecha de Inicio','Fecha Final','Horas de Inicio',
            'Horas Finales','Cantidad'];
            //Funcion para chequear estos elementos
            var checked = CheckNulls(items, strings);
            //Si all is fine
            if (checked != 0) {  

            if (empHours.fd.Cant === '') {
                swal('Debes llenar los Campos requeridos');
                return false;
            }
            swal({
                title: "Confirmación de guardado",
                text: "Estas seguro de guardar "+empHours.fd.Cant+" horas trabajadas?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function() {
                var arr = {
                    //DATOS DE FORMULARIO
                    cant: empHours.fd.Cant,
                    startdate: FormatDateReverse(empHours.fd.Startdate),
                    finishdate: FormatDateReverse(empHours.fd.Finishdate),
                    starthour: empHours.fd.StartHourMoment,
                    finishhour: empHours.fd.FinishHourMoment,
                    observation: empHours.fd.Observation,
                    employee: empDP.fd.Idn
                };
                //VUE POST
                Vue.http.post(empHours.globalurl + "/save", arr).then(response => {
                    // Si todo sale correcto
                    swal('Guardado Correctamente', '', 'success');
                    $('#ModalHoursWorked').modal('hide');
                    empHours.cleanform();
                    empHours.updatetable();
                }, response => {
                    //Si no sale correcto
                    swal('Ocurrio un error al guardar', '', 'error')
                    // empVaca.cleanform();
                    // empVaca.updatetable();
                });
            })
          }
            //FIN VUE POST
        },
        editarHorasTrabajadas: function(event) {
            swal({
                title: "Confirmación de editado",
                text: "Estas seguro de editar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function() {
                var arr = {
                    cant: empHours.fd.Cant,
                    startdate: FormatDateReverse(empHours.fd.Startdate),
                    finishdate: FormatDateReverse(empHours.fd.Finishdate),
                    starthour: empHours.fd.StartHourMoment,
                    finishhour: empHours.fd.FinishHourMoment,
                    observation: empHours.fd.Observation,
                    employee: empDP.fd.Idn
                };
                //VUE POST
                Vue.http.put(empHours.globalurl + "/update/" + empHours.fd.Idn, arr).then(response => {
                    // Si todo sale correcto
                    swal('Modificada Correctamente', '', 'success');
                    $('#ModalHoursWorked').modal('hide');
                    empHours.cleanform();
                    empHours.updatetable();
                }, response => {
                    //Si no sale correcto
                    swal('Ocurrio un error al modificar', '', 'error')
                    empHours.cleanform();
                    empHours.updatetable();
                });
            })
            //FIN VUE POST
        },
        borrarHorasTrabajadas: function(event) {
            swal({
                title: "Confirmar Eliminación",
                text: "Estas seguro de eliminar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function() {
                Vue.http.put(empHours.globalurl + "/delete/" + empHours.fd.Idn).then(response => {
                    // success callback
                    swal('Eliminado Correctamente', '', 'success');
                    $('#ModalHoursWorked').modal('hide');
                    empHours.cleanform();
                    empHours.updatetable();
                }, response => {
                    swal('Ocurrio un error al eliminar', '', 'error');
                    // error callback
                });
            })
        },
        cleanform: function() {
            empHours.fd.Idn = '';
            empHours.fd.Startdate = moment().subtract(1, 'day').format('DD-MM-YYYY');
            empHours.fd.Finishdate = moment().format('DD-MM-YYYY');
            empHours.fd.StartHour = moment().subtract(1, 'hour').format('hh:mm A');
            empHours.fd.FinishHour = moment().format('hh:mm A');
            empHours.fd.StartHourMoment = '';
            empHours.fd.FinishHourmMoment = '';
            empHours.fd.Observation = '';
            empHours.fd.Cant = '';
            calcularHoras();
        },
        updatetable: function() {
            $('#ModalHoursWorked').modal('hide');
            var table = $('#TableHoursworked').dataTable();
            //RECARGA LOS DATOS DE LA TABLA
            table.fnReloadAjax();
        },
        openmodaltonew: function() {
            empHours.cleanform();
            $('#ModalHoursWorked').modal('toggle');
            //OCULTAR BOTONES O MOSTRAR
            empHours.ff.save = true;
            empHours.ff.edit = false;
        },
        openmodaltoedit: function() {
            $('#ModalHoursWorked').modal('toggle');
            //OCULTAR BOTONES O MOSTRAR
            empHours.ff.save = false;
            empHours.ff.edit = true;
        },
    }
});
var init = new Vue({
    methods: {
        loademployee: function() {
            var url = window.location;
            var qs = getUrlParams(url);
            empDP.fd.Idn = qs.id;
            console.log("PARAMETROS", qs);
        },
        LoadTables: function() {
            LoadTableFamily();
            LoadTableBankAcount();
            LoadTableCareer();
            LoadTableLanguage();
            LoadTableHours();
            LoadTablePayrunOpenByEmployee();
            LoadTablePayrunClosedByEmployee();
            LoadTableGroupi();
            LoadTableAsignationdeductions();
            LoadTableEmployeeAbsences();
            LoadTableEmployeeVacations();
            LoadTableEmployeeExclusions();
            LoadTableEmployeeConstants();
            $("#ListPayrunOpen").show();
            $("#ListPayrunClosed").hide();
            //  LoadTableAsignationdeductions();
            // LoadTableAbsences();
            //  LoadTableHoursworked();
        },
        loaddata: function() {
            // GET /someUrl
            this.$http.get('../api/v1/employee/id/' + empDP.fd.Idn).then(response => {
                /*
                  empHead.fd.HeadRoster=response.body[0].roster;

                  empHead.fd.HeadCuil=response.body[0].cuil;
                  empHead.fd.HeadSalary=response.body[0].basicsalary;
                  empHead.fd.HeadStartdate=response.body[0].dateentry;
                  empHead.fd.HeadSitrevision=response.body[0].sitrevision;
                  empHead.fd.HeadModcontract=response.body[0].modcontract;
                  empHead.fd.Headcct=response.body[0].ccts;
                  empHead.fd.HeadCuil=response.body[0].cuil; */
                var rb = response.body[0];
                empHead.fd.HeadCod = rb.cod;
                empHead.fd.HeadName = rb.firstname + " " + rb.lastname;
                empHead.fd.HeadCuil = rb.cuil;
                empHead.fd.HeadSalary = rb.basicsalary;
                empHead.fd.Headcct = rb.ccts;
                empHead.fd.Active = rb.active;
                empHead.fd.HeadRoster = rb.roster;
                empHead.fd.HeadCondition = rb.employeecondition;
                empHead.fd.HeadStartDate = rb.dateentry;
                empHead.fd.HeadFinishDate = rb.dateexit;
                empHead.fd.HeadAntiquity = rb.antiquity;
                //empRun.fd.HeadDiasTrab = empHead.fd.HeadDiasTrab;
                empHead.fd.HeadJournalType = rb.journaltype;
                empHead.fd.HeadModcontract = rb.modcontract;
                empHead.fd.HeadSitRevision = rb.sitrevision;
                // DATA PERSONAL
                empDP.fd.Cod = rb.cod;
                empDP.fd.Firstname = rb.firstname;
                empDP.fd.Lastname = rb.lastname;
                empDP.fd.Gender = rb.gender;
                empDP.fd.Dni = rb.dni;
                empDP.fd.Cuil = rb.cuil;
                empDP.fd.Birthdate = FormatDate(rb.birthdate);
                empDP.fd.Telephone = rb.telephone;
                empDP.fd.Cellphone = rb.cellphone;
                empDP.fd.Civilstatus = getactualvalue(empDP.fd.Civilstatuslist, rb.idncivilstatus);
                empDP.fd.Workemail = rb.workemail;
                empDP.fd.Personemail = rb.personemail;
                empDP.fd.Roster = rb.idnroster;
                //RESIDENCIA
                empRE.fd.Nationality = getactualvalue(empRE.fd.Nationalitylist, rb.idnnationality);
                empRE.fd.City = rb.city;
                empRE.fd.Province = rb.province;
                empRE.fd.Postalcode = rb.postalcode;
                empRE.fd.Street = rb.street;
                empRE.fd.Number = rb.number;
                empRE.fd.Floor = rb.floor;
                empRE.fd.Department = rb.departmentresidence;
                //RELACION LABORAL
                //empWR.fd.Costcenter = rb.idncostcenter;
                empWR.fd.Costcenter = getactualvalue(empWR.fd.Costcenterlist, rb.idncostcenter);
                empWR.fd.Department = getactualvalue(empWR.fd.Departmentlist, rb.idndepartment);
                empWR.fd.Positionjob = getactualvalue(empWR.fd.Positionjoblist, rb.idnpositionjob);
                empWR.fd.Workcalendar = getactualvalue(empWR.fd.Workcalendarlist, rb.idnworkcalendar);
                empWR.fd.Cct = getactualvalue(empWR.fd.Cctlist, rb.idncct);
                empWR.fd.Categorysalary = getactualvalue(empWR.fd.Categorysalarylist, rb.idncategorysalary);
                empWR.fd.CategorySalaryAmount = rb.categorysalaryamount;
                empWR.fd.Basicsalary = rb.basicsalary;
                empCAC.fd.Situation = rb.active;
                empCAC.fd.Employeecondition = getactualvalue(empCAC.fd.Employeeconditionlist, rb.idnemployeecondition);
                empCAC.fd.Dateentry = FormatDate(rb.dateentry);
                empCAC.fd.Dateentryrecognized = FormatDate(rb.dateentryrecognized);
                empCAC.fd.Dateexit = FormatDate(rb.dateexit);
                empCAC.fd.Exitreason = getactualvalue(empCAC.fd.Exitreasonlist, rb.idnexitreason);
                empCAC.fd.Modcontract = getactualvalue(empCAC.fd.Modcontractlist, rb.idnmodcontract);
                empCAC.fd.Roster = getactualvalue(empCAC.fd.Rosterlist, rb.idnroster);
                console.log(empOBS.fd.Obsociallist);
                console.log(rb.idnsocialwork);
                console.log(getactualvalue(empOBS.fd.Obsociallist, rb.idnsocialwork));
                empOBS.fd.Obsocial = {
                    id: rb.idnsocialwork,
                    label: rb.socialwork
                };
                empOBS.fd.Adherents = rb.adherents;
                empOBS.fd.Medicplan = getactualvalue(empOBS.fd.Medicplanlist, rb.idnmedicalplan);
                empOBS.fd.Numberofcap = rb.numberofcap;
                empOTH.fd.Sitrevision = getactualvalue(empOTH.fd.Sitrevisionlist, rb.idnsitrevision);
                empOTH.fd.Workplace = getactualvalue(empOTH.fd.Workplacelist, rb.idnworkplace);
                empOTH.fd.Sindicate = getactualvalue(empOTH.fd.Sindicatelist, rb.idnsindicate);
                empOTH.fd.Affiliate = rb.affiliate;
                empOTH.fd.Regprevisional = getactualvalue(empOTH.fd.Regprevisionallist, rb.idnregprevisional);
                empOTH.fd.Lifesecure = rb.lifesecure;
                empOTH.fd.Benefitsoc = rb.benefitsoccharges;
                empOTH.fd.Agreed = rb.agreed;
                empOTH.fd.Sinestercode = getactualvalue(empOTH.fd.Sinestercodelist, rb.idnsinestercode);
                empOTH.fd.Levelstudies = getactualvalue(empOTH.fd.Levelstudieslist, rb.idnlevelstudies);;
                // emp.fd.Levelstudies = rb.idnlevelstudies;
                /* emp.fd.Socialwork = rb.idnsocialwork;
                 emp.fd.Company = rb.idncompany;


                 emp.fd.Workplace = rb.idnworkplace;

                 emp.fd.Sindicate = rb.idnsindicate;
                 emp.fd.Reqprevisional = rb.idnregprevisional;
                 emp.fd.Sinestercode = rb.idnsinestercode;

                 emp.fd.Medicalplan = rb.idnmedicalplan;



                 emp.fd.Adherents =rb.adherents;
                 emp.fd.Numberofcap =rb.numberofcap;
                 emp.fd.Affiliate =rb.affiliate;
                 emp.fd.Lifesecure =rb.lifesecure;
                 emp.fd.Benefitsoccharges =rb.benefitsoccharges;
                 emp.fd.Agreed = rb.agreed;
                 emp.fd.Active = rb.active;

                 */
                //  alert(emp.fd.Firstname);
            }, response => {
                // error callback
            });
        }
    }
});

function getactualvalue(list, idn) {
    var result = list[parseInt(idn) - 1];
    return result;
}

function CalcularDiasAsignaciones() {
    var start = $("#DPStardate").datepicker("getDate");
    var end = $("#DPFinishdate").datepicker("getDate");
    var days = (end - start) / (3600 * 24 * 1000);
    var total = Math.round(days);
    if (empAsig.fd.Startdate == "") {
        swal('Por favor seleccione una fecha de inicio', '', 'error');
        return false;
        empAsig.fd.Cant = '';
    }
    if (empAsig.fd.Finishdate == "") {
        swal('Por favor seleccione una fecha Final', '', 'error');
        return false;
        empAsig.fd.Cant = '';
    }
    empAsig.fd.Cant = total;
}

function CalcularDiasAusencias() {
    var start = $("#EABStarDate").datepicker("getDate");
    var end = $("#EABFinishDate").datepicker("getDate");
    var days = (end - start) / (3600 * 24 * 1000);
    var total = Math.round(days);
    if (empAbsen.fd.Startdate == "") {
        swal('Por favor seleccione una fecha de inicio', '', 'error');
        return false;
        empAbsen.fd.Cant = '';
    }
    if (empAbsen.fd.Finishdate == "") {
        swal('Por favor seleccione una fecha Final', '', 'error');
        return false;
        empAbsen.fd.Cant = '';
    }
    empAbsen.fd.Cant = total;
}

function CalcularDiasVacaciones() {
    var start = $("#EVacStardate").datepicker("getDate");
    var end = $("#EVacFinishdate").datepicker("getDate");
    var days = (end - start) / (3600 * 24 * 1000);
    var total = Math.round(days);
    if (empVaca.fd.Startdate == "") {
        swal('Por favor seleccione una fecha de inicio', '', 'error');
        return false;
        empVaca.fd.Cant = '';
    }
    if (empVaca.fd.Finishdate == "") {
        swal('Por favor seleccione una fecha Final', '', 'error');
        return false;
        empVaca.fd.Cant = '';
    }
    empVaca.fd.Cant = total;
}

function abrirnuevolegajo() {
    window.location = "/legajo/legajonuevo";
}

function calcularHoras() {
    var fecha1 = moment(FormatDateReverse(empHours.fd.Startdate));
    var fecha2 = moment(FormatDateReverse(empHours.fd.Finishdate));
    var inithour = $('#HoursWorkedStartdatehour').datetimepicker('viewDate').hour();
    var endhour = $('#HoursWorkedFinishdatehour').datetimepicker('viewDate').hour();
    var initminute = $('#HoursWorkedStartdatehour').datetimepicker('viewDate').minute();
    var endminute = $('#HoursWorkedFinishdatehour').datetimepicker('viewDate').minute();
    var dayreference = moment().hour(inithour).minute(initminute);
    var dayreference2 = moment().hour(endhour).minute(endminute);
    $total_hours = 0;
    $hours = dayreference2.diff(dayreference, 'hours');
    $days = fecha2.diff(fecha1, 'days') + 1;
    $actualdate = fecha2.hour(Number);
    for ($i = 0; $i < $days; $i++) {
        $total_hours = $total_hours + $hours;
    }
  /*  if ($total_hours === 0) {
        swal({
            title: 'Error',
            text: 'La hora y fecha inicial no pueden ser iguales que la hora y fecha final'
        });
        //return false;
    }
    if ($total_hours < 0) {
        swal({
            title: 'Error',
            text: 'La hora y fecha inicial no pueden ser mayores que la hora y fecha final'
        });
       // return false;
    }*/
    empHours.fd.Cant = $total_hours;
    empHours.fd.StartHourMoment = $('#HoursWorkedStartdatehour').datetimepicker('date').format('HH:mm:ss');
    empHours.fd.FinishHourMoment = $('#HoursWorkedFinishdatehour').datetimepicker('date').format('HH:mm:ss');
}
//FUNCIONES GENERICAS PARA INICIO DE LA PAGINA
//FUNCIONALIDADES INICIALES AL ARRANCAR EL FORM
init.loademployee();
init.loaddata();
init.LoadTables();
//ausencias
//combo.loadcomboabsencetype();
function get_cuil_cuit(document_number, gender) {
    console.log(document_number, gender);
    'use strict';
    var HOMBRE = ['HOMBRE', 'M', 'MALE', '1', 1],
        MUJER = ['MUJER', 'F', 'FEMALE', '0', 0],
        SOCIEDAD = ['SOCIEDAD', 'S', 'SOCIETY', '2', 2];
    var AB, C;
    if (document_number.length !== 8 || isNaN(document_number)) {
        if (document_number.length === 7 && !isNaN(document_number)) {
            document_number = '0'.concat(document_number);
        } else {
            // Muestro un error en caso de no serlo.
            throw 'El numero de document_number ingresado no es correcto.';
        }
    }
    gender = gender.toUpperCase();
    if (HOMBRE.indexOf(gender) >= 0) {
        AB = '20';
    } else if (MUJER.indexOf(gender) >= 0) {
        AB = '27';
    } else {
        AB = '30';
    }
    var multiplicadores = [3, 2, 7, 6, 5, 4, 3, 2];
    var calculo = ((parseInt(AB.charAt(0)) * 5) + (parseInt(AB.charAt(1)) * 4));
    for (var i = 0; i < 8; i++) {
        calculo += (parseInt(document_number.charAt(i)) * multiplicadores[i]);
    }
    var resto = (parseInt(calculo)) % 11;
    if ((SOCIEDAD.indexOf(gender) < 0) && (resto === 1)) {
        if (HOMBRE.indexOf(gender) >= 0) {
            C = '9';
        } else {
            C = '4';
        }
        AB = '23';
    } else if (resto === 0) {
        C = '0';
    } else {
        C = 11 - resto;
    }
    var cuil_cuit = [AB, document_number, C].join('');
    return cuil_cuit;
}