Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');
//FUNCIONES EN LA MODAL DE EDICION

var empDP = new Vue({
  el: '#vue-DataPerson',
  data: {
    globalurl:'../api/v1/employee',    
    fd:
        {
          HeadCod:'',
          HeadRoster:'',
          HeadSalary:'',
          HeadStartdate:'',
          HeadSitrevision:'',
          HeadModcontract:'',
          Headcct:'',
            
      Photo:'imagen.pnp',
            Antiquity:'',
      Idn:'',
      Cod:'',
      Firstname:'',
      Lastname:'',      
      Gender:'',
      Dni:'',
      Cuil:'',
      Birthdate:'',
      Workemail:'',

      Personemail:'',
      Telephone:'',
      Cellphone:'',
      Civilstatus:'',
      Civilstatuslist:'',
      startDate: '',
      Company:'',
            
      Nationality:'',
      City:'',
      Cant:'',
      Province:'',
      Postalcode:'',
      Street:'',
      Number:'',
      Floor:'',
      Department:'',
      Nationalitylist:'', 
            
      Costcenter:'',
      Idndepartment:'',
      Positionjob:'',
      Workcalendar:'',
      Cct:'',
      Group:'',
      Categorysalary:'',
      CategorySalaryAmount:'',
      BasicSalaryAmount:'',
      Basicsalary:'',
      //Listados de Combos
      Costcenterlist:'',
      Departmentlist:'',
      Workcalendarlist:'',
      Positionjoblist:'',
      Cctlist:'',
      Grouplist:'',
      Categorysalarylist:'',
            
      Situation:'',
      Employeecondition:'',
      Employeeconditionlist:'',
      Dateentry:'',
      Dateentryrecognized:'',
      Dateexit:'',
      Exitreason:'',
      Exitreasonlist:'',
      Modcontract:'',
      Modcontractlist:'',
      Roster:'',
      Rosterlist:'',
            
      Obsocial:'',
      Obsociallist:'',
      Adherents:'',
      Medicplan:'',
      Medicplanlist:'',
      Numberofcap:'',
            
            Sitrevision:'',
      Sitrevisionlist:'',
      Workplace:'',
      Workplacelist:'',
      Sindicate:'',
      Sindicatelist:'',
      Affiliate:'',
      Regprevisional:'',
      Regprevisionallist:'',
      Lifesecure:'',
      Benefitsoc:'',
      Agreed:'',
      Sinestercode:'',
      Sinestercodelist:'',
      Levelstudies:'',
      Levelstudieslist:''
            
            
                    
        },    
        ff:
        {
          save:false,
          edit:true
        }   
    },
    
   mounted() {
        //Formato del Datepicker
       $('#DPickerBirthdate').datepicker({
                format: 'dd-mm-yyyy',
                autoclose:true,
                language: 'es'                
            });
       //Obtener Datos del Datepicker
       $("#DPickerBirthdate").datepicker().on(
        
        "changeDate", () => {
          //this.startDate = $('#DPickerBirthdate').val();
          empDP.fd.Birthdate = $('#DPickerBirthdate').val();
        });
  //Formato del Datepicker
       $('#DPDateentry').datepicker({
                format: 'dd-mm-yyyy',
                autoclose:true,
                language: 'es'                
            });
       //Obtener Datos del Datepicker
       $("#DPDateentry").datepicker().on(
        
        "changeDate", () => {
          //this.startDate = $('#DPickerBirthdate').val();
          empDP.fd.Dateentry = $('#DPDateentry').val();
        });
      $('#DPDateentryrecognized').datepicker({
                format: 'dd-mm-yyyy',
                autoclose:true,
                language: 'es'                
            });
       //Obtener Datos del Datepicker
       $("#DPDateentryrecognized").datepicker().on(
        
        "changeDate", () => {
          //this.startDate = $('#DPickerBirthdate').val();
          empDP.fd.Dateentryrecognized = $('#DPDateentryrecognized').val();
        });
          $('#DPDateexit').datepicker({
                format: 'dd-mm-yyyy',
                autoclose:true,
                language: 'es'                
            });
       //Obtener Datos del Datepicker
       $("#DPDateexit").datepicker().on(
        
        "changeDate", () => {
          //this.startDate = $('#DPickerBirthdate').val();
          empDP.fd.Dateexit = $('#DPDateexit').val();
        });
        
  },
   
    methods:
    {
      UpdateSalary:function()
      {
        for (x in empDP.fd.Categorysalarylist)
        {
          console.log(empDP.fd.Categorysalarylist[x].idn);
            if (empDP.fd.Categorysalarylist[x].idn === empDP.fd.Categorysalary)
            {

              empDP.fd.CategorySalaryAmount = empDP.fd.Categorysalarylist[x].salaryamount;
              console.log(empDP.fd.Categorysalarylist[x].salaryamount);
            }
        }
     
        
      },
      returndata:function()
      {
        alert("Volver al listado");
      },
      calcularcuil:function () {
        if(empDP.fd.Dni !== '' && empDP.fd.Dni.length === 8 && empDP.fd.Gender !== ''){
            empDP.fd.Cuil = get_cuil_cuit(empDP.fd.Dni,empDP.fd.Gender);
        }else{
            empDP.fd.Cuil = '';
        }

        console.log('hola');
      },
    guardarlegajonew: function()
     {
         
          CalcularAntiguedad();
           // alert('la antiguedad es de :'+ empDP.fd.Antiquity+ 'annos');
                 if (empDP.fd.Cod === "" || 
              empDP.fd.Firstname === "" || 
              empDP.fd.Lastname === "" || 
              empDP.fd.Birthdate === "" ||
              empDP.fd.Dni === "" || 
              empDP.fd.Cuil === "" || 
              empDP.fd.Gender === "" ||                 
                empDP.fd.Civilstatus === "" ||
                empDP.fd.Nationality === "" || 
                empDP.fd.Roster === "" ||                 
                empDP.fd.Sinestercode === ""   )
          { 
              swal('Datos incompletos ','Por favor completa todos los campos obligatorios para guardar correctamente el legajo','warning');
            
          }else{
         
        swal({
                title: "Confirmación de guardado",
                text: "¿Estas seguro de guardar los datos personales?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {
                var arr = {
                //DATOS DE FORMULARIO                    
                      cod:empDP.fd.Cod,
                      firstname:empDP.fd.Firstname,
                      lastname:empDP.fd.Lastname,      
                      gender:empDP.fd.Gender,
                      dni:empDP.fd.Dni,
                      cuil:empDP.fd.Cuil,
                      birthdate:FormatDateReverse(empDP.fd.Birthdate), //
                    photo:empDP.fd.Photo,  
                     city:empDP.fd.City,
                      province:empDP.fd.Province,
                      postalcode:empDP.fd.Postalcode,
                      street:empDP.fd.Street,
                      number:empDP.fd.Number,
                      floor:empDP.fd.Floor,
                      department:empDP.fd.Department,
                     telephone:empDP.fd.Telephone,
                      cellphone:empDP.fd.Cellphone,
                    workemail:empDP.fd.Workemail,
                      personemail:empDP.fd.Personemail,
                     idnnationality:empDP.fd.Nationality, 
                      idncivilstatus:empDP.fd.Civilstatus ,
                        idnlevelstudies:empDP.fd.Levelstudies,
                     idncostcenter:empDP.fd.Costcenter,
                     idndepartment:empDP.fd.Idndepartment,
                      idnpositionjob:empDP.fd.Positionjob,
                       idnworkcalendar:empDP.fd.Workcalendar,
                      idncct:empDP.fd.Cct,
                    idncategorysalary:empDP.fd.Categorysalary,  
                     idnexitreason:empDP.fd.Exitreason,  
                    idnsocialwork:empDP.fd.Obsocial,
                    idncompany:empDP.fd.Company,
                     idnsitrevision:empDP.fd.Sitrevision,
                   idnmodcontract:empDP.fd.Modcontract,
                      idnworkplace:empDP.fd.Workplace,
                     idnroster:empDP.fd.Roster,
                     idnsindicate:empDP.fd.Sindicate,
                     idnregprevisional:empDP.fd.Regprevisional,
                     idnsinestercode:empDP.fd.Sinestercode,
                     idnemployeecondition:empDP.fd.Condition,  
                     idnmedicalplan:empDP.fd.Medicplan,  
                    basicsalary:empDP.fd.Basicsalary, 
                     employeestatus:empDP.fd.Situation,
                dateentry:FormatDateReverse(empDP.fd.Dateentry),
                    dateentryrecognized:FormatDateReverse(empDP.fd.Dateentryrecognized),
            dateexit:FormatDateReverse(empDP.fd.Dateexit),
                          antiquity:empDP.fd.Antiquity,             
                      adherents:empDP.fd.Adherents,    
                      numberofcap:empDP.fd.Numberofcap,
                        affiliate:empDP.fd.Affiliate,
                        lifesecure:empDP.fd.Lifesecure,
                        benefitsoccharges:empDP.fd.Benefitsoc,
                        agreed:empDP.fd.Agreed
                   // idncompany:empDP.fd.Company
                                                 
                  };
            
      
              
                  //VUE POST
              Vue.http.post(empDP.globalurl+"/persondata/save",arr).then(response => {
                    // Si todo sale correcto              
                    swal('Datos guardados correctamente','','success');
                  empDP.clean();
                    }, response => {
                      //Si no sale correcto
                            swal('Ocurrio un error al guardar','','error');
                  //empDP.clean();
                           
                        });
                        
                    })}
                    //FIN VUE POST
    },
        
    clean: function()
        {
          empDP.fd.Cod = "" ;
              empDP.fd.Firstname = "" ;
              empDP.fd.Lastname = "" ; 
              empDP.fd.Dni = "" ; 
              empDP.fd.Cuil = "" ; 
                empDP.fd.Gender = "" ;
                empDP.fd.Civilstatus = "" ;
                empDP.fd.Nationality = "" ; 
                empDP.fd.Costcenter = "" ;
              empDP.fd.Birthdate = "";
              empDP.fd.Basicsalary = "" ;
                empDP.fd.Idndepartment = "" ;
                empDP.fd.Basicsalary = "" ;
                empDP.fd.Modcontract = "" ;
                empDP.fd.Exitreason = "" ;
                empDP.fd.Roster = "" ;
                    empDP.fd.Cct = "" ;
                empDP.fd.Workcalendar = "" ;
                empDP.fd.Categorysalary = "";
            empDP.fd.Dateentryrecognized = ""; 
            empDP.fd.Condition = "" ;
                empDP.fd.Situation = "" ;
                empDP.fd.Positionjob = "" ;
                empDP.fd.Obsocial = "" ;
                empDP.fd.Medicplan = "" ;
                empDP.fd.Sitrevision = "" ;
                empDP.fd.Workplace = "" ;
                empDP.fd.Sindicate = "" ;
                empDP.fd.Affiliate = "" ;
                empDP.fd.Regprevisional = "" ; 
                empDP.fd.Lifesecure = "" ;
                empDP.fd.Benefitsoc = "" ; 
                empDP.fd.Agreed = "" ;
                empDP.fd.Sinestercode = "" ; 
                empDP.fd.Levelstudies = ""  ;
                empDP.fd.Province = "" ;
                empDP.fd.CategorySalaryAmount = "" ; 
                empDP.fd.Dateentry = "" ;
                empDP.fd.Dateexit = "" ; 
                empDP.fd.Workemail = "" ;
                empDP.fd.Personemail = "" ;
                empDP.fd.Telephone = "" ; 
                empDP.fd.Cellphone = "" ;
                empDP.fd.City = "" ; 
                empDP.fd.Street = "" ;
                empDP.fd.Number = "" ; 
                empDP.fd.Floor = ""  ;
                empDP.fd.Department = "" ;
                empDP.fd.Adherents = "" ; 
                empDP.fd.Postalcode = "" ;
                empDP.fd.Numberofcap = "" ; 

        },
        
     /*clonar: function()
     {
           if(empDP.fd.Cod == "")
      {
        swal('Por favor ingrese el nro legajo','','error');
        return false;
      }
       if(empDP.fd.Firstname == "")
      {
        swal('Por favor indique el primer nombre','','error');
        return false;
      }
      if (empDP.fd.Dni == "")
      {
        swal('Porfavor ingrese el Dni ','','error');
        return false;
      }
             if (empDP.fd.Cuil == "")
      {
        swal('Porfavor ingrese el Cuil ','','error');
        return false;
      }
        
             this.cargarclones();
                     
         
        swal({
                title: "Confirmación de clonado",
                text: "¿Estas seguro de clonar los datos personales?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {
             
            var arr = {
                //DATOS DE FORMULARIO                    
                      cod:empDP.fd.Cod,
                      firstname:empDP.fd.Firstname,
                      lastname:empDP.fd.Lastname,      
                      gender:empDP.fd.Gender,
                      dni:empDP.fd.Dni+"-c",
                      cuil:empDP.fd.Cuil+"-c",
                      birthdate:empDP.fd.Birthdate,
                      photo:empDP.fd.Photo,  
                      city:empDP.fd.City,
                      province:empDP.fd.Province,
                      postalcode:empDP.fd.Postalcode,
                      street:empDP.fd.Street,
                      number:empDP.fd.Number,
                      floor:empDP.fd.Floor,
                      department:empDP.fd.Department,
                      telephone:empDP.fd.Telephone,
                      cellphone:empDP.fd.Cellphone,
                      workemail:empDP.fd.Workemail,
                      personemail:empDP.fd.Personemail,
                      idnnationality:empDP.fd.Nationality, 
                      idncivilstatus:empDP.fd.Civilstatus ,
                      idnlevelstudies:empDP.fd.Levelstudies,
                      idncostcenter:empDP.fd.Costcenter,
                      idndepartment:empDP.fd.Idndepartment,
                      idnpositionjob:empDP.fd.Positionjob,
                      idnworkcalendar:empDP.fd.Workcalendar,
                      idncct:empDP.fd.Cct,
                      idncategorysalary:empDP.fd.Categorysalary,  
                      idnexitreason:empDP.fd.Exitreason,  
                      idnsocialwork:empDP.fd.Obsocial,
                      idnsitrevision:empDP.fd.Sitrevision,
                      idnmodcontract:empDP.fd.Modcontract,
                      idnworkplace:empDP.fd.Workplace,
                      idnroster:empDP.fd.Roster,
                      idnsindicate:empDP.fd.Sindicate,
                      idnregprevisional:empDP.fd.Regprevisional,
                      idnsinestercode:empDP.fd.Sinestercode,
                      idnemployeecondition:empDP.fd.Condition,  
                      idnmedicalplan:empDP.fd.Medicplan,  
                      basicsalary:empDP.fd.Basicsalary, 
                      employeestatus:empDP.fd.Situation,
                      dateentry:empDP.fd.Dateentry,//FormatDateReverse(
                      dateentryrecognized:empDP.fd.Dateentryrecognized,//FormatDateReverse(
                      dateexit:empDP.fd.Dateexit,//FormatDateReverse(
                      antiquity:empDP.fd.Antiquity,             
                      adherents:empDP.fd.Adherents,    
                      numberofcap:empDP.fd.Numberofcap,
                      affiliate:empDP.fd.Affiliate,
                      lifesecure:empDP.fd.Lifesecure,
                      benefitsoccharges:empDP.fd.Benefitsoc,
                      agreed:empDP.fd.Agreed
                   // idncompany:empDP.fd.Company
                                                 
                  };
                  //VUE POST
             Vue.http.post(empDP.globalurl+"/persondata/save",arr).then(response => 
                    {
                    // Si todo sale correcto              
                    swal('Datos clonados correctamente','','success'); 
                    empDP.clean();
                    }, response =>
                        {
                      //Si no sale correcto
                            swal('Ocurrio un error al clonar','','error');
                        empDP.clean();
                           
                        })
                        
        })},*/
    
    /* cargarclones: function()
        {
            if(empDP.fd.Cod == "")
      {
        swal('Por favor ingrese el nro legajo','','error');
        return false;
      }
       if(empDP.fd.Firstname == "")
      {
        swal('Por favor indique el primer nombre','','error');
        return false;
      }
      if (empDP.fd.Dni == "")
      {
        swal('Porfavor ingrese el Dni ','','error');
        return false;
      }
             if (empDP.fd.Cuil == "")
      {
        swal('Porfavor ingrese el Cuil ','','error');
        return false;
      }
                   // GET /someUrl
             var arr = {cod:empDP.fd.Cod,
                        firstname:empDP.fd.Firstname,
                        dni:empDP.fd.Dni,
                        cuil:empDP.fd.Cuil};
                 Vue.http.get(empDP.globalurl+"/params/"+empDP.fd.Cod+"/"+empDP.fd.Firstname+"/"+empDP.fd.Dni+"/"+empDP.fd.Cuil,arr).then(response =>
           // this.$http.get('{cod,firstname,dni,cuil}').then(response => 
         {

            // get body data
                   
            empDP.fd.Cod = response.body[0].cod;
                empDP.fd.Firstname = response.body[0].firstname;
                empDP.fd.Dni = response.body[0].dni ;
                empDP.fd.Cuil = response.body[0].cuil;
                
                empDP.fd.Lastname= response.body[0].lastname;      
                    empDP.fd.Gender= response.body[0].gender;
                empDP.fd.Birthdate = response.body[0].birthdate;
                    empDP.fd.Photo= response.body[0].photo;  
                     empDP.fd.City= response.body[0].city;
                    empDP.fd.Province= response.body[0].province;
                    empDP.fd.Postalcode= response.body[0].postalcode;
                    empDP.fd.Street= response.body[0].street;
                    empDP.fd.Number= response.body[0].number;
                    empDP.fd.Floor= response.body[0].floor;
                    empDP.fd.Department= response.body[0].department;
                    empDP.fd.Telephone= response.body[0].telephone;
                    empDP.fd.Cellphone= response.body[0].cellphone;
                    empDP.fd.Workemail= response.body[0].workemail;
                    empDP.fd.Personemail= response.body[0].personemail;
                    empDP.fd.Nationality= response.body[0].idnnationality;
                    empDP.fd.Civilstatus = response.body[0].idncivilstatus;
                    empDP.fd.Levelstudies= response.body[0].idnlevelstudies;
                    empDP.fd.Costcenter= response.body[0].idncostcenter;
                    empDP.fd.Idndepartment= response.body[0].idndepartment;
                    empDP.fd.Positionjob= response.body[0].idnpositionjob;
                    empDP.fd.Workcalendar= response.body[0].idnworkcalendar;
                    empDP.fd.Cct= response.body[0].idncct;
                    empDP.fd.Categorysalary= response.body[0].idncategorysalary;  
                     empDP.fd.Exitreason= response.body[0].idnexitreason;
                   empDP.fd.Obsocial= response.body[0].idnsocialwork;
                   empDP.fd.Company= response.body[0].idncompany;
                   empDP.fd.Sitrevision= response.body[0].idnsitrevision;
                   empDP.fd.Modcontract= response.body[0].idnmodcontract;
                    empDP.fd.Workplace= response.body[0].idnworkplace;
                    empDP.fd.Roster= response.body[0].idnroster;
                    empDP.fd.Sindicate= response.body[0].idnsindicate;
                    empDP.fd.Regprevisional= response.body[0].idnregprevisional;
                    empDP.fd.Sinestercode= response.body[0].idnsinestercode;
                    empDP.fd.Condition = response.body[0].idnemployeecondition;
                    empDP.fd.Medicplan= response.body[0].idnmedicalplan;
                    empDP.fd.Basicsalary= response.body[0].basicsalary;
                     empDP.fd.Situation= response.body[0].employeestatus;
                empDP.fd.Dateentry= response.body[0].dateentry;
                    empDP.fd.Dateentryrecognized= response.body[0].dateentryrecognized;
            empDP.fd.Dateexit= response.body[0].dateexit;
                          empDP.fd.Antiquity= response.body[0].antiquity;             
                      empDP.fd.Adherents= response.body[0].adherents;    
                      empDP.fd.Numberofcap= response.body[0].numberofcap;
                        empDP.fd.Affiliate= response.body[0].affiliate;
                        empDP.fd.Lifesecure= response.body[0].lifesecure;
                        empDP.fd.Benefitsoc= response.body[0].benefitsoccharges;
                        empDP.fd.Agreed= response.body[0].agreed;
          },
                                                                                      
            response => {
            // error callback
                    });
        },*/
    
        calcularantiguedad: function()
        {
                if (empDP.fd.Dateentryrecognized === "")
      {
        swal('fecha de entrada vacia  ','','warning');
        return false;
      }
          CalcularAntiguedad();
            alert('la antiguedad es de :'+ empDP.fd.Antiquity+ 'annos');
      
             
        }
       
      }
    
    });


function CalcularAntiguedad()
{
var start= $("#DPDateentryrecognized").datepicker("getDate");
 var now = new Date();

var days = (now - start)/(3600 * 24 * 1000 ) ;
    
    var total = Math.round(days);
    
    if( empDP.fd.Dateentryrecognized == "")
      {
        swal('Por favor seleccione una fecha de inicio','','error');
        return false;
         // empDP.fd.Antiquity='';
          
      }
        
         empDP.fd.Antiquity =  Math.round(total/(365)) ;    
        
    
}


//FUNCIONALIDADES INICIALES AL ARRANCAR EL FORM

combo.loadCivilStatus();
combo.loadNationality();

//RELACION LABORAL
combo.loadCostCenter();
combo.loadDepartment();
combo.loadPositionJob();
combo.loadWorkCalendar();
combo.loadCct();
combo.loadCategorySalary();

combo.loadCondition();
combo.loadExitReason();
combo.loadModContract();
combo.loadRoster();

combo.loadObsocial();
combo.loadMedicplan();

//Asignaciones
combo.loadPeriod();
combo.loadConcept();
combo.loadSpecialType();

//NEws- Ausencias
combo.loadAbsenceType();


//CORRIDA DE NOMINA
combo.loadPeriod();

combo.loadGroup();
combo.loadLanguage();
combo.loadPayMethodPayrun();
combo.loadCareer();

combo.loadConstant();
//Others
combo.loadSitRevision();
combo.loadWorkPlace();
combo.loadSindicate();
combo.loadRegPrevisional();
combo.loadSinestercode();
combo.loadLevelStudies();

function get_cuil_cuit(document_number, gender){
    console.log(document_number, gender);
    'use strict';
    var HOMBRE = ['HOMBRE', 'M', 'MALE','1',1],
        MUJER = ['MUJER', 'F', 'FEMALE','0',0],
        SOCIEDAD = ['SOCIEDAD', 'S', 'SOCIETY','2',2];
    var AB, C;
    if(document_number.length !== 8 || isNaN(document_number)) {
        if (document_number.length === 7 && !isNaN(document_number)) {
            document_number = '0'.concat(document_number);
        } else {
            // Muestro un error en caso de no serlo.
            throw 'El numero de document_number ingresado no es correcto.';
        }
    }
    gender = gender.toUpperCase();
    if(HOMBRE.indexOf(gender) >= 0) {
        AB = '20';
    } else if(MUJER.indexOf(gender) >= 0) {
        AB = '27';
    } else {
        AB = '30';
    }
    var multiplicadores = [3, 2, 7, 6, 5, 4, 3, 2];
    var calculo = ((parseInt(AB.charAt(0)) * 5) + (parseInt(AB.charAt(1)) * 4));
    for(var i=0;i<8;i++) {
        calculo += (parseInt(document_number.charAt(i)) * multiplicadores[i]);
    }
    var resto = (parseInt(calculo)) % 11;
    if((SOCIEDAD.indexOf(gender) < 0)&&(resto===1)){
        if(HOMBRE.indexOf(gender) >= 0){
            C = '9';
        } else {
            C = '4';
        }
        AB = '23';
    } else if(resto === 0){
        C = '0';
    } else {
        C = 11 - resto;
    }
    var cuil_cuit = [AB, document_number, C].join('');
    return cuil_cuit;

}