Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');

//FUNCIONES GENERICAS PARA INICIO DE LA PAGINA
 var vm = new Vue({
  methods: 
      { 
        cargartabla:function()
        {
          CargarTabla();
        }
      }
    });


vm.cargartabla();

//FUNCIONES EN LA MODAL DE EDICION
var mod = new Vue({
  el: '#vuedata',
  data: {
    globalurl:'../api/v1/employee/hoursworked',
            
    fd:
    {
      Idn:'',
     Startdate:'',
      Finishdate:'',
        Starthour:'',
        Cant:'',
        Observation:'',
        IdnEmployee:'',
        Finishhour:''  
    },
    ff:
    {
      save:false,
      edit:true,
    }    
    
  },
  
  methods: {
   
    guardarHoursWorked: function (event) 
    {
       
       swal({
                title: "Confirmación de guardado",
                text: "Estas seguro de guardar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {
                    //DATOS DE FORMULARIO

                    
                   
                    startdate:mod.fd.Startdate,
                    finishdate:mod.fd.Finishdate,
                    starthour:mod.fd.Starthour,
                    cant:mod.fd.Cant,
                    observation:mod.fd.Observation,
                    idnemployee:1,
                    finishhour:mod.fd.Finishhour
                  };
                  //VUE POST
                Vue.http.post(mod.globalurl+"/save",arr).then(response => {
                    // Si todo sale correcto              
                    swal('Guardado Correctamente','','success');
                    $('#ModalHoursWorked').modal('hide');
                    mod.cleanform();

                    }, response => {
                      //Si no sale correcto
                            swal('Ocurrio un error al guardar','','error')
                            mod.cleanform();
                        });
                        
                    })
                    //FIN VUE POST
           
       
    },

    editarHoursWorked: function (event) 
    { 
       
       swal({
                title: "Confirmación de editado",
                text: "Estas seguro de editar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {

                      startdate:mod.fd.Startdate,
                    finishdate:mod.fd.Finishdate,
                    starthour:mod.fd.Starthour,
                    cant:mod.fd.Cant,
                    observation:mod.fd.Observation,
                    idnemployee:1,
                    finishhour:mod.fd.Finishhour
                                      
                  };
                  //VUE POST
                Vue.http.put(mod.globalurl+"/update/"+mod.fd.Idn,arr).then(response => {
                    // Si todo sale correcto              
                    swal('Modificada Correctamente','','success');
                    $('#ModalHoursWorked').modal('hide');
                    mod.cleanform();

                    }, response => {
                      //Si no sale correcto
                            swal('Ocurrio un error al modificar','','error')
                            mod.cleanform();
                        });
                        
                    })
                    //FIN VUE POST
    },

    borrarHoursWorked: function (event)
    {
        swal({
                title: "Confirmar Eliminación",
                text: "Estas seguro de eliminar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {startdate:mod.fd.Startdate};
                Vue.http.put(mod.globalurl+"/delete/"+mod.fd.Idn,arr).then(response => {
                    // success callback
                    swal('Eliminado Correctamente','','success');
                $('#ModalHoursWorked').modal('hide');
                mod.cleanform();


            }, response => {
                    swal('Ocurrio un error al eliminar','','error');
                    // error callback
                });
            })
    },

    cleanform: function()
    {
      mod.fd.Idn='';
      mod.fd.Starthour='';
        mod.fd.Finishhour='';
      mod.fd.Startdate='';
        mod.fd.Finishdate='';
        mod.fd.Cant='';
        mod.fd.Observation='';
      $('#ModalHoursWorked').modal('hide');
      var table = $('#TableHoursWorked').dataTable();
      //RECARGA LOS DATOS DE LA TABLA
     table.fnReloadAjax();
     
    },
   
     openmodaltonew:function()
        {
          mod.cleanform();
          $('#ModalHoursWorked').modal('toggle');
          //OCULTAR BOTONES O MOSTRAR
          mod.ff.save = true;
          mod.ff.edit = false;
        },
        openmodaltoedit:function()
        {
           $('#ModalHoursWorked').modal('toggle');
          //OCULTAR BOTONES O MOSTRAR
          mod.ff.save = false;
          mod.ff.edit = true;
        }
  }
 
});

   //======INICIO DE FUNCIONES ============
    function CargarTabla()
    {   
      var table = $("#TableHoursWorked").DataTable({
        //COMPROBACION PARA PINTAR Y CAMBIAR TEXTO DE TABLA


        "destroy": true,
        "scrollY":        "200px",
        "scrollX":        "1000px",
        "language": {
            "url": "/../Spanish.json"
        },
        //Especificaciones de las Columnas que vienen y deben mostrarse
        "columns" : [
            { data : 'idn' },
            { data : 'starthour' },
            { data : 'finishhour' },
            { data : 'startdate'},
            { data : 'finishdate'},
            { data : 'observation'},
            { data : 'employeename'},
            { data : 'cant'},
            { data : 'active'}
                
           
        ],
        //Especificaciones de la URL del servicio
        "ajax": {
            url: '../api/v1/employee/hoursworked',
            dataSrc : ''
        }

    });

            $('#TableHoursWorked tbody').on( 'click', 'tr', function () {
        
        //OBTENGO LOS VARLOES DE LA TABLA
        mod.fd.Idn = table.row( this ).data().idn;
        mod.fd.Starthour = table.row( this ).data().starthour;
        mod.fd.Finishhour = table.row( this ).data().finishhour;  
       
                mod.fd.Observation = table.row( this ).data().observation;  
                 mod.fd.Cant = table.row( this ).data().cant;  
        mod.fd.Startdate = table.row( this ).data().startdate;
        mod.fd.Finishdate = table.row( this ).data().finishdate;    
      
        //idnabsencetype=mod.fd.NameAbsenceType;
      //ABRO LA MODAL
        mod.openmodaltoedit();

       });

     } 
