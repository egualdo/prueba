Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');

//FUNCIONES GENERICAS PARA INICIO DE LA PAGINA
 var vm = new Vue({
  methods: 
      { 

           cargarcombocct:function()
        {
                   // GET /someUrl
            this.$http.get('../api/v1/ccts').then(response => {

            // get body data
            mod.fd.Cctoptions = response.body;

          }, response => {
            // error callback
          });
        },

        cargartabla:function()
        {
          CargarTabla();
        }
      }
    });

vm.cargarcombocct();
vm.cargartabla();

//FUNCIONES EN LA MODAL DE EDICION
var mod = new Vue({
  el: '#vuedata',
  data: {
    globalurl:'../api/v1/categorysalary',
            
    fd:
    {
      Idn:'',
      IdnCct:'',
      SalaryAmount:'',
      Name:'',
      NameCct:'',
      Cct:'',
      Cctoptions:''


     
      
    },
    ff:
    {
      save:false,
      edit:true,
    }    
    
  },
  
  methods: {
   
    guardarCategorySalary: function (event) 
    {
        var items = [mod.fd.Name,mod.fd.SalaryAmount,mod.fd.IdnCct];
        var strings = ['Nombre', 'Monto Salarial', 'CCT'];
        var checked = CheckNulls(items, strings);

        if(checked !== 0){
       swal({
                title: "Confirmación de guardado",
                text: "Estas seguro de guardar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {
                    //DATOS DE FORMULARIO

                    
                    idncct:mod.fd.IdnCct,
                    //idnemployee:1,
                    salaryamount:mod.fd.SalaryAmount,
                    name:mod.fd.Name
                                        
                  };
                  //VUE POST
                Vue.http.post(mod.globalurl+"/save",arr).then(response => {
                    // Si todo sale correcto              
                    swal('Guardado Correctamente','','success');
                    $('#ModalCategorySalary').modal('hide');
                    mod.cleanform();

                    }, response => {
                      //Si no sale correcto
                            swal(response.body,'','error')

                        });
                        
                    })
                    //FIN VUE POST
           
       
     }

    },

    editarCategorySalary: function (event) 
    {
        var items = [mod.fd.Name,mod.fd.SalaryAmount,mod.fd.IdnCct];
        var strings = ['Nombre', 'Monto Salarial', 'CCT'];
        var checked = CheckNulls(items, strings);
        if(checked !== 0){
       swal({
                title: "Confirmación de editado",
                text: "Estas seguro de editar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {

                    idncct:mod.fd.IdnCct,
                    //idnemployee:1,
                    salaryamount:mod.fd.SalaryAmount,
                    name:mod.fd.Name
                                      
                  };
                  //VUE POST
                Vue.http.put(mod.globalurl+"/update/"+mod.fd.Idn,arr).then(response => {
                    // Si todo sale correcto              
                    swal('Modificada Correctamente','','success');
                    $('#ModalCategorySalary').modal('hide');
                    mod.cleanform();

                    }, response => {
                      //Si no sale correcto
                            swal(response.body,'','error')

                        });
                        
                    })
                    //FIN VUE POST
        }
    },

    borrarCategorySalary: function (event)
    {
        swal({
                title: "Confirmar Eliminación",
                text: "Estas seguro de eliminar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {name:mod.fd.Name};
                Vue.http.put(mod.globalurl+"/delete/"+mod.fd.Idn,arr).then(response => {
                    // success callback
                    swal('Eliminado Correctamente','','success');
                $('#ModalCategorySalary').modal('hide');
                mod.cleanform();


            }, response => {
                    swal('Ocurrio un error al eliminar','','error');
                    // error callback
                });
            })
    },

    cleanform: function()
    {
      mod.fd.Idn='';
      mod.fd.IdnCct='';
      mod.fd.SalaryAmount='';
      mod.fd.Name='';
        
      $('#ModalCategorySalary').modal('hide');
      var table = $('#TablaCategorySalary').dataTable();
      //RECARGA LOS DATOS DE LA TABLA
     table.fnReloadAjax();
     
    },
   
     openmodaltonew:function()
        {
          mod.cleanform();
          $('#ModalCategorySalary').modal('toggle');
          //OCULTAR BOTONES O MOSTRAR
          mod.ff.save = true;
          mod.ff.edit = false;
        },
        openmodaltoedit:function()
        {
           $('#ModalCategorySalary').modal('toggle');
          //OCULTAR BOTONES O MOSTRAR
          mod.ff.save = false;
          mod.ff.edit = true;
        }
  }
 
});

   //======INICIO DE FUNCIONES ============
    function CargarTabla()
    {   
      var table = $("#TablaCategorySalary").DataTable({
        //COMPROBACION PARA PINTAR Y CAMBIAR TEXTO DE TABLA


        "destroy": true,
        "scrollY":        "200px",
        "scrollX":        "1000px",
        "language": {
            "url": "/../Spanish.json"
        },
        //Especificaciones de las Columnas que vienen y deben mostrarse
        "columns" : [
            { data : 'idn' },
            { data : 'name' },
            { data : 'namecct' },
            { data : 'salaryamount'},
            { data : 'active'}
                
           
        ],
        //Especificaciones de la URL del servicio
        "ajax": {
            url: '../api/v1/categorysalary',
            dataSrc : ''
        }

    });

            $('#TablaCategorySalary tbody').on( 'click', 'tr', function () {
        
        //OBTENGO LOS VARLOES DE LA TABLA
        mod.fd.Idn = table.row( this ).data().idn;
        mod.fd.IdnCct = table.row( this ).data().idncct;

        mod.fd.Cct = table.row( this ).data().idncct;
      
                
        mod.fd.SalaryAmount = table.row( this ).data().salaryamount;
        mod.fd.Name = table.row( this ).data().name;    
      
        //idnabsencetype=mod.fd.NameAbsenceType;
      //ABRO LA MODAL
        mod.openmodaltoedit();

       });

     } 
