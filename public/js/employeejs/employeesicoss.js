Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');

//FUNCIONES GENERICAS PARA INICIO DE LA PAGINA
var vm = new Vue({
  methods: 
  {
     
    cargartabla:function()
    {
      CargarTabla();
  }
}
});


vm.cargartabla();


//FUNCIONES EN LA MODAL DE EDICION
var mod = new Vue({
  el: '#vuedata',
  data: {
    globalurl:'../api/v1/employeesicoss',
    fd:
    {
      Idn:'',
      Names:'',
      Cuil:'',
      Spouse:'',
      Sons:'',
      Sitrevision:'',
      Condition:'',
      Activity:'',
      Zone:'',
      Appss:'',
      Modcontract:'',
      Obsocial:'',
      Adherents:'',
      Totalbrute:'',
      Remimp1:'',
      Remimp2:'',
      Remimp3:'',
      Remimp4:'',
      Remimp5:'',
      Remimp6:'',
      Remimp7:'',
      Remimp8:'',
      Remimp9:'',
      Asigfam:'',
      Aportevol:'',
      Adicos:'',
      Excedos:'',
      Adicos2:'',
      Province:'',
      Codsinester:'',
      Markreduc:'',
      Recomplrt:'',
      Typecompany:'',
      Adicos3:'',
      Regprevisional:'',
      Codrevision1:'',
      Dayrevision1:'',
      Codrevision2:'',
      Dayrevision2:'',
      Codrevision3:'',
      Dayrevision3:'',
      Salaryadditionals:'',
      Sac:'',
      Extrahoursamount:'',
      Desfavzone:'',
      Vacations:'',
      Daysworked:'',
      Agreess:'',
      Operationtype:'',
      Adics:'',
      Awards:'',
      Extrahours:'',
      Totnoremun:'',
      Maternity:'',
      Rectremuns:'',
      Homeworkdif:'',
      Hoursworked:'',
      Segoblig:'',
  },
  ff:
  {
      save:false,
      edit:true,
  }    
  
},
methods: {
  /*  guardar: function (event) 
    {
      swal({
                title: "Confirmación de guardado",
                text: "Estas seguro de guardar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {
                    //DATOS DE FORMULARIO
                    name:mod.fd.Name,
                    description:mod.fd.Description                    
                  };
                  //VUE POST
                Vue.http.post(mod.globalurl+"/save",arr).then(response => {
                    // Si todo sale correcto              
                    swal('Guardado Correctamente','','success');
                    $('#ModalEdicion').modal('hide');
                    mod.cleanform();

                    }, response => {
                      //Si no sale correcto
                            swal('Ocurrio un error al guardar','','error')
                            mod.cleanform();
                        });
                        
                    })
                    //FIN VUE POST
           
       
                },*/
                editar: function (event) 
                {
                 swal({
                    title: "Confirmación de editado",
                    text: "Estas seguro de editar este elemento",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: 'Aceptar',
                    cancelButtonText: 'Cancelar'
                }).then(function () {


                    var arr = {
                    //DATOS DE FORMULARIO
                    names:mod.fd.Names,
                    cuil:mod.fd.Cuil,
                    spouse:mod.fd.Spouse,
                    sons:mod.fd.Sons,
                    sitrevision:mod.fd.Sitrevision,
                    condition:mod.fd.Condition,
                    activity:mod.fd.Activity,
                    zone:mod.fd.Zone,
                    appss:mod.fd.Appss,
                    modcontract:mod.fd.Modcontract,
                    obsocial:mod.fd.Obsocial,
                    adherents:mod.fd.Adherents,
                    totalbrute:mod.fd.Totalbrute,
                    remimp1:mod.fd.Remimp1,
                    remimp2:mod.fd.Remimp2,
                    remimp3:mod.fd.Remimp3,
                    remimp4:mod.fd.Remimp4,
                    remimp5:mod.fd.Remimp5,
                    remimp6:mod.fd.Remimp6,
                    remimp7:mod.fd.Remimp7,
                    remimp8:mod.fd.Remimp8,
                    remimp9:mod.fd.Remimp9,
                    asigfam:mod.fd.Asigfam,
                    aportevol:mod.fd.Aportevol,
                    adicos:mod.fd.Adicos,
                    excedos:mod.fd.Excedos,
                    adicos2:mod.fd.Adicos2,
                    province:mod.fd.Province,
                    codsinester:mod.fd.Codsinester,
                    markreduc:mod.fd.Markreduc,
                    recomplrt:mod.fd.Recomplrt,
                    typecompany:mod.fd.Typecompany,
                    adicos3:mod.fd.Adicos3,
                    regprevisional:mod.fd.Regprevisional,
                    codrevision1:mod.fd.Codrevision1,
                    dayrevision1:mod.fd.Dayrevision1,
                    codrevision2:mod.fd.Codrevision2,
                    dayrevision2:mod.fd.Dayrevision2,
                    codrevision3:mod.fd.Codrevision3,
                    dayrevision3:mod.fd.Dayrevision3,
                    salaryadditionals:mod.fd.Salaryadditionals,
                    sac:mod.fd.Sac,
                    extrahoursamount:mod.fd.Extrahoursamount,
                    desfavzone:mod.fd.Desfavzone,
                    vacations:mod.fd.Vacations,
                    daysworked:mod.fd.Daysworked,
                    agreess:mod.fd.Agreess,
                    operationtype:mod.fd.Operationtype,
                    adics:mod.fd.Adics,
                    awards:mod.fd.Awards,
                    extrahours:mod.fd.Extrahours,
                    totnoremun:mod.fd.Totnoremun,
                    maternity:mod.fd.Maternity,
                    rectremuns:mod.fd.Rectremuns,
                    homeworkdif:mod.fd.Homeworkdif,
                    hoursworked:mod.fd.Hoursworked,
                    segoblig:mod.fd.Segoblig
                    
                };
                  //VUE POST
                  Vue.http.put(mod.globalurl+"/update/"+mod.fd.Idn,arr).then(response => {
                    // Si todo sale correcto              
                    swal('Modificada Correctamente','','success');
                    $('#ModalEdicion').modal('hide');
                    mod.cleanform();

                }, response => {
                      //Si no sale correcto
                      swal('Ocurrio un error al modificar','','error')
                      mod.cleanform();
                  });
                  
              })
                    //FIN VUE POST
                },   
                
                cleanform: function()
                {
                  mod.fd.Namec='';
                  mod.fd.Emailc='';
                  mod.fd.Telephonec='';
                  $('#ModalEdicion').modal('hide');
                  var table = $('#TablaMaster').dataTable();
      //RECARGA LOS DATOS DE LA TABLA
      table.fnReloadAjax();
      
  },
  
  openmodaltoedit:function()
  {
     $('#ModalEdicion').modal('toggle');
          //OCULTAR BOTONES O MOSTRAR
          mod.ff.save = false;
          mod.ff.edit = true;
      }
  }
  
});

   //======INICIO DE FUNCIONES ============
   
   function CargarTabla()
   {   
      var table = $("#TablaMaster").DataTable({
        //COMPROBACION PARA PINTAR Y CAMBIAR TEXTO DE TABLA


        "destroy": true,
        "scrollY":        "200px",
        "scrollX":        "1000px",
        "language": {
            "url": "/../Spanish.json"
        },
        //Especificaciones de las Columnas que vienen y deben mostrarse
        "columns" : [
        { data : 'idn' },
        { data : 'employeename' },
        { data : 'typerunname' },
        { data : 'periodname'},
        { data : 'active'}
        
        
        ],
        //Especificaciones de la URL del servicio
        "ajax": {
            url: '../api/v1/employeesicoss',
            dataSrc : ''
        }

    });

      $('#TablaMaster tbody').on( 'click', 'tr', function () {
        
        //OBTENGO LOS VARLOES DE LA TABLA
        mod.fd.Idn = table.row( this ).data().idn;
        
        
        
        mod.fd.Names= table.row( this ).data().names;
        mod.fd.Cuil= table.row( this ).data().cuil;
        mod.fd.Spouse= table.row( this ).data().spouse;
        mod.fd.Sons= table.row( this ).data().sons;
        mod.fd.Sitrevision= table.row( this ).data().sitrevision;
        mod.fd.Condition= table.row( this ).data().condition;
        mod.fd.Activity= table.row( this ).data().activity;
        mod.fd.Appss= table.row( this ).data().appss;
        mod.fd.Zone= table.row( this ).data().zone;
        mod.fd.Modcontract= table.row( this ).data().modcontract;
        mod.fd.Obsocial= table.row( this ).data().obsocial;
        mod.fd.Adherents= table.row( this ).data().adherents;
        mod.fd.Totalbrute= table.row( this ).data().totalbrute;
        mod.fd.Remimp1= table.row( this ).data().remimp1;
        mod.fd.Remimp2= table.row( this ).data().remimp2;
        mod.fd.Remimp3= table.row( this ).data().remimp3;
        mod.fd.Remimp4= table.row( this ).data().remimp4;
        mod.fd.Remimp5= table.row( this ).data().remimp5;
        mod.fd.Remimp6= table.row( this ).data().remimp6;
        mod.fd.Remimp7= table.row( this ).data().remimp7;
        mod.fd.Remimp8= table.row( this ).data().remimp8;
        mod.fd.Asigfam= table.row( this ).data().asigfam;
        mod.fd.Remimp9= table.row( this ).data().remimp9;
        mod.fd.Aportevol= table.row( this ).data().aportevol;
        mod.fd.Adicos= table.row( this ).data().adicos;
        mod.fd.Excedos= table.row( this ).data().excedos;
        mod.fd.Adicos2= table.row( this ).data().adicos2;
        mod.fd.Province= table.row( this ).data().province;
        mod.fd.Codsinester= table.row( this ).data().codsinester;
        mod.fd.Markreduc= table.row( this ).data().markreduc;
        mod.fd.Recomplrt= table.row( this ).data().recomplrt;
        mod.fd.Typecompany= table.row( this ).data().typecompany;
        mod.fd.Adicos3= table.row( this ).data().adicos3;
        mod.fd.Regprevisional= table.row( this ).data().regprevisional;
        mod.fd.Codrevision1= table.row( this ).data().codrevision1;
        mod.fd.Dayrevision1= table.row( this ).data().dayrevision1;
        mod.fd.Codrevision2= table.row( this ).data().codrevision2;
        mod.fd.Dayrevision2= table.row( this ).data().dayrevision2;
        mod.fd.Codrevision3= table.row( this ).data().codrevision3;
        mod.fd.Dayrevision3= table.row( this ).data().dayrevision3;
        mod.fd.Salaryadditionals= table.row( this ).data().salaryadditionals;
        mod.fd.Sac= table.row( this ).data().sac;
        mod.fd.Extrahoursamount= table.row( this ).data().extrahoursamount;
        mod.fd.Desfavzone= table.row( this ).data().desfavzone;
        mod.fd.Vacations= table.row( this ).data().vacations;
        mod.fd.Daysworked= table.row( this ).data().daysworked;
        mod.fd.Agreess= table.row( this ).data().agreess;
        mod.fd.Operationtype= table.row( this ).data().operationtype;
        mod.fd.Adics= table.row( this ).data().adics;
        mod.fd.Awards= table.row( this ).data().awards;
        mod.fd.Extrahours= table.row( this ).data().extrahours;
        mod.fd.Totnoremun= table.row( this ).data().totnoremun;
        mod.fd.Maternity= table.row( this ).data().maternity;
        mod.fd.Rectremuns= table.row( this ).data().rectremuns;
        mod.fd.Homeworkdif= table.row( this ).data().homeworkdif;
        mod.fd.Hoursworked= table.row( this ).data().hoursworked;
        mod.fd.Segobli= table.row( this ).data().segobli;
        
        mod.openmodaltoedit();

    });

  } 



