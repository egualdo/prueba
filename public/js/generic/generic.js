var getUrlParams = function (url) {
  var params = {};
  (url + '?').split('?')[1].split('&').forEach(function (pair) {
    pair = (pair + '=').split('=').map(decodeURIComponent);
    if (pair[0].length) {
      params[pair[0]] = pair[1];
    }
  });
  return params;
};


$.fn.datepicker.dates['es'] = {
    days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
    daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
    today: "Hoy",
    clear: "Limpiar",
    format: "yyyy-mm-dd",
    titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
    weekStart: 0
};
function CheckNulls(items,strings)
{

    for (i = 0; i < items.length; i++) {
        if(items[i] === null || items[i] === "") {
            toastr.warning('Por favor indica ' + strings[i]);
            return 0;
        }
    }
}
function getactualvalue(list, idn) {
    var result = list[parseInt(idn) - 1];
    return result;
}
/*function FormatDate(dates)
{
  var month = dates.getMonth();
  alert(month);
  return month;

}*/
 function FormatDate(fechaentrante) {

  if (fechaentrante === null)
  {
      fechaentrante = "";
  }
  if (fechaentrante === "")
  {
    var newdate = "";
  }
  else
  {
var myDate = fechaentrante.toString();
//console.log(myDate);
var d = myDate.substring(8,10);
var m =  myDate.substring(5,7);  
var y = myDate.substring(0,4);

    var newdate=(d+ "-" + m + "-" + y);
   // console.log("FORMATEANDO FECHA:"+newdate);
  }

   // console.log("FORMATEANDO FECHA:"+newdate);
  return newdate;

  
};
 function FormatDateReverse(fechaentrante) {
  if (fechaentrante === "")
  {
    var newdate = "";
  }
  else
  {
    //console.log("Entrante:" +fechaentrante);
var myDate = fechaentrante.toString();
//console.log(myDate);
var d = myDate.substring(0,2);
var m =  myDate.substring(3,5);  
var y = myDate.substring(6,10);

    var newdate=(y+ "-" + m + "-" + d);
  }

   // console.log("FORMATEANDO FECHA:"+newdate);
  return newdate;


  
};