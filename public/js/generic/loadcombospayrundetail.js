var combo = new Vue({
    methods:
        {

            loadPeriod:function()
            {
                // GET /someUrl
                this.$http.get('../api/v1/period').then(response => {

                    //Adding Data to Combos
                    $.each(response.body,function(i,v)
                {
                    empAsig.fd.Periodlist.push({id:v.idn,label:v.title});
                    empVaca.fd.Periodlist.push({id:v.idn,label:v.title});
                    empExclu.fd.Periodlist.push({id:v.idn,label:v.title});
                    empConst.fd.Periodlist.push({id:v.idn,label:v.title});
                });
                //Asignando un dato inicial al combo
                empAsig.fd.Period = response.body[0].idn;
                empVaca.fd.Period = response.body[0].idn;
                empExclu.fd.Period = response.body[0].idn;
                empConst.fd.Period = response.body[0].idn;

            }, response => {
                // error callback
            });
            },
            loadCivilStatus:function()
            {
                this.$http.get('../api/v1/civilstatus').then(response => {
                    // get body data
                    $.each(response.body,function(i,v)
                {
                    empDP.fd.Civilstatuslist.push({id:v.idn,label:v.name});
                    empHead.fd.Civilstatuslist.push({id:v.idn,label:v.name});
                });
            }, response => {  // error callback
            });
            },
            loadNationality:function()
            {
                this.$http.get('../api/v1/nationality').then(response => {
                    // get body data
                    $.each(response.body,function(i,v)
                {
                    empRE.fd.Nationalitylist.push({id:v.idn,label:v.name});
                    empHead.fd.Nationalitylist.push({id:v.idn,label:v.name});
                });
            }, response => {  // error callback
            });
            },
            loadCostCenter:function()
            {
                this.$http.get('../api/v1/costcenter').then(response => {
                    $.each(response.body,function(i,v)
                {
                    empWR.fd.Costcenterlist.push({id:v.idn,label:v.name});
                });

            }, response => {  // error callback
            });
            },
            loadDepartment:function()
            {
                this.$http.get('../api/v1/department').then(response => {
                    $.each(response.body,function(i,v)
                {
                    empWR.fd.Departmentlist.push({id:v.idn,label:v.name});
                });

            }, response => {  // error callback
            });
            },
            loadPositionJob:function()
            {
                this.$http.get('../api/v1/positionjob').then(response => {
                    $.each(response.body,function(i,v)
                {
                    empWR.fd.Positionjoblist.push({id:v.idn,label:v.name});
                });

            }, response => {  // error callback
            });
            },
            loadWorkCalendar:function()
            {
                this.$http.get('../api/v1/workcalendar').then(response => {
                    $.each(response.body,function(i,v)
                {
                    empWR.fd.Workcalendarlist.push({id:v.idn,label:v.name});
                });

            }, response => {  // error callback
            });
            },
            loadCct:function()
            {
                this.$http.get('../api/v1/ccts').then(response => {
                    $.each(response.body,function(i,v)
                {
                    empWR.fd.Cctlist.push({id:v.idn,label:v.name});
                });

            }, response => {  // error callback
            });
            },
            loadCareer:function()
            {
                this.$http.get('../api/v1/career').then(response => {
                    $.each(response.body,function(i,v)
                {
                    empCa.fd.Careerlist.push({id:v.idn,label:v.name});
                });

            }, response => {  // error callback
            });
            },
            loadLanguage:function()
            {
                this.$http.get('../api/v1/languaje').then(response => {
                    $.each(response.body,function(i,v)
                {
                    empLa.fd.Languagelist.push({id:v.idn,label:v.name});
                });

            }, response => {  // error callback
            });
            },
            loadCategorySalary:function()
            {
                this.$http.get('../api/v1/categorysalary').then(response => {
                    $.each(response.body,function(i,v)
                {
                    empWR.fd.Categorysalarylist.push({id:v.idn,label:v.name});
                });

            }, response => {  // error callback
            });
            },


            loadGroup:function()
            {
                this.$http.get('../api/v1/group').then(response => {
                    $.each(response.body,function(i,v)
                {
                    empGroup.fd.Grouplist.push({id:v.idn,label:v.name});
                });

            }, response => {  // error callback
            });
            },

            loadCondition:function()
            {
                this.$http.get('../api/v1/employeecondition').then(response => {
                    $.each(response.body,function(i,v)
                {
                    empCAC.fd.Employeeconditionlist.push({id:v.idn,label:v.name});
                });

            }, response => {  // error callback
            });
            },
            loadExitReason:function()
            {
                this.$http.get('../api/v1/exitreason').then(response => {
                    $.each(response.body,function(i,v)
                {
                    empCAC.fd.Exitreasonlist.push({id:v.idn,label:v.name});
                });

            }, response => {  // error callback
            });
            },
            loadModContract:function()
            {
                this.$http.get('../api/v1/modcontract').then(response => {
                    $.each(response.body,function(i,v)
                {
                    empCAC.fd.Modcontractlist.push({id:v.idn,label:v.name});
                });

            }, response => {  // error callback
            });
            },
            loadRoster:function()
            {
                this.$http.get('../api/v1/roster').then(response => {
                    $.each(response.body,function(i,v)
                {
                    empCAC.fd.Rosterlist.push({id:v.idn,label:v.name});
                });

            }, response => {  // error callback
            });
            },
            loadObsocial:function()
            {
                this.$http.get('../api/v1/socworks').then(response => {
                    $.each(response.body,function(i,v)
                {
                    empOBS.fd.Obsociallist.push({id:v.idn,label:v.name});
                });

            }, response => {  // error callback
            });
            },
            loadMedicplan:function()
            {
                this.$http.get('../api/v1/medicalplan').then(response => {
                    $.each(response.body,function(i,v)
                {
                    empOBS.fd.Medicplanlist.push({id:v.idn,label:v.name});
                });

            }, response => {  // error callback
            });
            },
            loadSitRevision:function()
            {
                this.$http.get('../api/v1/sitrevision').then(response => {
                    $.each(response.body,function(i,v)
                {
                    empOTH.fd.Sitrevisionlist.push({id:v.idn,label:v.name});
                });

            }, response => {  // error callback
            });
            },
            loadWorkPlace:function()
            {
                this.$http.get('../api/v1/workplaces').then(response => {
                    $.each(response.body,function(i,v)
                {
                    empOTH.fd.Workplacelist.push({id:v.idn,label:v.name});
                });

            }, response => {  // error callback
            });
            },
            loadSindicate:function()
            {
                this.$http.get('../api/v1/sindicate').then(response => {
                    $.each(response.body,function(i,v)
                {
                    empOTH.fd.Sindicatelist.push({id:v.idn,label:v.name});
                });

            }, response => {  // error callback
            });
            },
            loadRegPrevisional:function()
            {
                this.$http.get('../api/v1/regprevisional').then(response => {
                    $.each(response.body,function(i,v)
                {
                    empOTH.fd.Regprevisionallist.push({id:v.idn,label:v.name});
                });

            }, response => {  // error callback
            });
            },
            loadSinestercode:function()
            {
                this.$http.get('../api/v1/sinestercode').then(response => {
                    $.each(response.body,function(i,v)
                {
                    empOTH.fd.Sinestercodelist.push({id:v.idn,label:v.name});
                });

            }, response => {  // error callback
            });
            },
            loadLevelStudies:function()
            {
                this.$http.get('../api/v1/level').then(response => {
                    $.each(response.body,function(i,v)
                {
                    empOTH.fd.Levelstudieslist.push({id:v.idn,label:v.name});
                });

            }, response => {  // error callback
            });
            },
            loadPeriodByType:function(idnperiod)
            {
                run.fd.Periodlist = [];
                this.$http.get('../api/v1/periodbytype/'+idnperiod).then(response => {
                    $.each(response.body,function(i,v)
                {
                    run.fd.Periodlist.push({id:v.idn,label:v.title});

                });

                // empRun.fd.Period = response.body[0].idn;
            }, response => {  // error callback
            });
            },
            loadRunType:function(idnperiod)
            {
                this.$http.get('../api/v1/runtype').then(response => {
                    $.each(response.body,function(i,v)
                {
                    // run.fd.Typerunlist.push({id:v.idn,label:v.name});
                    empRun.fd.Typerunlist.push({id:v.idn,label:v.name});
                    empAsig.fd.Typerunlist.push({id:v.idn,label:v.name});
                    empExclu.fd.Typerunlist.push({id:v.idn,label:v.name});
                    empVaca.fd.Typerunlist.push({id:v.idn,label:v.name});
                    empConst.fd.Typerunlist.push({id:v.idn,label:v.name});

                });

                // empRun.fd.Typerun = response.body[0].idn;
            }, response => {  // error callback
            });
            },
            loadPayMethodPayrun:function()
            {
                this.$http.get('../api/v1/paymethodpayrun').then(response => {
                    $.each(response.body,function(i,v)
                {
                    empRun.fd.Paymentmethodlist.push({id:v.idn,label:v.name});
                });

                //  empRun.fd.Paymentmethod = response.body[0].idn;
            }, response => {  // error callback
            });
            },
            loadConcept:function()
            {

                this.$http.get('../api/v1/concept').then(response => {

                    // get body data
                    $.each(response.body,function(i,v)
                {
                    empAsig.fd.Conceptlist.push({id:v.idn,label:v.name});
                    empExclu.fd.Conceptlist.push({id:v.idn,label:v.name});
                });
                empAsig.fd.Concept = response.body[0].idn;
                // empExclu.fd.Concept = response.body[0].idn;

            }, response => {
                // error callback
            });
            },
            loadSpecialType:function()
            {
                // GET /someUrl
                this.$http.get('../api/v1/specialtyp').then(response => {

                    $.each(response.body,function(i,v)
                {

                    empAsig.fd.Specialtypelist.push({id:v.idn,label:v.name});
                });

                empAsig.fd.Specialtype = response.body[0].idn;
            }, response => {
                // error callback
            });
            },
            loadAbsenceType:function()
            {
                // GET /someUrl
                this.$http.get('../api/v1/absencetype').then(response => {

                    $.each(response.body,function(i,v)
                {

                    empAbsen.fd.AbsenceTypelist.push({id:v.idn,label:v.name});
                });

                //empAbsen.fd.AbsenceType = response.body[0].idn;
            }, response => {
                // error callback
            });
            },
            loadConstant:function()
            {
                // GET /someUrl
                this.$http.get('../api/v1/constant').then(response => {

                    $.each(response.body,function(i,v)
                {

                    empConst.fd.Constantlist.push({id:v.idn,label:v.name});
                });

                empConst.fd.Constant = response.body[0].idn;
            }, response => {
                // error callback
            });
            },
        }
});