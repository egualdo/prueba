Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');

//FUNCIONES GENERICAS PARA INICIO DE LA PAGINA
 var vm = new Vue({
  methods: 
      { 
 cargarcomboperiod:function()
        {
                   // GET /someUrl
            this.$http.get('../api/v1/period').then(response => {

            // get body data
            mod.fd.Periodoptions = response.body;

          }, response => {
            // error callback
          });
        },
          
 cargarcombocostcenter:function()
        {
                   // GET /someUrl
            this.$http.get('../api/v1/costcenter').then(response => {

            // get body data
            mod.fd.Costcenteroptions = response.body;

          }, response => {
            // error callback
          });
        },
        

        cargarcomboemployee:function()
        {
                   // GET /someUrl
            this.$http.get('../api/v1/employee').then(response => {

            // get body data
            mod.fd.Employeeoptions = response.body;

          }, response => {
            // error callback
          });
        },
        cargartabla:function()
        {
          CargarTabla();
        }
      }
    });


vm.cargartabla();
vm.cargarcomboperiod();
vm.cargarcomboemployee();
vm.cargarcombocostcenter();
//FUNCIONES EN LA MODAL DE EDICION
var mod = new Vue({
  el: '#vuedata',
  data: {
    globalurl:'../api/v1/costcenter',
    fd:
    {
      Idn:'',
      IdnPeriod:"",
       IdnEmployee:"",
      Name:"",

      Nameemployee:'',
      Employee:'',
      Employeeoptions:'',

      Nameperiod:'',
      Period:'',
      Periodoptions:'',

       Namecostcenter:'',
      Costcenter:'',
      Costcenteroptions:'',

      Rangedate:""
     
      
    },
    ff:
    {
     // descargarpdf:true,
      //descargarexcel:true,
    }    
    
  },
  
  methods: {
   

    cleanform: function()
    {
     //CargarTabla
      $('#ModalEdicion').modal('hide');
      var table = $('#TablaMaster').dataTable();
      //RECARGA LOS DATOS DE LA TABLA
     table.fnReloadAjax();
     
    },
   
     openmodaltonew:function()
        {
          //mod.cleanform();
          $('#ModalEdicion').modal('toggle');
          //OCULTAR BOTONES O MOSTRAR
         // mod.ff.descargarpdf= true;
          //mod.ff.descargarexcel = true;
        }
       
  }
 
});

   //======INICIO DE FUNCIONES ============
    function CargarTabla()
    {   
      var table = $("#TablaMaster").DataTable({
        //COMPROBACION PARA PINTAR Y CAMBIAR TEXTO DE TABLA


        "destroy": true,
        "scrollY":        "200px",
        "scrollX":        "1000px",
        "language": {
            "url": "/../Spanish.json"
        },
        //Especificaciones de las Columnas que vienen y deben mostrarse
        "columns" : [
            { data : 'idn' },
            { data : 'name' }
                
           
        ],
        //Especificaciones de la URL del servicio
        "ajax": {
            url: '../api/v1/costcenter',
            dataSrc : ''
        }

    });

          /*  $('#TablaMaster tbody').on( 'click', 'tr', function () {
        
        //OBTENGO LOS VARLOES DE LA TABLA
        mod.fd.Idn = table.row( this ).data().idn;
        mod.fd.IdnCompany = table.row( this ).data().idncompany;

        mod.fd.Company = table.row( this ).data().idncompany;
        mod.fd.Transmitter = table.row( this ).data().idntransmitter;
        mod.fd.Receiver = table.row( this ).data().idnreceiver;
        mod.fd.Tittle = table.row( this ).data().tittle;
        mod.fd.Description = table.row( this ).data().description;    
      
        idncompany=mod.fd.Company;
      //ABRO LA MODAL
        mod.openmodaltoedit();

       });*/

     } 
