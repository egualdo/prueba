Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');

//FUNCIONES GENERICAS PARA INICIO DE LA PAGINA
 var vm = new Vue({
  methods: 
      { 

           cargarcomboperiod:function()
        {
                   // GET /someUrl
            this.$http.get('../api/v1/period').then(response => {

            // get body data
            mod.fd.Periodoptions = response.body;

          }, response => {
            // error callback
          });
        },
         cargarcomboworkplaces:function()
        {
                   // GET /someUrl
            this.$http.get('../api/v1/workplaces').then(response => {

            // get body data
            mod.fd.Workplacesoptions = response.body;

          }, response => {
            // error callback
          });
        },
             cargarcombogroup:function()
        {
                   // GET /someUrl
            this.$http.get('../api/v1/group').then(response => {

            // get body data
            mod.fd.Groupoptions = response.body;

          }, response => {
            // error callback
          });
        },

        cargartabla:function()
        {
          CargarTabla();
        }
      }
    });

vm.cargarcomboperiod();
vm.cargarcomboworkplaces();
vm.cargarcombogroup();
vm.cargartabla();


//FUNCIONES EN LA MODAL DE EDICION
var mod = new Vue({
  el: '#vuedata',
  data: {
    globalurl:'../api/v1/period',
    fd:
    {
      Idn:'',

      IdnPeriod:'',
      
        
         
        
      Title:'',
      Description:'',

       Nameperiod:'',
      Period:'',
      Periodoptions:'',

 Namegroup:'',
      Group:'',
      Groupoptions:'',

Nameworkplaces:'',
      Workplaces:'',
      Workplacesoptions:'',      

      Rangedate: '',
      
      Bajames: '',
      
    },
    ff:
    {
     // descargarpdf:true,
      //descargarexcel:true,
    }    
    
  },
  
  methods: {
   

    cleanform: function()
    {
     //CargarTabla
      $('#ModalEdicion').modal('hide');
      var table = $('#TablaMaster').dataTable();
      //RECARGA LOS DATOS DE LA TABLA
     table.fnReloadAjax();
     
    },
   
     openmodaltonew:function()
        {
          //mod.cleanform();
          $('#ModalEdicion').modal('toggle');
          //OCULTAR BOTONES O MOSTRAR
         // mod.ff.descargarpdf= true;
          //mod.ff.descargarexcel = true;
        }
       
  }
 
});

   //======INICIO DE FUNCIONES ============
    function CargarTabla()
    {   
      var table = $("#TablaMaster").DataTable({
        //COMPROBACION PARA PINTAR Y CAMBIAR TEXTO DE TABLA


        "destroy": true,
        "scrollY":        "200px",
        "scrollX":        "1000px",
        "language": {
            "url": "/../Spanish.json"
        },
        //Especificaciones de las Columnas que vienen y deben mostrarse
        "columns" : [
            { data : 'idn' },
            { data : 'title' }
           
                
           
        ],
        //Especificaciones de la URL del servicio
        "ajax": {
            url: '../api/v1/period',
            dataSrc : ''
        }

    });

            $('#TablaMaster tbody').on( 'click', 'tr', function () {
        
        //OBTENGO LOS VARLOES DE LA TABLA
        mod.fd.Idn = table.row( this ).data().idn;
        mod.fd.Title = table.row( this ).data().title;

       /* mod.fd.Company = table.row( this ).data().idncompany;
        mod.fd.Transmitter = table.row( this ).data().idntransmitter;
        mod.fd.Receiver = table.row( this ).data().idnreceiver;
        mod.fd.Tittle = table.row( this ).data().tittle;
        mod.fd.Description = table.row( this ).data().description;    
      
        idncompany=mod.fd.Company;
      //ABRO LA MODAL
        mod.openmodaltoedit();
*/
       });

     } 
