/**
 * Created by Hember on 16/2/2018.
 */
Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');

var vm = new Vue({
    methods: {
        cargartabla: function () {
            CargarTabla();
        }
    }
});

vm.cargartabla();

function CargarTabla() {
    var table = $("#TablaMaster").DataTable({
        "destroy": true,
        "scrollY": "200px",
        "scrollX": "1000px",
        "language": {
            "url": "/../Spanish.json"
        },
        "columns": [
            { data : 'idn' },
            { data : 'nametyperun'},
            { data : 'nameroster'},
            { data : 'titleperiod'},
            { data : 'payments' },
            { data : 'unremuns' },
            { data : 'deductions' },
            { data : 'totalpay' },
            { data : 'vacations' },
            { data : 'hoursworked' },
            { data : 'absencesjustified' },
            { data : 'absencesinjustified' }

        ],
        "ajax": {
            url: '../api/v1/payrunhead/open/news',
            dataSrc: ''
        },
        "rowCallback": function (row, data, index) {
            $('td:eq(8)', row).html('<b>'+data.vacations +' Dias</b>');
            $('td:eq(9)', row).html('<b>'+data.hoursworked +' Horas</b>');
            $('td:eq(10)', row).html('<b>'+data.absencesjustified +' Dias</b>');
            $('td:eq(11)', row).html('<b>'+data.absencesinjustified +' Dias</b>');
        }
    });
}
