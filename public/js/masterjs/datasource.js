Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');

//FUNCIONES GENERICAS PARA INICIO DE LA PAGINA
 var vm = new Vue({
  methods: 
     
      {
            cargarcombocompany:function()
        {
                   // GET /someUrl
            this.$http.get('../api/v1/company').then(response => {

            // get body data
            mod.fd.Companyoptions = response.body;

          }, response => {
            // error callback
          });
        },
        cargartabla:function()
        {
          CargarTabla();
        }
      }
    });

vm.cargartabla();
vm.cargarcombocompany();

//FUNCIONES EN LA MODAL DE EDICION
var mod = new Vue({
  el: '#vuedata',
  data: {
    globalurl:'../api/v1/datasource',
    fd:
    {
      Idn:'',
      Idncompany:'',
        Companyoptions:'',
        Company:'',
      Host:'',
        Database:'',
        Username:'',
        Password:''
    },
    ff:
    {
      save:false,
      edit:true,
    }    
    
  },
  methods: {
    guardar: function (event) 
    {
        var items = [
            mod.fd.Idncompany,
            mod.fd.Host,
            mod.fd.Database,
            mod.fd.Username,
            mod.fd.Password
        ];
        var strings = [
            'Empresa',
            'Host',
            'Nombre de la Base de datos',
            'Usuario de la Base de datos',
            'Contraseña de la Base de datos'
        ];
        var checked = CheckNulls(items, strings);
        if(checked !== 0){
       swal({
                title: "Confirmación de guardado",
                text: "Estas seguro de guardar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {
                    //DATOS DE FORMULARIO
                    idncompany:mod.fd.Idncompany,
                    host:mod.fd.Host,
                    database:mod.fd.Database,
                    username:mod.fd.Username,
                    password:mod.fd.Password
                  };
                  //VUE POST
                Vue.http.post(mod.globalurl+"/save",arr).then(response => {
                    // Si todo sale correcto              
                    swal('Guardado Correctamente','','success');
                    $('#ModalEdicion').modal('hide');
                    mod.cleanform();

                    }, response => {
                      //Si no sale correcto
                            swal(response.body,'','error')

                        });
                        
                    })
                    //FIN VUE POST
           
       
            }

    },
    editar: function (event) 
    {

        var items = [
            mod.fd.Idncompany,
            mod.fd.Host,
            mod.fd.Database,
            mod.fd.Username,
            mod.fd.Password
        ];
        var strings = [
            'Empresa',
            'Host',
            'Nombre de la Base de datos',
            'Usuario de la Base de datos',
            'Contraseña de la Base de datos'
        ];
        var checked = CheckNulls(items, strings);
        if(checked !== 0){
       swal({
                title: "Confirmación de editado",
                text: "Estas seguro de editar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {
                    //DATOS DE FORMULARIO
                   idncompany:mod.fd.Idncompany,
                    host:mod.fd.Host,
                    database:mod.fd.Database,
                    username:mod.fd.Username,
                    password:mod.fd.Password
                                      
                  };
                  //VUE POST
                Vue.http.put(mod.globalurl+"/update/"+mod.fd.Idn,arr).then(response => {
                    // Si todo sale correcto              
                    swal('Modificada Correctamente','','success');
                    $('#ModalEdicion').modal('hide');
                    mod.cleanform();

                    }, response => {
                      //Si no sale correcto
                            swal(response.body,'','error')

                        });
                        
                    })
                    //FIN VUE POST
          }

    },
    borrar: function (event)
    {
        swal({
                title: "Confirmar Eliminación",
                text: "Estas seguro de eliminar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {name:mod.fd.Name};
                Vue.http.put(mod.globalurl+"/delete/"+mod.fd.Idn,arr).then(response => {
                    // success callback
                    swal('Eliminado Correctamente','','success');
                $('#ModalEdicion').modal('hide');
                mod.cleanform();


            }, response => {
                    swal(response.body,'','error');
                    // error callback
                });
            })
    },
    cleanform: function()
    {
      mod.fd.Idncompany='';
      mod.fd.Host='';
      mod.fd.Database='';
        mod.fd.Username='';
        mod.fd.Password='';
      $('#ModalEdicion').modal('hide');
      var table = $('#TablaMaster').dataTable();
      //RECARGA LOS DATOS DE LA TABLA
     table.fnReloadAjax();
     
    },
   
     openmodaltonew:function()
        {
          mod.cleanform();
          $('#ModalEdicion').modal('toggle');
          //OCULTAR BOTONES O MOSTRAR
          mod.ff.save = true;
          mod.ff.edit = false;
        },
        openmodaltoedit:function()
        {
           $('#ModalEdicion').modal('toggle');
          //OCULTAR BOTONES O MOSTRAR
          mod.ff.save = false;
          mod.ff.edit = true;
        }
  }
 
});

   //======INICIO DE FUNCIONES ============
    function CargarTabla()
    {   
      var table = $("#TablaMaster").DataTable({
        //COMPROBACION PARA PINTAR Y CAMBIAR TEXTO DE TABLA


        "destroy": true,
        "scrollY":        "200px",
        "scrollX":        "1000px",
        "language": {
            "url": "/../Spanish.json"
        },
        //Especificaciones de las Columnas que vienen y deben mostrarse
        "columns" : [
            { data : 'idn' },
            { data : 'namecompany'},
            { data : 'host'},
            { data : 'database'},
            { data : 'username'},
            { data : 'password'}
                
           
        ],
        //Especificaciones de la URL del servicio
        "ajax": {
            url: '../api/v1/datasource',
            dataSrc : ''
        }

    });

            $('#TablaMaster tbody').on( 'click', 'tr', function () {
        
        //OBTENGO LOS VARLOES DE LA TABLA
        mod.fd.Idn = table.row( this ).data().idn;
        mod.fd.Idncompany = table.row( this ).data().idncompany;
               //  mod.fd.Idncompany = table.row( this ).data().idncompany;
        mod.fd.Host = table.row( this ).data().host;
                mod.fd.Database = table.row( this ).data().database;
                mod.fd.Username = table.row( this ).data().username;
                mod.fd.Password = table.row( this ).data().password;
      
      //ABRO LA MODAL
        mod.openmodaltoedit();

       });

     } 




