Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');

var vm = new Vue({
    methods: {
        clear: function () {
            mod.fd.DataPost = '';
            mod.fd.File = '';
            mod.fd.FileName = '';
            mod.fd.CsvJson = '';
            mod.fd.TypeImportFile = 2;
            mod.fd.Verified = false;
        },
        importdata: function (data) {
            this.$http.post('/api/v1/importararchivo', data).then(response => {
                console.log(response.body);
            $datos = response.body;
            if ($datos.errors === 0 && $datos.success > 0) {
                if ($datos.success === 1) {
                    $text = "Se procesó " + $datos.success + " Registro";
                } else {
                    $text = "Se procesaron " + $datos.success + " Registros";
                }
                swal({
                    title: "Se ha importado el archivo correctamente",
                    text: "Se procesaron " + $datos.success + " Registros",
                    type: "success",
                    showCancelButton: false,
                    confirmButtonText: 'Aceptar'
                });
               vm.clear();
            } else if ($datos.errors > 0 && $datos.success > 0) {
                swal({
                    title: "Se ha importado el archivo pero se han omitido algunos registros",
                    html: "Se procesaron " + $datos.success + " Registros <br> Se Omitieron " + $datos.errors + " Registros",
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonText: 'Aceptar'
                });
            } else {
                $html = '<h3>Ocurrieron los siguientes Errores</h3>' +
                    '<ul style="list-style: none">';
                $.each($datos.array_errors, function (i, v) {
                    $html += '<li>' + v + '</li>';
                });

                $html += '</ul>';
                swal({
                    title: "Ocurrio un error y no se importó el archivo",
                    type: "error",
                    html: $html,
                    showCancelButton: false,
                    confirmButtonText: 'Aceptar'
                });
            }
        },
            response =>{});
        }
    }
});

var mod = new Vue({
    el: '#vuedata',
    data: {
        fd: {
            TypeImportFile: 2,
            TypeImportFiles: [
                // {id: 1, name: 'Legajo'},
                {id: 2, name: 'Constantes de Legajo'},
                {id: 3, name: 'Constantes de CCT'},
                {id: 4, name: 'Constantes de LCT'},
                {id: 5, name: 'Constantes por empresa'},
                {id: 6, name: 'Asignaciones y Deducciones'}
            ],
            DataPost: '',
            File: '',
            FileName: '',
            CsvJson: '',
            Verified: false
        },
        ff: {}

    },
    methods: {
        verificar: function () {
            ParseCsv();
        },
        importar: function () {
            if (mod.fd.File === '') {
                toastr.error('Debes Seleccionar un archivo ');
            }else if(!mod.fd.Verified){
                toastr.error('Debes Verificar el archivo');
            }else{
                ImportCsv();
            }
        }
    }
});

function ParseCsv() {
    if (mod.fd.File === '') {
        toastr.error('Debes Seleccionar un archivo ');
        return false;
    }
    $('#file').parse({
        config: {
            delimiter: "",	// auto-detect
            newline: "",	// auto-detect
            quoteChar: '"',
            header: false,
            dynamicTyping: false,
            preview: 0,
            encoding: "",
            worker: false,
            comments: false,
            step: undefined,
            complete: verifyFn,
            error: undefined,
            download: true,
            skipEmptyLines: false,
            chunk: undefined,
            fastMode: undefined,
            beforeFirstChunk: undefined,
            withCredentials: undefined
        }
    });
}


function ImportCsv() {
    $('#file').parse({
        config: {
            delimiter: "",	// auto-detect
            newline: "",	// auto-detect
            quoteChar: '"',
            header: false,
            dynamicTyping: false,
            preview: 0,
            encoding: "",
            worker: false,
            comments: false,
            step: undefined,
            complete: importFn,
            error: undefined,
            download: true,
            skipEmptyLines: false,
            chunk: undefined,
            fastMode: undefined,
            beforeFirstChunk: undefined,
            withCredentials: undefined
        }
    });
}

function verifyFn(results) {
    if (mod.fd.TypeImportFile === '') {
        toastr.error('Debes Seleccionar un tipo de Importación');
        return false;
    }
    if (results && results.errors) {
        if (results.errors) {
            errorCount = results.errors.length;
            firstError = results.errors[0];
            if (errorCount > 0) {
                toastr.error('Ha ocurrido un error por favor verifica el csv he intentalo de nuevo');
                vm.clear();
                return false;
            }
        }
        if (results.data && results.data.length < 2) {
            rowCount = results.data.length;
            toastr.error('Csv Vacío por favor verifiquelo');
            vm.clear();
            return false;
        } else {
            rowCount = results.data.length;

            var cant, errors = 0;
            switch (mod.fd.TypeImportFile) {
                case 2:
                    cant = 5;
                    break;
                case 3:
                    cant = 5;
                    break;
                case 4:
                    cant = 4;
                    break;
                case 5:
                    cant = 5;
                    break;
                case 6:
                    cant = 9;
                    break;
            }
            $.each(results.data, function (i, v) {
                console.log(v);
                if (v.length !== cant) {
                    if (v.length > cant ) {
                        toastr.error('Existen lineas con datos sobrantes, por favor verifica el csv e intentalo de nuevo');
                        errors++;
                        return false;
                    }
                    if(v.length < cant && i !== rowCount){
                        toastr.error('Existen lineas con datos faltantes, por favor verifica el csv e intentalo de nuevo');
                        errors++;
                        return false;
                    }
                }
            });
            if(errors > 0){
                return false;
            }
            mod.fd.Verified = true;
            toastr.success('Csv Verificado, puedes continuar');
        }
    } else {
        vm.clear();
        toastr.error('Ha ocurrido un error por favor verifica el csv he intentalo de nuevo');
        return false;
    }
}

function importFn(results) {
    if (mod.fd.TypeImportFile === '') {
        toastr.error('Debes Seleccionar un tipo de Importación');
        return false;
    }
    if (results && results.errors) {
        if (results.errors) {
            errorCount = results.errors.length;
            firstError = results.errors[0];
        }
        if (results.data && results.data.length > 0)
            rowCount = results.data.length;
    }
    mod.fd.DataPost = JSON.stringify(results.data);
    mod.fd.CsvJson = results.data;
    data = {
        data: mod.fd.DataPost,
        type: mod.fd.TypeImportFile
    };
    vm.importdata(data);
}
