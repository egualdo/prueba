Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');

//FUNCIONES GENERICAS PARA INICIO DE LA PAGINA



//FUNCIONES EN LA MODAL DE EDICION
var mod = new Vue({
  el: '#vuedata',
  data: {
    globalurl:'../api/v1/usercompany',

    fd:
    {
        Idn:'',
        Name:'',
        Idnuser:'',
        
    },
    ff:
    {
      save:false,
      edit:true,
    }    
    
  },
  methods: {
    
  }
 
});

   //======INICIO DE FUNCIONES ============
    function CargarTabla()
    {   
      var table = $("#TablaCompanyforuser").DataTable({
        //COMPROBACION PARA PINTAR Y CAMBIAR TEXTO DE TABLA


        "destroy": true,
        "scrollY":        "200px",
        "scrollX":        "1000px",
        "language": {
            "url": "/../Spanish.json"
        },
        //Especificaciones de las Columnas que vienen y deben mostrarse
        "columns" : [
            { data : 'idncompany' },
            { data : 'codcomp'},
            { data : 'company'},
            { data : 'cuitcomp'},
            { data : 'typecompany'},
            { data : 'cctcomp'}
                
           
        ],
        //Especificaciones de la URL del servicio
        "ajax": {
          // url: '../api/v1/companyforuser/'+ mod.fd.Idnuser,
            url: '../api/v1/companyforuser/',
            dataSrc : ''
        }

    });

            $('#TablaCompanyforuser tbody').on( 'click', 'tr', function () {
        
        //OBTENGO LOS VARLOES DE LA TABLA
        mod.fd.Idn = table.row( this ).data().idncompany;
        mod.fd.Name = table.row( this ).data().company;
       
      
//ABRO LA MODAL
          swal({
                title: "¿Desea cambiar de entorno a esta empresa?",
                text: "Ahora verá toda la información de la empresa "+mod.fd.Name,
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Cambiar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {
                    //DATOS DE FORMULARIO
                    Idncompany:mod.fd.Idn,
                  
                                      
                  };
                     Vue.http.post("../api/v1/changueenviroment",arr).then(response => {
                    // Si todo sale correcto   
                                          
                      
                           swal('Cambio de Entorno','Ahora vera los datos de la empresa seleccionada','success')
                          setInterval(function(){
                            window.location.reload();
                          },1500);
                          
                
                                         
                    }, response => {                    
                            
                           swal('Cambio de Entorno','No hay comunicación correcta con el servidor','error');
                        }); 
                        
                    });
                    //FIN VUE POST


       });

     }



 var vm = new Vue({
  methods: 
       {
                cargartabla:function()
        {
          CargarTabla();
        }

        
      }
    });

vm.cargartabla();