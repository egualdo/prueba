Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');

//FUNCIONES GENERICAS PARA INICIO DE LA PAGINA
 var vm = new Vue({
  methods: 
      {
       
        cargartabla:function()
        {
          CargarTabla();
        }
      }
    });

vm.cargartabla();


    function CargarTabla()
    {
     var table = $("#TablaMaster").DataTable({
         
        // "destroy": true,
        "scrollY":        "200px",
        "scrollX":        "1000px",
          "language": {
            "url": "/../Spanish.json"
        },
       //Especificaciones de las Columnas que vienen y deben mostrarse
       "columns" : [
       { data : 'idn' },
       { data : 'username' },
       { data : 'accion' },
       { data : 'window' },
           { data : 'created_at' },
           { data : 'updated_at' },
       ],    
       //Especificaciones de la URL del servicio
       "ajax": {
        url: "../api/v1/audit",
        dataSrc : ''
      }
    });  
       
 }

     