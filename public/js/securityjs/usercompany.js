Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');

//FUNCIONES GENERICAS PARA INICIO DE LA PAGINA

//FUNCIONES EN LA MODAL DE EDICION
var mod = new Vue({
  el: '#vuedata',
  data: {
    globalurl:'../api/v1/usercompany',
    Useroptions:'',
    Companyoptions:[],
    fd:
    {
      Idn:'',
      User:'',
      Company:'',
      Asignation:'1',
    },
    ff:
    {
      save:false,
      edit:true
    }    
    
  },
  methods: {
    guardar: function (event) 
    {
       
        

          if(mod.fd.Asignation === '2' || mod.fd.Asignation === 2)
          {
            var items = [mod.fd.User];
            var strings = ['Usuario'];
            var checked = CheckNulls(items, strings);
            if(checked !== 0){            
            swal({
              title: "Confirmación de guardado masivo",
              text: "¿Estas seguro de guardar esta asignación masivamente?",
              type: "warning",
              showCancelButton: true,
              confirmButtonText: 'Aceptar',
              cancelButtonText: 'Cancelar'
          }).then(function () {
              var arr = {
                  //DATOS DE FORMULARIO
                  User:mod.fd.User
                };
                 Vue.http.post(mod.globalurl+"/savemasive",arr).then(response => {
                  // Si todo sale correcto              
                  swal('Guardado correctamente','','success'); 
                  $('#ModalEdicion').modal('hide');
                  mod.cleanform();

                }, response => {
                  if (response.status === 409)
                  {
                    swal('Elemento ya existente',response.body,'error')
                  }
                  if (response.status === 500)
                  {
                   swal('Error interno',response.body,'error')
                 }
                 if (response.status === 404)
                 {
                  swal('No encontrado','No hay comunicación correcta con el servidor','error')
                }
                });               
                     
                  })
          }
        }
          else
          {
            var items = [mod.fd.Company,mod.fd.User];
        var strings = ['Empresa', 'Usuario'];
        var checked = CheckNulls(items, strings);
        if(checked !== 0){
            swal({
              title: "Confirmación de guardado",
              text: "¿Estas seguro de guardar esta asignación?",
              type: "warning",
              showCancelButton: true,
              confirmButtonText: 'Aceptar',
              cancelButtonText: 'Cancelar'
          }).then(function () {
              var arr = {
                  //DATOS DE FORMULARIO
                  User:mod.fd.User,
                  Company:mod.fd.Company
                };

                 Vue.http.post(mod.globalurl+"/save",arr).then(response => {
                  // Si todo sale correcto              
                  swal('Guardado correctamente','','success'); 
                  $('#ModalEdicion').modal('hide');
                  mod.cleanform();

                }, response => {
                  if (response.status === 409)
                  {
                    swal('Elemento ya existente',response.body,'error')
                  }
                  if (response.status === 500)
                  {
                   swal('Error interno',response.body,'error')
                 }
                 if (response.status === 404)
                 {
                  swal('No encontrado','No hay comunicación correcta con el servidor','error')
                }
                });               
                     
                  })
          }
        }
      
                    //FIN VUE POST
           
       
     
    },
    editar: function (event) 
    {
        var items = [mod.fd.Company,mod.fd.User];
        var strings = ['Empresa', 'Usuario'];
        var checked = CheckNulls(items, strings);
        if(checked !== 0){
        swal({
                title: "Confirmación de editado",
                text: "Estas seguro de editar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


              var arr = {
                    //DATOS DE FORMULARIO
                    User:mod.fd.User,
                    Company:mod.fd.Company          
                  };
                  //VUE POST
                Vue.http.put(mod.globalurl+"/update/"+mod.fd.Idn,arr).then(response => {
                    // Si todo sale correcto   

                    swal('Modificado Correctamente','','success');
                    $('#ModalEdicion').modal('hide');
                    if (response.status === 409)
                    {
                      swal('La asignacion ya existe','','error')
                    }
                    mod.cleanform();

                    },
                    response => {
                    // Si todo sale correcto              
                    swal('Ya existen ese asignación','','success');
                    $('#ModalEdicion').modal('hide');

                    }, response => {
                      //Si no sale correcto
                            swal('Ocurrio un error al modificar','','error')
                            mod.cleanform();
                           
                        });
                        
                    })
                    //FIN VUE POST
         }

    },
    borrar: function (event)
    {
       swal({
                title: "Confirmar Eliminación",
                text: "Estas seguro de eliminar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {
                    //DATOS DE FORMULARIO
                    User:mod.fd.User,
                    Company:mod.fd.Company        
                  };
                Vue.http.put(mod.globalurl+"/delete/"+mod.fd.Idn,arr).then(response => {
                    // success callback
                    swal('Eliminado Correctamente','','success');
                $('#ModalEdicion').modal('hide');
                mod.cleanform();

               

            }, response => {
                    swal('Ocurrio un error al eliminar','','error');
                    // error callback
                });
            })
    },
    deleteasignations:function(event)
    {
      swal({
        title: "Confirmar Eliminación",
        text: "¿Estas seguro de eliminar todas las asignaciones para este usuario?",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: 'Aceptar',
        cancelButtonText: 'Cancelar'
    }).then(function () {
        var arr = {
            //DATOS DE FORMULARIO
            User:mod.fd.User               
          };
        Vue.http.post(mod.globalurl+"/deletemasive",arr).then(response => {
            // success callback
            swal('Eliminado Correctamente','','success');
        $('#ModalEdicion').modal('hide');
        mod.cleanform();

       

    }, response => {
            swal('Ocurrio un error al eliminar','','error');
            // error callback
        });
    })
    },
    cleanform: function()
    {
      mod.fd.Idn='';
      mod.fd.User=1;
      mod.fd.Concept=1;
      mod.fd.Asignation='1';
     // mod.fd.Cct="";
       $('#ModalEdicion').modal('hide');
      var table = $('#TablaUserCompany').dataTable();
      //RECARGA LOS DATOS DE LA TABLA
     table.fnReloadAjax();
     
    },
 
     openmodaltonew:function()
        {
          mod.cleanform();
          $('#ModalEdicion').modal('toggle');
          //OCULTAR BOTONES O MOSTRAR
          mod.ff.save = true;
          mod.ff.edit = false;
        },
        openmodaltoedit:function()
        {
           $('#ModalEdicion').modal('toggle');
          //OCULTAR BOTONES O MOSTRAR
          mod.ff.save = false;
          mod.ff.edit = true;
        },
        openmodaltodelete:function()
        {
           $('#ModalDelete').modal('toggle');
          //OCULTAR BOTONES O MOSTRAR
          mod.ff.save = false;
          mod.ff.edit = true;
        }
  }
 
});

   //======INICIO DE FUNCIONES ============
    function CargarTabla()
    {   
      var table = $("#TablaUserCompany").DataTable({
        //COMPROBACION PARA PINTAR Y CAMBIAR TEXTO DE TABLA


        "destroy": true,
        "scrollY":        "200px",
        "scrollX":        "1000px",
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        //Especificaciones de las Columnas que vienen y deben mostrarse
        "columns" : [
            { data : 'idn' },
            { data : 'user'},
            { data : 'company'},

            
                
           
        ],
        //Especificaciones de la URL del servicio
        "ajax": {
             url: '../api/v1/usercompany/',
            dataSrc : ''
        }

    });


      $('#TablaUserCompany tbody').on( 'click', 'tr', function () {
        
        //OBTENGO LOS VARLOES DE LA TABLA
        mod.fd.Idn = table.row( this ).data().idn;
        mod.fd.User = table.row( this ).data().idnuser;
        mod.fd.Company = checkIdn(mod.Companyoptions,parseInt(table.row( this ).data().idncompany)); 
      
      //ABRO LA MODAL
        mod.openmodaltoedit();

       });

     } 
function checkIdn(a,idn) {
  for (i = 0; i < a.length; ++i) {
   
     if(idn === a[i].id)
     {
      return a[i];
     }
  }
}

 var vm = new Vue({
  methods: 
      {
        cargartabla:function()
        {
          CargarTabla();
        },
         cargarcombouser:function()
        {
                   // GET /someUrl
            this.$http.get('../api/v1/userall').then(response => {

            // get body data
            mod.Useroptions = response.body;

          }, response => {
            // error callback
          });
        },
         cargarcombocompany:function()
        {
                   // GET /someUrl
            this.$http.get('../api/v1/company').then(response => {

            // get body data
           // mod.Companyoptions = response.body;
             $.each(response.body,function(i,v)
             { 
            mod.Companyoptions.push({id:v.idn,label:v.name});
              });
          }, response => {
            // error callback
          });
        },
        /* cargardatos:function()
        {
                   // GET /someUrl
            this.$http.get('../api/v1/ccts/'+ mod.fd.Idncct).then(response => {

            // get body data
            mod.fd.Idncct = response.body[0].idn;
          }, response => {
            // error callback
          });
        }*/


      }
    });
vm.cargarcombouser();
vm.cargarcombocompany();
//vm.cargardatos();
  var vm = new Vue({
  methods: 
      {
       /* loadcct:function()
        {
       var query = window.location;
    var qs = getUrlParams(query);
    console.log("PARAMETROS",qs.id);
    mod.fd.Idncct=qs.id;


        },*/
        cargartabla:function()
        {
          CargarTabla();
        }
      }
    });
//vm.loadcct();

vm.cargartabla();


