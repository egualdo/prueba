var combo = new Vue({
  methods:
  {
    loadRoster:function()
    {
      this.$http.get('../api/v1/roster').then(response => {
        // get body data
        run.fd.Rosterlist = response.body;
      }, response => {  // error callback
      });
    },
    loadPayMethodPayrun:function()
    {
      this.$http.get('../api/v1/paymethodpayrun').then(response => {
        // get body data
        run.fd.Paymentmethodlist = response.body;
        run.fd.Paymentmethod = response.body[0].idn;
      }, response => {  // error callback
      });
    },
      loadRunType:function()
      {
          this.$http.get('../api/v1/runtype').then(function(response){
              $.each(response.body,function(i,v)
              {
                  run.fd.Typerunlist.push({id:v.idn,label:v.name});
              });
          });
      },
    loadPeriodByType:function(idnperiod)
    {
      this.$http.get('../api/v1/periodbytype/'+idnperiod).then(response => {
        // get body data
        run.fd.Periodlist = response.body;
        run.fd.Period = response.body[0].idn;
      }, response => {  // error callback
      });
    }
  }
});


var run = new Vue({
  el: '#vue-Payrunall',
  data: {
    globalurl:'../api/v1/payrunhead',
    fd:
    {
      Idn:'',
      Month:'Diciembre',
      Period:'1',
      Periodlist:'',
      Typeperiod:'1',
      Paymentmethod:'',
      Paymentmethodlist:'',
      PaymentRundate:'05-05-2017',
      PayContributiondate:'04-08-2017',
      Roster:'',
      Rosterlist:'',
      Typerun:'Default',
      Typerunlist:[],
        Payrunbodylist:[],
      HeadRoster:'',
      HeadSalary:'',
      HeadStartdate:'',
      HeadSitrevision:'',
      HeadModcontract:'',
      Headcct:'',
      HeadName:'',
      HeadCod:'',
      IdnEmployee:'',
      ColTotPays:0,
      ColTotNorem:0,
      ColTotDeduc:0,
      ColTotExen:0,
      NetPay:0,
        EmployeeToRun:''
    },
    ff:
    {
      save:false,
      edit:true,
      openPayruns:true,
      closedPayruns:false,
      close:false,
      reopen:false,
      delete:false
    }
  },
  mounted() {

    //Formato del Datepicker
    $('#DPickerPaymentdate').datepicker({
      format: 'dd-mm-yyyy',
      autoclose:true,
      language: 'es'
    });
    //Obtener Datos del Datepicker
    $("#DPickerPaymentdate").datepicker().on(

      "changeDate", () => {
        //this.startDate = $('#DPickerBirthdate').val();
        run.fd.PaymentRundate = $('#DPickerPaymentdate').val();
      });
      //////
      $('#DPickerPayContributiondate').datepicker({
        format: 'dd-mm-yyyy',
        autoclose:true,
        language: 'es'
      });
      //Obtener Datos del Datepicker
      $("#DPickerPayContributiondate").datepicker().on(

        "changeDate", () => {
          //this.startDate = $('#DPickerBirthdate').val();
          run.fd.PayContributiondate = $('#DPickerPayContributiondate').val();
        });

      },
      methods: {
      loadMonth:function()
      {
        var val;
        run.fd.Periodlist.forEach(function(perio) {         
          if (perio.idn === run.fd.Period)
          {            
            val = perio.month;
          }
        });
       
       // empRun.fd.Period = val;
        if (val === 1)
        {
          run.fd.Month = 'Diciembre';          
        }
        if (val === 2)
        {
          run.fd.Month = 'Enero';          
        }
        if (val === 3)
        {
          run.fd.Month = 'Febrero';          
        }
        if (val === 4)
        {
          run.fd.Month = 'Marzo';          
        }
        if (val === 5)
        {
          run.fd.Month = 'Abril';          
        }
        if (val === 6)
        {
          run.fd.Month = 'Mayo';          
        }
        if (val === 7)
        {
          run.fd.Month = 'Junio';          
        }
        if (val === 8)
        {
          run.fd.Month = 'Julio';          
        }
        if (val === 9)
        {
          run.fd.Month = 'Agosto';          
        }
        if (val === 10)
        {
          run.fd.Month = 'Septiembre';          
        }
        if (val === 11)
        {
          run.fd.Month = 'Octubre';          
        }
        if (val === 12)
        {
          run.fd.Month = 'Noviembre';          
        }
 
      },
          gestionRun: function(type){
              GestionRun(type);
          },
          openModalDelete:function () {
              run.ff.close = false;
              run.ff.reopen = false;
              run.ff.delete = true;
              $('#ModalGestionRun').modal('show');
          },
          openModalClose:function () {
              run.ff.close = true;
              run.ff.reopen = false;
              run.ff.delete = false;
              $('#ModalGestionRun').modal('show');
          },
          openModalReopen:function () {
              run.ff.close = false;
              run.ff.reopen = true;
              run.ff.delete = false;
              $('#ModalGestionRun').modal('show');
          },
        showClosed:function()
        {
          run.ff.openPayruns = false;
          run.ff.closedPayruns = true;
        },
        showOpen:function()
        {
          run.ff.openPayruns = true;
          run.ff.closedPayruns = false;
        },
        suma:function()
        {
          //Obtengo la lista de montos
          var amountList = run.fd.Payrunbodylist;
          var totpays = 0;
          var totnorem = 0;
          var totdeduc = 0;
          var totexen = 0;

          //recorro el objeto para sumar los totales
          for (var i in amountList) {
            var amountConcept = amountList[i].amount;
            var columnConcept = amountList[i].columnconcept;

            if (columnConcept === 1)
            {
              totpays =totpays+ amountConcept;
            }
            if (columnConcept === 2)
            {
              totnorem =totnorem+ amountConcept;
            }
            if (columnConcept === 3)
            {

              totdeduc =totdeduc+ amountConcept;
            }
            if (columnConcept === 4)
            {
              totexen =totexen+ amountConcept;
            }
            /*   alert(objeto[i].amount);
            console.log(objeto[i]);
            run.fd.Sumtotal =+
            ColTotPagos:'',
            ColTotNorem:'',
            ColTotDeduc:'',
            ColTotExen:''*/
          }

          var totpays = parseFloat(totpays.toFixed(2));
          var totnorem = parseFloat(totnorem.toFixed(2));
          var totdeduc = parseFloat(totdeduc.toFixed(2));
          var totexen = parseFloat(totexen.toFixed(2));
          run.fd.ColTotPays = totpays;
          run.fd.ColTotNorem = totnorem;
          run.fd.ColTotDeduc = totdeduc;
          run.fd.ColTotExen = totexen;

          run.fd.NetPay = (totpays + totnorem - totdeduc + totexen).toFixed(2);
        },
        remakepayrun:function(IdnEmployee)
        {
          swal({
            title: "Atención",
            text: "¿Estas seguro de correr la nomina",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: 'Correr',
            cancelButtonText: 'Cancelar'
          }).then(function () {
            var arr = {
              //DATOS DE FORMULARIO
              Period:run.fd.Period,
              Employee:run.fd.IdnEmployee,
              Roster:run.fd.Roster,
              Paymentmethod:run.fd.Paymentmethod,
              Paydate:FormatDateReverse(run.fd.PaymentRundate),
              Paycontributiondate:FormatDateReverse(run.fd.PayContributiondate),
              Typerun:run.fd.Typerun,
              idnpayrun:run.fd.Idn
            };
            //VUE POST
            Vue.http.post('../api/v1/payrunbody/remake',arr).then(response => {
              // Si todo sale correcto
              swal('Corrida almacenada correctamente','','success');
              run.GetPayrunBody();
              run.cleanform();
            }, response => {
              if (response.status === 409)
              {
                swal('Elemento ya existente','Ya existe una corrida con estos parametros','error')
              }
              if (response.status === 500)
              {
                swal('Error interno',response.text,'error')
              }
              if (response.status === 404)
              {
                swal('No encontrado','No hay comunicación correcta con el servidor','error')
              }
              //Si no sale correcto


            });

          })
          //FIN VUE POST
        },
        closepayrun:function()
        {
          swal({
            title: "Atención",
            text: "¿Estas seguro de cerrar la corrida de "+empDP.fd.Firstname+" "+empDP.fd.Lastname+"?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: 'Cerrar Corrida',
            cancelButtonText: 'Cancelar'
          }).then(function () {
            //VUE POST
            Vue.http.put('../api/v1/payrunhead/close/'+run.fd.Idn).then(response => {
              // Si todo sale correcto
              swal('Corrida cerrada correctamente','','success');
              run.cleanform();
              $('#ModalPayrunDetails').modal('toggle');

            }, response => {
              if (response.status === 409)
              {
                swal('Elemento ya existente','Ya existe una corrida con estos parametros','error')
              }
              if (response.status === 500)
              {
                swal('Error interno','Ocurrio un error interno','error')
              }
              if (response.status === 404)
              {
                swal('No encontrado','No hay comunicación correcta con el servidor','error')
              }
              //Si no sale correcto


            });

          })
        },
        openpayrun:function()
        {
          swal({
            title: "Cuidado",
            text: "¿Estas seguro de reabrir la corrida de "+empDP.fd.Firstname+" "+empDP.fd.Lastname+"?, si reabre una corrida puede modificar datos históricos",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: 'Estoy seguro',
            cancelButtonText: 'Cancelar'
          }).then(function () {
            //VUE POST
            Vue.http.put('../api/v1/payrunhead/open/'+run.fd.Idn).then(response => {
              // Si todo sale correcto
              swal('Corrida abierta correctamente','','success');
              run.cleanform();
              $('#ModalPayrunDetails').modal('toggle');
            }, response => {
              if (response.status === 409)
              {
                swal('Elemento ya existente','Ya existe una corrida con estos parametros','error')
              }
              if (response.status === 500)
              {
                swal('Error interno','Ocurrio un error interno','error')
              }
              if (response.status === 404)
              {
                swal('No encontrado','No hay comunicación correcta con el servidor','error')
              }
              //Si no sale correcto


            });

          })
        },
        downloadreceive:function()
        {
          swal({
            title: "Confirmación",
            text: "¿Desea descargar el recibo de "+empDP.fd.Firstname+" "+empDP.fd.Lastname+"?, la información será mostrada en un documento PDF",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: 'Estoy seguro',
            cancelButtonText: 'Cancelar'
          }).then(function () {                               //VUE POST

            // Si todo sale correcto
            var arr = {
              Employee:run.fd.IdnEmployee,
              PayrunBody:run.fd.Idn
            };
            //VUE POST
            Vue.http.post(run.globalurl+"/validateDownloadPDF",arr).then(response => {
              // Si todo sale correcto
              var Token = response.body;

              window.open('../exportPDF_process/'+Token, '_blank');
            }, response => {
              if (response.status === 409)
              {
                swal('Elemento ya existente','Ya existe una corrida con estos parametros','error')
              }
              if (response.status === 500)
              {
                swal('Error interno','Ocurrio un error interno','error')
              }
              if (response.status === 404)
              {
                swal('No encontrado','No hay comunicación correcta con el servidor','error')
              }
              //Si no sale correcto


            });



          })
        },
        TypePeriodOnChange: function()
        {
          combo.loadPeriodByType(run.fd.Typeperiod);
          run.loadMonth();
        },
        RunComplete:function()
        {
          if(run.fd.Roster == 0)
          {
            toastr.warning('Por favor indica una Nomina');
            return false;
          }
          if(run.fd.Period == 0)
          {
            toastr.warning('Por favor indica una Periodo');
            return false;
          }
          if(run.fd.Month == 0)
          {
            toastr.warning('Por favor indica una Mes');
            return false;
          }
          if(run.fd.PaymentRundate == 0 || run.fd.PaymentRundate === null)
          {
            toastr.warning('Por favor indica una fecha de Pago');
            return false;
          }
          if(run.fd.PayContributiondate === '' || run.fd.PayContributiondate === null)
          {
            toastr.warning('Por favor indica una fecha de Pago de Contribucion');
            return false;
          }
          if(run.fd.Paymentmethod == 0)
          {
            toastr.warning('Por favor indica un metodo de Pago');
            return false;
          }
          swal({
            title: "Atención",
            text: "¿Estas seguro de correr la nómina para multiples legajos",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: 'Correr',
            cancelButtonText: 'Cancelar'
          }).then(function () {

              run.GetEmployeesByRoster(run.fd.Roster);

              });
        },
          GetEmployeesByRoster:function(IdnRoster)
          {
              this.$http.get('../api/v1/employee/roster/'+IdnRoster).then(response => {

                  // get body data
                  var EmployeeToRun = response.body;
              run.ProcessPayrunMultiple(EmployeeToRun);

          }, response => {
              // error callback
          });
          },
          ProcessPayrunMultiple:function(EmployeeToRun)
          {


              var emprun;
              console.log(EmployeeToRun);
              NProgress.start();

              for (var i = 0; i < EmployeeToRun.length; i++) {
                  console.log(EmployeeToRun[i].idn);
                  var arr = {
                      //DATOS DE FORMULARIO
                      Period:run.fd.Period,
                      Employee:EmployeeToRun[i].idn,
                      Roster:run.fd.Roster,
                      Paymentmethod:run.fd.Paymentmethod,
                      Paydate:FormatDateReverse(run.fd.PaymentRundate),
                      Paycontributiondate:FormatDateReverse(run.fd.PayContributiondate),
                      Typerun:1,
                      TypeExecution:1
                  };
                  //VUE POST
                  Vue.http.post('../api/v1/payrun/process',arr).then(response => {
                     // toastr.info('Corrida Procesada:'+i)
                  }, response => {

                  });
              }
              NProgress.done();
              swal('Corridas Procesadas correctamente','','success');
              run.cleanform();



          },
        RunIndividual: function()
        {
          var arr = {
            //DATOS DE FORMULARIO
            Period:run.fd.Period,
            Employee:run.fd.IdnEmployee,
            Roster:run.fd.Roster,
            Paymentmethod:run.fd.Paymentmethod,
            Paydate:FormatDateReverse(run.fd.PaymentRundate),
            Paycontributiondate:FormatDateReverse(run.fd.PayContributiondate),
            Typerun:run.fd.Typerun
          };
          //VUE POST
          Vue.http.post(run.globalurl+"/save",arr).then(response => {
            // Si todo sale correcto
            swal('Corrida almacenada correctamente','','success');
            run.GetPayrunBody();
            run.cleanform();;
            $('#ModalPayrunDetails').modal('toggle');
          }, response => {
            if (response.status === 409)
            {
              swal('Elemento ya existente','Ya existe una corrida con estos parametros','error')
            }
            if (response.status === 500)
            {
              swal('Error interno','Ocurrio un error interno','error')
            }
            if (response.status === 404)
            {
              swal('No encontrado','No hay comunicación correcta con el servidor','error')
            }
            //Si no sale correcto


          });


          //FIN VUE POST
        },

          GetPayrunSicoss:function(Type)
          {

              //GetSicoss($IdnEmployee,$IdnPeriod,$IdnTypeRun,$IdnCompany)
              // GET /someUrl
              this.$http.get('../api/v1/sicoss/employee/'+rundetail.fd.DetEmployee+'/'+rundetail.fd.DetPeriod+'/'+rundetail.fd.DetTyperun+'/'+Type).then(response => {

                  // get body data
                  rundetail.fd.Sicossdata = response.body[0];


          }, response => {
              // error callback
          });
          },
        GetPayrunBody:function()
        {
          // GET /someUrl
          this.$http.get('../api/v1/payrunbodybyid/'+run.fd.Idn).then(response => {

            // get body data
            run.fd.Payrunbodylist = response.body;
            run.suma();

          }, response => {
            // error callback
          });
        },
        cleanform:function()
        {
          run.fd.Month='';
          run.fd.Period='';

          run.fd.Paymentmethod='';

          run.fd.PaymentRundate='';
          run.fd.PayContributiondate='';
            var table = $('#TablePayrunOpen').dataTable();
            var table2 = $('#TablePayrunClosed').dataTable();
            //RECARGA LOS DATOS DE LA TABLA
            table.fnReloadAjax();
            table2.fnReloadAjax();
        },
        openpayrundetail:function()
        {

          $('#ModalPayrunDetails').modal('toggle');
          //OCULTAR BOTONES O MOSTRAR
          run.GetPayrunBody();
            run.GetPayrunSicoss(1);

        }

      }

    });

    function LoadTablePayrunOpened()
    {

      var table = $("#TablePayrunOpen").DataTable({
            //COMPROBACION PARA PINTAR Y CAMBIAR TEXTO DE TABLA
            "destroy": true,
            "scrollY":        "400px",
            "scrollX": "1100px",

            "language": {
                "url": "../Spanish.json"
            },
            //Especificaciones de las Columnas que vienen y deben mostrarse
            "columns" : [
            { data : 'nameemployee' },
            { data : 'nametyperun'},
            { data : 'nameroster'},
            { data : 'titleperiod'},
            { data : 'payments' },
            { data : 'unremuns' },
            { data : 'deductions' },
            { data : 'totalpay' }


            ],
            //Especificaciones de la URL del servicio
            "ajax": {
                url: '../api/v1/employee/payrunheadopen',
                dataSrc : ''
            }
        });
      $('#TablePayrunOpen tbody').on( 'click', 'tr', function () {
          var row = table.row( this ).data();
          rundetail.fd.Idn = row.idn;
          rundetail.fd.DetPayrunHead = row.idn;
          rundetail.fd.DetTyperun = row.idntyperun;
          rundetail.fd.Period = row.idnperiod;
          rundetail.fd.DetNamePeriod = row.titleperiod;
          rundetail.fd.DetNameTyperun = row.nametyperun;
          rundetail.fd.Employee = row.idnemployee;
          rundetail.fd.Roster = row.idnroster;
          rundetail.fd.Paymentmethod = row.idnpaymentmethod;
          rundetail.fd.Paydate = row.paydate;
          rundetail.fd.PayContributiondate = row.paycontributiondate;

              rundetail.fd.DetPayrunHead= row.idn;
              rundetail.fd.DetPeriod = row.idnperiod;
              rundetail.fd.DetEmployee = row.idnemployee;
              rundetail.fd.DetRoster = row.idnroster;
              rundetail.fd.DetTyperun = row.idntyperun;
              rundetail.fd.DetPaymentmethod = row.idnpaymentmethod;
              rundetail.fd.DetPayContributiondate = row.paycontributiondate;
         // console.log(rundetail.fd);
          rundetail.openpayrundetail();
           rundetail.ff.openPayruns = true;
          rundetail.ff.closedPayruns = false;
        });

    }
    function LoadTablePayrunClosed()
    {

      var table = $("#TablePayrunClosed").DataTable({
            //COMPROBACION PARA PINTAR Y CAMBIAR TEXTO DE TABLA
            "destroy": true,
            "scrollY":        "400px",
            "scrollX": "1100px",

            "language": {
                "url": "../Spanish.json"
            },
            //Especificaciones de las Columnas que vienen y deben mostrarse
            "columns" : [
            { data : 'nameemployee' },
            { data : 'nametyperun'},
            { data : 'nameroster'},
            { data : 'titleperiod'},
            { data : 'payments' },
            { data : 'unremuns' },
            { data : 'deductions' },
            { data : 'totalpay' }


            ],
            //Especificaciones de la URL del servicio
            "ajax": {
                url: '../api/v1/employee/payrunheadclosed',
                dataSrc : ''
            }
        });
      $('#TablePayrunClosed tbody').on( 'click', 'tr', function () {
          var row = table.row( this ).data();
          rundetail.fd.Idn = row.idn;
          rundetail.fd.DetPayrunHead = row.idn;
          rundetail.fd.DetTyperun = row.idntyperun;
          rundetail.fd.Period = row.idnperiod;
          rundetail.fd.Employee = row.idnemployee;
          rundetail.fd.Roster = row.idnroster;
          rundetail.fd.Paymentmethod = row.idnpaymentmethod;
          rundetail.fd.Paydate = row.paydate;
          rundetail.fd.PayContributiondate = row.paycontributiondate;

          rundetail.fd.DetPayrunHead= row.idn;
          rundetail.fd.DetPeriod = row.idnperiod;
          rundetail.fd.DetEmployee = row.idnemployee;
          rundetail.fd.DetRoster = row.idnroster;
          rundetail.fd.DetTyperun = row.idntyperun;
          rundetail.fd.DetPaymentmethod = row.idnpaymentmethod;
          rundetail.fd.DetPayContributiondate = row.paycontributiondate;
          rundetail.ff.openPayruns = false;
          rundetail.ff.closedPayruns = true;
          rundetail.openpayrundetail();
      });

    }
    function GestionRun(type){
    if(run.fd.Typerun === ''){
        swal({
            title: "Atención",
            text: "Por favor seleccione un tipo de corrida",
            type: "warning"});
    }
    if(run.fd.Period === ''){
        swal({
            title: "Atención",
            text: "Por favor seleccione un Periodo",
            type: "warning"});
    }
        $('.loader').show();
       var url = '../api/v1/payrunmasive/'+type+'?typerun='+run.fd.Typerun+'&period='+run.fd.Period;
        swal({
            title: "Confirmación",
            text: "¿Esta seguro de Ejecutar esta acción?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: 'Estoy seguro',
            cancelButtonText: 'Cancelar'
        }).then(function () {
            $.ajax({
                url:url,
                success:function(data){
                    $('.loader').hide();
                if(parseInt(data) > 0){
                    if(type === 'delete'){
                        swal('Corridas Borradas correctamente','','success');
                    }else if(type === 'close'){
                        swal('Corrida Cerradas correctamente','','success');
                    }else{
                        swal('Corrida Reabiertas correctamente','','success');
                    }
                }else{
                    swal('No se Procesaron registros con este criterio','','error');
                }
                    $('#ModalGestionRun').modal('hide');
                    var table = $('#TablePayrunOpen').dataTable();
                    var table2 = $('#TablePayrunClosed').dataTable();

                    table.fnReloadAjax();
                    table2.fnReloadAjax();
                },error:function(){
                    $('.loader').hide();
                    swal('Ha Ocurrido un error','','error');
                }
            });
        });

    }
    LoadTablePayrunOpened();
    LoadTablePayrunClosed();

    combo.loadRoster();
    combo.loadPayMethodPayrun();
    combo.loadPeriodByType(run.fd.Typeperiod);
    combo.loadRunType();

