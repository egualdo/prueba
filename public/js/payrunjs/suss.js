var combo = new Vue({
    methods: {
        loadRoster: function () {
            this.$http.get('../api/v1/roster').then(function (response) {
                run.fd.Rosterlist = response.body;
            });
        },
        loadPeriodByType: function (idnperiod) {
            this.$http.get('../api/v1/periodbytype/' + idnperiod).then(function (response) {
                run.fd.Periodlist = response.body;
                run.fd.Period = response.body[0].idn;
            });
        }
    }
});
var run = new Vue({
    el: '#vuedata',
    data: {
        globalurl: '../api/v1/payrunhead',
        fd: {
            Period: '1',
            Periodlist: '',
            Typeperiod: '1',
            Typereport: '1',
            Roster: '1',
            Rosterlist: '',
            Range: ''
        }
    },
    methods: {
        TypePeriodOnChange: function () {
            combo.loadPeriodByType(run.fd.Typeperiod);
        },
        DownloadReport: function () {
            this.$http.get('/check/excel/payruns?type=' + run.fd.Typereport + '&range=' + run.fd.Range + '&period=' + run.fd.Period+'&roster='+run.fd.Roster).then(function (response) {
                if (response.body.length > 0) {
                    window.open('/export/excel/payruns?type=' + run.fd.Typereport + '&range=' + run.fd.Range + '&period=' + run.fd.Period+'&roster='+run.fd.Roster);
                } else {
                    swal({
                        title: 'Lo sentimos',
                        text: 'la consulta no obtuno ningun resultado, no se generara el xls',
                        type: 'info'
                    });
                }
            });
        }
    }
});

combo.loadRoster();
combo.loadPeriodByType(run.fd.Typeperiod);

