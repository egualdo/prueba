Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');

//FUNCIONES GENERICAS PARA INICIO DE LA PAGINA
 var vm = new Vue({
  methods: 
      { 

          
        cargartabla:function()
        {
          CargarTabla();
        }
      }
    });



vm.cargartabla();

//FUNCIONES EN LA MODAL DE EDICION
var mod = new Vue({
  el: '#vuedata',
  data: {
    globalurl:'../api/v1/payrunhead',
    fd:
    {
      Idn:'',

      IdnPaymethod:'',
      IdnPeriod:'',
      IdnPayroll:'',
        IdnGroup:'',
        IdnEmployee:'',
        
          NamePaymethod:'',
        Paymethod:'',
        Paymethodoptions: '',
        
          NamePeriod:'',
        Period:'',
        Periodoptions: '',
    
        NamePayroll:'',
      Payroll:'',
      Payrolloptions:'',

           NameGroup:'',
      Group:'',
      Groupoptions:'',
        
        NameEmployee:'',
      Employee:'',
      Employeeoptions:'',
        
        
        
      Month:'',
      Paydate:'',
        Paydatecontrib:'',


      
    },
    ff:
    {
      save:false,
      edit:true,
    }    
    
  },
  
  methods: {
   
   
 

   


   
   
    
  }
 
});

   //======INICIO DE FUNCIONES ============
    function CargarTabla()
    {   
      var table = $("#TablaMaster").DataTable({
        //COMPROBACION PARA PINTAR Y CAMBIAR TEXTO DE TABLA


        "destroy": true,
        "scrollY":        "200px",
        "scrollX":        "1000px",
        "language": {
            "url": "/../Spanish.json"
        },
        //Especificaciones de las Columnas que vienen y deben mostrarse
        "columns" : [
            { data : 'idn' },
            { data : 'idnemployee' },
            { data : 'nameemployee' },
           // { data : 'payroll' },
            { data : 'titleperiod' },
            { data : 'payments'},
            { data : 'deductions'},
            { data : 'unremuns'},
            { data : 'exempts'},
            { data : 'status'}
           
        ],
        //Especificaciones de la URL del servicio
        "ajax": {
            url: '../api/v1/employee/payrunheadopen',
            dataSrc : ''
        }

    });

            $('#TablaMaster tbody').on( 'click', 'tr', function () {
        
        //OBTENGO LOS VARLOES DE LA TABLA
        mod.fd.Idn = table.row( this ).data().idn;
        mod.fd.IdnPeriod = table.row( this ).data().idnperiod;

        mod.fd.Payroll = table.row( this ).data().idnpayroll;
        mod.fd.Group = table.row( this ).data().idngroup;
        mod.fd.Employee = table.row( this ).data().idnemployee;
        mod.fd.Paymethod = table.row( this ).data().idnpaymethod;
       // mod.fd.Month = table.row( this ).data().month;
         //       mod.fd.Paydate = table.row( this ).data().paydate;
           //     mod.fd.Month = table.row( this ).data().month;
      
    //    idncompany=mod.fd.Company;
      //ABRO LA MODAL
      //  mod.openmodaltoedit();

       });

     } 
