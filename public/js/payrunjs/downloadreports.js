var combo = new Vue({
    methods: {
        loadTyperun: function () {
            this.$http.get('../api/v1/runtype').then(function (response) {

                $.each(response.body,function(i,v)
                {
                    dsic.fd.Typerunlist.push({id:v.idn,label:v.name});
                    dpdf.fd.Typerunlist.push({id:v.idn,label:v.name});
                    dcon.fd.Typerunlist.push({id:v.idn,label:v.name});

                });
            });
        },
        loadRoster: function () {
            this.$http.get('../api/v1/roster').then(function (response) {

                $.each(response.body,function(i,v)
                {
                    dsic.fd.Rosterlist.push({id:v.idn,label:v.name});
                    dpdf.fd.Rosterlist.push({id:v.idn,label:v.name});
                    dcon.fd.Rosterlist.push({id:v.idn,label:v.name});

                });
                repnet.fd.Rosterlist = response.body;
                book.fd.Rosterlist = response.body;
                dsus.fd.Rosterlist = response.body;
            });
        },
        loadPeriod: function (idnperiod) {
            this.$http.get('../api/v1/period').then(function (response) {
                $.each(response.body,function(i,v)
                {
                    dsic.fd.Periodlist.push({id:v.idn,label:v.title});
                    dpdf.fd.Periodlist.push({id:v.idn,label:v.title});
                    dcon.fd.Periodlist.push({id:v.idn,label:v.title});

                });
            });
        },
         loadPeriodByType: function (idnperiod) {
             this.$http.get('../api/v1/periodbytype/' + idnperiod).then(function (response) {
                 dsus.fd.Periodlist = response.body;
                 dsus.fd.Period = response.body[0].idn;
                 repnet.fd.Periodlist = response.body;
                 repnet.fd.Period = response.body[0].idn;
                 book.fd.Periodlist = response.body;
                 book.fd.Period = response.body[0].idn;
             });
         }
    }
});

var dpdf = new Vue({
    el: '#recibospdf',
    data: {
        globalurl: '../api/v1/payrunhead',
        fd: {
            Period: '',
            Periodlist: [],
            Typeperiod: '',
            Typereport: '',
            Typerun:'',
            Typerunlist:[],
            Roster: '',
            Rosterlist: [],
            Range: '',

        }
    },
    methods: {
        TypePeriodOnChange: function () {
            combo.loadPeriodByType(dpdf.fd.Typeperiod);
        },
        DownloadPDF: function (Typedownload) {
             //Items para ser chequeados si estan nulos
          

               

                if(Typedownload === 2)
                {
                    if(dpdf.fd.Period === '' || dpdf.fd.Period === null)
                    {
                        toastr.warning("Por favor indica un periodo");
                        return false;
                    }
                     if(dpdf.fd.Typerun === '' || dpdf.fd.Typerun === null)
                    {
                        toastr.warning("Por favor indica un tipo de corrida");
                        return false;
                    }
                }
                 var arr = {
                    Period:dpdf.fd.Period.id,
                    Typerun:dpdf.fd.Typerun.id,
                    Range:dpdf.fd.Range,
                    Typedownload:Typedownload
                };
           
                //VUE POST
                Vue.http.post(dpdf.globalurl+"/validateDownloadPDF",arr).then(response => {
                    // Si todo sale correcto
                    var Token = response.body;

                window.open('../exportPDF_process/'+Token, '_blank');
            }, response => {
                if (response.status === 409)
                {
                    swal('Elemento ya existente','Ya existe una corrida con estos parametros','error')
                }
                if (response.status === 500)
                {
                    swal('Error interno','Ocurrio un error interno','error')
                }
                if (response.status === 404)
                {
                    swal('No encontrado','No hay comunicación correcta con el servidor','error')
                }
                //Si no sale correcto
            });

            


        }
    }
});

var dsic = new Vue({
    el: '#txtsicoss',
    data: {
        globalurl: '../api/v1/paydsichead',
        fd: {
            Period: '',
            Periodlist: [],
            Typeperiod: '',
            Typereport: '',
            Typerun:'',
            Typerunlist:[],
            Roster: '',
            Rosterlist: [],
            Range: ''
        }
    },
    methods: {
        TypePeriodOnChange: function () {
            combo.loadPeriodByType(dsic.fd.Typeperiod);
        },
        Downloadsic: function () {
            var IdnRoster = dsic.fd.Roster.id;
            var IdnTyperun = dsic.fd.Typerun.id;
            var IdnPeriod = dsic.fd.Period.id;

                    if(dsic.fd.Period === '' || dsic.fd.Period === null)
                    {
                        toastr.warning("Por favor indica un periodo");
                        return false;
                    }
                     if(dsic.fd.Typerun === '' || dsic.fd.Typerun === null)
                    {
                        toastr.warning("Por favor indica un tipo de corrida");
                        return false;
                    }
                     if(dsic.fd.Roster === '' || dsic.fd.Roster === null)
                    {
                        toastr.warning("Por favor indica una Nomina");
                        return false;
                    }         

                window.open('../api/v1/sicoss/download/'+IdnRoster+'/'+IdnPeriod+'/'+IdnTyperun, '_blank');
            
        }
    }
});

var dsus = new Vue({
    el: '#suss',
    data: {
        globalurl: '../api/v1/payrunhead',
        fd: {
            Period: '1',
            Periodlist: '',
            Typeperiod: '1',
            Typereport: '1',
            Roster: '1',
            Rosterlist: '',
            Range: ''
        }
    },
    methods: {
        TypePeriodOnChange: function () {
            combo.loadPeriodByType(run.fd.Typeperiod);
        },
        DownloadReport: function () {
            this.$http.get('/check/excel/payruns?type=' + run.fd.Typereport + '&range=' + run.fd.Range + '&period=' + run.fd.Period+'&roster='+run.fd.Roster).then(function (response) {
                if (response.body.length > 0) {
                    window.open('/export/excel/payruns?type=' + run.fd.Typereport + '&range=' + run.fd.Range + '&period=' + run.fd.Period+'&roster='+run.fd.Roster);
                } else {
                    swal({
                        title: 'Lo sentimos',
                        text: 'la consulta no obtuno ningun resultado, no se generara el xls',
                        type: 'info'
                    });
                }
            });
        }
    }
});

var dcon = new Vue({
    el: '#controlliquidacion',
    data: {
        globalurl: '../export/excel/controlliquidation',
        fd: {
            Period: '',
            Periodlist: [],
            Typeperiod: '',
            Typereport: '',
            Typerun:'',
            Typerunlist:[],
            Roster: '',
            Rosterlist: [],
            Range: '',
            Horizontal:0
        }
    },
    methods: {
        TypePeriodOnChange: function () {
            combo.loadPeriodByType(dcon.fd.Typeperiod);
        },
        DownloadReport: function () {
                    if(dcon.Horizontal === 0)
                    {
                        window.open('/export/excel/controlliquidation?type=' + run.fd.Typereport + '&range=' + run.fd.Range + '&period=' + run.fd.Period+'&roster='+run.fd.Roster);

                    }
                    else {
                        window.open('/export/excel/controlliquidationhorizontal?type=' + run.fd.Typereport + '&range=' + run.fd.Range + '&period=' + run.fd.Period+'&roster='+run.fd.Roster);

                    }


        }


    }
});

var repnet = new Vue({
    el: '#report_net',
    data: {
        globalurl: '../api/v1/payrunhead',
        fd: {
            Period: '1',
            Periodlist: '',
            Typeperiod: '1',
            Typereport: '1',
            Roster: '1',
            Rosterlist: '',
            Range: ''
        }
    },
    methods: {
        TypePeriodOnChange: function () {
            combo.loadPeriodByType(repnet.fd.Typeperiod);
        },
        DownloadReport: function () {
            this.$http.get('/check/excel/nets?type=' + repnet.fd.Typereport + '&range=' + repnet.fd.Range + '&period=' + repnet.fd.Period+'&roster='+repnet.fd.Roster).then(function (response) {
                if (response.body.length > 0) {
                    window.open('/export/excel/nets?type=' + repnet.fd.Typereport + '&range=' + repnet.fd.Range + '&period=' + repnet.fd.Period+'&roster='+repnet.fd.Roster);
                } else {
                    swal({
                        title: 'Lo sentimos',
                        text: 'la consulta no obtuno ningun resultado, no se generara el xls',
                        type: 'info'
                    });
                }
            });
        }
    }
});

var book = new Vue({
    el: '#report_book',
    data: {
        globalurl: '../api/v1/payrunhead',
        fd: {
            Period: '1',
            Periodlist: '',
            Typeperiod: '1',
            Typereport: '1',
            Roster: '1',
            Rosterlist: '',
            Range: ''
        }
    },
    methods: {
        TypePeriodOnChange: function () {
            combo.loadPeriodByType(book.fd.Typeperiod);
        },
        DownloadReport: function () {
            this.$http.get('/check/excel/nets?type=' + book.fd.Typereport + '&range=' + book.fd.Range + '&period=' + book.fd.Period+'&roster='+book.fd.Roster).then(function (response) {
                if (response.body.length > 0) {
                    window.open('/export/book/law?type=' + book.fd.Typereport + '&range=' + book.fd.Range + '&period=' + book.fd.Period+'&roster='+book.fd.Roster);
                } else {
                    swal({
                        title: 'Lo sentimos',
                        text: 'la consulta no obtuno ningun resultado, no se generara el xls',
                        type: 'info'
                    });
                }
            });
        }
    }
});

var dcore = new Vue({
    el: '#sicore',
    data: {
        globalurl: '../api/v1/paydsichead',
        fd: {
            Period: '',
            Periodlist: [],
            Typeperiod: '',
            Typereport: '',
            Typerun:'',
            Typerunlist:[],
            Roster: '',
            Rosterlist: [],
            Range: ''
        }
    },
    methods: {
        TypePeriodOnChange: function () {
            combo.loadPeriodByType(dsic.fd.Typeperiod);
        },
        Downloadsic: function () {
            var IdnRoster = dsic.fd.Roster.id;
            var IdnTyperun = dsic.fd.Typerun.id;
            var IdnPeriod = dsic.fd.Period.id;
            if(Object.keys(dsic.fd.Roster).length === 0)
            {
                IdnRoster = 0;
            }
            if(Object.keys(dsic.fd.Typerun).length === 0)
            {
                IdnTyperun = 0;
            }
            if(Object.keys(dsic.fd.Period).length === 0)
            {
                swal('Selecciona Un periodo','El periodo es obligatorio','error')
            }
            else
            {

                window.open('../api/v1/sicoss/download/'+IdnRoster+'/'+IdnPeriod+'/'+IdnTyperun, '_blank');
            }

        }
    }
});


combo.loadTyperun();
combo.loadPeriod();
combo.loadRoster();
combo.loadPeriodByType(dsus.fd.Typeperiod);

function isEmpty(obj) {
    for(var prop in obj) {
        if(obj.hasOwnProperty(prop))
            return false;
    }

    return JSON.stringify(obj) === JSON.stringify({});
}
