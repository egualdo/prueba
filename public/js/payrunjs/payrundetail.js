NProgress.configure({ easing: 'ease', speed: 3000 });
var rundetail = new Vue({
    el: '#vue-Payrundetail',
    data: {
        globalurl:'../api/v1/payrunhead',
        fd:
            {
                Idn:'',
                Month:'Enero',
                Period:'',
                Periodlist:[],
                //DataCombos
                Typeperiod:'',
                Paymentmethod:'',
                Paymentmethodlist:[],
                PaymentRundate:'05-05-2017',
                PayContributiondate:'04-08-2017',
                Typerun:'',
                //Detail - DataRemake
                DetPayrunHead:'',
                DetPeriod:'',
                DetNamePeriod:'',
                DetEmployee:'',
                DetRoster:'',
                DetTyperun:'',
                DetNameTyperun:'',
                DetPaymentmethod:'',
                DetPaymentRundate:'',
                DetPayContributiondate:'',

                Typerunlist:[],
                Payrunbodylist:[],
                Typeperiodlist:
                    [{id:1,label:'Mensual'},{id:2,label:'Quincenal'}],
                HeadRoster:'',
                HeadSalary:'',
                HeadStartdate:'',
                HeadSitrevision:'',
                HeadModcontract:'',
                Headcct:'',
                HeadName:'',
                HeadCod:'',
                ColTotPays:0,
                ColTotNorem:0,
                ColTotDeduc:0,
                ColTotExen:0,
                NetPay:0,
                ConstantfooterLCTlist:[],
                ConstantfooterCCTlist:[],
                ConstantfooterCompanylist:[],
                ConstantfooterEmployeelist:[],
                Asignationfooterlist:[],
                Exclusionfooterlist:[],
                Newfooterlist:[],
                Vacationfooterlist:[],
                Otherfooterlist:[],
                Conceptdetaillist:[],
                ConceptDetailCant:'',
                ConceptDetailAmount:'',
                Sicossdata:[]
            },
        ff:
            {
                save:false,
                edit:true,
                openPayruns:true,
                closedPayruns:false
            }
    },

    methods: {

        suma:function()
        {
            //Obtengo la lista de montos
            var amountList = rundetail.fd.Payrunbodylist;
            var totpays = 0;
            var totnorem = 0;
            var totdeduc = 0;
            var totexen = 0;

            //recorro el objeto para sumar los totales
            for (var i in amountList) {
                var amountConcept = amountList[i].amount;
                var columnConcept = amountList[i].columnconcept;

                if (columnConcept === 1)
                {
                    totpays =totpays+ amountConcept;
                }
                if (columnConcept === 2)
                {
                    totnorem =totnorem+ amountConcept;
                }
                if (columnConcept === 3)
                {

                    totdeduc =totdeduc+ amountConcept;
                }
                if (columnConcept === 4)
                {
                    totexen =totexen+ amountConcept;
                }
                /*   alert(objeto[i].amount);
                   console.log(objeto[i]);
                   rundetail.fd.Sumtotal =+
                   ColTotPagos:'',
                 ColTotNorem:'',
                 ColTotDeduc:'',
                 ColTotExen:''*/
            }

            var totpays = parseFloat(totpays.toFixed(2));
            var totnorem = parseFloat(totnorem.toFixed(2));
            var totdeduc = parseFloat(totdeduc.toFixed(2));
            var totexen = parseFloat(totexen.toFixed(2));
            rundetail.fd.ColTotPays = totpays.toFixed(2);
            rundetail.fd.ColTotNorem = totnorem.toFixed(2);
            rundetail.fd.ColTotDeduc = totdeduc.toFixed(2);
            rundetail.fd.ColTotExen = totexen.toFixed(2);

            rundetail.fd.NetPay = (totpays + totnorem - totdeduc + totexen).toFixed(2);

        },
        remakepayrun:function()
        {
            swal({
                title: "Atención",
                text: "¿Estas seguro de correr de nuevo la nómina de "+rundetail.fd.HeadName+"?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Correr',
                cancelButtonText: 'Cancelar'
            }).then(function () {

                console.log('Sending - Payrunhead:'+rundetail.fd.DetPayrunHead);
                console.log('Sending - Typerun:'+rundetail.fd.DetTyperun);
                console.log('Sending - Period:'+rundetail.fd.DetPeriod);
                console.log('Sending - Employee:'+rundetail.fd.DetEmployee);
                console.log('Sending - Roster:'+rundetail.fd.DetRoster);
                console.log('Sending - Payment method:'+rundetail.fd.DetPaymentmethod);
                console.log('Sending - Payment RunDate:'+rundetail.fd.DetPaymentRundate);
                console.log('Sending - Contribution Date:'+rundetail.fd.DetPayContributiondate);
                var arr = {
                    //DATOS DE FORMULARIO
                    Period:rundetail.fd.DetPeriod,
                    Employee:rundetail.fd.DetEmployee,
                    Roster:rundetail.fd.DetRoster,
                    Paymentmethod:rundetail.fd.DetPaymentmethod,
                    Paydate:rundetail.fd.DetPaymentmethod,
                    Paycontributiondate:rundetail.fd.DetPayContributiondate,
                    Typerun:rundetail.fd.DetTyperun,
                    TypeExecution:2,
                    idnpayrun:rundetail.fd.DetPayrunHead
                };
                //VUE POST
                Vue.http.post('../api/v1/payrun/process',arr).then(response => {
                    // Si todo sale correcto
                    swal('Corrida almacenada correctamente','','success');
                rundetail.GetPayrunBody();
                rundetail.getpayrunfooterbyhead();
                rundetail.GetPayrunSicoss(1);
                rundetail.cleanform();
            }, response => {
                    if (response.status === 409)
                    {
                        swal('Elemento ya existente','Ya existe una corrida con estos parametros','error')
                    }
                    if (response.status === 500)
                    {
                        swal('Error interno',response.body,'error')
                    }
                    if (response.status === 404)
                    {
                        swal('No encontrado','No hay comunicación correcta con el servidor','error')
                    }
                    //Si no sale correcto


                });

            })
            //FIN VUE POST
        },
        closepayrun:function()
        {
            swal({
                title: "Atención",
                text: "¿Estas seguro de cerrar la corrida de "+rundetail.fd.HeadName+"?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Cerrar Corrida',
                cancelButtonText: 'Cancelar'
            }).then(function () {
                //VUE POST
                Vue.http.put('../api/v1/payrunhead/close/'+rundetail.fd.DetPayrunHead).then(response => {
                    // Si todo sale correcto
                    swal('Corrida cerrada correctamente','','success');
                rundetail.cleanform();
                $('#ModalPayrunDetails').modal('toggle');

            }, response => {
                    if (response.status === 409)
                    {
                        swal('Elemento ya existente','Ya existe una corrida con estos parametros','error')
                    }
                    if (response.status === 500)
                    {
                        swal('Error interno','Ocurrio un error interno','error')
                    }
                    if (response.status === 404)
                    {
                        swal('No encontrado','No hay comunicación correcta con el servidor','error')
                    }
                    //Si no sale correcto
                });
            })
        },
        openpayrun:function()
        {
            swal({
                title: "Cuidado",
                text: "¿Estas seguro de reabrir la corrida de "+rundetail.fd.HeadName+"?, si reabre una corrida puede modificar datos históricos",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Estoy seguro',
                cancelButtonText: 'Cancelar'
            }).then(function () {
                //VUE POST
                Vue.http.put('../api/v1/payrunhead/open/'+rundetail.fd.DetPayrunHead).then(response => {
                    // Si todo sale correcto
                    swal('Corrida abierta correctamente','','success');
                rundetail.cleanform();
                $('#ModalPayrunDetails').modal('toggle');
            }, response => {
                    if (response.status === 409)
                    {
                        swal('Elemento ya existente','Ya existe una corrida con estos parametros','error')
                    }
                    if (response.status === 500)
                    {
                        swal('Error interno','Ocurrio un error interno','error')
                    }
                    if (response.status === 404)
                    {
                        swal('No encontrado','No hay comunicación correcta con el servidor','error')
                    }
                    //Si no sale correcto
                });
            })
        },
        deletepayrun:function()
        {
            swal({
                title: "Atención",
                text: "¿Estas seguro de Eliminar la corrida de "+rundetail.fd.HeadName+"?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Cerrar Corrida',
                cancelButtonText: 'Cancelar'
            }).then(function () {
                //VUE POST
                Vue.http.put('../api/v1/payrunheadt/delete/'+rundetail.fd.DetPayrunHead).then(response => {
                    // Si todo sale correcto
                    swal('Corrida Eliminada correctamente','','success');
                rundetail.cleanform();
                $('#ModalPayrunDetails').modal('toggle');

            }, response => {
                    if (response.status === 500)
                    {
                        swal('Error interno','Ocurrio un error interno','error')
                    }
                    if (response.status === 404)
                    {
                        swal('No encontrado','No hay comunicación correcta con el servidor','error')
                    }
                    //Si no sale correcto
                });
            });
        },
        downloadreceive:function()
        {
            swal({
                title: "Confirmación",
                text: "¿Desea descargar el recibo de "+rundetail.fd.HeadName+"?, la información será mostrada en un documento PDF",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Estoy seguro',
                cancelButtonText: 'Cancelar'
            }).then(function () {                               //VUE POST
                console.log('EMPLOYEE-'+rundetail.fd.Employee);
                console.log('PBody-'+rundetail.fd.Idn);
                console.log('PHead-'+rundetail.fd.DetPayrunHead);
                // Si todo sale correcto
                var arr = {
                    Employee:rundetail.fd.Employee,
                    PayrunBody:rundetail.fd.Idn,
                    PayrunHead:rundetail.fd.DetPayrunHead,
                };
                //VUE POST
                Vue.http.post(rundetail.globalurl+"/validateDownloadPDF",arr).then(response => {
                    // Si todo sale correcto
                    var Token = response.body;

                window.open('../exportPDF_process/'+Token, '_blank');
            }, response => {
                    if (response.status === 409)
                    {
                        swal('Elemento ya existente','Ya existe una corrida con estos parametros','error')
                    }
                    if (response.status === 500)
                    {
                        swal('Error interno','Ocurrio un error interno','error')
                    }
                    if (response.status === 404)
                    {
                        swal('No encontrado','No hay comunicación correcta con el servidor','error')
                    }
                    //Si no sale correcto
                });
            })
        },
        editreceive:function(idnpayrunbody)
        {
            rundetail.fd.ConceptDetailCant = '';
            rundetail.fd.ConceptDetailAmount = '';
            $('#ManualEdition').modal('toggle');
            // GET /someUrl
            this.$http.get('../api/v1/payrunbodyconceptbyid/'+idnpayrunbody).then(response => {

                // get body data
                rundetail.fd.Conceptdetaillist = response.body;


        }, response => {
            // error callback
        });

        },
        saveconceptmanual:function()
        {
            if (rundetail.fd.ConceptDetailCant === '' || rundetail.fd.ConceptDetailAmount === '')
            {
                swal('Indica ambos Campos','Por favor indica Cantidad y monto','error')
                return false;
            }
            var idnpayrunbody = rundetail.fd.Conceptdetaillist[0]['idn'];
            swal({
                title: "Confirmación",
                text: "¿Esta seguro de editar manualmente el monto de este concepto?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Estoy seguro',
                cancelButtonText: 'Cancelar'
            }).then(function () {                            //VUE POST

                // Si todo sale correcto
                var arr = {
                    IdnPayrunBody:idnpayrunbody,
                    IdnPayrunHead:rundetail.fd.DetPayrunHead,
                    CantConcept:rundetail.fd.ConceptDetailCant,
                    AmountConcept:rundetail.fd.ConceptDetailAmount
                };
                //VUE POST
                Vue.http.put("../api/v1/payrunbodyconcept/update",arr).then(response => {
                    // Si todo sale correcto

                    swal('Montos del concepto actualizado','Puede visualizar en la corrida el monto indicado manualmente','success')
                rundetail.GetPayrunBody();
                rundetail.getpayrunfooterbyhead();
                rundetail.GetPayrunSicoss();
                rundetail.cleanform();
                $('#ManualEdition').modal('hide');
            }, response => {

                    if (response.status === 500)
                    {
                        swal('Error interno','Ocurrio un error interno','error')
                    }
                    //Si no sale correcto
                });
            })
        },
        TypePeriodOnChange:function(val)
        {

            rundetail.fd.Typeperiod = val;

            combo.loadPeriodByType(val.id);
        },
        GetPayrunSicoss:function(Type)
        {

            //GetSicoss($IdnEmployee,$IdnPeriod,$IdnTypeRun,$IdnCompany)
            // GET /someUrl
            this.$http.get('../api/v1/sicoss/employee/'+rundetail.fd.DetEmployee+'/'+rundetail.fd.DetPeriod+'/'+rundetail.fd.DetTyperun+'/'+Type).then(response => {

                // get body data
                rundetail.fd.Sicossdata = response.body[0];


        }, response => {
            // error callback
        });
        },
        GetPayrunBody:function()
        {

            // GET /someUrl
            this.$http.get('../api/v1/payrunbodybyid/'+rundetail.fd.DetPayrunHead).then(response => {

                // get body data
                rundetail.fd.Payrunbodylist = response.body;
            console.log(response.body);
                console.log(rundetail.fd.Payrunbodylist);
                rundetail.suma();

        }, response => {
            // error callback
        });

            this.$http.get('../api/v1/employee/id/'+rundetail.fd.Employee).then(response => {

                /*
                  empHead.fd.HeadRoster=response.body[0].roster;

                  empHead.fd.HeadCuil=response.body[0].cuil;
                  empHead.fd.HeadSalary=response.body[0].basicsalary;
                  empHead.fd.HeadStartdate=response.body[0].dateentry;
                  empHead.fd.HeadSitrevision=response.body[0].sitrevision;
                  empHead.fd.HeadModcontract=response.body[0].modcontract;
                  empHead.fd.Headcct=response.body[0].ccts;
                  empHead.fd.HeadCuil=response.body[0].cuil; */
                var rb = response.body[0];
            rundetail.fd.HeadCod=rb.cod;
            rundetail.fd.HeadName=rb.firstname +" "+rb.lastname;
            rundetail.fd.HeadCuil = rb.cuil;
            rundetail.fd.HeadSalary = rb.basicsalary;
            rundetail.fd.Headcct = rb.ccts;
            rundetail.fd.Active = rb.active;
            rundetail.fd.HeadRoster = rb.roster;
            rundetail.fd.HeadCondition = rb.employeecondition;
            rundetail.fd.HeadStartDate = rb.dateentry;
            rundetail.fd.HeadFinishDate = rb.dateexit;
            rundetail.fd.HeadAntiquity = rb.antiquity;

            //empRun.fd.HeadDiasTrab = rundetail.fd.HeadDiasTrab;
            rundetail.fd.HeadJournalType = rb.journaltype;
            rundetail.fd.HeadModcontract = rb.modcontract;

            rundetail.fd.HeadSitRevision = rb.sitrevision;

        }, response => {
            // error callback
        });
        },
        getpayrunfooterbyhead:function()
        {

            // GET /someUrl
            this.$http.get('../api/v1/payrunfooterbyhead/'+rundetail.fd.DetPayrunHead).then(response => {

                rundetail.fd.ConstantfooterLCTlist=[];
                rundetail.fd.ConstantfooterCCTlist=[];
                rundetail.fd.ConstantfooterCompanylist=[];
                rundetail.fd.ConstantfooterEmployeelist=[];
            rundetail.fd.Asignationfooterlist=[];
            rundetail.fd.Exclusionfooterlist=[];
            rundetail.fd.Newfooterlist=[];
            rundetail.fd.Vacationfooterlist=[];
            rundetail.fd.Otherfooterlist=[];
            // get body data
            $.each(response.body,function(i,v)
            {
                if (parseInt(v.type) === 1)
                {
                    console.log('VARIABLE1:'+v.variable);
                    rundetail.fd.Constantfooterlist.push(v);
                }
                if (parseInt(v.type) === 2)
                {
                    console.log('VARIABLE2:'+v.variable);
                    rundetail.fd.Asignationfooterlist.push(v);
                }
                if (parseInt(v.type) === 3)
                {
                    console.log('VARIABLE3:'+v.variable);
                    rundetail.fd.Exclusionfooterlist.push(v);
                }
                if (parseInt(v.type) === 4)
                {
                    console.log('VARIABLE4:'+v.variable);
                    rundetail.fd.Newfooterlist.push(v);
                }
                if (parseInt(v.type) === 5)
                {
                    console.log('VARIABLE5:'+v.variable);
                    rundetail.fd.Vacationfooterlist.push(v);
                }
                if (parseInt(v.type) === 6)
                {
                    console.log('VARIABLE6:'+v.variable);
                    rundetail.fd.Otherfooterlist.push(v);
                }
                if (parseInt(v.type) === 7)
                {
                  console.log('VARIABLE7:'+v.variable);
                  rundetail.fd.ConstantfooterLCTlist.push(v);              
                }
                if (parseInt(v.type) === 8)
                {
                  console.log('VARIABLE8:'+v.variable);
                  rundetail.fd.ConstantfooterCCTlist.push(v);              
                }
                if (parseInt(v.type) === 9)
                {
                  console.log('VARIABLE9:'+v.variable);
                  rundetail.fd.ConstantfooterCompanylist.push(v);              
                }
                if (parseInt(v.type) === 10)
                {
                  console.log('VARIABLE10:'+v.variable);
                  rundetail.fd.ConstantfooterEmployeelist.push(v);              
                }
            });

            rundetail.suma();

        }, response => {
            // error callback
        });

        },
        cleanform:function()
        {
            rundetail.fd.Month='';
            rundetail.fd.Period='';

            rundetail.fd.Paymentmethod='';

            rundetail.fd.PaymentRundate='';
            rundetail.fd.PayContributiondate='';
            var table = $('#TablePayrunOpen').dataTable();
            var table2 = $('#TablePayrunClosed').dataTable();
            //RECARGA LOS DATOS DE LA TABLA
            table.fnReloadAjax();
            table2.fnReloadAjax();
        },
        openpayrundetail:function()
        {
            rundetail.fd.Payrunbodylist = [];

            $('#ModalPayrunDetails').modal('toggle');
            //OCULTAR BOTONES O MOSTRAR
            setTimeout(
                function() {
                    rundetail.GetPayrunBody();
                    rundetail.GetPayrunSicoss(2);
                    rundetail.getpayrunfooterbyhead();
                }, 200);


        }

    }

});


//combo.loadPeriodByType(rundetail.fd.Typeperiod.id);
//combo.loadRunType();