Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');

//FUNCIONES GENERICAS PARA INICIO DE LA PAGINA
 var vm = new Vue({
  methods: 
      { 

           cargarcomboposts:function()
        {
                   // GET /someUrl
            this.$http.get('../api/v1/posts').then(response => {

            // get body data
            mod.fd.Postsoptions = response.body;

          }, response => {
            // error callback
          });
        },

        

           cargarcombouser:function()
        {
                   // GET /someUrl
            this.$http.get('../api/v1/userall').then(response => {

            // get body data

           
            mod.fd.Useroptions = response.body;
            
          }, response => {
            // error callback
          });
        },

        cargartabla:function()
        {
          CargarTabla();
        }
      }
    });

vm.cargarcombouser();
vm.cargartabla();
vm.cargarcomboposts();

//FUNCIONES EN LA MODAL DE EDICION
var mod = new Vue({
  el: '#vuedata',
  data: {
    globalurl:'../api/v1/likes',
    fd:
    {
        Idn: '',
        IdnUser: '',
        IdnPosts: '',
        
        NameUser: '',
        User: '',
        Useroptions: '',
       
        Date: '',
       
        TittlePosts: '',
        Posts: '',
        Postsoptions: ''
       
    },
    ff:
    {
      save:true,
      edit:false
    }    
    
  },
   mounted() {
        //Formato del Datepicker
        $('#Date').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            language: 'es'
        });
        //Obtener Datos del Datepicker
        $("#Date").datepicker().on("changeDate", () => {
            //this.startDate = $('#DPickerBirthdate').val();
            mod.fd.StartDate = $('#Date').val();
        });
   },
  methods: {
   
    guardar: function (event)
    {
        var items = [mod.fd.Posts,mod.fd.User,mod.fd.Date];
        var strings = ['Post', 'Usuario','Fecha'];
        var checked = CheckNulls(items, strings);
        if(checked !== 0){
            swal({
                title: "Confirmación de guardado",
                text: "Estas seguro de guardar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {
                var arr = {
                    //DATOS DE FORMULARIO
                    idnuser:mod.fd.User,
                    idnpost:mod.fd.Posts,
                    date:mod.fd.Date
                };
                //VUE POST
                Vue.http.post(mod.globalurl+"/save",arr).then(response => {
                    // Si todo sale correcto
                    swal('Guardado Correctamente','','success');
                $('#ModalEdicion').modal('hide');
                mod.cleanform();

            }, response => {
                    //Si no sale correcto
                    swal(response.body,'','error');
                    mod.cleanform();
                });

            })
        }

                    //FIN VUE POST

       
    },

    editar: function (event) 
    {
        var items = [mod.fd.Posts,mod.fd.User,mod.fd.Date];
        var strings = ['Post', 'Usuario','Fecha'];
        var checked = CheckNulls(items, strings);
        if(checked !== 0){
       swal({
                title: "Confirmación de editado",
                text: "Estas seguro de editar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {
                    //DATOS DE FORMULARIO 
                     idnuser:mod.fd.User,
                    idnpost:mod.fd.Posts,
                    date:mod.fd.Date
                                      
                  };
                  //VUE POST
                Vue.http.put(mod.globalurl+"/update/"+mod.fd.Idn,arr).then(response => {
                    // Si todo sale correcto              
                    swal('Modificada Correctamente','','success');
                    $('#ModalEdicion').modal('hide');
                    mod.cleanform();

                    }, response => {
                      //Si no sale correcto
                            swal(response.body,'','error')
                            mod.cleanform();
                        });
                        
                    })
                    //FIN VUE POST
            }
    },

    borrar: function (event)
    {
        swal({
                title: "Confirmar Eliminación",
                text: "Estas seguro de eliminar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {

                //revisar esta condicion 
                var arr = {date:mod.fd.Date};
                Vue.http.put(mod.globalurl+"/delete/"+mod.fd.Idn,arr).then(response => {
                    // success callback
                    swal('Eliminado Correctamente','','success');
                $('#ModalEdicion').modal('hide');
                mod.cleanform();


            }, response => {
                    swal('Ocurrio un error al eliminar','','error');
                    // error callback
                });
            })
    },


    cleanform: function()
    {
      mod.fd.Idn='';
      mod.fd.IdnUser='';
        mod.fd.IdnPosts='';
      $('#ModalEdicion').modal('hide');
      var table = $('#TablaMaster').dataTable();
      //RECARGA LOS DATOS DE LA TABLA
     table.fnReloadAjax();
     
    },
   
     openmodaltonew:function()
        {
          mod.cleanform();
          $('#ModalEdicion').modal('toggle');
          //OCULTAR BOTONES O MOSTRAR
          mod.ff.save = true;
          mod.ff.edit = false;
        },
        openmodaltoedit:function()
        {
           $('#ModalEdicion').modal('toggle');
          //OCULTAR BOTONES O MOSTRAR
          mod.ff.save = false;
          mod.ff.edit = true;
        }
  }
 
});

   //======INICIO DE FUNCIONES ============
    function CargarTabla()
    {   
      var table = $("#TablaMaster").DataTable({
        //COMPROBACION PARA PINTAR Y CAMBIAR TEXTO DE TABLA


        "destroy": true,
        "scrollY":        "200px",
        "scrollX":        "1000px",
        "language": {
            "url": "/../Spanish.json"
        },
        //Especificaciones de las Columnas que vienen y deben mostrarse
        "columns" : [
            { data : 'idn' },
            { data : 'user' },
            { data : 'post'},
            { data : 'date'}
                
           
        ],
        //Especificaciones de la URL del servicio
        "ajax": {
            url: '../api/v1/likes',
            dataSrc : ''
        }

    });

            $('#TablaMaster tbody').on( 'click', 'tr', function () {
        
        //OBTENGO LOS VARLOES DE LA TABLA
        mod.fd.Idn = table.row( this ).data().idn;
        mod.fd.IdnUser = table.row( this ).data().idnuser;
        mod.fd.IdnPosts = table.row( this ).data().idnpost;
                
        mod.fd.User = table.row( this ).data().idnuser;
        mod.fd.Posts = table.row( this ).data().idnpost;
                
        mod.fd.Date = table.row( this ).data().date;

      
        idnuser=mod.fd.User;
                idnpost=mod.fd.Posts;
      //ABRO LA MODAL
        mod.openmodaltoedit();

       });

     } 
