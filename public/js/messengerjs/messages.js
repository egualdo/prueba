Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');

//FUNCIONES GENERICAS PARA INICIO DE LA PAGINA
 var vm = new Vue({
  methods: 
      { 

         

        

           cargarcombouser:function()
        {
                   // GET /someUrl
            this.$http.get('../api/v1/userall').then(response => {

            // get body data

           
            mod.fd.Transmitteroptions = response.body;
             mod.fd.Receiveroptions = response.body;
          }, response => {
            // error callback
          });
        },

        cargartabla:function()
        {
          CargarTabla();
        }
      }
    });


vm.cargartabla();
vm.cargarcombouser();

//FUNCIONES EN LA MODAL DE EDICION
var mod = new Vue({
  el: '#vuedata',
  data: {
    globalurl:'../api/v1/message',
    fd:
    {
        Idn: '',
        IdnCompany: '',
        IdnTransmitter: '',
        IdnReceiver: '',
        Nametransmitter: '',
        Transmitter: '',
        Transmitteroptions: '',
        Namereceiver: '',
        Receiver: '',
        Receiveroptions: '',
        Tittle: '',
        Description: '',
        Namecompany: '',
        Company: '',
        Companyoptions: '',
        Useroptions2: '',
        Useroptions1: ''
    },
    ff:
    {
      save:true,
      edit:false
    }    
    
  },
  
  methods: {
   
    guardar: function (event)
    {
        var items = [mod.fd.Company,mod.fd.Transmitter,mod.fd.Receiver,mod.fd.Tittle,mod.fd.Description];
        var strings = ['Empresa', 'Emisor','Receptor', 'Titulo','Mensaje'];
        var checked = CheckNulls(items, strings);
        if(checked !== 0){
            swal({
                title: "Confirmación de guardado",
                text: "Estas seguro de guardar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {
                var arr = {
                    //DATOS DE FORMULARIO
                    idncompany:mod.fd.Company,
                    idntransmitter:mod.fd.Transmitter,
                    idnreceiver:mod.fd.Receiver,
                    tittle:mod.fd.Tittle,
                    description:mod.fd.Description
                };
                //VUE POST
                Vue.http.post(mod.globalurl+"/save",arr).then(response => {
                    // Si todo sale correcto
                    swal('Guardado Correctamente','','success');
                $('#ModalEdicion').modal('hide');
                mod.cleanform();

            }, response => {
                    //Si no sale correcto
                    swal(response.body,'','error');
                    mod.cleanform();
                });

            })
        }

                    //FIN VUE POST

       
    },

    editar: function (event) 
    {
        var items = [mod.fd.Company,mod.fd.Transmitter,mod.fd.Receiver,mod.fd.Tittle,mod.fd.Description];
        var strings = ['Empresa', 'Transmisor','Receptor', 'Titulo','Mensaje'];
        var checked = CheckNulls(items, strings);
        if(checked !== 0){
       swal({
                title: "Confirmación de editado",
                text: "Estas seguro de editar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {
                    //DATOS DE FORMULARIO 
                    idncompany:mod.fd.Company,
                    idntransmitter:mod.fd.Transmitter,
                    idnreceiver:mod.fd.Receiver,
                    tittle:mod.fd.Tittle,
                    description:mod.fd.Description 
                                      
                  };
                  //VUE POST
                Vue.http.put(mod.globalurl+"/update/"+mod.fd.Idn,arr).then(response => {
                    // Si todo sale correcto              
                    swal('Modificada Correctamente','','success');
                    $('#ModalEdicion').modal('hide');
                    mod.cleanform();

                    }, response => {
                      //Si no sale correcto
                            swal(response.body,'','error')
                            mod.cleanform();
                        });
                        
                    })
                    //FIN VUE POST
            }
    },

    borrar: function (event)
    {
        swal({
                title: "Confirmar Eliminación",
                text: "Estas seguro de eliminar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {description:mod.fd.Description};
                Vue.http.put(mod.globalurl+"/delete/"+mod.fd.Idn,arr).then(response => {
                    // success callback
                    swal('Eliminado Correctamente','','success');
                $('#ModalEdicion').modal('hide');
                mod.cleanform();


            }, response => {
                    swal('Ocurrio un error al eliminar','','error');
                    // error callback
                });
            })
    },


    cleanform: function()
    {
      mod.fd.Idn='';
      mod.fd.IdnCompany='';
      mod.fd.Description='';
      $('#ModalEdicion').modal('hide');
      var table = $('#TablaMaster').dataTable();
      //RECARGA LOS DATOS DE LA TABLA
     table.fnReloadAjax();
     
    },
   
     openmodaltonew:function()
        {
          mod.cleanform();
          $('#ModalEdicion').modal('toggle');
          //OCULTAR BOTONES O MOSTRAR
          mod.ff.save = true;
          mod.ff.edit = false;
        },
        openmodaltoedit:function()
        {
           $('#ModalEdicion').modal('toggle');
          //OCULTAR BOTONES O MOSTRAR
          mod.ff.save = false;
          mod.ff.edit = true;
        }
  }
 
});

   //======INICIO DE FUNCIONES ============
    function CargarTabla()
    {   
      var table = $("#TablaMaster").DataTable({
        //COMPROBACION PARA PINTAR Y CAMBIAR TEXTO DE TABLA


        "destroy": true,
        "scrollY":        "200px",
        "scrollX":        "1000px",
        "language": {
            "url": "/../Spanish.json"
        },
        //Especificaciones de las Columnas que vienen y deben mostrarse
        "columns" : [
            { data : 'idn' },
            { data : 'transmitter' },
            { data : 'receiver' },
            { data : 'tittle'},
            { data : 'description'}
                
           
        ],
        //Especificaciones de la URL del servicio
        "ajax": {
            url: '../api/v1/messages',
            dataSrc : ''
        }

    });

            $('#TablaMaster tbody').on( 'click', 'tr', function () {
        
        //OBTENGO LOS VARLOES DE LA TABLA
        mod.fd.Idn = table.row( this ).data().idn;
        mod.fd.IdnCompany = table.row( this ).data().idncompany;

        mod.fd.Company = table.row( this ).data().idncompany;
        mod.fd.Transmitter = table.row( this ).data().idntransmitter;
        mod.fd.Receiver = table.row( this ).data().idnreceiver;
        mod.fd.Tittle = table.row( this ).data().tittle;
        mod.fd.Description = table.row( this ).data().description;    
      
        idncompany=mod.fd.Company;
      //ABRO LA MODAL
        mod.openmodaltoedit();

       });

     } 
