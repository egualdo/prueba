


//FUNCIONES GENERICAS PARA INICIO DE LA PAGINA
var vm = new Vue({
    methods:
        {
            cargartabla:function()
            {
                CargarTablaAbiertos();
              
                }
        }
});

vm.cargartabla();


//FUNCIONES EN LA MODAL DE EDICION
var mod = new Vue({
    el: '#modedit',
    data: {
        Idn:'',
        //DATOS DE FORMULARIO
        MasterUrl:'../api/v1/statuspostulation',
        Name:'',       
        Description:'',
        Usercreated:'',
        Usermodified:'',
        Createddate: '',
        Updateddate: '',
        Userloged:$("#UsernameUsuario").text(),
        disabled:false,
        save:false,
        edit:true,
    },
    methods: {
        setresult:function()
        {
            $('#ModalEdicion').modal('hide');
            $('#ModalDetail').modal('toggle');

            //OCULTAR BOTONES O MOSTRAR

        },
        guardar: function (event)
        {
            swal({
                title: "Confirmación de guardado",
                text: "Estas seguro de guardar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {
                    //DATOS DE FORMULARIO
                
                    name:mod.Name,                   
                    description:mod.Description,
                    usercreated:mod.Userloged};

                Vue.http.post(mod.MasterUrl,arr).then(response => {
                    // success callback
                    swal('Guardado Correctamente','','success');
                $('#ModalEdicion').modal('hide');
                cleanform();

            }, response => {
                    swal('Ocurrio un error al guardar','','error')
                    cleanform();
                });
            })
        },
        editar: function (event)
        {
            swal({
                title: "Confirmar edición",
                text: "Estas seguro de editar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {
                    //DATOS DE FORMULARIO
                  
                    name:mod.Name,
                  
                    description:mod.Description,
                    usermodified:mod.Userloged
                };
                Vue.http.put(mod.MasterUrl+'/'+mod.Idn+'',arr).then(response => {
                    // success callback
                    swal('Guardado Correctamente','','success');
                $('#ModalEdicion').modal('hide');
                cleanform();

            }, response => {
                    swal('Ocurrio un error al editar','','error');
                    // error callback
                });
            })
        },
      
        borrar: function (event)
        {
            swal({
                title: "Confirmar Eliminación",
                text: "Estas seguro de eliminar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {name:mod.Nombre};
                Vue.http.delete(mod.MasterUrl+'/'+mod.Idn+'',arr).then(response => {
                    // success callback
                    swal('Eliminado Correctamente','','success');
                $('#ModalEdicion').modal('hide');
                cleanform();


            }, response => {
                    swal('Ocurrio un error al eliminar','','error');
                    // error callback
                });
            })
        },
  
          openmodaltonew:function()
        {
            cleanform()
            $('#ModalEdicion').modal('hide');
            $('#ModalEdicion').modal('show');
            //OCULTAR BOTONES O MOSTRAR
            mod.save = true;
            mod.edit = false;
        },
        openmodaltoedit:function()
        {
            $('#ModalEdicion').modal('hide');
            $('#ModalEdicion').modal('show');
            //OCULTAR BOTONES O MOSTRAR


            mod.save = false;
            mod.edit = true;
        }

    }

});

//======INICIO DE FUNCIONES ============
function cleanform()
{
    mod.Idn='';
    //DATOS DE FORMULARIO
  
    mod.Name='';
    
    mod.Description='';

    mod.disabled=false;
    mod.save=true;
    mod.edit=false;


    var table = $('#OfferTable').dataTable();
    //RECARGA LOS DATOS DE LA TABLA
    table.fnReloadAjax();
    var table2 = $('#OfferTable').dataTable();
    //RECARGA LOS DATOS DE LA TABLA
    table2.fnReloadAjax();
}
//======INICIO DE FUNCIONES ============
function CargarTablaAbiertos()
{


    var table = $("#OfferTable").DataTable({
        //COMPROBACION PARA PINTAR Y CAMBIAR TEXTO DE TABLA


        "destroy": true,
        "scrollY":        "500px",
        "scrollX":        "1000px",
         "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
            },
        //Especificaciones de las Columnas que vienen y deben mostrarse
        "columns" : [
            { data : 'idn' },
            { data : 'name'}
          
            
        ],
        //Especificaciones de la URL del servicio
        "ajax": {
            url: "../api/v1/statuspostulation",
            dataSrc : ''
        }

    });

    //Al Hacer clic en la tabla, obtengo los datos y los cargo a los TextBox
    $('#OfferTable tbody').on( 'click', 'tr', function () {

        //ASIGNO LOS VALORES A LA
        mod.Idn = table.row( this ).data().idn;
        mod.Name = table.row( this ).data().name;        

        mod.Usercreated = table.row( this ).data().usercreated;
        mod.Usermodified = table.row( this ).data().usermodified;
        mod.Createddate = table.row( this ).data().created_at;
        mod.Updateddate = table.row( this ).data().updated_at;
        //ABRO LA MODAL
        mod.openmodaltoedit();


    } );

}





