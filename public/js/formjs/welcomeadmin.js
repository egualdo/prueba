


var mod = new Vue({
    el: '#modedit',
    data: {
        Idn:'',
        //DATOS DE FORMULARIO
        MasterUrl:'../api/v1/statuspostulation',
        Name:'',       
        Description:'',
        Usercreated:'',
        Usermodified:'',
        Createddate: '',
        Updateddate: '',
        Userloged:$("#UsernameUsuario").text(),
        disabled:false,
        save:false,
        edit:true
    },
    methods: {
        setresult:function()
        {
            $('#ModalEdicion').modal('hide');
            $('#ModalDetail').modal('toggle');

            //OCULTAR BOTONES O MOSTRAR

        },
        guardar: function (event)
        {
            swal({
                title: "Confirmación de guardado",
                text: "Estas seguro de guardar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {
                    //DATOS DE FORMULARIO
                
                    name:mod.Name,                   
                    description:mod.Description,
                    usercreated:mod.Userloged};

                Vue.http.post(mod.MasterUrl,arr).then(response => {
                    // success callback
                    swal('Guardado Correctamente','','success');
                $('#ModalEdicion').modal('hide');
                cleanform();

            }, response => {
                    swal('Ocurrio un error al guardar','','error')
                    cleanform();
                });
            })
        },
        editar: function (event)
        {
            swal({
                title: "Confirmar edición",
                text: "Estas seguro de editar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {
                    //DATOS DE FORMULARIO
                  
                    name:mod.Name,
                  
                    description:mod.Description,
                    usermodified:mod.Userloged
                };
                Vue.http.put(mod.MasterUrl+'/'+mod.Idn+'',arr).then(response => {
                    // success callback
                    swal('Guardado Correctamente','','success');
                $('#ModalEdicion').modal('hide');
                cleanform();

            }, response => {
                    swal('Ocurrio un error al editar','','error');
                    // error callback
                });
            })
        },
      
        borrar: function (event)
        {
            swal({
                title: "Confirmar Eliminación",
                text: "Estas seguro de eliminar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {name:mod.Nombre};
                Vue.http.delete(mod.MasterUrl+'/'+mod.Idn+'',arr).then(response => {
                    // success callback
                    swal('Eliminado Correctamente','','success');
                $('#ModalEdicion').modal('hide');
                cleanform();


            }, response => {
                    swal('Ocurrio un error al eliminar','','error');
                    // error callback
                });
            })
        },
  
          openmodaltonew:function()
        {
           // cleanform()
            $('#ModalEdicion').modal('hide');
            $('#ModalEdicion').modal('show');
            //OCULTAR BOTONES O MOSTRAR
            mod.save = true;
            mod.edit = false;
        },
        openmodaltoedit:function()
        {
            $('#ModalEdicion').modal('hide');
            $('#ModalEdicion').modal('show');
            //OCULTAR BOTONES O MOSTRAR
            mod.save = false;
            mod.edit = true;
        }

    }

});








