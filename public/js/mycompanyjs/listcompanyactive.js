Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');

//FUNCIONES GENERICAS PARA INICIO DE LA PAGINA



//FUNCIONES EN LA MODAL DE EDICION
var mod = new Vue({
  el: '#vuedata',
  data: {
    globalurl:'../api/v1/listcompanyactive',
Typecompanyoptions:[],
Activityoptions: [],
Zoneoptions: [],
Taxesoptions:[],
Fatheroptions:[],
      CompanyGroupList:[],
TypePerson:[{id: 1, label: 'Persona Fisica'},
    {id: 2, label: 'Persona Juridica'}],
    fd:
    {
      Idn:'',
        Code:'',
        Name:'',
        Cuit:'',
        Cuiladmin:'',
        Father:'',
        Typeperson:'',
        Companytype:'',
        Companyactivity: '',
        Companyzone: '',
        CompanyGroup:'',

       //   Idnzone: '',
        Fiscaldomicile: '',
        Benefitsoccharges:'',
        Photosing:'',
        Photologo:'',
        FileSingName:'',
        FileLogoName:'',
        TCompanyactivity:'',
        companycct:'',
        Monthclose: '',
        Taxes:'',
        Observation:'',
        DBUsername:'',
        DBPassword:'',

        //Selects
        Meses: [
    {id: 1, label: 'Enero'},
    {id: 2, label: 'Febrero'},
    {id: 3, label: 'Marzo'},
    {id: 4, label: 'Abril'},
    {id: 5, label: 'Mayo'},
    {id: 6, label: 'Junio'},
    {id: 7, label: 'Julio'},
    {id: 8, label: 'Agosto'},
    {id: 9, label: 'Septiembre'},
    {id: 10, label: 'Octubre'},
    {id: 11, label: 'Noviembre'},
    {id: 12, label: 'Diciembre'}
    ]

    },
    ff:
    {
      save:false,
      edit:true,
    }    
    
  },
  methods: {
    guardar: function (event) 
    {
      if(this.fd.Companytype === "")
      {
        swal('Por favor seleccione un tipo de empresa','','error');
        return false;
      }
      if(this.fd.Companyactivity === "")
      {
        swal('Por favor seleccione una actividad','','error');
        return false;
      }
      if(this.fd.Companyzone === "")
      {
        swal('Por favor seleccione una zona','','error');
        return false;
      }
      if(this.fd.Monthclose === "")
      {
        swal('Por favor seleccione un Mes de Cierre','','error');
        return false;
      }

      if(this.fd.Typeperson === "")
      {
        swal('Por favor seleccione un Tipo de Persona','','error');
        return false;
      }
      if(this.fd.CompanyGroup === "")
      {
        swal('Por favor seleccione un Grupo para la empresa','','error');
        return false;
      }

       swal({
        title: "Confirmación de guardado",
        text: "Estas seguro de guardar este elemento",
        type: "warning",
        showCancelButton: true,       
        confirmButtonText: 'Aceptar',
        cancelButtonText: 'Cancelar'
      }).then(function () {
         var arr = {
             Code:mod.fd.Code,
                    Name:mod.fd.Name,
                    Group:mod.fd.CompanyGroup,
                    Cuit:mod.fd.Cuit,
                    Cuiladmin:mod.fd.Cuiladmin,
                    Typeperson:mod.fd.Typeperson,
                    Companytype:mod.fd.Companytype,
                    Companyactivity:mod.fd.Companyactivity,
                    Idndatasource:mod.fd.Idndatasource,
                    Companyzone:mod.fd.Companyzone,
                    Fiscaldomicile:mod.fd.Fiscaldomicile,
                    Benefitsoccharges:mod.fd.Benefitsoccharges,
                    Photosing:mod.fd.Photosing,
                    Photologo:mod.fd.Photologo,
                    TCompanyactivity:mod.fd.TCompanyactivity,
                    companycct:mod.fd.companycct,
                    Monthclose:mod.fd.Monthclose,
                    Observation:mod.fd.Observation,
                    DBUsername:mod.fd.DBUsername,
                    DBPassword:mod.fd.DBPassword
                    };

               Vue.http.post('../api/v1/company/save',arr).then(response => {
                // success callback
                swal('Guardado Correctamente','','success');
                cleanform();
     
              }, response => {
                  swal('Ocurrio un error al guardar','','error')
                cleanform();
              });
      })
                    //FIN VUE POST
           
       
    },
    editar: function (event) 
    {
       swal({
                title: "Confirmación de editado",
                text: "Estas seguro de editar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {
                    //DATOS DE FORMULARIO
                    name:mod.fd.Name,
                    description:mod.fd.Description
                                      
                  };
                  //VUE POST
                Vue.http.put(mod.globalurl+"/update/"+mod.fd.Idn,arr).then(response => {
                    // Si todo sale correcto              
                    swal('Modificada Correctamente','','success');
                    $('#ModalEdicion').modal('hide');
                    mod.cleanform();

                    }, response => {
                      //Si no sale correcto
                            swal('Ocurrio un error al modificar','','error')
                            mod.cleanform();
                        });
                        
                    })
                    //FIN VUE POST
    },   
    borrar: function (event)
    {
       swal({
                title: "Confirmar Eliminación",
                text: "Estas seguro de eliminar este elemento",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {name:mod.fd.Name};
                Vue.http.put(mod.globalurl+"/delete/"+mod.fd.Idn,arr).then(response => {
                    // success callback
                    swal('Eliminado Correctamente','','success');
                $('#ModalEdicion').modal('hide');
                mod.cleanform();


            }, response => {
                    swal('Ocurrio un error al eliminar','','error');
                    // error callback
                });
            })
    },
    cleanform: function()
    {
      mod.fd.Idn='';
      mod.fd.Name='';
      mod.fd.Description='';
      $('#ModalEdicion').modal('hide');
      var table = $('#TablaMaster').dataTable();
      //RECARGA LOS DATOS DE LA TABLA
     table.fnReloadAjax();
     
    },
   
     openmodaltonew:function()
        {
          mod.cleanform();
          $('#ModalEdicion').modal('toggle');
          //OCULTAR BOTONES O MOSTRAR
          mod.ff.save = true;
          mod.ff.edit = false;
        },
        openmodaltoedit:function()
        {
           $('#ModalEdicion').modal('toggle');
          //OCULTAR BOTONES O MOSTRAR
          mod.ff.save = false;
          mod.ff.edit = true;
        }
  }
 
});

    function getBase64(file,type) {
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            if(type === true){
                mod.fd.Photosing = reader.result;
                mod.fd.FileSingName = file.name;
            }else{
                mod.fd.Photologo = reader.result;
                mod.fd.FileLogoName = file.name;
            }
            console.log(reader.result);
        };
        reader.onerror = function (error) {
            console.log('Error: ', error);
        };
    }

    $(document).on('change','#SingInput',function () {

        var files = document.getElementById('SingInput').files;
        if (files.length > 0) {
          getBase64(files[0],true);
        }
    });

    $(document).on('change','#LogoInput',function () {
        var files = document.getElementById('LogoInput').files;
        if (files.length > 0) {
           getBase64(files[0],false);
        }

    });

   //======INICIO DE FUNCIONES ============
    function CargarTabla()
    {   
      var table = $("#TablaMaster").DataTable({
        //COMPROBACION PARA PINTAR Y CAMBIAR TEXTO DE TABLA


        "destroy": true,
        "scrollY":        "200px",
        "scrollX":        "1000px",
        "language": {
            "url": "/../Spanish.json"
        },
        //Especificaciones de las Columnas que vienen y deben mostrarse
        "columns" : [
            { data : 'idn' },
            { data : 'cod'},
            { data : 'name'},
            { data : 'cuit'},
            { data : 'typecompany'},
            { data : 'companycct'}
                
           
        ],
        //Especificaciones de la URL del servicio
        "ajax": {
            url: '../api/v1/listcompanyactive',
            dataSrc : ''
        }

    });

            $('#TablaMaster tbody').on( 'click', 'tr', function () {
        
        //OBTENGO LOS VARLOES DE LA TABLA
        mod.fd.Idn = table.row( this ).data().idn;
        mod.fd.Name = table.row( this ).data().name;
       
      
//ABRO LA MODAL
          swal({
                title: "Confirmación",
                text: "¿Desea ver el detalle de este empresa?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Ver Detalle',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {
                    //DATOS DE FORMULARIO
                    name:mod.fd.Name,
                    description:mod.fd.Description
                                      
                  };

                  window.location.href = "../compania/companiadetalle?id="+mod.fd.Idn;
                  //VUE POST
             
                        
                    });
                    //FIN VUE POST


       });

     }


 var vm = new Vue({
  methods: 
       {
                cargartabla:function()
        {
          CargarTabla();
        },
        cargarcombotypecompany:function()
        {
                   // GET /someUrl
            this.$http.get('../api/v1/typecompany').then(response => {
                $.each(response.body,function(i,v){
                mod.Typecompanyoptions.push({id:v.idn,label:v.name});
            });

          }, response => {
            // error callback
          });
        },

         cargarcomboactivity:function()
        {
                   // GET /someUrl
            this.$http.get('../api/v1/activity').then(response => {

                $.each(response.body,function(i,v){
                mod.Activityoptions.push({id:v.idn,label:v.name});
            });


          }, response => {
            // error callback
          });
        },
        cargarcombozone:function()
        {
                   // GET /someUrl
            this.$http.get('../api/v1/zone').then(response => {

            $.each(response.body,function(i,v){
                mod.Zoneoptions.push({id:v.idn,label:v.name});
            });

          }, response => {
            // error callback
          });
        },
         
        cargarcombotaxes:function()
        {
                   // GET /someUrl
            this.$http.get('../api/v1/taxes').then(response => {

                $.each(response.body,function(i,v){
                mod.Taxesoptions.push({id:v.idn,label:v.name});
            });

          }, response => {
            // error callback
          });
        },
        cargarcombofather:function()
        {
                   // GET /someUrl
            this.$http.get('../api/v1/companiagroup').then(response => {

                $.each(response.body,function(i,v){
                mod.CompanyGroupList.push({id:v.idn,label:v.name});
            });

          }, response => {
            // error callback
          });
        }

        
      }
    });

vm.cargartabla();
vm.cargarcombofather();
vm.cargarcombotypecompany();
vm.cargarcomboactivity();
vm.cargarcombozone();
vm.cargarcombotaxes();