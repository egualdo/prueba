Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');

//FUNCIONES GENERICAS PARA INICIO DE LA PAGINA


//FUNCIONES EN LA MODAL DE EDICION
var mod = new Vue({
  el: '#vuedata',
  data: { 
    globalurl:'../api/v1/contactcompany',
    fd:
      {
        Idn:'',
        Name:'',
        Email:'',
        Telephone:''
      },
    ff:
      {
        save:false,
        edit:true
      }  
  },
  methods: {
    guardar: function (event) 
    { 


       swal({
        title: "Confirmación de guardado",
        text: "Estas seguro de guardar este elemento",
        type: "warning",
        showCancelButton: true,       
        confirmButtonText: 'Aceptar',
        cancelButtonText: 'Cancelar'
      }).then(function () {
       
       
         var arr = {Name:mod.fd.Name,
                    Email:mod.fd.Email,
                    Telephone:mod.fd.Telephone
                    };

               Vue.http.post('api/v1/contactcompany',arr).then(response => {
                // success callback
                swal('Guardado Correctamente','','success');
                cleanform();
     
              }, response => {
                  swal('Ocurrio un error al guardar','','error')
                cleanform();
              });
      })
    },

    editar: function (event) 
    {
            swal({
        title: "Confirmar edición",
        text: "Estas seguro de editar este elemento",
        type: "warning",
        showCancelButton: true,       
        confirmButtonText: 'Aceptar',
        cancelButtonText: 'Cancelar'
      }).then(function () {
       
       

         var arr = {{Name:mod.fd.Name,
                    Email:mod.fd.Email,
                    Telephone:mod.fd.Telephone
                    };
               Vue.http.put('../api/v1/contactcompania/update/'+ mod.fd.Idn,arr).then(response => {
                // success callback
                swal(
                    'Editado Correctamente','','success');
                cleanform();
     
              }, response => {
                  swal('Ocurrio un error al editar','','error');
                // error callback
              });
      })
    },
    borrar: function (event)
    {
      swal({
        title: "Confirmar Eliminación",
        text: "Estas seguro de eliminar este elemento",
        type: "warning",
        showCancelButton: true,       
        confirmButtonText: 'Aceptar',
        cancelButtonText: 'Cancelar'
      }).then(function () {
       
       
         var arr = {Name:mod.fd.Name};
               Vue.http.put('../api/v1/contactcompania/delete/'+ mod.fd.Idn,arr).then(response => {
                // success callback
                 swal('Eliminado Correctamente','','success');
                cleanform();
                  
     
              }, response => {
                swal('Ocurrio un error al eliminar','','error');
                // error callback
              });
      })
    }

     openmodaltonew:function()
        {
          $('#ModalEdicion').modal('toggle');
          //OCULTAR BOTONES O MOSTRAR
          modedit.save = true;
          modedit.edit = false;
        },
      /*  openmodaltoedit:function()
        {
           $('#ModalEdicion').modal('toggle');
          //OCULTAR BOTONES O MOSTRAR
          modedit.save = false;
          modedit.edit = true;
        }*/
  }
 
});





