Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');

//FUNCIONES GENERICAS PARA INICIO DE LA PAGINA


//FUNCIONES EN LA MODAL DE EDICION
var mod = new Vue({
  el: '#vuedata',
  data: { 
    globalurl:'../api/v1/company',
      Typecompanyoptions:[],
      Activityoptions: [],
      Zoneoptions: [],
      Taxesoptions:[],

      TypePerson:[{id: 1, label: 'Persona Fisica'},
          {id: 2, label: 'Persona Juridica'}],
    fd:
      {
        Idn:'',
        Idncont:'',
        Code:'',
        Name:'',
        Cuit:'',
        NameClone:'',
        CuitClone:'',
        CodeClone:'',
        Cuiladmin:'',
       // Idnfather:'',
        Typeperson:'',
        Companytype:'',
        Companyactivity: '',
          CompanyGrouplist:[],
        Companyzone: '',
       //   Idnzone: '',
        Fiscaldomicile: '',
        Benefitsoccharges:'',
        Photosing:'',
        Photologo:'',
        PhotologoImg:'',
        PhotosingImg: '',
        TCompanyactivity:'',
        companycct:'',
        Monthclose:'',
        Taxes:'',
        Observation:'',
          CompanyGroup:'',
       Namec:'',
       Emailc:'',
       Telephonec:'',
       Active:'',
          Meses: [
              {id: 1, label: 'Enero'},
              {id: 2, label: 'Febrero'},
              {id: 3, label: 'Marzo'},
              {id: 4, label: 'Abril'},
              {id: 5, label: 'Mayo'},
              {id: 6, label: 'Junio'},
              {id: 7, label: 'Julio'},
              {id: 8, label: 'Agosto'},
              {id: 9, label: 'Septiembre'},
              {id: 10, label: 'Octubre'},
              {id: 11, label: 'Noviembre'},
              {id: 12, label: 'Diciembre'}
          ]
      },
    ff:
      {
        save:false,
        edit:true,
        savec:false,
        editc:true,

      }  
  },
  methods: {
    guardar: function (event) 
    {
        if(this.fd.Companytype === "")
        {
            swal('Por favor seleccione un tipo de empresa','','error');
            return false;
        }
        if(this.fd.Companyactivity === "")
        {
            swal('Por favor seleccione una actividad','','error');
            return false;
        }
        if(this.fd.Companyzone === "")
        {
            swal('Por favor seleccione una zona','','error');
            return false;
        }
        if(this.fd.Monthclose === "")
        {
            swal('Por favor seleccione un Mes de Cierre','','error');
            return false;
        }

        if(this.fd.Typeperson === "")
        {
            swal('Por favor seleccione un Tipo de Persona','','error');
            return false;
        }
        if(this.fd.Father === "")
        {
            swal('Por favor seleccione una Empresa Padre','','error');
            return false;
        }
       swal({
        title: "Confirmación de guardado",
        text: "Estas seguro de guardar este elemento",
        type: "warning",
        showCancelButton: true,       
        confirmButtonText: 'Aceptar',
        cancelButtonText: 'Cancelar'
      }).then(function () {
       
       
         var arr = {Code:mod.fd.Username,
                    Name:mod.fd.Name,
                    Group:mod.fd.Groupoptions,
                    Cuit:mod.fd.Cuit,
                    Cuitadmin:mod.fd.Cuitadmin,
                    Idntypeperson:mod.fd.Idntypeperson,
                    Idncompanytype:mod.fd.Idntypeperson,
                    Idncompanyactivity:mod.fd.Idncompanyactivity,
                   // Idndatasource:mod.fd.Idndatasource,
                    Idncompanyzone:mod.fd.Idncompanyzone,
                    Fiscaldomicile:mod.fd.Fiscaldomicile,
                    Benefitsoccharges:mod.fd.Benefitsoccharges,
                    Photosing:mod.fd.Photosing,
                    Photologo:mod.fd.Photologo,
                    Companyactivity:mod.fd.Companyactivity,
                    Companycct:mod.fd.Companycct,
                    Monthclose:mod.fd.Monthclose,
                    Idntaxes:mod.fd.Idntaxes,
                    Observation:mod.fd.Observation,
                    };

               Vue.http.post('api/v1/company',arr).then(response => {
                // success callback
                swal('Guardado Correctamente','','success');
                cleanform();
     
              }, response => {
                  swal('Ocurrio un error al guardar','','error')
                cleanform();
              });
      })
    },

    edit: function (event) 
    {
            swal({
        title: "Confirmar edición",
        text: "Estas seguro de editar este elemento",
        type: "warning",
        showCancelButton: true,       
        confirmButtonText: 'Aceptar',
        cancelButtonText: 'Cancelar'
      }).then(function () {
       
       

         var arr = {//Code:mod.fd.Code,
                    Name:mod.fd.Name,
                    Group:mod.fd.CompanyGroup,
                    Cuit:mod.fd.Cuit,
                    Cuiladmin:mod.fd.Cuiladmin,
                    Typeperson:mod.fd.Typeperson,
                    Companytype:mod.fd.Companytype,
                    Companyactivity:mod.fd.Companyactivity,
                   // Idndatasource:mod.fd.Idndatasource,
                    Companyzone:mod.fd.Companyzone,
                    Fiscaldomicile:mod.fd.Fiscaldomicile,
                    Benefitsoccharges:mod.fd.Benefitsoccharges,
                    Photosing:mod.fd.Photosing,
                    Photologo:mod.fd.Photologo,
                    TCompanyactivity:mod.fd.TCompanyactivity,
                    companycct:mod.fd.companycct,
                    Monthclose:mod.fd.Monthclose,
                    Observation:mod.fd.Observation
                    };
               Vue.http.put('../api/v1/compania/update/'+ mod.fd.Idn,arr).then(response => {
                // success callback
                swal(
                    'Editado Correctamente','','success');
                cleanform();
     
              }, response => {
                  swal('Ocurrio un error al editar','','error');
                // error callback
              });
      })
    },
    deletes: function (event)
    {
      swal({
        title: "Confirmar Inactivación",
        text: "¿Estas seguro de inactivar esta empresa?",
        type: "warning",
        showCancelButton: true,       
        confirmButtonText: 'Aceptar',
        cancelButtonText: 'Cancelar'
      }).then(function () {
       
       
         var arr = {Name:mod.fd.Name};
               Vue.http.put('../api/v1/compania/delete/'+ mod.fd.Idn,arr).then(response => {
                // success callback
                 swal('Eliminado Correctamente','','success');
                cleanform();
                  
     
              }, response => {
                swal('Ocurrio un error al eliminar','','error');
                // error callback
              });
      })
    },
    reactivate: function (event)
    {
      swal({
        title: "Confirmar Reactivación",
        text: "Estas seguro de reactivar esta empresa",
        type: "warning",
        showCancelButton: true,       
        confirmButtonText: 'Aceptar',
        cancelButtonText: 'Cancelar'
      }).then(function () {
       
       
         var arr = {Name:mod.fd.Name};
               Vue.http.put('../api/v1/compania/reactivate/'+ mod.fd.Idn,arr).then(response => {
                // success callback
                 swal('Reactivado Correctamente','','success');
                cleanform();                 
     
              }, response => {
                swal('Ocurrio un error al eliminar','','error');
                // error callback
              });
      })
    },

      clonar: function (event)
      {
          if(mod.fd.NameClone === ''){
              swal('Por Favor ingrese un Nombre','','warning');
              return false;
          }
          if(mod.fd.CuitClone === ''){
              swal('Por Favor ingrese un Cuit','','warning');
              return false;
          }

          if(mod.fd.CodeClone === ''){
              swal('Por Favor ingrese un Codigo','','warning');
              return false;
          }
          swal({
            title: "Confirmar Clonar",
            text: "Estas seguro de Clonar este elemento",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: 'Aceptar',
            cancelButtonText: 'Cancelar'
          }).then(function () {

            $('.loader').show();
            var arr = {Name:mod.fd.NameClone, Cuit:mod.fd.CuitClone, Cod:mod.fd.CodeClone};
            Vue.http.put('../api/v1/compania/clone/'+ mod.fd.Idn,arr).then(response => {
                  // success callback
                  $('#ModalClonar').modal('toggle');
                  $('.loader').hide();

                  swal('Clonado Correctamente','','success');
                  mod.fd.NameClone = '';
                  mod.fd.CuitClone = '';
                  mod.fd.CodeClone = '';
                }, response => {
                  $('.loader').hide();

                  swal('Ocurrio un error al Clonar','','error');
                  // error callback
                });
          });
      },
    guardarc: function (event) 
    { 


       swal({
        title: "Confirmación de guardado",
        text: "Estas seguro de guardar este elemento",
        type: "warning",
        showCancelButton: true,       
        confirmButtonText: 'Aceptar',
        cancelButtonText: 'Cancelar'
      }).then(function () {
       
       
         var arr = {Idncompany:mod.fd.Idn,
                    Namec:mod.fd.Namec,
                    Emailc:mod.fd.Emailc,
                    Telephonec:mod.fd.Telephonec
                    };

               Vue.http.post('../api/v1/contactcompany/save',arr).then(response => {
                // success callback
                swal('Guardado Correctamente','','success');
                cleanform();
     
              }, response => {
                  swal('Ocurrio un error al guardar','','error')
                cleanform();
              });
      })
    },

    editarc: function (event) 
    {
            swal({
        title: "Confirmar edición",
        text: "Estas seguro de editar este elemento",
        type: "warning",
        showCancelButton: true,       
        confirmButtonText: 'Aceptar',
        cancelButtonText: 'Cancelar'
      }).then(function () {
       
       

         var arr = {Idncompany:mod.fd.Idn,
                    Namec:mod.fd.Namec,
                    Emailc:mod.fd.Emailc,
                    Telephonec:mod.fd.Telephonec
                    };
               Vue.http.put('../api/v1/contactcompany/update/'+ mod.fd.Idncont,arr).then(response => {
                // success callback
                swal(
                    'Editado Correctamente','','success');
                cleanform();
     
              }, response => {
                  swal('Ocurrio un error al editar','','error');
                // error callback
              });
      })
    },
    borrarc: function (event)
    {
      swal({
        title: "Confirmar Eliminación",
        text: "Estas seguro de eliminar este elemento",
        type: "warning",
        showCancelButton: true,       
        confirmButtonText: 'Aceptar',
        cancelButtonText: 'Cancelar'
      }).then(function () {
       
       
         var arr = {Namec:mod.fd.Namec};
               Vue.http.put('../api/v1/contactcompany/delete/'+ mod.fd.Idncont,arr).then(response => {
                // success callback
                 swal('Eliminado Correctamente','','success');
                cleanform();
                  
     
              }, response => {
                swal('Ocurrio un error al eliminar','','error');
                // error callback
              });
      })
    },
     openmodaltonew:function()
        {
          $('#ModalEdicion').modal('toggle');
          //OCULTAR BOTONES O MOSTRAR
          mod.ff.savec = true;
          mod.ff.editc = false;
        },
     openmodaltoedit:function()
        {
           $('#ModalEdicion').modal('toggle');
          //OCULTAR BOTONES O MOSTRAR
          mod.ff.savec = false;
          mod.ff.editc = true;
        },
     openmodaltoclone:function()
        {
           $('#ModalClonar').modal('toggle');
        }
  }
 
});
function getBase64(file,type) {
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
        if(type === true){
            mod.fd.Photosing = reader.result;
            mod.fd.FileSingName = file.name;
            $('#ImgSing').attr('src',reader.result).show();
        }else{
            mod.fd.Photologo = reader.result;
            mod.fd.FileLogoName = file.name;
            $('#ImgLogo').attr('src',reader.result).show();
        }
    };
    reader.onerror = function (error) {
        console.log('Error: ', error);
    };
}

$(document).on('change','#SingInput',function () {

    var files = document.getElementById('SingInput').files;
    if (files.length > 0) {
        getBase64(files[0],true);
    }
});

$(document).on('change','#LogoInput',function () {
    var files = document.getElementById('LogoInput').files;
    if (files.length > 0) {
        getBase64(files[0],false);
    }

});

 function CargarTabla()
    {   
      var table = $("#TablaContactCompany").DataTable({
        //COMPROBACION PARA PINTAR Y CAMBIAR TEXTO DE TABLA


        "destroy": true,
        "scrollY":        "200px",
        "scrollX":        "1000px",
        "language": {
            "url": "/../Spanish.json"
        },
        //Especificaciones de las Columnas que vienen y deben mostrarse
        "columns" : [
            { data : 'idn'},
            { data : 'name'},
            { data : 'email'},
            { data : 'telephone'}
                
           
        ],
        //Especificaciones de la URL del servicio
        "ajax": {
            url: '../api/v1/contactcompany/'+ mod.fd.Idn,
            dataSrc : ''
        }

    });

            $('#TablaContactCompany tbody').on( 'click', 'tr', function () {
        
        //OBTENGO LOS VARLOES DE LA TABLA
        //mod.fd.Idn = table.row( this ).data().idn;
        mod.fd.Idncont = table.row( this ).data().idn;
        mod.fd.Idn = table.row( this ).data().idncompany;
        mod.fd.Namec = table.row( this ).data().name;
        mod.fd.Emailc = table.row( this ).data().email;
        mod.fd.Telephonec = table.row( this ).data().telephone;
       
      
//ABRO LA MODAL
          swal({
                title: "Confirmación",
                text: "¿Desea ver contacto?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Ver Detalle',
                cancelButtonText: 'Cancelar'
            }).then(function () {


                var arr = {
                    //DATOS DE FORMULARIO
                    idncont:mod.fd.Idncont,
                    idncompany:mod.fd.Idn,
                    name:mod.fd.Namec,
                    email:mod.fd.Emailc,
                    telephone:mod.fd.Telephonec
                                      
                  };

                 // window.location.href = "../compania/companiadetalle?id="+mod.fd.Idn;
                  //VUE POST
                  mod.openmodaltoedit();
             
                        
                    });
                    //FIN VUE POST


       });

     }

 var vm = new Vue({
  methods:
       {

           cargarcombocompanygroup:function()
        {
            // GET /someUrl
            this.$http.get('../api/v1/companiagroup').then(response => {
                $.each(response.body,function(i,v){
                mod.fd.CompanyGrouplist.push({id:v.idn,label:v.name});
            });

        }, response => {
            // error callback
        });
        },
        cargarcombotypecompany:function()
        {
                   // GET /someUrl
            this.$http.get('../api/v1/typecompany').then(response => {
                $.each(response.body,function(i,v){
                mod.Typecompanyoptions.push({id:v.idn,label:v.name});
            });

          }, response => {
            // error callback
          });
        },
         cargarcomboactivity:function()
        {
                   // GET /someUrl
            this.$http.get('../api/v1/activity').then(response => {
                $.each(response.body,function(i,v){
                mod.Activityoptions.push({id:v.idn,label:v.name});
            });

          }, response => {
            // error callback
          });
        },
        cargarcombozone:function()
        {
                   // GET /someUrl
            this.$http.get('../api/v1/zone').then(response => {

                $.each(response.body,function(i,v){
                mod.Zoneoptions.push({id:v.idn,label:v.name});
            });
          }, response => {
            // error callback
          });
        },
        cargarcombotaxes:function()
        {
                   // GET /someUrl
            this.$http.get('../api/v1/taxes').then(response => {

                $.each(response.body,function(i,v){
                mod.Taxesoptions.push({id:v.idn,label:v.name});
            });

          }, response => {
            // error callback
          });
        },
        loadcompany:function()
        {
           var query = window.location;
           var qs = getUrlParams(query);//error en el idn de compania 
           console.log("PARAMETROS",qs);
           mod.fd.Idn = qs.id;

        },
        cargardatos:function()
        {
                   // GET /someUrl
            this.$http.get('../api/v1/compania/'+ mod.fd.Idn).then(response => {

         setTimeout(function(){
                    mod.fd.Name = response.body[0].name;
                    mod.fd.Cuit = response.body[0].cuit;
                    mod.fd.Cuiladmin = response.body[0].cuiladmin;
                    mod.fd.Typeperson = mod.TypePerson[parseInt(response.body[0].idntypeperson) - 1];
                    mod.fd.Companytype = mod.Typecompanyoptions[parseInt(response.body[0].idncompanytype) - 1];
                    mod.fd.Idn = response.body[0].idn;
                    mod.fd.Companyactivity = mod.Activityoptions[parseInt(response.body[0].idncompanyactivity) - 1];
                    mod.fd.Companyzone = mod.Zoneoptions[parseInt(response.body[0].idncompanyzone) - 1];
                    mod.fd.Fiscaldomicile = response.body[0].fiscaldomicile;
                    mod.fd.Benefitsoccharges = response.body[0].benefitsoccharges;
                    mod.fd.Photosing = '';
                    mod.fd.Photologo = '';
                    mod.fd.PhotosingImg = '/uploads/'+response.body[0].photosing;
                    mod.fd.PhotologoImg = '/uploads/'+response.body[0].photologo;
                    mod.fd.TCompanyactivity = response.body[0].companyactivity;
                    mod.fd.companycct = response.body[0].companycct;
                    mod.fd.Monthclose = mod.fd.Meses[parseInt(response.body[0].monthclose) - 1];
                    mod.fd.Active = response.body[0].active;
                    // mod.fd.Taxes = response.body[0].taxes;
                    mod.fd.Observation = response.body[0].observation;
                },500);



          }, response => {
            // error callback
          });
        }

        
        
      }
    });

vm.loadcompany();
vm.cargardatos();
vm.cargarcombotypecompany();
vm.cargarcomboactivity();
vm.cargarcombozone();
vm.cargarcombotaxes();
vm.cargarcombocompanygroup();
CargarTabla();




